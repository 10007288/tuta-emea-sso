<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="SetRights, App_Web_yuclf4ni" %>
<%@ Register TagName="userRightsControl" TagPrefix="wus" Src="~/controls/userRightsControl.ascx"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" Runat="Server">
<asp:Panel ID="pnlMain" runat="server" CssClass="panelStandard" HorizontalAlign="center" Width="100%">
<asp:Label ID="lblHasRights" runat="server" Visible="false" Text="false" />
    <table style="text-align:left" id="tblSelect" runat="server">
        <tr>
            <td>
                <asp:Label ID="lblRightsLevel" runat="server" Text="Level:" />
            </td>
            <td>
                <asp:DropDownList ID="ddlRightsLevel" runat="server" DataTextField="RightsLevelName" DataValueField="RightsLevelID" OnSelectedIndexChanged="ddlRightsLevel_SelectedIndexChanged" AutoPostBack="true" />
            </td>
        </tr>
        <tr id="trRoles" runat="server" visible="false">
            <td>
                <asp:Label ID="lblRoles" runat="server" Text="Choose Role:" />
            </td>
            <td>
                <asp:DropDownList ID="ddlRoles" runat="server" DataTextField="Role" DataValueField="RoleID" />
            </td>
        </tr>
        <tr id="trUsers" runat="server" visible="false">
            <td>
                <asp:Label ID="lblUser" runat="server" Text="Input CimNumber:" />
            </td>
            <td>
                <asp:TextBox ID="txtUser" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnApplyRules" runat="server" Text="Set Rights" CssClass="buttons" OnClick="btnApplyRules_Click" />
            </td>
        </tr>
    </table>
    <table id="tblRights" runat="server" style="text-align:center" visible="false">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblApplyingTo" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <wus:userRightsControl ID="usrUserRightsControl" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button ID="btnRightChangeOk" runat="server" Text="OK" CssClass="buttons" OnClick="btnRightChangeOk_Click" />
            </td>
            <td align="right">
                <asp:Button ID="btnRightChangeCancel" runat="server" Text="Cancel" CssClass="buttons" OnClick="btnRightChangeCancel_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlSuccessStatus" runat="server" Visible="false" HorizontalAlign="center" Width="100%">
    <table cellpadding="10">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnStatusConfirm" runat="server" Text="Return" OnClick="btnStatusConfirm_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
</asp:Content>

