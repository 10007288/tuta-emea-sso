using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using System.Data.SqlClient;

public partial class customcampaigns : System.Web.UI.Page
{
    string _Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserRole();

        if (_Role != "Admin")
        {
            Response.Redirect("Home.aspx");
        }

        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
        //if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(33))
        //this.btnAddNew.Visible = false;
        this.lblParentCampaignID.Visible = true;
        this.ddlParentCampaignID.Visible = true;
        if (!this.IsPostBack)
        {
            GetUserRole();

            if (_Role != "Admin")
            {
                Response.Redirect("Home.aspx");
            }

            this.PopulateGrids();
        }
    }

    protected void GetUserRole()
    {
        //Check user role of the current user
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }

    protected void gvCampaigns_RowEditing(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Modify")
            {
                this.trHide.Visible = true;
                int campaignID = Convert.ToInt32(e.CommandArgument);
                NonCimCampaign campaign = NonCimCampaign.Select(campaignID);
                NonCimCampaign[] campaigns = NonCimCampaign.SelectExcept(campaignID);
                this.ddlParentCampaignID.Items.Clear();
                this.ddlParentCampaignID.DataSource = NonCimCampaign.SelectExcept(campaignID);
                this.lblCampaignVal.Text = e.CommandArgument.ToString();
                this.ddlParentCampaignID.DataBind();
                this.ddlParentCampaignID.SelectedValue = campaign.ParentCampaignID.ToString();
                this.txtCampaignName.Text = campaign.Campaign;
                this.txtStartDate.Text = campaign.StartDate.ToString("yyyy/MM/dd");
                this.txtEndDate.Text = campaign.EndDate.ToString("yyyy/MM/dd");
                this.chkHide.Checked = campaign.Hide;
                this.mpeEdit.Show();
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "customcampaigns.aspx", "gvCampaigns_RowEditing", ex.Message);
        }

    }

    protected void gvExpiredCampaigns_RowEditing(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Modify")
            {
                this.trHide.Visible = true;
                int campaignID = Convert.ToInt32(e.CommandArgument);
                NonCimCampaign campaign = NonCimCampaign.Select(campaignID);
                NonCimCampaign[] campaigns = NonCimCampaign.SelectExcept(campaignID);
                this.ddlParentCampaignID.Items.Clear();
                this.ddlParentCampaignID.DataSource = NonCimCampaign.SelectExcept(campaignID);
                this.lblCampaignVal.Text = e.CommandArgument.ToString();
                this.ddlParentCampaignID.DataBind();
                this.ddlParentCampaignID.SelectedValue = campaign.ParentCampaignID.ToString();
                this.txtCampaignName.Text = campaign.Campaign;
                this.txtStartDate.Text = campaign.StartDate.ToString("yyyy/MM/dd");
                this.txtEndDate.Text = campaign.EndDate.ToString("yyyy/MM/dd");
                this.chkHide.Checked = campaign.Hide;
                this.mpeEdit.Show();
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "customcampaigns.aspx", "gvCampaigns_RowEditing", ex.Message);
        }

    }

    protected void accCampaigns_RowEditing(object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Modify")
            {
                this.trHide.Visible = true;
                int campaignID = Convert.ToInt32(e.CommandArgument);
                this.lblParentCampaignIDValue.Text = campaignID.ToString();
                NonCimCampaign campaign = NonCimCampaign.Select(campaignID);
                NonCimCampaign[] campaigns = NonCimCampaign.SelectExcept(campaignID);
                this.lblParentCampaignID.Visible = false;
                this.ddlParentCampaignID.Visible = false;
                this.lblCampaignVal.Text = e.CommandArgument.ToString();
                this.txtCampaignName.Text = campaign.Campaign;
                this.txtStartDate.Text = campaign.StartDate.ToString("yyyy/MM/dd");
                this.txtEndDate.Text = campaign.EndDate.ToString("yyyy/MM/dd");
                this.chkHide.Checked = campaign.Hide;
                this.mpeEdit.Show();
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "customcampaigns.aspx", "accCampaigns_RowEditing", ex.Message);
        }

    }

    protected void Accordion1_RowEditing(object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Modify")
            {
                this.trHide.Visible = true;
                int campaignID = Convert.ToInt32(e.CommandArgument);
                this.lblParentCampaignIDValue.Text = campaignID.ToString();
                NonCimCampaign campaign = NonCimCampaign.Select(campaignID);
                NonCimCampaign[] campaigns = NonCimCampaign.SelectExcept(campaignID);
                this.lblParentCampaignID.Visible = false;
                this.ddlParentCampaignID.Visible = false;
                this.lblCampaignVal.Text = e.CommandArgument.ToString();
                this.txtCampaignName.Text = campaign.Campaign;
                this.txtStartDate.Text = campaign.StartDate.ToString("yyyy/MM/dd");
                this.txtEndDate.Text = campaign.EndDate.ToString("yyyy/MM/dd");
                this.chkHide.Checked = campaign.Hide;
                this.mpeEdit.Show();
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "customcampaigns.aspx", "accCampaigns_RowEditing", ex.Message);
        }

    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(this.lblCampaignVal.Text.Trim()))
            {
                NonCimCampaign campaign = new NonCimCampaign();
                campaign.Campaign = this.txtCampaignName.Text.Trim();
                campaign.StartDate = Convert.ToDateTime(this.txtStartDate.Text.Trim());
                campaign.EndDate = Convert.ToDateTime(this.txtEndDate.Text.Trim());
                campaign.Hide = this.chkHide.Checked;
                campaign.ParentCampaignID = Convert.ToInt32(this.ddlParentCampaignID.Text);
                campaign.Insert();
                this.PopulateGrids();
            }
            else
            {
                NonCimCampaign campaign = NonCimCampaign.Select(Convert.ToInt32(this.lblCampaignVal.Text.Trim()));
                campaign.Campaign = this.txtCampaignName.Text.Trim();
                campaign.StartDate = Convert.ToDateTime(this.txtStartDate.Text.Trim());
                campaign.EndDate = Convert.ToDateTime(this.txtEndDate.Text.Trim());
                campaign.Hide = this.chkHide.Checked;
                if (ddlParentCampaignID.Visible)
                    campaign.ParentCampaignID = Convert.ToInt32(this.ddlParentCampaignID.Text);
                else
                    campaign.ParentCampaignID = 0;
                campaign.Update();
                this.PopulateGrids();
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "customcampaigns.aspx", "btnOK_Click", ex.Message);
        }
    }

    protected void PopulateGrids()
    {
        try
        {
            this.accCampaigns.DataSource = NonCimCampaign.SelectParents();
            this.accCampaigns.DataBind();
            foreach (AjaxControlToolkit.AccordionPane item in this.accCampaigns.Panes)
            {
                GridView gvCampaigns = item.FindControl("gvCampaigns") as GridView;
                Label lblCampaignID = item.FindControl("lblCampaignID") as Label;
                gvCampaigns.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(lblCampaignID.Text), false);
                gvCampaigns.DataBind();
                //if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(33))
                //{
                //    foreach (GridViewRow row in gvCampaigns.Rows)
                //    {
                //        LinkButton lnkEdit = row.FindControl("lnkEdit") as LinkButton;
                //        if (lnkEdit != null)
                //            lnkEdit.Enabled = false;
                //    }
                //}
                this.Accordion1.DataSource = NonCimCampaign.SelectExpiredParents();
                this.Accordion1.DataBind();
                foreach (AjaxControlToolkit.AccordionPane expitem in this.Accordion1.Panes)
                {
                    GridView gvExpiredCampaigns = expitem.FindControl("gvExpiredCampaigns") as GridView;
                    Label lblExpiredCampaignID = expitem.FindControl("lblCampaignID") as Label;
                    gvExpiredCampaigns.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(lblExpiredCampaignID.Text), false);
                    gvExpiredCampaigns.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "customcampaigns.aspx", "PopulateGrids", ex.Message);
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        try
        {
            this.lblCampaignVal.Text = string.Empty;
            this.trHide.Visible = false;
            this.chkHide.Checked = false;
            this.txtCampaignName.Text = string.Empty;
            this.txtEndDate.Text = string.Empty;
            this.txtStartDate.Text = string.Empty;
            this.ddlParentCampaignID.Items.Clear();
            ListItem item = new ListItem();
            item.Text = string.Empty;
            item.Value = "0";
            this.ddlParentCampaignID.Items.Add(item);
            this.ddlParentCampaignID.DataSource = NonCimCampaign.SelectParents();
            this.ddlParentCampaignID.DataBind();
            this.ddlParentCampaignID.SelectedIndex = 0;
            this.mpeEdit.Show();
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "customcampaigns.aspx", "btnAddNew_Click", ex.Message);
        }
    }
}
