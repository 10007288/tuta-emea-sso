using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using NuSkill.Business;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class ViewReports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //GetUserClient();

        if (!this.IsPostBack)
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());

            txtTWWID1.Text = SessionManager.SessionUsername;
            txtTWWID2.Text = SessionManager.SessionUsername;
            txtTWWID3.Text = SessionManager.SessionUsername;
            txtTWWID4.Text = SessionManager.SessionUsername;
            txtTWWID5.Text = SessionManager.SessionUsername;
            txtTWWID6.Text = SessionManager.SessionUsername;
            txtTWWID7.Text = SessionManager.SessionUsername;
            txtTWWID8.Text = SessionManager.SessionUsername;
            txtTWWID9.Text = SessionManager.SessionUsername;
            txtTWWID10.Text = SessionManager.SessionUsername;
            txtTWWID11.Text = SessionManager.SessionUsername;
            txtTWWID12.Text = SessionManager.SessionUsername;

            int report = 0;
            report = Convert.ToInt32(Request.QueryString["report"]);
            switch (report)
            {
                case 1:
                    Panel1.Visible = true;
                    break;
                case 2:
                    Panel2.Visible = true;
                    break;
                case 3:
                    Panel3.Visible = true;
                    break;
                case 4:
                    Panel4.Visible = true;
                    break;
                case 5:
                    Panel5.Visible = true;
                    break;
                case 6:
                    Panel6.Visible = true;
                    break;
                case 7:
                    Panel7.Visible = true;
                    break;
                case 8:
                    Panel8.Visible = true;
                    break;
                case 10:
                    Panel9.Visible = true;
                    break;
                case 11:
                    Panel10.Visible = true;
                    break;
                //ADDED BY RAYMARK COSME <01/31/2018>
                case 12:
                    Panel11.Visible = true;
                    break;
                //ADDED BY HAROLD SENIER <09/19/2018>
                case 13:
                    pnlGdprReport.Visible = true;
                    break;
                case 17:
                    pnl_DailyReport.Visible = true;
                    break;
            }
            //GetUserClient();
        }
    }

    protected void RadComboBox1_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox2.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox2.Items.Add(item);
        RadComboBox2.DataBind();
        RadComboBox3.Items.Clear();
    }

    protected void RadComboBox2_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox3.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox3.Items.Add(item);

        TestCategoryUserHistory[] categories = null;
        categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, false, Convert.ToInt32(RadComboBox1.SelectedValue), Convert.ToInt32(RadComboBox2.SelectedValue));

        RadComboBox3.DataSource = categories;
        RadComboBox3.DataTextField = "TestName";
        RadComboBox3.DataValueField = "TestCategoryID";
        RadComboBox3.DataBind();

        //rcbHiddenClient.DataSource = categories;
        //rcbHiddenClient.DataTextField = "TestName";
        //rcbHiddenClient.DataValueField = "ClientID";
        //rcbHiddenClient.DataBind();

        //foreach (RadComboBoxItem _client1 in rcbHiddenClient.Items)
        //{
        //    foreach (RadComboBoxItem _test1 in RadComboBox3.Items)
        //    {
        //        if (lblClientID.Text != _client1.Value)
        //        {
        //            _test1.Visible = false;
        //        }
        //    }
        //}

        //foreach (RadComboBoxItem _client2 in rcbHiddenClient.Items)
        //{
        //    foreach (RadComboBoxItem _test2 in RadComboBox3.Items)
        //    {
        //        if (lblClientID.Text == _client2.Value || _client2.Value == "0")
        //        {
        //            _test2.Visible = true;
        //        }
        //        break;
        //    }
        //}
    }

    protected void cmdGenerate1_Click(object sender, EventArgs e)
    {
        RadGrid1.Rebind();
    }

    protected void RadComboBox4_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox5.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox5.Items.Add(item);
        RadComboBox5.DataBind();
        RadComboBox6.Items.Clear();
    }

    protected void RadComboBox5_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox6.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox6.Items.Add(item);
        TestCategoryUserHistory[] categories = null;
        categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, false, Convert.ToInt32(RadComboBox4.SelectedValue), Convert.ToInt32(RadComboBox5.SelectedValue));
        RadComboBox6.DataSource = categories;
        RadComboBox6.DataTextField = "TestName";
        RadComboBox6.DataValueField = "TestCategoryID";
        RadComboBox6.DataBind();
    }

    protected void cmdGenerate2_Click(object sender, EventArgs e)
    {
        RadGrid2.Rebind();
    }

    protected void RadComboBox7_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox8.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox8.Items.Add(item);
        RadComboBox8.DataBind();
        RadComboBox9.Items.Clear();
    }

    protected void RadComboBox8_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox9.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox9.Items.Add(item);
        RadComboBox9.DataBind();
    }

    protected void cmdGenerate3_Click(object sender, EventArgs e)
    {
        RadGrid3.Rebind();
    }

    protected void RadComboBox10_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox11.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox11.Items.Add(item);
        RadComboBox11.DataBind();
        RadComboBox12.Items.Clear();
    }

    protected void RadComboBox11_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox12.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox12.Items.Add(item);
        RadComboBox12.DataBind();
    }

    protected void cmdGenerate4_Click(object sender, EventArgs e)
    {
        RadGrid4.Rebind();
    }

    protected void RadComboBox13_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox14.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox14.Items.Add(item);
        RadComboBox14.DataBind();
        RadComboBox15.Items.Clear();
    }

    protected void RadComboBox14_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox15.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox15.Items.Add(item);
        TestCategoryUserHistory[] categories = null;
        categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, false, Convert.ToInt32(RadComboBox13.SelectedValue), Convert.ToInt32(RadComboBox14.SelectedValue));
        RadComboBox15.DataSource = categories;
        RadComboBox15.DataTextField = "TestName";
        RadComboBox15.DataValueField = "TestCategoryID";
        RadComboBox15.DataBind();
    }

    protected void cmdGenerate5_Click(object sender, EventArgs e)
    {
        RadGrid5.Rebind();
    }

    protected void RadComboBox16_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox17.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox17.Items.Add(item);
        RadComboBox17.DataBind();
        RadComboBox18.Items.Clear();
    }

    protected void RadComboBox17_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox18.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox18.Items.Add(item);
        RadComboBox18.DataBind();
    }

    protected void cmdGenerate6_Click(object sender, EventArgs e)
    {
        RadGrid6.Rebind();
    }

    protected void RadComboBox19_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox20.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox20.Items.Add(item);
        RadComboBox20.DataBind();
        RadComboBox21.Items.Clear();
    }

    protected void RadComboBox20_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox21.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox21.Items.Add(item);
        TestCategoryUserHistory[] categories = null;
        categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, false, Convert.ToInt32(RadComboBox19.SelectedValue), Convert.ToInt32(RadComboBox20.SelectedValue));
        RadComboBox21.DataSource = categories;
        RadComboBox21.DataTextField = "TestName";
        RadComboBox21.DataValueField = "TestCategoryID";
        RadComboBox21.DataBind();
    }

    protected void cmdGenerate7_Click(object sender, EventArgs e)
    {
        RadGrid7.Rebind();
    }

    protected void RadComboBox22_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox23.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox23.Items.Add(item);
        RadComboBox23.DataBind();
        RadComboBox24.Items.Clear();
    }

    protected void RadComboBox23_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox24.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox24.Items.Add(item);
        TestCategoryUserHistory[] categories = null;
        categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, false, Convert.ToInt32(RadComboBox22.SelectedValue), Convert.ToInt32(RadComboBox23.SelectedValue));
        RadComboBox24.DataSource = categories;
        RadComboBox24.DataTextField = "TestName";
        RadComboBox24.DataValueField = "TestCategoryID";
        RadComboBox24.DataBind();
    }

    protected void cmdGenerate8_Click(object sender, EventArgs e)
    {
        RadGrid8.Rebind();
    }

    protected void RadComboBox25_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox26.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox26.Items.Add(item);
        RadComboBox26.DataBind();
        RadComboBox27.Items.Clear();
    }

    protected void RadComboBox26_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox27.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox27.Items.Add(item);
        RadComboBox27.DataBind();
    }

    protected void cmdGenerate9_Click(object sender, EventArgs e)
    {
        RadGrid9.Rebind();
    }
    //ADDED BY RAYMARK COSME <01/31/2018>
    protected void RadComboBox28_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox29.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox29.Items.Add(item);
        RadComboBox29.DataBind();
        RadComboBox30.Items.Clear();
    }
    //ADDED BY RAYMARK COSME <01/31/2018>
    protected void RadComboBox29_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBox30.Items.Clear();
        RadComboBoxItem item = new RadComboBoxItem();
        item.Text = string.Empty;
        item.Value = string.Empty;
        RadComboBox30.Items.Add(item);
        TestCategoryUserHistory[] categories = null;
        categories = TestCategoryUserHistory.SelectAllUserHistory(SessionManager.SessionUsername, false, false, false, Convert.ToInt32(RadComboBox28.SelectedValue), Convert.ToInt32(RadComboBox29.SelectedValue));
        RadComboBox30.DataSource = categories;
        RadComboBox30.DataTextField = "TestName";
        RadComboBox30.DataValueField = "TestCategoryID";
        RadComboBox30.DataBind();
    }
    //ADDED BY RAYMARK COSME <01/31/2018>
    protected void cmdGenerate10_Click(object sender, EventArgs e)
    {
        RadGrid10.Rebind();
    }

    protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid2_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid3_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid4_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid5_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid6_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid7_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid8_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid9_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void RadGrid10_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridCommandItem)
        {
            e.Item.FindControl("InitInsertButton").Visible = false;
            e.Item.FindControl("AddNewRecordButton").Visible = false;

            //e.Item.FindControl("RefreshButton").Visible = false;
            //e.Item.FindControl("RebindGridButton").Visible = false;
        }
    }

    protected void GetUserClient()
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT ClientID FROM TranscomUniv_Local.dbo.Users WHERE TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    lblClientID.Text = String.Format("{0}", reader["ClientID"].ToString());
                }
            }
        }
    }

    //ADDED BY HAROLD SENIER <09/19/2018>

    protected void gridGdprReport_NeedDataSource(object sender, EventArgs e)
    {

        try
        {
            string courseId = rcbGdprCourse.SelectedValue;
            DateTime startDate = (DateTime)rdpGdprStartDate.SelectedDate;
            DateTime endDate = (DateTime)rdpGdprEndDate.SelectedDate;

            gridGdprReport.ExportSettings.IgnorePaging = true;
            gridGdprReport.ExportSettings.ExportOnlyData = true;
            gridGdprReport.ExportSettings.OpenInNewWindow = true;
            gridGdprReport.ExportSettings.FileName = rcbGdprCourse.Text + " Raw Data As Of " + DateTime.Now.ToString("MM/dd/yyyy");
            lblCourseName.Text = rcbGdprCourse.Text;

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["INTRANETConnectionString"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_transcomuniversity_getGdprCompletionReport";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CourseIDs", courseId);
                cmd.Parameters.Add("@StartDate", startDate);
                cmd.Parameters.Add("@EndDate", endDate);
                cmd.CommandTimeout = 50000;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                da.Fill(ds);

                gridGdprReport.DataSource = ds;
                //gridGdprReport.DataBind();
            }


        }
        catch (Exception)
        {
            gridGdprReport.DataSource = null;
            //throw;
        }


    }

    protected void btnGenerateGdprReport_Click(object sender, EventArgs e)
    {
        //RebindGdprReport();
        gridGdprReport.Rebind();
    }


    #region DAILY VIEWS REPORT MDQUERUBIN 05/02/2019
    protected void grid_DailyViewsReport_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            DateTime startDate = (DateTime)rad_DailyViewsDateStart.SelectedDate;
            DateTime endDate = (DateTime)rad_DailyViewsDateEnd.SelectedDate;

            grid_DailyViewsReport.ExportSettings.IgnorePaging = true;
            grid_DailyViewsReport.ExportSettings.ExportOnlyData = true;
            grid_DailyViewsReport.ExportSettings.OpenInNewWindow = true;
            grid_DailyViewsReport.ExportSettings.FileName = "Daily_Views_RawData_As_Of " + DateTime.Now.ToString("MM/dd/yyyy");


            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"rpt_DailyViews";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@dt_start", startDate);
                cmd.Parameters.Add("@dt_end", endDate);
                cmd.CommandTimeout = 50000;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                da.Fill(ds);

                grid_DailyViewsReport.DataSource = ds;
            }


        }
        catch (Exception)
        {
            grid_DailyViewsReport.DataSource = null;
        }
    }
    protected void btn_DailyViewsGenerateReport_Click(object sender, EventArgs e)
    {
        grid_DailyViewsReport.Rebind();
    }
    #endregion

}
