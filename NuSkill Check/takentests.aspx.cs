using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class takentests : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            ViewState["sortOrder"] = "";
            this.loadingMethod("","");
        }
    }

    protected void gv_Sorting(object sender, GridViewSortEventArgs e)
    {

        loadingMethod(e.SortExpression, sortOrder);

    }

    public string sortOrder
    {
        get
        {
            if (ViewState["sortOrder"].ToString() == "desc")
            {
                ViewState["sortOrder"] = "asc";
            }
            else
            {
                ViewState["sortOrder"] = "desc";
            }

            return ViewState["sortOrder"].ToString();
        }
        set
        {
            ViewState["sortOrder"] = value;
        }
    }  

    protected void gvRecentTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "View")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                Label lblDateStartTaken = row.FindControl("lblStartDate") as Label;
                Label lblDateEndTaken = row.FindControl("lblEndDate") as Label;
                Label lblTestName = row.FindControl("lblTestName") as Label;



                TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), true);
                if (category != null)
                {
                    this.lblResultsExamName.Text = lblTestName.Text;
                    this.lblDateStartedVal.Text = lblDateStartTaken.Text;
                    this.lblDateFinishedVal.Text = lblDateEndTaken.Text;
                    this.lblCIMVal.Text = SessionManager.SessionUsername.ToString();
                    Questionnaire[] questions = Questionnaire.SelectByCategory(category.TestCategoryID);
                    TestResponse[] responses = TestResponse.SelectByTestTaken(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    this.gvExamResults.DataSource = responses;
                    this.gvExamResults.DataBind();
                    if (responses.Length < 1)
                        this.gvExamResults.EmptyDataText = "This exam has not been finished.";
                    foreach (GridViewRow inRow in this.gvExamResults.Rows)
                    {
                        Label lblQuestionnaireID = inRow.FindControl("lblQuestionnaireID") as Label;
                        Label lblTestResponseID = inRow.FindControl("lblTestResponseID") as Label;
                        Label lblQuestion = inRow.FindControl("lblQuestion") as Label;
                        Label lblEssayScore = inRow.FindControl("lblEssayScore") as Label;
                        Label lblYourAnswer = inRow.FindControl("lblYourAnswer") as Label;
                        Label lblCheckerComments = inRow.FindControl("lblCheckerComments") as Label;
                        Image imgResult = inRow.FindControl("imgResult") as Image;
                        Questionnaire question = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), true);
                        if (question != null)
                            lblQuestion.Text = question.Question;
                        TestResponse tResponse = TestResponse.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()), question.QuestionnaireID, SessionManager.SessionUsername);
                        if (tResponse != null)
                        {
                            string response = tResponse.Response1;

                            response += string.IsNullOrEmpty(tResponse.Response2) ? "" : " " + tResponse.Response2;
                            response += string.IsNullOrEmpty(tResponse.Response3) ? "" : " " + tResponse.Response3;
                            response += string.IsNullOrEmpty(tResponse.Response4) ? "" : " " + tResponse.Response4;
                            response += string.IsNullOrEmpty(tResponse.Response5) ? "" : " " + tResponse.Response5;
                            response += string.IsNullOrEmpty(tResponse.Response6) ? "" : " " + tResponse.Response6;
                            response += string.IsNullOrEmpty(tResponse.Response7) ? "" : " " + tResponse.Response7;
                            response += string.IsNullOrEmpty(tResponse.Response8) ? "" : " " + tResponse.Response8;
                            response += string.IsNullOrEmpty(tResponse.Response9) ? "" : " " + tResponse.Response9;
                            response += string.IsNullOrEmpty(tResponse.Response10) ? "" : " " + tResponse.Response10;

                            lblYourAnswer.Text = response.Trim();


                            if (question.TypeCode == "essay")
                            {
                                lblYourAnswer.Text = tResponse.EssayResponse;
                                EssayMarker marker = EssayMarker.SelectByTestResponse(tResponse.TestResponseID);
                                lblCheckerComments.Text = marker.CheckerComments.Trim();
                            }

                            if (!string.IsNullOrEmpty(tResponse.EssayResponse))
                            {
                                imgResult.ImageUrl = "~/images/icon-essay.gif";
                                lblEssayScore.Text = tResponse.EssayScore.ToString();
                            } 
                            else if (question.TypeCode == "matching")
                            {
                                if (tResponse.Response1 == question.Ans1.Split('|')[0] && tResponse.Response2 == question.Ans2.Split('|')[0] &&
                                tResponse.Response3 == question.Ans3.Split('|')[0] && tResponse.Response4 == question.Ans4.Split('|')[0] &&
                                tResponse.Response5 == question.Ans5.Split('|')[0] && tResponse.Response6 == question.Ans6.Split('|')[0] &&
                                tResponse.Response7 == question.Ans7.Split('|')[0] && tResponse.Response8 == question.Ans8.Split('|')[0] &&
                                tResponse.Response9 == question.Ans9.Split('|')[0] && tResponse.Response10 == question.Ans10.Split('|')[0])
                                    imgResult.ImageUrl = "~/images/icon-pass.gif";
                                else
                                    imgResult.ImageUrl = "~/images/icon-fail.gif";
                            }
                            else if (
                                tResponse.Response1 == question.Ans1 && tResponse.Response2 == question.Ans2 && tResponse.Response3 == question.Ans3 &&
                                tResponse.Response4 == question.Ans4 && tResponse.Response5 == question.Ans5 && tResponse.Response6 == question.Ans6 &&
                                tResponse.Response7 == question.Ans7 && tResponse.Response8 == question.Ans8 && tResponse.Response9 == question.Ans9 &&
                                tResponse.Response10 == question.Ans10
                              )
                                imgResult.ImageUrl = "~/images/icon-pass.gif";
                            else
                                imgResult.ImageUrl = "~/images/icon-fail.gif";
                        }
                    }
                    this.mpeAnswers.Show();
                }
                else
                {
                    lblMessage.Text = "The exam could not be found. It may have been deleted.";
                    this.mpeMessage.Show();
                }
            }

        }
        catch(Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(4, "takentests.aspx", "gvRecentTests_RowCommand", ex.Message);
            SessionManager.SessionMainText = "An error was encountered while trying to open a taken exam.";
            Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
        }
    }

    protected void loadingMethod(string sortExp, string sortDir)
    {
        try
        {
            DataSet set = TestTaken.SelectByUser(SessionManager.SessionUsername, false);

            DataView myDataView = new DataView();
            myDataView = set.Tables[0].DefaultView;

            myDataView.Sort = "DateEndTaken desc";

            if (sortExp != string.Empty)
            {
                myDataView.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }


            this.gvRecentTests.DataSource = myDataView;
            this.gvRecentTests.DataBind();

            foreach (GridViewRow row in this.gvRecentTests.Rows)
            {
                Image image = row.FindControl("imgTestStatus") as Image;
                Label label = row.FindControl("lblTestName") as Label;
                Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                Label lblStartDate = row.FindControl("lblStartDate") as Label;
                Label lblEndDate = row.FindControl("lblEndDate") as Label;
                Label lblDateLastSave = row.FindControl("DateLastSave") as Label;
                Label lblPassed = row.FindControl("lblPassed") as Label;
                Label lblResponseCount = row.FindControl("lblResponseCount") as Label;
                Label lblHideScores = row.FindControl("lblHideScores") as Label;
                LinkButton lnkViewAnswers = row.FindControl("lnkViewAnswers") as LinkButton;

                if (lblHideScores.Text.ToLower() != "true")
                {
                    if (Convert.ToInt32(lblResponseCount.Text.Trim()) > 0)
                    {
                        image.ImageUrl = "~/images/icon-pending.gif";
                        if (string.IsNullOrEmpty(lblEndDate.Text))
                            lblEndDate.Text = "Pending";
                        this.gvExamResults.EmptyDataText = "This exam is pending checking.";
                    }
                    else if (string.IsNullOrEmpty(lblEndDate.Text))
                    {
                        image.ImageUrl = "~/images/icon-resume.gif";
                        lblEndDate.Text = "Unfinished";
                        this.gvExamResults.EmptyDataText = "This exam is unfinished.";
                    }
                    else if (lblPassed.Text == "True")
                        image.ImageUrl = "~/images/icon-pass.gif";
                    else
                        image.ImageUrl = "~/images/icon-fail.gif";
                    if (label.Text.Length > 50)
                        label.Text = label.Text.Substring(0, 47) + "...";
                }
                else
                {
                    image.Visible = false;
                    lnkViewAnswers.Enabled = false;
                }
                //TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                //if (taken != null)
                //{
                //    SaveTestResponse[] response = SaveTestResponse.CheckPendingEssays(taken.UserID, taken.TestTakenID);

                //    if (response.Length > 0)
                //        image.ImageUrl = "~/images/icon-pending.gif";
                //    else if (taken.DateEndTaken.ToString().Contains("1/1/0001"))
                //        image.ImageUrl = "~/images/icon-resume.gif";
                //    else if (taken.Passed)
                //        image.ImageUrl = "~/images/icon-pass.gif";
                //    else if (!taken.Passed)
                //        image.ImageUrl = "~/images/icon-fail.gif";
                //    if (taken.DateLastSave.Value.ToString().Contains("1/1/0001"))
                //        lblEndDate.Text = "N/A";
                //    if (label.Text.Length > 50)
                //        label.Text = label.Text.Substring(0, 47) + "...";
                //}
            }
        }
        catch(Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(4, "takentests.aspx", "loadingMethod", ex.Message);
            SessionManager.SessionMainText = "An error was encountered while trying to load your recent tests.";
            Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
        }
    }

    protected void btnMessage_Click(object sender, EventArgs e)
    {
        this.loadingMethod("","");
    }

    protected void btnCloseSummary_Click(object sender, EventArgs e)
    {
        //this.loadingMethod();
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        this.loadingMethod("","");
    }
}
