<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="home, App_Web_yuclf4ni" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblHome" runat="server" Text="Home" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <br /><br /><br /><br /><br />
                                <asp:Label ID="lblWelcome" runat="server" Text="Welcome to Transcom University Testing Admin. Please select from the menu above." />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
