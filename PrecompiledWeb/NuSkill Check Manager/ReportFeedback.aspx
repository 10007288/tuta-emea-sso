﻿<%@ page language="C#" autoeventwireup="true" inherits="ReportFeedback, App_Web_yuclf4ni" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Feedback Report</title>
    <link id="CSS" rel="stylesheet" type="text/css" href="styles.css" runat="server" />
    <style type="text/css">
        .reportTitle
        {
            margin-top: 15px;
            padding-left: 5px;
            height: 30px;
            font-size: 1.7em;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock runat="server">
        <script type="text/javascript">
            function conditionalPostback(sender, args) {
                if (args.get_eventTarget() == "<%= btnExportToExcel.UniqueID %>") {
                    args.set_enableAjax(false);
                }
            }
            function RequestFullScreen(sender, args) {
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnViewReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridFeedback" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnExportToExcel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div class="container">
                <div runat="server" id="divParameterControls" style="width: 100%; padding-bottom: 10px">
                    <div class="insidecontainer" style="height: 50px">
                        <div style="float: left; width: 175px">
                            <div class="divFieldLabel">
                                <label>
                                    Course</label>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cbCourse"
                                    ValidationGroup="view" SetFocusOnError="True" ErrorMessage="* select course"
                                    Display="Dynamic" CssClass="displayerror" />--%>
                            </div>
                            <div class="divField">
                                <telerik:RadComboBox runat="server" ID="cbCourse" DataTextField="Title" DataValueField="CourseID"
                                    EmptyMessage="- select course -" DataSourceID="CoursesDataSource" DropDownAutoWidth="Enabled"
                                    CheckBoxes="False" MaxHeight="300px" MarkFirstMatch="True" ChangeTextOnKeyBoardNavigation="True"
                                    HighlightTemplatedItems="True" AllowCustomText="False" AppendDataBoundItems="True">
                                    <Items>
                                        <telerik:RadComboBoxItem runat="server" Text="- select -" />
                                    </Items>
                                </telerik:RadComboBox>
                            </div>
                        </div>
                        <div style="float: left; width: 225px">
                            <div class="divFieldLabel">
                                <label>
                                    Date From - To</label>
                                <asp:CompareValidator ID="DateCompareValidator" runat="server" ControlToValidate="dpEndDate"
                                    ControlToCompare="dpStartDate" Operator="GreaterThanEqual" Type="Date" ErrorMessage="Invalid entry!"
                                    ValidationGroup="view" Display="Dynamic" CssClass="displayerror" />
                            </div>
                            <div class="divField">
                                <telerik:RadDatePicker runat="server" ID="dpStartDate" Width="100" />
                                <label>
                                    -</label>
                                <telerik:RadDatePicker runat="server" ID="dpEndDate" Width="100" />
                            </div>
                        </div>
                    </div>
                    <br />
                    <div id="managecontrol" style="width: 100%">
                        <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1" ClientEvents-OnRequestStart="conditionalPostback"
                            Width="100%">
                            <asp:LinkButton runat="server" ID="btnViewReport" Text="Generate Report" ValidationGroup="view"
                                OnClick="BtnViewReportClick" />
                            <asp:LinkButton runat="server" ID="btnClearParams" Text="Reset Filters" CausesValidation="False"
                                OnClick="BtnClearParamsClick" />
                            <asp:LinkButton ID="btnExportToExcel" runat="server" Text="Export Grid result to Excel"
                                AlternateText="ExcelML" ToolTip="Export to Excel" OnClick="BtnExportToExcelClick"
                                CausesValidation="False" />
                        </telerik:RadAjaxPanel>
                    </div>
                </div>
                <div class="paneldefault">
                    <div class="insidecontainer">
                        <div runat="server" id="divTUMain">
                            <telerik:RadGrid ID="gridFeedback" runat="server" CellSpacing="0" AutoGenerateColumns="False"
                                DataSourceID="FeedbackData" GridLines="None" AllowPaging="False" Skin="Sitefinity"
                                Width="100%">
                                <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" TableLayout="Auto" DataSourceID="FeedbackData">
                                    <CommandItemTemplate>
                                        <div class="reportTitle">
                                            <asp:Label ID="lblTitle" Text="User Feedback" runat="server" />
                                        </div>
                                    </CommandItemTemplate>
                                    <CommandItemSettings ExportToPdfText="Export to Pdf" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" HeaderText="FeebackID" UniqueName="ID" DataType="System.Decimal"
                                            ReadOnly="True" SortExpression="ID" />
                                        <telerik:GridBoundColumn DataField="CourseID" HeaderText="CourseID" UniqueName="CourseID"
                                            DataType="System.Int32" SortExpression="CourseID" />
                                        <telerik:GridBoundColumn DataField="Course" HeaderText="Course" UniqueName="Course"
                                            SortExpression="Course" />
                                        <telerik:GridBoundColumn DataField="CIM" HeaderText="CIM" UniqueName="CIM" DataType="System.Int64"
                                            SortExpression="CIM" />
                                        <telerik:GridBoundColumn DataField="EmployeeName" HeaderText="EmployeeName" UniqueName="EmployeeName"
                                            SortExpression="EmployeeName" />
                                        <telerik:GridBoundColumn DataField="Email" HeaderText="Email" UniqueName="Email"
                                            SortExpression="Email" />
                                        <telerik:GridBoundColumn DataField="DateAdded" HeaderText="DateAdded" UniqueName="DateAdded"
                                            DataType="System.DateTime" SortExpression="DateAdded" />
                                        <telerik:GridBoundColumn DataField="FeedID" HeaderText="FeedID" UniqueName="FeedID"
                                            DataType="System.Decimal" SortExpression="FeedID" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q1_A_Ans1" HeaderText="Answer1" UniqueName="Q1_A_Ans1"
                                            SortExpression="Q1_A_Ans1" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q1_B_Ans2" HeaderText="Answer2" UniqueName="Q1_B_Ans2"
                                            SortExpression="Q1_B_Ans2" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q1_C_Ans3" HeaderText="Answer3" UniqueName="Q1_C_Ans3"
                                            SortExpression="Q1_C_Ans3" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q1_D_Ans4" HeaderText="Answer4" UniqueName="Q1_D_Ans4"
                                            SortExpression="Q1_D_Ans4" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q1_E_Ans5" HeaderText="Answer5" UniqueName="Q1_E_Ans5"
                                            SortExpression="Q1_E_Ans5" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q1_F_Ans6" HeaderText="Answer6" UniqueName="Q1_F_Ans6"
                                            SortExpression="Q1_F_Ans6" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q1_G_Ans7" HeaderText="Answer7" UniqueName="Q1_G_Ans7"
                                            SortExpression="Q1_G_Ans7" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q2_A_Ans1" HeaderText="Answer8" UniqueName="Q2_A_Ans1"
                                            SortExpression="Q2_A_Ans1" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q2_B_Ans2" HeaderText="Answer9" UniqueName="Q2_B_Ans2"
                                            SortExpression="Q2_B_Ans2" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q2_C_Ans3" HeaderText="Answer10" UniqueName="Q2_C_Ans3"
                                            SortExpression="Q2_C_Ans3" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q2_D_Ans4" HeaderText="Answer11" UniqueName="Q2_D_Ans4"
                                            SortExpression="Q2_D_Ans4" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q2_E_Ans5" HeaderText="Answer12" UniqueName="Q2_E_Ans5"
                                            SortExpression="Q2_E_Ans5" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q2_F_Ans6" HeaderText="Answer13" UniqueName="Q2_F_Ans6"
                                            SortExpression="Q2_F_Ans6" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q2_G_Ans7" HeaderText="Answer14" UniqueName="Q2_G_Ans7"
                                            SortExpression="Q2_G_Ans7" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q3_A_Ans1" HeaderText="Answer15" UniqueName="Q3_A_Ans1"
                                            SortExpression="Q3_A_Ans1" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q3_B_Ans2" HeaderText="Answer16" UniqueName="Q3_B_Ans2"
                                            SortExpression="Q3_B_Ans2" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q3_C_Ans3" HeaderText="Answer17" UniqueName="Q3_C_Ans3"
                                            SortExpression="Q3_C_Ans3" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q4_A_Ans1" HeaderText="Answer18" UniqueName="Q4_A_Ans1"
                                            SortExpression="Q4_A_Ans1" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q4_B_Ans2" HeaderText="Answer19" UniqueName="Q4_B_Ans2"
                                            SortExpression="Q4_B_Ans2" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q4_C_Ans3" HeaderText="Answer20" UniqueName="Q4_C_Ans3"
                                            SortExpression="Q4_C_Ans3" Display="False" />
                                        <telerik:GridBoundColumn DataField="Q4_D_Ans4" HeaderText="Answer21" UniqueName="Q4_D_Ans4"
                                            SortExpression="Q4_D_Ans4" Display="False" />
                                    </Columns>
                                </MasterTableView>
                                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
                                </HeaderContextMenu>
                            </telerik:RadGrid>
                        </div>
                        <div align="right" style="width: 100%; padding-top: 10px">
                            <asp:LinkButton runat="server" ID="btnBack" Text="Back to top" OnClientClick="javascript:scroll(0,0); return false;" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="CoursesDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:INTRANETConnectionString %>"
            SelectCommand="SELECT [CourseID], [Title] FROM [tbl_TranscomUniversity_Cor_Course] WHERE ([HideFromList] = @HideFromList) ORDER BY [Title]">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="HideFromList" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="FeedbackData" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="pr_RPT_GetUserFeedback" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbCourse" DefaultValue="" Name="CourseID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="dpStartDate" DefaultValue="04/01/2014" Name="StartDate"
                    PropertyName="SelectedDate" Type="DateTime" />
                <asp:ControlParameter ControlID="dpEndDate" DefaultValue="01/01/2020" Name="EndDate"
                    PropertyName="SelectedDate" Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
