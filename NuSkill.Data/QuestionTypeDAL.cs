using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class QuestionTypeDAL : TheLibrary.DBImportTool.Base
    {
        public QuestionTypeDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public QuestionTypeDAL(string connString)
        {
            this.ConnectionString = connString;
        }

        public void Insert(string typeCode, string description, bool hideFromList, string username)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTypeCode", SqlDbType.VarChar, typeCode));
            paramList.Add(Helper.CreateParam("@pDescription", SqlDbType.VarChar, description));
            paramList.Add(Helper.CreateParam("@pDateCreated", SqlDbType.DateTime, DateTime.Now));
            paramList.Add(Helper.CreateParam("@pDateLastUpdated", SqlDbType.DateTime, DateTime.Now));
            paramList.Add(Helper.CreateParam("@pLastUpdated", SqlDbType.VarChar, username));
            paramList.Add(Helper.CreateParam("@pLastUpdatedBy", SqlDbType.VarChar, username));
            paramList.Add(Helper.CreateParam("@pHideFromList", SqlDbType.Bit, hideFromList));
            Connection.ExecuteSP("pr_questiontype_lkp_insert", paramList.ToArray());
        }

        public DataSet Select(string typeCode)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTypeCode", SqlDbType.VarChar, typeCode));
            return Connection.ExecuteSPQuery("pr_questiontype_lkp_getqtype", paramList.ToArray());
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_questiontype_lkp_getqtypeall");
        }
    }
}
