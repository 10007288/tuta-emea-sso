﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuSkill.Business;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class directreoprts : System.Web.UI.Page
{
    string _Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserRole();

        if (_Role != "Admin")
        {
            Response.Redirect("Home.aspx");
        }

        if (!IsPostBack)
        {
            GetUserRole();

            if (_Role != "Admin")
            {
                Response.Redirect("Home.aspx");
            }

            Label1.Text = SessionManager.SessionUsername;
            //this.loadingMethod("", "");
        }
    }

    protected void GetUserRole()
    {
        //Check user role of the current user
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }

    protected void gv_Sorting(object sender, GridViewSortEventArgs e)
    {

        loadingMethod(e.SortExpression, sortOrder);

    }

    public string sortOrder
    {
        get
        {
            if (ViewState["sortOrder"].ToString() == "desc")
            {
                ViewState["sortOrder"] = "asc";
            }
            else
            {
                ViewState["sortOrder"] = "desc";
            }

            return ViewState["sortOrder"].ToString();
        }
        set
        {
            ViewState["sortOrder"] = value;
        }
    }

    protected void loadingMethod(string sortExp, string sortDir)
    {
        try
        {
            DataSet set = TestTaken.SelectByUser(TextBox1.Text, false);

            DataView myDataView = new DataView();
            myDataView = set.Tables[0].DefaultView;

            myDataView.Sort = "DateEndTaken desc";

            if (sortExp != string.Empty)
            {
                myDataView.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            this.gvRecentTests.DataSource = myDataView;
            this.gvRecentTests.DataBind();

            foreach (GridViewRow row in this.gvRecentTests.Rows)
            {
                Image image = row.FindControl("imgTestStatus") as Image;
                Label label = row.FindControl("lblTestName") as Label;
                Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                Label lblStartDate = row.FindControl("lblStartDate") as Label;
                Label lblEndDate = row.FindControl("lblEndDate") as Label;
                Label lblDateLastSave = row.FindControl("DateLastSave") as Label;
                Label lblPassed = row.FindControl("lblPassed") as Label;
                Label lblResponseCount = row.FindControl("lblResponseCount") as Label;
                Label lblHideScores = row.FindControl("lblHideScores") as Label;
                LinkButton lnkViewAnswers = row.FindControl("lnkViewAnswers") as LinkButton;

                if (lblHideScores.Text.ToLower() != "true")
                {
                    if (Convert.ToInt32(lblResponseCount.Text.Trim()) > 0)
                    {
                        image.ImageUrl = "~/images/icon-pending.gif";
                        if (string.IsNullOrEmpty(lblEndDate.Text))
                            lblEndDate.Text = "Pending";
                        this.gvExamResults.EmptyDataText = "This exam is pending checking.";
                    }
                    else if (string.IsNullOrEmpty(lblEndDate.Text))
                    {
                        image.ImageUrl = "~/images/icon-resume.gif";
                        lblEndDate.Text = "Unfinished";
                        this.gvExamResults.EmptyDataText = "This exam is unfinished.";
                    }
                    else if (lblPassed.Text == "True")
                        image.ImageUrl = "~/images/icon-pass.gif";
                    else
                        image.ImageUrl = "~/images/icon-fail.gif";
                    if (label.Text.Length > 50)
                        label.Text = label.Text.Substring(0, 47) + "...";
                }
                else
                {
                    image.Visible = false;
                    lnkViewAnswers.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(4, "takentests.aspx", "loadingMethod", ex.Message);
            Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
        }
    }

    protected void gvRecentTests_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "View")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
                Label lblTestCategoryID = row.FindControl("lblTestCategoryID") as Label;
                Label lblDateStartTaken = row.FindControl("lblStartDate") as Label;
                Label lblDateEndTaken = row.FindControl("lblEndDate") as Label;
                Label lblTestName = row.FindControl("lblTestName") as Label;

                TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), true);
                if (category != null)
                {
                    this.lblResultsExamName.Text = lblTestName.Text;
                    this.lblDateStartedVal.Text = lblDateStartTaken.Text;
                    this.lblDateFinishedVal.Text = lblDateEndTaken.Text;
                    Questionnaire[] questions = Questionnaire.SelectByCategory(category.TestCategoryID);
                    TestResponse[] responses = TestResponse.SelectByTestTaken(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                    this.gvExamResults.DataSource = responses;
                    this.gvExamResults.DataBind();
                    if (responses.Length < 1)
                        this.gvExamResults.EmptyDataText = "This exam has not been finished.";
                    foreach (GridViewRow inRow in this.gvExamResults.Rows)
                    {
                        Label lblQuestionnaireID = inRow.FindControl("lblQuestionnaireID") as Label;
                        Label lblTestResponseID = inRow.FindControl("lblTestResponseID") as Label;
                        Label lblQuestion = inRow.FindControl("lblQuestion") as Label;
                        Label lblEssayScore = inRow.FindControl("lblEssayScore") as Label;
                        Label lblYourAnswer = inRow.FindControl("lblYourAnswer") as Label;
                        Label lblCheckerComments = inRow.FindControl("lblCheckerComments") as Label;
                        Image imgResult = inRow.FindControl("imgResult") as Image;
                        Questionnaire question = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), true);
                        if (question != null)
                            lblQuestion.Text = question.Question;
                        TestResponse tResponse = TestResponse.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()), question.QuestionnaireID, TextBox1.Text); //SessionManager.SessionUsername);
                        if (tResponse != null)
                        {
                            string response = tResponse.Response1;

                            response += string.IsNullOrEmpty(tResponse.Response2) ? "" : " " + tResponse.Response2;
                            response += string.IsNullOrEmpty(tResponse.Response3) ? "" : " " + tResponse.Response3;
                            response += string.IsNullOrEmpty(tResponse.Response4) ? "" : " " + tResponse.Response4;
                            response += string.IsNullOrEmpty(tResponse.Response5) ? "" : " " + tResponse.Response5;
                            response += string.IsNullOrEmpty(tResponse.Response6) ? "" : " " + tResponse.Response6;
                            response += string.IsNullOrEmpty(tResponse.Response7) ? "" : " " + tResponse.Response7;
                            response += string.IsNullOrEmpty(tResponse.Response8) ? "" : " " + tResponse.Response8;
                            response += string.IsNullOrEmpty(tResponse.Response9) ? "" : " " + tResponse.Response9;
                            response += string.IsNullOrEmpty(tResponse.Response10) ? "" : " " + tResponse.Response10;

                            lblYourAnswer.Text = response.Trim();


                            if (question.TypeCode == "essay")
                            {
                                lblYourAnswer.Text = tResponse.EssayResponse;
                                EssayMarker marker = EssayMarker.SelectByTestResponse(tResponse.TestResponseID);
                                lblCheckerComments.Text = marker.CheckerComments.Trim();
                            }

                            if (!string.IsNullOrEmpty(tResponse.EssayResponse))
                            {
                                imgResult.ImageUrl = "~/images/icon-essay.gif";
                                lblEssayScore.Text = tResponse.EssayScore.ToString();
                            }
                            else if (question.TypeCode == "matching")
                            {
                                if (tResponse.Response1 == question.Ans1.Split('|')[0] && tResponse.Response2 == question.Ans2.Split('|')[0] &&
                                tResponse.Response3 == question.Ans3.Split('|')[0] && tResponse.Response4 == question.Ans4.Split('|')[0] &&
                                tResponse.Response5 == question.Ans5.Split('|')[0] && tResponse.Response6 == question.Ans6.Split('|')[0] &&
                                tResponse.Response7 == question.Ans7.Split('|')[0] && tResponse.Response8 == question.Ans8.Split('|')[0] &&
                                tResponse.Response9 == question.Ans9.Split('|')[0] && tResponse.Response10 == question.Ans10.Split('|')[0])
                                    imgResult.ImageUrl = "~/images/icon-pass.gif";
                                else
                                    imgResult.ImageUrl = "~/images/icon-fail.gif";
                            }
                            else if (
                                tResponse.Response1 == question.Ans1 && tResponse.Response2 == question.Ans2 && tResponse.Response3 == question.Ans3 &&
                                tResponse.Response4 == question.Ans4 && tResponse.Response5 == question.Ans5 && tResponse.Response6 == question.Ans6 &&
                                tResponse.Response7 == question.Ans7 && tResponse.Response8 == question.Ans8 && tResponse.Response9 == question.Ans9 &&
                                tResponse.Response10 == question.Ans10
                              )
                                imgResult.ImageUrl = "~/images/icon-pass.gif";
                            else
                                imgResult.ImageUrl = "~/images/icon-fail.gif";
                        }
                    }
                    this.mpeAnswers.Show();
                }
                else
                {
                    lblMessage.Text = "The exam could not be found. It may have been deleted.";
                    this.mpeMessage.Show();
                }
            }
            else if (e.CommandName == "Delete")
            {

            }

        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(4, "takentests.aspx", "gvRecentTests_RowCommand", ex.Message);
            //SessionManager.SessionMainText = "An error was encountered while trying to open a taken exam.";
            Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
        }
    }

    protected void btnMessage_Click(object sender, EventArgs e)
    {
        this.loadingMethod("", "");
    }

    protected void btnCloseSummary_Click(object sender, EventArgs e)
    {
        //this.loadingMethod();
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        this.loadingMethod("", "");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        this.loadingMethod("", "");
    }
}