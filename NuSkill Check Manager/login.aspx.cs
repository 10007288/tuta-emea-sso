using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls.WebParts;
using System.Threading;
using System.Globalization;
using Microsoft.AspNet.Membership.OpenAuth;

public partial class login : System.Web.UI.Page
{
    #region TUTAOld

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    if (!this.IsPostBack)
    //    {
    //        if (Request.QueryString.Count > 0)
    //        {
    //            try
    //            {
    //                if (!string.IsNullOrEmpty(Request.QueryString.Get("uid")))
    //                    this.txtUsername.Text = NuComm.Security.Encryption.UTF8.DecryptText(Request.QueryString.Get("uid"));
    //            }
    //            catch
    //            {
    //                this.txtUsername.Text = string.Empty;
    //            }
    //            try
    //            {
    //                if (!string.IsNullOrEmpty(Request.QueryString.Get("rid")))
    //                    SessionManager.SessionRole = Convert.ToInt32(NuComm.Security.Encryption.UTF8.DecryptText(Request.QueryString.Get("rid")));
    //            }
    //            catch
    //            {
    //                this.txtPassword.Text = string.Empty;
    //            }
    //        }
    //    }
    //}

    //protected void lnkLogin_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int temp = 0;
    //        if (int.TryParse(this.txtUsername.Text.Trim(), out temp))
    //        {
    //            if (this.LoadVerifyUser(temp.ToString(), this.txtPassword.Text.Trim()))
    //            {
    //                int userRights = 0;
    //                using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
    //                {
    //                    oconn.Open();
    //                    using (SqlCommand ocomm = new SqlCommand("select roleid from users where twwid='" + txtUsername.Text + "'", oconn))
    //                    {
    //                        ocomm.CommandType = CommandType.Text;
    //                        SqlDataReader rsdata = ocomm.ExecuteReader();
    //                        rsdata.Read();
    //                        if (rsdata.HasRows)
    //                            userRights = Convert.ToInt16(rsdata[0]);
    //                    }
    //                    oconn.Close();
    //                }

    //                if (userRights != null)
    //                {
    //                    SessionManager.SessionUserRights = userRights.ToString();
    //                    if (userRights == 1 || userRights == 5 || userRights == 6)
    //                    {
    //                        SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //                        Response.Redirect("~/home.aspx?" + Request.QueryString.ToString());
    //                    }
    //                    else
    //                    {
    //                        this.cvLoginError.IsValid = false;
    //                    }
    //                }
    //                else
    //                {
    //                    this.cvLoginError.IsValid = false;
    //                }
    //            }
    //        }
    //        else if (Registration.ValidateLogin(this.txtUsername.Text.Trim(), this.txtPassword.Text.Trim()))
    //        {
    //            UserRole[] roles = UserRole.GetUserRoles(this.txtUsername.Text.Trim());
    //            if (roles == null || roles.Length < 1)
    //                this.cvLoginError.IsValid = false;
    //            else
    //            {
    //                UserRights userRights = UserRights.Select(this.txtUsername.Text.Trim(), 1);
    //                if (userRights != null)
    //                {
    //                    SessionManager.SessionUserRights = userRights.Rights;
    //                    List<int> rights = RightsManagement.ProcessRightsList(userRights.Rights);
    //                    if (RightsManagement.ContainsOnly(userRights.Rights, 0, 19))
    //                    {
    //                        SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //                        Response.Redirect("~/deletebycim.aspx?" + Request.QueryString.ToString());
    //                    }
    //                    else if (rights.Contains(0))
    //                    {
    //                        SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //                        Response.Redirect("~/home.aspx?" + Request.QueryString.ToString());
    //                    }
    //                    else
    //                    {
    //                        this.cvLoginError.IsValid = false;
    //                    }
    //                }
    //                else
    //                {
    //                    this.cvLoginError.IsValid = false;
    //                }
    //            }
    //        }
    //        else if (LoadVerifyUser())
    //        {
    //            UserRights userRights = UserRights.Select(this.txtUsername.Text.Trim(), 1);
    //            if (userRights != null)
    //            {
    //                SessionManager.SessionUserRights = userRights.Rights;
    //                List<int> rights = RightsManagement.ProcessRightsList(userRights.Rights);
    //                if (RightsManagement.ContainsOnly(userRights.Rights, 0, 19))
    //                {
    //                    SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //                    Response.Redirect("~/deletebycim.aspx?" + Request.QueryString.ToString());
    //                }
    //                else if (rights.Contains(0))
    //                {
    //                    SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //                    Response.Redirect("~/home.aspx?" + Request.QueryString.ToString());
    //                }
    //                else
    //                {
    //                    this.cvLoginError.IsValid = false;
    //                }
    //            }
    //            else
    //            {
    //                this.cvLoginError.IsValid = false;
    //            }
    //        }
    //        else
    //        {
    //            this.cvLoginError.IsValid = false;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        this.cvLoginError.IsValid = false;
    //    }
    //}

    //private bool LoadVerifyUser()
    //{
    //    return this.LoadVerifyUser(this.txtUsername.Text.Trim(), this.txtPassword.Text.Trim());
    //}

    //private bool LoadVerifyUser(string username, string password)
    //{
    //    bool isvaliduser = false;
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        int num = 0;
    //        NuComm.Connection connection = new NuComm.Connection();
    //        connection.DatabaseServer = Config.Vader2DatabaseServer();// @"Vader2\Callisto";
    //        connection.DatabaseName = Config.Vader2DatabaseName();// "BlackBox";
    //        connection.UserName = Config.Vader2DatabaseUsername();// "us_RAS";
    //        connection.Password = Config.Vader2DatabasePassword();// "";

    //        string passencrypt = string.Empty;
    //        using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
    //        {
    //            oconn.Open();
    //            using (SqlCommand ocomm = new SqlCommand("select password from users where twwid='" + txtUsername.Text + "'", oconn))
    //            {
    //                ocomm.CommandType = CommandType.Text;
    //                SqlDataReader rsdata;
    //                rsdata = ocomm.ExecuteReader();
    //                rsdata.Read();
    //                if (rsdata.HasRows)
    //                    passencrypt = rsdata[0].ToString();
    //            }
    //            oconn.Close();
    //        }
    //        bool isCorrectPasssword = Password.ValidatePassword(txtPassword.Text, passencrypt);
    //        if (isCorrectPasssword)
    //        {
    //            connection.AddParameter("@EmpID", this.txtUsername.Text.Trim(), SqlDbType.VarChar);
    //            connection.AddParameter("@Password", passencrypt, SqlDbType.VarChar);
    //            connection.StoredProcedureName = "dbo.pr_RAS_ValidateUser";
    //            if (connection.Fill(ref ds) && (ds.Tables[0].Rows.Count > 0))
    //            {
    //                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
    //                    isvaliduser = true;
    //            }
    //            connection.DisconnectInstance();
    //            connection = null;
    //        }
    //        else
    //            isvaliduser = false;
    //        return isvaliduser;
    //    }
    //    catch (Exception ex)
    //    {
    //        //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
    //        //service.WriteError(Config.ApplicationID(), "login.aspx", "LoadVerifyUser", ex.Message);
    //        return false;
    //    }
    //}

    #endregion

    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];
    }

    public IEnumerable<ProviderDetails> GetProviderNames()
    {
        return OpenAuth.AuthenticationClients.GetAll();
    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
}
