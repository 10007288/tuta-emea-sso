using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuSkill.Business;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class reports : Page
{
    string _Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserRole();

        if (_Role == "Normal User")
        {
            Response.Redirect("Home.aspx");
        }

        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString);
        if (!IsPostBack)
        {
            GetUserRole();

            if (_Role == "Normal User")
            {
                Response.Redirect("Home.aspx");
            }

            gvReports.DataSource = TestReports.SelectAll();
            gvReports.DataBind();
            foreach (GridViewRow row in gvReports.Rows)
            {
                LinkButton lnkReport = row.FindControl("lnkReport") as LinkButton;
                //lnkReport.Attributes.Add("onClick", "window.open('" + UTF8.DecryptText(Server.UrlDecode(lnkReport.CommandArgument)).Replace("http://home.nucomm.net/applications/ReportingServicesPrototype/CustomView.aspx?Report=[NuSkillCheckV2]", "ViewReports.aspx?report=") + "')");
                string url = string.Empty;
                url = "ViewReports.aspx?report=" + lnkReport.CommandArgument;
                lnkReport.Attributes.Add("onClick", "window.open('" + url + "');");
            }
            GridView1.DataSource = TestReports.SelectAllExpired();
            GridView1.DataBind();
            foreach (GridViewRow row in GridView1.Rows)
            {
                LinkButton lnkReport = row.FindControl("lnkReport") as LinkButton;
                //lnkReport.Attributes.Add("onClick", "window.open('" + UTF8.DecryptText(Server.UrlDecode(lnkReport.CommandArgument)).Replace("http://home.nucomm.net/applications/ReportingServicesPrototype/CustomView.aspx?Report=[NuSkillCheckV2]", "ViewReports.aspx?report=") + "')");
                string url = string.Empty;
                url = "ViewReports.aspx?report=" + lnkReport.CommandArgument;
                lnkReport.Attributes.Add("onClick", "window.open('" + url + "');");
            }
        }
    }

    protected void GetUserRole()
    {
        //Check user role of the current user
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }

    protected void gvReports_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Report")
        {

        }
    }
}
