using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IUserBatch
    {
        int BatchID { get; set; }
        string BatchName { get; set; }
        DateTime DateCreated { get; set; }
        bool HideFromList { get; set; }

        IUserBatch[] SelectAll();
        int Insert(string batchName);
        IUserBatch Select(string batchName);
    }
}
