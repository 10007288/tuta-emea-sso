using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class testover : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if(!this.IsPostBack)
        {
            if (SessionManager.SessionIsEssay)
            {
                this.lblNoMoreTime.Text = "The alloted time for this exam has expired." + Environment.NewLine + "Your exam will now await the evaluation of a trainer.";
                this.btnContinue.Text = "Return";
            }
            else if (!SessionManager.SessionIsEssay)
            {
                this.lblNoMoreTime.Text = "The alloted time for this exam has expired." + Environment.NewLine + "Your exam will now be graded.";
                this.btnContinue.Text = "Grade Exam";
            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (SessionManager.SessionIsEssay)
            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
        else if (!SessionManager.SessionIsEssay)
            Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
    }
}
