<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="essays, App_Web_yuclf4ni" title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center">
        <table cellpadding="10" width="100%" cellspacing="0">
            <tr>
                <td align="left">
                    <asp:LinkButton ID="lnkGoToByTestTaken" runat="server" Text="By Test Taken" Font-Size="10"
                        ForeColor="black" Font-Names="Arial" Font-Bold="true" OnClick="lnkGoToByTestTaken_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvEssays" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="20" CellPadding="5" BorderColor="black" Font-Names="Arial" Font-Size="12px"
                        EmptyDataText="There are no pending essays." Width="100%" OnPageIndexChanging="gvEssays_PageIndexChanging"
                        OnSelectedIndexChanging="gvEssays_SelectedIndexChanging">
                        <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                        <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                            NextPageText=">" Position="Bottom" PreviousPageText="<" />
                        <RowStyle BorderColor="black" />
                        <RowStyle ForeColor="black" HorizontalAlign="left" />
                        <Columns>
                            <asp:TemplateField HeaderText="Username">
                                <ItemTemplate>
                                    <asp:Label ID="lblSaveTestResponseID" runat="server" Text='<%#Bind("SaveTestResponseID") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblUsername" runat="server" Text='<%#Bind("Username") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Test Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Question">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblQuestionName" runat="server" Text='<%#Bind("Question") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemStyle ForeColor="black" Font-Bold="true" HorizontalAlign="center" Width="60px" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkView" runat="server" Text="View" CommandName="Select" CssClass="linkButtons" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
