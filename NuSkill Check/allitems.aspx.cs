using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class allitems : System.Web.UI.Page
{
    public long seconds;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/default.aspx?" + Request.QueryString.ToString());
            if (!this.IsPostBack)
            {
                SessionManager.SessionRolItems = null;
                this.gvQuestions.DataSource = SessionManager.SessionQuestionnaire;
                this.gvQuestions.DataBind();
                SessionManager.SessionQuestionNumber = 0;
                foreach (GridViewRow row in this.gvQuestions.Rows)
                {
                    controls_examquestioncontrol control = row.FindControl("wusQuestion") as controls_examquestioncontrol;
                    if (control != null)
                    {
                        this.GenerateItem(control);
                    }
                    SessionManager.SessionQuestionNumber++;
                }
                if (SessionManager.SessionTestTaken != null)
                {
                    TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
                    if (category != null)
                    {
                        if (category.TimeLimit == 0)
                        {
                            this.lblTimeRemainingValue.Text = "No Time Limit";
                            this.tmrTimeRemaining.Enabled = false;
                        }
                        else if (SessionManager.SessionTestTaken.MinutesRemaining != 0 && SessionManager.SessionTestTaken.DateEndTaken.ToString() == "1/1/0001 12:00:00 AM")
                        {
                            seconds = SessionManager.SessionTestTaken.MinutesRemaining;
                            this.lblActualTimeLeft.Text = SessionManager.SessionTestTaken.MinutesRemaining.ToString();
                            this.ProcessTime();
                        }
                        else
                        {
                            seconds = SessionManager.SessionTestTaken.MinutesRemaining;
                            this.lblActualTimeLeft.Text = category.TimeLimit.ToString();
                            this.ProcessTime();
                        }
                    }
                }
                if (SessionManager.SessionExamIsRetake == "retake")
                {
                    lblNote.Text = "Note: This is an exam retake. The questions you answered incorrectly from the previous take are highlighted with a red border.";
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(4, "allitems.aspx", "Page_Load", ex.Message);
        }
    }

    protected void GenerateItem(controls_examquestioncontrol testControl1)
    {
        try
        {
            int qNum = SessionManager.SessionQuestionNumber + 1;
            //Get question from the session
            Questionnaire q = SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber];
            QuestionType type = QuestionType.Select(q.TypeCode);

            //ADDED BY RAYMARK COSME <01/31/2018>
            if (q.TypeCode == "rate" || q.TypeCode == "comments" || q.TypeCode == "dropdown" || q.TypeCode == "masurvey" || q.TypeCode == "calendar")
            {
                testControl1.LblItemNumber.Text = "Survey Question " + qNum.ToString() + "/" +
                                              SessionManager.SessionQuestionnaire.Count.ToString() + " - " +
                                              type.Description;
            }
            else
            {
                testControl1.LblItemNumber.Text = "Question " + qNum.ToString() + "/" +
                                                  SessionManager.SessionQuestionnaire.Count.ToString() + " - " +
                                                  type.Description;
            }

            //ADDED BY RAYMARK COSME <01/31/2018>
            testControl1.LblOther.Visible = false;
            testControl1.TxtOther.Visible = false;

            if (type.Description == "sequencing")
            {
                testControl1.LblItemMessage.Text = "Note: Answers to Sequencing questions will not be preserved when you save your test!";
            }
            SaveTestResponse response = null;
            //See if the user has saved the exam before

            string prevAction = SessionManager.SessionExamAction;
            if (SessionManager.SessionTestTaken != null) // resume exam
            {
                response = SaveTestResponse.Retrieve(SessionManager.SessionUsername, SessionManager.SessionTestTaken.TestTakenID, q.QuestionnaireID);
            }

            if (SessionManager.SessionExamIsRetake == "retake") // && SessionManager.SessionExamAction == "newexam")
            {
                response = TestResponse.RetrievePreviousAnswer(SessionManager.SessionUsername, SessionManager.SessionTestTaken.TestTakenID, q.QuestionnaireID);                

                //TODO: GLA 01102014 -- check if answer is correct -- only display correct answers
                bool bAnswerCorrect = CheckIfPreviousAnswerCorrect(response, q);

                if (bAnswerCorrect == false)
                {
                    response = null;
                    testControl1.AnsweredCorrectly = false; //TODO: GLA 01142014
                }
            }

            //if (SessionManager.SessionTestTaken != null)
            //    response = SaveTestResponse.Retrieve(SessionManager.SessionUsername, SessionManager.SessionTestTaken.TestTakenID, q.QuestionnaireID);
            
            if (q != null)
            {
                testControl1.LblQuestion.Text = q.Question;

                if (string.IsNullOrEmpty(q.Hyperlink.Trim()))
                    testControl1.HypHyperlink.Visible = false;
                else
                {
                    testControl1.HypHyperlink.NavigateUrl = q.Hyperlink;
                    testControl1.HypHyperlink.Text = q.Hyperlink;
                }

                if (q.TypeCode == "multiple")
                {
                    testControl1.PnlMultipleChoice.Visible = true;
                    if (string.IsNullOrEmpty(q.Choice10))
                        ((HtmlTableRow)testControl1.Objects["trCheck10"]).Visible = false;
                    else
                        testControl1.ChkChoice10.Text = q.Choice10;

                    if (string.IsNullOrEmpty(q.Choice9))
                        ((HtmlTableRow)testControl1.Objects["trCheck9"]).Visible = false;
                    else
                        testControl1.ChkChoice9.Text = q.Choice9;

                    if (string.IsNullOrEmpty(q.Choice8))
                        ((HtmlTableRow)testControl1.Objects["trCheck8"]).Visible = false;
                    else
                        testControl1.ChkChoice8.Text = q.Choice8;

                    if (string.IsNullOrEmpty(q.Choice7))
                        ((HtmlTableRow)testControl1.Objects["trCheck7"]).Visible = false;
                    else
                        testControl1.ChkChoice7.Text = q.Choice7;

                    if (string.IsNullOrEmpty(q.Choice6))
                        ((HtmlTableRow)testControl1.Objects["trCheck6"]).Visible = false;
                    else
                        testControl1.ChkChoice6.Text = q.Choice6;

                    if (string.IsNullOrEmpty(q.Choice5))
                        ((HtmlTableRow)testControl1.Objects["trCheck5"]).Visible = false;
                    else
                        testControl1.ChkChoice5.Text = q.Choice5;

                    if (string.IsNullOrEmpty(q.Choice4))
                        ((HtmlTableRow)testControl1.Objects["trCheck4"]).Visible = false;
                    else
                        testControl1.ChkChoice4.Text = q.Choice4;

                    if (string.IsNullOrEmpty(q.Choice3))
                        ((HtmlTableRow)testControl1.Objects["trCheck3"]).Visible = false;
                    else
                        testControl1.ChkChoice3.Text = q.Choice3;

                    if (string.IsNullOrEmpty(q.Choice2))
                        ((HtmlTableRow)testControl1.Objects["trCheck2"]).Visible = false;
                    else
                        testControl1.ChkChoice2.Text = q.Choice2;

                    if (string.IsNullOrEmpty(q.Choice1))
                        ((HtmlTableRow)testControl1.Objects["trCheck1"]).Visible = false;
                    else
                        testControl1.ChkChoice1.Text = q.Choice1;

                    if (response != null)
                    {
                        testControl1.ChkChoice1.Checked = !string.IsNullOrEmpty(response.Response1);
                        testControl1.ChkChoice2.Checked = !string.IsNullOrEmpty(response.Response2);
                        testControl1.ChkChoice3.Checked = !string.IsNullOrEmpty(response.Response3);
                        testControl1.ChkChoice4.Checked = !string.IsNullOrEmpty(response.Response4);
                        testControl1.ChkChoice5.Checked = !string.IsNullOrEmpty(response.Response5);
                        testControl1.ChkChoice6.Checked = !string.IsNullOrEmpty(response.Response6);
                        testControl1.ChkChoice7.Checked = !string.IsNullOrEmpty(response.Response7);
                        testControl1.ChkChoice8.Checked = !string.IsNullOrEmpty(response.Response8);
                        testControl1.ChkChoice9.Checked = !string.IsNullOrEmpty(response.Response9);
                        testControl1.ChkChoice10.Checked = !string.IsNullOrEmpty(response.Response10);
                    }
                }
                else if (q.TypeCode == "hotspot")
                {
                    testControl1.PnlHotspot.Visible = true;
                    testControl1.ImgChoice1.ImageUrl = q.Choice1;
                    testControl1.ImgChoice2.ImageUrl = q.Choice2;
                    testControl1.ImgChoice3.ImageUrl = q.Choice3;
                    testControl1.ImgChoice4.ImageUrl = q.Choice4;
                    testControl1.ImgChoice5.ImageUrl = q.Choice5;
                    testControl1.ImgChoice6.ImageUrl = q.Choice6;
                    testControl1.ImgChoice7.ImageUrl = q.Choice7;
                    testControl1.ImgChoice8.ImageUrl = q.Choice8;
                    testControl1.ImgChoice9.ImageUrl = q.Choice9;
                    testControl1.ImgChoice10.ImageUrl = q.Choice10;

                    if (string.IsNullOrEmpty(q.Choice10))
                        testControl1.ImgCell10.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice9))
                        testControl1.ImgCell9.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice8))
                        testControl1.ImgCell8.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice7))
                        testControl1.ImgCell7.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice6))
                        testControl1.ImgCell6.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice5))
                        testControl1.ImgCell5.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice4))
                        testControl1.ImgCell4.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice3))
                        testControl1.ImgCell3.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice2))
                        testControl1.ImgCell2.Visible = false;

                    if (string.IsNullOrEmpty(q.Choice1))
                        testControl1.ImgCell1.Visible = false;

                    if (response != null)
                    {
                        testControl1.ImgChoice1.BorderColor = string.IsNullOrEmpty(response.Response1) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice2.BorderColor = string.IsNullOrEmpty(response.Response2) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice3.BorderColor = string.IsNullOrEmpty(response.Response3) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice4.BorderColor = string.IsNullOrEmpty(response.Response4) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice5.BorderColor = string.IsNullOrEmpty(response.Response5) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice6.BorderColor = string.IsNullOrEmpty(response.Response6) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice7.BorderColor = string.IsNullOrEmpty(response.Response7) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice8.BorderColor = string.IsNullOrEmpty(response.Response8) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice9.BorderColor = string.IsNullOrEmpty(response.Response9) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        testControl1.ImgChoice10.BorderColor = string.IsNullOrEmpty(response.Response10) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;

                        testControl1.ImgChoice1.BorderStyle = string.IsNullOrEmpty(response.Response1) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice2.BorderStyle = string.IsNullOrEmpty(response.Response2) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice3.BorderStyle = string.IsNullOrEmpty(response.Response3) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice4.BorderStyle = string.IsNullOrEmpty(response.Response4) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice5.BorderStyle = string.IsNullOrEmpty(response.Response5) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice6.BorderStyle = string.IsNullOrEmpty(response.Response6) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice7.BorderStyle = string.IsNullOrEmpty(response.Response7) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice8.BorderStyle = string.IsNullOrEmpty(response.Response8) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice9.BorderStyle = string.IsNullOrEmpty(response.Response9) ? BorderStyle.Solid : BorderStyle.None;
                        testControl1.ImgChoice10.BorderStyle = string.IsNullOrEmpty(response.Response10) ? BorderStyle.Solid : BorderStyle.None;
                    }
                }
                else if (q.TypeCode == "torf")
                {
                    testControl1.PnlTrueOrFalse.Visible = true;

                    if (response != null)
                    {
                        testControl1.RdoTrue.Checked = Convert.ToBoolean(response.Response1);
                        testControl1.RdoFalse.Checked = !Convert.ToBoolean(response.Response1);
                    }
                }
                else if (q.TypeCode == "yorn")
                {
                    testControl1.PnlYesOrNo.Visible = true;

                    if (response != null)
                    {
                        testControl1.RdoYes.Checked = response.Response1 == "Yes";
                        testControl1.RdoNo.Checked = response.Response1 == "No";
                    }
                }
                else if (q.TypeCode == "matching")
                {
                    string[] parts = new string[2];
                    testControl1.PnlMatchingType.Visible = true;

                    ((Label)testControl1.Objects["lblMatchItem1"]).Text = q.Choice1;
                    ((Label)testControl1.Objects["lblMatchItem2"]).Text = q.Choice2;
                    ((Label)testControl1.Objects["lblMatchItem3"]).Text = q.Choice3;
                    ((Label)testControl1.Objects["lblMatchItem4"]).Text = q.Choice4;
                    ((Label)testControl1.Objects["lblMatchItem5"]).Text = q.Choice5;
                    ((Label)testControl1.Objects["lblMatchItem6"]).Text = q.Choice6;
                    ((Label)testControl1.Objects["lblMatchItem7"]).Text = q.Choice7;
                    ((Label)testControl1.Objects["lblMatchItem8"]).Text = q.Choice8;
                    ((Label)testControl1.Objects["lblMatchItem9"]).Text = q.Choice9;
                    ((Label)testControl1.Objects["lblMatchItem10"]).Text = q.Choice10;

                    if (string.IsNullOrEmpty(q.Choice1))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch1"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem1"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter1"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice2))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch2"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem2"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter2"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice3))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch3"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem3"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter3"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice4))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch4"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem4"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter4"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice5))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch5"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem5"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter5"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice6))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch6"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem6"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter6"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice7))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch7"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem7"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter7"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice8))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch8"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem8"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter8"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice9))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch9"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem9"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter9"]).Visible = false;
                    }
                    if (string.IsNullOrEmpty(q.Choice10))
                    {
                        ((HtmlTableCell)testControl1.Objects["tdCorrectMatch10"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchItem10"]).Visible = false;
                        ((HtmlTableCell)testControl1.Objects["tdMatchletter10"]).Visible = false;
                    }

                    parts = q.Ans1.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice1"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice1"]).Text = parts[1];
                    parts = q.Ans2.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice2"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice2"]).Text = parts[1];
                    parts = q.Ans3.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice3"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice3"]).Text = parts[1];
                    parts = q.Ans4.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice4"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice4"]).Text = parts[1];
                    parts = q.Ans5.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice5"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice5"]).Text = parts[1];
                    parts = q.Ans6.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice6"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice6"]).Text = parts[1];
                    parts = q.Ans7.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice7"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice7"]).Text = parts[1];
                    parts = q.Ans8.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice8"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice8"]).Text = parts[1];
                    parts = q.Ans9.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice9"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice9"]).Text = parts[1];
                    parts = q.Ans10.Split('|');
                    if (parts[1].Length < 1)
                        ((HtmlTableCell)testControl1.Objects["tdMatchChoice10"]).Visible = false;
                    else
                        ((Label)testControl1.Objects["lblMatchChoice10"]).Text = parts[1];

                    if (response != null)
                    {
                        ((TextBox)testControl1.Objects["txtCorrectMatch1"]).Text = response.Response1;
                        ((TextBox)testControl1.Objects["txtCorrectMatch2"]).Text = response.Response2;
                        ((TextBox)testControl1.Objects["txtCorrectMatch3"]).Text = response.Response3;
                        ((TextBox)testControl1.Objects["txtCorrectMatch4"]).Text = response.Response4;
                        ((TextBox)testControl1.Objects["txtCorrectMatch5"]).Text = response.Response5;
                        ((TextBox)testControl1.Objects["txtCorrectMatch6"]).Text = response.Response6;
                        ((TextBox)testControl1.Objects["txtCorrectMatch7"]).Text = response.Response7;
                        ((TextBox)testControl1.Objects["txtCorrectMatch8"]).Text = response.Response8;
                        ((TextBox)testControl1.Objects["txtCorrectMatch9"]).Text = response.Response9;
                        ((TextBox)testControl1.Objects["txtCorrectMatch10"]).Text = response.Response10;
                    }
                }
                else if (q.TypeCode == "sequencing")
                {
                    Dictionary<int, string> dictionary = new Dictionary<int, string>();
                    if (q != null)
                    {
                        dictionary.Add(1, q.Choice1);
                        dictionary.Add(2, q.Choice2);
                        dictionary.Add(3, q.Choice3);
                        dictionary.Add(4, q.Choice4);
                        dictionary.Add(5, q.Choice5);
                        dictionary.Add(6, q.Choice6);
                        dictionary.Add(7, q.Choice7);
                        dictionary.Add(8, q.Choice8);
                        dictionary.Add(9, q.Choice9);
                        dictionary.Add(10, q.Choice10);
                    }
                    int totItems = 10;
                    if (string.IsNullOrEmpty(q.Choice1))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder1"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice2))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder2"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice3))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder3"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice4))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder4"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice5))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder5"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice6))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder6"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice7))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder7"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice8))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder8"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice9))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder9"]).Visible = false;
                        totItems--;
                    }
                    if (string.IsNullOrEmpty(q.Choice10))
                    {
                        ((HtmlTableRow)testControl1.ReorderObjects["trReorder10"]).Visible = false;
                        totItems--;
                    }

                    List<string> list = new List<string>();
                    list.Add("");
                    for (int x = 1; x <= totItems; x++)
                    {
                        list.Add(x.ToString());
                    }

                    if (totItems >= 1)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder1"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder1"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder1"]).Text = q.Choice1;
                    }
                    if (totItems >= 2)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder2"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder2"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder2"]).Text = q.Choice2;
                    }
                    if (totItems >= 3)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder3"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder3"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder3"]).Text = q.Choice3;
                    }
                    if (totItems >= 4)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder4"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder4"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder4"]).Text = q.Choice4;
                    }
                    if (totItems >= 5)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder5"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder5"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder5"]).Text = q.Choice5;
                    }
                    if (totItems >= 6)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder6"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder6"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder6"]).Text = q.Choice6;
                    }
                    if (totItems >= 7)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder7"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder7"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder7"]).Text = q.Choice7;
                    }
                    if (totItems >= 8)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder8"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder8"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder8"]).Text = q.Choice8;
                    }
                    if (totItems >= 9)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder9"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder9"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder9"]).Text = q.Choice9;
                    }
                    if (totItems >= 10)
                    {
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder10"]).DataSource = list;
                        ((DropDownList)testControl1.ReorderObjects["ddlReorder10"]).DataBind();
                        ((Label)testControl1.ReorderObjects["lblReorder10"]).Text = q.Choice10;
                    }

                    if (response != null)
                    {
                        Dictionary<string, string> listOfAnswers = new Dictionary<string, string>();
                        if (!string.IsNullOrEmpty(response.Response1))
                            listOfAnswers.Add("1", response.Response1);
                        if (!string.IsNullOrEmpty(response.Response2))
                            listOfAnswers.Add("2", response.Response2);
                        if (!string.IsNullOrEmpty(response.Response3))
                            listOfAnswers.Add("3", response.Response3);
                        if (!string.IsNullOrEmpty(response.Response4))
                            listOfAnswers.Add("4", response.Response4);
                        if (!string.IsNullOrEmpty(response.Response5))
                            listOfAnswers.Add("5", response.Response5);
                        if (!string.IsNullOrEmpty(response.Response6))
                            listOfAnswers.Add("6", response.Response6);
                        if (!string.IsNullOrEmpty(response.Response7))
                            listOfAnswers.Add("7", response.Response7);
                        if (!string.IsNullOrEmpty(response.Response8))
                            listOfAnswers.Add("8", response.Response8);
                        if (!string.IsNullOrEmpty(response.Response9))
                            listOfAnswers.Add("9", response.Response9);
                        if (!string.IsNullOrEmpty(response.Response10))
                            listOfAnswers.Add("10", response.Response10);

                        for (int x = 1; x <= 10; x++)
                        {
                            Label answer = (Label)testControl1.ReorderObjects["lblReorder" + x.ToString()];
                            DropDownList dropdown = (DropDownList)testControl1.ReorderObjects["ddlReorder" + x.ToString()];
                            if (answer != null && !string.IsNullOrEmpty(answer.Text.Trim()))
                            {
                                foreach (KeyValuePair<string, string> pair in listOfAnswers)
                                    if (pair.Value == answer.Text)
                                    {
                                        dropdown.Text = pair.Key;
                                        break;
                                    }
                            }
                        }
                    }
                    testControl1.PnlSequencing.Visible = true;
                }
                else if (q.TypeCode == "essay")
                {
                    testControl1.PnlEssay.Visible = true;
                    if (response != null)
                    {
                        testControl1.TxtEssay.Text = response.EssayResponse;
                    }
                }
                //ADDED BY RAYMARK COSME <01/31/2018>
                else if (q.TypeCode == "rate")
                {
                    testControl1.PnlRate.Visible = true;
                    testControl1.RdoRate.Items.Clear();

                    controls_userNavigationControl ctrl = Page.Master.FindControl("UserNavigationControl1") as controls_userNavigationControl;
                    LinkButton lnk1 = ctrl.FindControl("lnkTestHub") as LinkButton;
                    LinkButton lnk2 = ctrl.FindControl("lnkResumeTests") as LinkButton;
                    LinkButton lnk3 = ctrl.FindControl("lnkViewResults") as LinkButton;
                    Image img = ctrl.FindControl("imgHome") as Image;

                    lnk1.Text = "Survey Hub";
                    lnk2.Text = "Resume Survey";
                    lnk3.Text = "Previous Survey";
                    img.ImageUrl = "~/images/Transcom_Survey.png";

                    if (!string.IsNullOrEmpty(q.Choice1))
                        testControl1.RdoRate.Items.Add(q.Choice1);
                    if (!string.IsNullOrEmpty(q.Choice2))
                        testControl1.RdoRate.Items.Add(q.Choice2);
                    if (!string.IsNullOrEmpty(q.Choice3))
                        testControl1.RdoRate.Items.Add(q.Choice3);
                    if (!string.IsNullOrEmpty(q.Choice4))
                        testControl1.RdoRate.Items.Add(q.Choice4);
                    if (!string.IsNullOrEmpty(q.Choice5))
                        testControl1.RdoRate.Items.Add(q.Choice5);
                    if (!string.IsNullOrEmpty(q.Choice6))
                        testControl1.RdoRate.Items.Add(q.Choice6);
                    if (!string.IsNullOrEmpty(q.Choice7))
                        testControl1.RdoRate.Items.Add(q.Choice7);
                    if (!string.IsNullOrEmpty(q.Choice8))
                        testControl1.RdoRate.Items.Add(q.Choice8);
                    if (!string.IsNullOrEmpty(q.Choice9))
                        testControl1.RdoRate.Items.Add(q.Choice9);
                    if (!string.IsNullOrEmpty(q.Choice10))
                        testControl1.RdoRate.Items.Add(q.Choice10);

                    if (response != null)
                    {
                        foreach (ListItem item in testControl1.RdoRate.Items)
                            item.Selected = false;
                        foreach (ListItem item in testControl1.RdoRate.Items)
                        {
                            if (item.Text == response.Response1)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                }
                //ADDED BY RAYMARK COSME <01/31/2018>
                else if (q.TypeCode == "comments")
                {
                    testControl1.PnlComments.Visible = true;
                    controls_userNavigationControl ctrl = Page.Master.FindControl("UserNavigationControl1") as controls_userNavigationControl;
                    LinkButton lnk1 = ctrl.FindControl("lnkTestHub") as LinkButton;
                    LinkButton lnk2 = ctrl.FindControl("lnkResumeTests") as LinkButton;
                    LinkButton lnk3 = ctrl.FindControl("lnkViewResults") as LinkButton;
                    Image img = ctrl.FindControl("imgHome") as Image;

                    lnk1.Text = "Survey Hub";
                    lnk2.Text = "Resume Survey";
                    lnk3.Text = "Previous Survey";
                    img.ImageUrl = "~/images/Transcom_Survey.png";

                    if (response != null)
                    {
                        testControl1.TxtComments.Text = response.Response1;
                    }
                }
                //ADDED BY RAYMARK COSME <01/31/2018>
                else if (q.TypeCode == "dropdown")
                {
                    testControl1.PnlDropDown.Visible = true;
                    testControl1.DdlValues.Items.Clear();

                    controls_userNavigationControl ctrl = Page.Master.FindControl("UserNavigationControl1") as controls_userNavigationControl;
                    LinkButton lnk1 = ctrl.FindControl("lnkTestHub") as LinkButton;
                    LinkButton lnk2 = ctrl.FindControl("lnkResumeTests") as LinkButton;
                    LinkButton lnk3 = ctrl.FindControl("lnkViewResults") as LinkButton;
                    Image img = ctrl.FindControl("imgHome") as Image;

                    lnk1.Text = "Survey Hub";
                    lnk2.Text = "Resume Survey";
                    lnk3.Text = "Previous Survey";
                    img.ImageUrl = "~/images/Transcom_Survey.png";

                    if (!string.IsNullOrEmpty(q.Choice1))
                        testControl1.DdlValues.Items.Add(q.Choice1);
                    if (!string.IsNullOrEmpty(q.Choice2))
                        testControl1.DdlValues.Items.Add(q.Choice2);
                    if (!string.IsNullOrEmpty(q.Choice3))
                        testControl1.DdlValues.Items.Add(q.Choice3);
                    if (!string.IsNullOrEmpty(q.Choice4))
                        testControl1.DdlValues.Items.Add(q.Choice4);
                    if (!string.IsNullOrEmpty(q.Choice5))
                        testControl1.DdlValues.Items.Add(q.Choice5);
                    if (!string.IsNullOrEmpty(q.Choice6))
                        testControl1.DdlValues.Items.Add(q.Choice6);
                    if (!string.IsNullOrEmpty(q.Choice7))
                        testControl1.DdlValues.Items.Add(q.Choice7);
                    if (!string.IsNullOrEmpty(q.Choice8))
                        testControl1.DdlValues.Items.Add(q.Choice8);
                    if (!string.IsNullOrEmpty(q.Choice9))
                        testControl1.DdlValues.Items.Add(q.Choice9);
                    if (!string.IsNullOrEmpty(q.Choice10))
                        testControl1.DdlValues.Items.Add(q.Choice10);

                    if (response != null)
                    {
                        foreach (ListItem item in testControl1.DdlValues.Items)
                            item.Selected = false;
                        foreach (ListItem item in testControl1.DdlValues.Items)
                        {
                            if (item.Text == response.Response1)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                }
                //ADDED BY RAYMARK COSME <01/31/2018>
                else if (q.TypeCode == "masurvey")
                {
                    testControl1.PnlMultipleAnswer.Visible = true;
                    //testControl1.RdoMA.Items.Clear();

                    controls_userNavigationControl ctrl = Page.Master.FindControl("UserNavigationControl1") as controls_userNavigationControl;
                    LinkButton lnk1 = ctrl.FindControl("lnkTestHub") as LinkButton;
                    LinkButton lnk2 = ctrl.FindControl("lnkResumeTests") as LinkButton;
                    LinkButton lnk3 = ctrl.FindControl("lnkViewResults") as LinkButton;
                    Image img = ctrl.FindControl("imgHome") as Image;

                    lnk1.Text = "Survey Hub";
                    lnk2.Text = "Resume Survey";
                    lnk3.Text = "Previous Survey";
                    img.ImageUrl = "~/images/Transcom_Survey.png";

                    if (!string.IsNullOrEmpty(q.Choice1))
                    {
                        testControl1.ChkMA1.Text = q.Choice1;
                        if (q.Choice1 == "Other" || q.Choice1 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA1.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice2))
                    {
                        testControl1.ChkMA2.Text = q.Choice2;
                        if (q.Choice2 == "Other" || q.Choice2 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA2.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice3))
                    {
                        testControl1.ChkMA3.Text = q.Choice3;
                        if (q.Choice3 == "Other" || q.Choice3 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA3.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice4))
                    {
                        testControl1.ChkMA4.Text = q.Choice4;
                        if (q.Choice4 == "Other" || q.Choice4 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA4.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice5))
                    {
                        testControl1.ChkMA5.Text = q.Choice5;
                        if (q.Choice5 == "Other" || q.Choice5 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA5.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice6))
                    {
                        testControl1.ChkMA6.Text = q.Choice6;
                        if (q.Choice6 == "Other" || q.Choice6 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA6.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice7))
                    {
                        testControl1.ChkMA7.Text = q.Choice7;
                        if (q.Choice7 == "Other" || q.Choice7 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA7.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice8))
                    {
                        testControl1.ChkMA8.Text = q.Choice8;
                        if (q.Choice8 == "Other" || q.Choice8 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA8.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice9))
                    {
                        testControl1.ChkMA9.Text = q.Choice9;
                        if (q.Choice9 == "Other" || q.Choice9 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA9.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(q.Choice10))
                    {
                        testControl1.ChkMA10.Text = q.Choice10;
                        if (q.Choice10 == "Other" || q.Choice10 == "Others")
                        {
                            testControl1.LblOther.Visible = true;
                            testControl1.TxtOther.Visible = true;
                        }
                    }
                    else
                    {
                        testControl1.ChkMA10.Visible = false;
                    }

                    if (response != null)
                    {
                        testControl1.ChkMA1.Checked = !string.IsNullOrEmpty(response.Response1);
                        testControl1.ChkMA2.Checked = !string.IsNullOrEmpty(response.Response2);
                        testControl1.ChkMA3.Checked = !string.IsNullOrEmpty(response.Response3);
                        testControl1.ChkMA4.Checked = !string.IsNullOrEmpty(response.Response4);
                        testControl1.ChkMA5.Checked = !string.IsNullOrEmpty(response.Response5);
                        testControl1.ChkMA6.Checked = !string.IsNullOrEmpty(response.Response6);
                        testControl1.ChkMA7.Checked = !string.IsNullOrEmpty(response.Response7);
                        testControl1.ChkMA8.Checked = !string.IsNullOrEmpty(response.Response8);
                        testControl1.ChkMA9.Checked = !string.IsNullOrEmpty(response.Response9);
                        testControl1.ChkMA10.Checked = !string.IsNullOrEmpty(response.Response10);
                    }
                }
                //ADDED BY RAYMARK COSME <01/31/2018>
                else if (q.TypeCode == "calendar")
                {
                    testControl1.PnlCalendar.Visible = true;
                    controls_userNavigationControl ctrl = Page.Master.FindControl("UserNavigationControl1") as controls_userNavigationControl;
                    LinkButton lnk1 = ctrl.FindControl("lnkTestHub") as LinkButton;
                    LinkButton lnk2 = ctrl.FindControl("lnkResumeTests") as LinkButton;
                    LinkButton lnk3 = ctrl.FindControl("lnkViewResults") as LinkButton;
                    Image img = ctrl.FindControl("imgHome") as Image;

                    lnk1.Text = "Survey Hub";
                    lnk2.Text = "Resume Survey";
                    lnk3.Text = "Previous Survey";
                    img.ImageUrl = "~/images/Transcom_Survey.png";

                    if (response != null)
                    {
                        testControl1.TxtDate.Text = response.Response1;
                    }
                }
                else if (q.TypeCode == "fillblanks")
                {
                    testControl1.PnlFillInTheBlanks.Visible = true;
                    testControl1.DdlAnswer.Items.Clear();

                    if (!string.IsNullOrEmpty(q.Choice1))
                        testControl1.DdlAnswer.Items.Add(q.Choice1);
                    if (!string.IsNullOrEmpty(q.Choice2))
                        testControl1.DdlAnswer.Items.Add(q.Choice2);
                    if (!string.IsNullOrEmpty(q.Choice3))
                        testControl1.DdlAnswer.Items.Add(q.Choice3);
                    if (!string.IsNullOrEmpty(q.Choice4))
                        testControl1.DdlAnswer.Items.Add(q.Choice4);
                    if (!string.IsNullOrEmpty(q.Choice5))
                        testControl1.DdlAnswer.Items.Add(q.Choice5);
                    if (!string.IsNullOrEmpty(q.Choice6))
                        testControl1.DdlAnswer.Items.Add(q.Choice6);
                    if (!string.IsNullOrEmpty(q.Choice7))
                        testControl1.DdlAnswer.Items.Add(q.Choice7);
                    if (!string.IsNullOrEmpty(q.Choice8))
                        testControl1.DdlAnswer.Items.Add(q.Choice8);
                    if (!string.IsNullOrEmpty(q.Choice9))
                        testControl1.DdlAnswer.Items.Add(q.Choice9);
                    if (!string.IsNullOrEmpty(q.Choice10))
                        testControl1.DdlAnswer.Items.Add(q.Choice10);
                    if (response != null)
                    {
                        foreach (ListItem item in testControl1.DdlAnswer.Items)
                            item.Selected = false;
                        foreach (ListItem item in testControl1.DdlAnswer.Items)
                        {
                            if (item.Text == response.Response1)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(4, "allitems.aspx", "GenerateItem", ex.Message);
            SessionManager.SessionMainText = "System could not initialize exam items.";
            Response.Redirect("~/main.aspx?" + Request.QueryString.ToString());
        }
    }

    protected void btnSaveAndQuit_Click(object sender, EventArgs e)
    {
        try
        {
            //this.tmrTimeRemaining.Enabled = false;
            SessionManager.SessionQuestionNumber = 0;
            foreach (GridViewRow row in this.gvQuestions.Rows)
            {
                controls_examquestioncontrol control = row.FindControl("wusQuestion") as controls_examquestioncontrol;
                if (control != null)
                    this.ProcessItem(control, false);
                SessionManager.SessionQuestionNumber++;
            }

            //Set TimeRemaining
            TestTaken testTaken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
            TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
            TimeSpan span = new TimeSpan();

            if (testTaken.DateLastSave.ToString().Contains("1/1/0001"))
                span = System.DateTime.Now.Subtract(testTaken.DateStartTaken);
            else
                span = System.DateTime.Now.Subtract(testTaken.DateLastSave.Value);

            //check if time difference is greater than the test time limit
            if (span.TotalSeconds > category.TimeLimit && category.TimeLimit > 0)
            {
                testTaken.DateEndTaken = DateTime.Now;
                testTaken.Finish();
                if (testTaken != null && !category.IsEssay)
                    Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString(), false);
                else
                    Response.Redirect("~/testend.aspx?" + Request.QueryString.ToString(), false);
            }
            else
            {
                testTaken.DateLastSave = System.DateTime.Now;
                if (category.TimeLimit > 0)
                    testTaken.MinutesRemaining = Convert.ToInt32(this.lblActualTimeLeft.Text);
                else
                    testTaken.MinutesRemaining = 0;
                testTaken.UpdateTime();
                Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString(), false);
            }
        }

        catch (Exception ex)
        {
            if (!ex.Message.Contains("stack"))
            {
                SessionManager.SessionMainText = "Saving answers failed.";
                Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
            }
        }
    }

    protected void btnNoReplacement_Click(object sender, EventArgs e)
    {
        //if (SessionManager.SessionQuestionNumber == SessionManager.SessionQuestionnaire.Count)
        //{
        //    TestTaken taken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
        //    if (taken != null)
        //    {
        //        taken.DateEndTaken = DateTime.Now;
        //        taken.Finish();
        //    }
        //    Response.Redirect("~/gradeexam.aspx");
        //}
        //Response.Redirect("~/test.aspx");
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        SessionManager.SessionQuestionNumber = 0;
        bool isMissingAnswer = false;
        foreach (GridViewRow row in this.gvQuestions.Rows)
        {
            controls_examquestioncontrol control = row.FindControl("wusQuestion") as controls_examquestioncontrol;
            if (control != null)
            {
                if (!CheckAllAnswers(control))
                {
                    control.LblQuestion.BackColor = System.Drawing.Color.Red;
                    control.LblQuestion.ForeColor = System.Drawing.Color.White;
                    isMissingAnswer = true;
                }
                else
                {
                    control.LblQuestion.BackColor = System.Drawing.Color.FromArgb(211,211,211);
                    control.LblQuestion.ForeColor = System.Drawing.Color.Black;
                }
                SessionManager.SessionQuestionNumber++;
            }
        }
        if (isMissingAnswer)
        {
            this.mpeMissingAnswer.Show();
            return;
        }
        SessionManager.SessionQuestionNumber = 0;
        foreach (GridViewRow row in this.gvQuestions.Rows)
        {
            controls_examquestioncontrol control = row.FindControl("wusQuestion") as controls_examquestioncontrol;
            if (control != null)
                this.ProcessItem(control, true);
            SessionManager.SessionQuestionNumber++;
        }
        TestTaken taken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
        TestCategory category = TestCategory.Select(taken.TestCategoryID, true);
        taken.DateEndTaken = DateTime.Now;
        taken.Finish();
        if (taken != null && !category.IsEssay)
            Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
        else
            Response.Redirect("~/testend.aspx?" + Request.QueryString.ToString());
    }

    protected bool CheckAllAnswers(controls_examquestioncontrol testControl1)
    {
        //Check if user has an answer
        bool hasAnswer = true;
        int totAnswered = 0;
        if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "multiple")
        {
            int totChecked = 0;
            totChecked += testControl1.ChkChoice1.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice2.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice3.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice4.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice5.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice6.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice7.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice8.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice9.Checked ? 1 : 0;
            totChecked += testControl1.ChkChoice10.Checked ? 1 : 0;
            if (totChecked < 1)
                hasAnswer = false;
            else
                totAnswered++;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "torf")
        {
            if (!testControl1.RdoFalse.Checked && !testControl1.RdoTrue.Checked)
                hasAnswer = false;
            else
                totAnswered++;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "fillblanks")
        {
            if (string.IsNullOrEmpty(testControl1.DdlAnswer.Text.Trim()))
                hasAnswer = false;
            else
                totAnswered++;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "yorn")
        {
            if (!testControl1.RdoYes.Checked && !testControl1.RdoNo.Checked)
                hasAnswer = false;
            else
                totAnswered++;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "essay")
        {
            if (string.IsNullOrEmpty(testControl1.TxtEssay.Text.Trim()))
                hasAnswer = false;
            else
                totAnswered++;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "hotspot")
        {
            int totChecked = 0;
            totChecked += testControl1.ImgChoice1.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice2.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice3.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice4.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice5.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice6.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice7.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice8.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice9.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            totChecked += testControl1.ImgChoice10.BorderColor == System.Drawing.Color.Red ? 1 : 0;
            if (totChecked < 1)
                hasAnswer = false;
            else
                totAnswered++;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "sequencing")
        {
            int totchecked = 0;
            DropDownList listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder1"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder1"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder2"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder2"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder3"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder3"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder4"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder4"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder5"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder5"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder6"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder6"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder7"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder7"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder8"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder8"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder9"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder9"]).SelectedItem.Text) ? 0 : 1;
            listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder10"]);
            if (listItem != null && listItem.SelectedIndex != -1)
                totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder10"]).SelectedItem.Text) ? 0 : 1;
            hasAnswer = totchecked != 0;
        }
        else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "matching")
        {
            int totchecked = 0;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch1"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch2"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch3"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch4"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch5"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch6"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch7"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch8"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch9"]).Text) ? 0 : 1;
            totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch10"]).Text) ? 0 : 1;
            if (totchecked < 1)
                hasAnswer = false;
            else
                totAnswered++;
        }
        if (hasAnswer)
            return true;
        return false;
    }

    protected void ProcessItem(controls_examquestioncontrol testControl1, bool isFinish)
    {
        try
        {
            //Check if user has an answer
            bool hasAnswer = true;
            int totAnswered = 0;
            if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "multiple")
            {
                int totChecked = 0;
                totChecked += testControl1.ChkChoice1.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice2.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice3.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice4.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice5.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice6.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice7.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice8.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice9.Checked ? 1 : 0;
                totChecked += testControl1.ChkChoice10.Checked ? 1 : 0;
                if (totChecked < 1)
                    hasAnswer = false;
                else
                    totAnswered++;
            }
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "torf")
            {
                if (!testControl1.RdoFalse.Checked && !testControl1.RdoTrue.Checked)
                    hasAnswer = false;
                else
                    totAnswered++;
            }
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "fillblanks")
            {
                if (string.IsNullOrEmpty(testControl1.DdlAnswer.Text.Trim()))
                    hasAnswer = false;
                else
                    totAnswered++;
            }
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "yorn")
            {
                if (!testControl1.RdoYes.Checked && !testControl1.RdoNo.Checked)
                    hasAnswer = false;
                else
                    totAnswered++;
            }
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "essay")
            {
                if (string.IsNullOrEmpty(testControl1.TxtEssay.Text.Trim()))
                    hasAnswer = false;
                else
                    totAnswered++;
            }
            //ADDED BY RAYMARK COSME <02/01/2018>
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "rate")
            {
                if (string.IsNullOrEmpty(testControl1.RdoRate.Text.Trim()))
                    hasAnswer = false;
                else
                    totAnswered++;
            }
            //ADDED BY RAYMARK COSME <03/09/2016>
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "comments")
            {
                if (string.IsNullOrEmpty(testControl1.TxtComments.Text.Trim()))
                {
                    testControl1.TxtComments.Text = " ";
                    hasAnswer = true;
                    totAnswered++;
                }
                else
                    totAnswered++;
            }
            //ADDED BY RAYMARK COSME <02/01/2018>
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "dropdown")
            {
                if (string.IsNullOrEmpty(testControl1.DdlValues.Text.Trim()))
                    hasAnswer = false;
                else
                    totAnswered++;
            }
            //ADDED BY RAYMARK COSME <02/01/2018>
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "masurvey")
            {
                int totChecked = 0;
                totChecked += testControl1.ChkMA1.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA2.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA3.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA4.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA5.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA6.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA7.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA8.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA9.Checked ? 1 : 0;
                totChecked += testControl1.ChkMA10.Checked ? 1 : 0;
                if (totChecked < 1)
                    hasAnswer = false;
                else
                    totAnswered++;

                //if (string.IsNullOrEmpty(testControl1.ChkMA.Text.Trim()))
                //    hasAnswer = false;
                //else
                //    totAnswered++;
            }
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "calendar")
            {
                if (string.IsNullOrEmpty(testControl1.TxtDate.Text.Trim()))
                {
                    testControl1.TxtDate.Text = " ";
                    hasAnswer = false;
                    totAnswered++;
                }
                else
                    totAnswered++;
            }
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "hotspot")
            {
                int totChecked = 0;
                totChecked += testControl1.ImgChoice1.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice2.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice3.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice4.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice5.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice6.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice7.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice8.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice9.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                totChecked += testControl1.ImgChoice10.BorderColor == System.Drawing.Color.Red ? 1 : 0;
                if (totChecked < 1)
                    hasAnswer = false;
                else
                    totAnswered++;
            }
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "sequencing")
            {
                int totchecked = 0;
                DropDownList listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder1"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder1"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder2"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder2"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder3"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder3"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder4"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder4"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder5"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder5"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder6"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder6"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder7"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder7"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder8"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder8"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder9"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder9"]).SelectedItem.Text) ? 0 : 1;
                listItem = ((DropDownList)testControl1.ReorderObjects["ddlReorder10"]);
                if (listItem != null && listItem.SelectedIndex != -1)
                    totchecked += string.IsNullOrEmpty(((DropDownList)testControl1.ReorderObjects["ddlReorder10"]).SelectedItem.Text) ? 0 : 1;
                if (totchecked < 1)
                    hasAnswer = false;
            }
            else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "matching")
            {
                int totchecked = 0;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch1"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch2"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch3"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch4"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch5"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch6"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch7"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch8"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch9"]).Text) ? 0 : 1;
                totchecked += string.IsNullOrEmpty(((TextBox)testControl1.Objects["txtCorrectMatch10"]).Text) ? 0 : 1;
                if (totchecked < 1)
                    hasAnswer = false;
                else
                    totAnswered++;
            }

            if (hasAnswer)
            {
                try
                {
                    TestTaken testTaken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
                    TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, false);
                    if (testTaken != null && category != null && SessionManager.SessionTimedExam)
                    {
                        if (category.EndDate <= System.DateTime.Now)
                        {
                            testTaken.DateEndTaken = System.DateTime.Now;
                            testTaken.Finish();
                            Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
                        }
                    }
                    else if (category == null)
                    {
                        testTaken.DateEndTaken = System.DateTime.Now;
                        testTaken.Finish();
                        Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
                    }
                    //save the answer into a session
                    SaveTestResponse response = new SaveTestResponse();
                    response.QuestionnaireID = SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].QuestionnaireID;
                    response.TestTakenID = SessionManager.SessionTestTaken.TestTakenID;
                    response.UserID = SessionManager.SessionUsername;
                    if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "multiple")
                    {
                        response.Response1 = testControl1.ChkChoice1.Checked ? testControl1.ChkChoice1.Text : string.Empty;
                        response.Response2 = testControl1.ChkChoice2.Checked ? testControl1.ChkChoice2.Text : string.Empty;
                        response.Response3 = testControl1.ChkChoice3.Checked ? testControl1.ChkChoice3.Text : string.Empty;
                        response.Response4 = testControl1.ChkChoice4.Checked ? testControl1.ChkChoice4.Text : string.Empty;
                        response.Response5 = testControl1.ChkChoice5.Checked ? testControl1.ChkChoice5.Text : string.Empty;
                        response.Response6 = testControl1.ChkChoice6.Checked ? testControl1.ChkChoice6.Text : string.Empty;
                        response.Response7 = testControl1.ChkChoice7.Checked ? testControl1.ChkChoice7.Text : string.Empty;
                        response.Response8 = testControl1.ChkChoice8.Checked ? testControl1.ChkChoice8.Text : string.Empty;
                        response.Response9 = testControl1.ChkChoice9.Checked ? testControl1.ChkChoice9.Text : string.Empty;
                        response.Response10 = testControl1.ChkChoice10.Checked ? testControl1.ChkChoice10.Text : string.Empty;
                    }
                    else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "torf")
                    {
                        response.Response1 = testControl1.RdoTrue.Checked ? "True" : "False";
                        response.Response2 = string.Empty;
                        response.Response3 = string.Empty;
                        response.Response4 = string.Empty;
                        response.Response5 = string.Empty;
                        response.Response6 = string.Empty;
                        response.Response7 = string.Empty;
                        response.Response8 = string.Empty;
                        response.Response9 = string.Empty;
                        response.Response10 = string.Empty;
                    }
                    else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "yorn")
                    {
                        response.Response1 = testControl1.RdoYes.Checked ? "Yes" : "No";
                        response.Response2 = string.Empty;
                        response.Response3 = string.Empty;
                        response.Response4 = string.Empty;
                        response.Response5 = string.Empty;
                        response.Response6 = string.Empty;
                        response.Response7 = string.Empty;
                        response.Response8 = string.Empty;
                        response.Response9 = string.Empty;
                        response.Response10 = string.Empty;
                    }

                    else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "fillblanks")
                    {
                        response.Response1 = testControl1.DdlAnswer.Text.Trim();
                        response.Response2 = string.Empty;
                        response.Response3 = string.Empty;
                        response.Response4 = string.Empty;
                        response.Response5 = string.Empty;
                        response.Response6 = string.Empty;
                        response.Response7 = string.Empty;
                        response.Response8 = string.Empty;
                        response.Response9 = string.Empty;
                        response.Response10 = string.Empty;
                    }

                    else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "sequencing")
                    {
                        #region Old Sequencing Using Ajax Reorder List
                        //List<string> tempList = new List<string>();
                        ////for (int i = 0; i < testControl1.RolSequencing.Items.Count; i++)
                        ////{
                        ////    Label lbltemplate = testControl1.RolSequencing.Items[i].FindControl("lblTemplate") as Label;
                        ////    tempList.Add(lbltemplate.Text.Trim());
                        ////}
                        //foreach (TempQuestionnaire questionnaireItem in SessionManager.SessionRolItems)
                        //    tempList.Add(questionnaireItem.Item);

                        //response.Response10 = tempList.Count > 9 ? tempList[9] : string.Empty;
                        //response.Response9 = tempList.Count > 8 ? tempList[8] : string.Empty;
                        //response.Response8 = tempList.Count > 7 ? tempList[7] : string.Empty;
                        //response.Response7 = tempList.Count > 6 ? tempList[6] : string.Empty;
                        //response.Response6 = tempList.Count > 5 ? tempList[5] : string.Empty;
                        //response.Response5 = tempList.Count > 4 ? tempList[4] : string.Empty;
                        //response.Response4 = tempList.Count > 3 ? tempList[3] : string.Empty;
                        //response.Response3 = tempList.Count > 2 ? tempList[2] : string.Empty;
                        //response.Response2 = tempList[1];
                        //response.Response1 = tempList[0];
                        #endregion

                        for (int x = 1; x <= 10; x++)
                        {
                            string ddlName = "ddlReorder" + x.ToString();
                            string lblName = "lblReorder" + x.ToString();
                            DropDownList ddlReorder = ((DropDownList)testControl1.ReorderObjects[ddlName]);
                            Label lblReorder = ((Label)testControl1.ReorderObjects[lblName]);
                            if (ddlReorder.Text.Trim() == "1")
                                response.Response1 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "2")
                                response.Response2 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "3")
                                response.Response3 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "4")
                                response.Response4 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "5")
                                response.Response5 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "6")
                                response.Response6 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "7")
                                response.Response7 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "8")
                                response.Response8 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "9")
                                response.Response9 = lblReorder.Text;
                            if (ddlReorder.Text.Trim() == "10")
                                response.Response10 = lblReorder.Text;
                        }
                    }
                    else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "essay")
                    {
                        response.EssayResponse = testControl1.TxtEssay.Text.Trim();
                    }
                    //ADDED BY RAYMARK COSME <02/01/2018>
                    else if (
                        SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber]
                            .TypeCode == "rate")
                    {
                        response.Response1 = testControl1.RdoRate.Text.Trim();
                        response.Response2 = string.Empty;
                        response.Response3 = string.Empty;
                        response.Response4 = string.Empty;
                        response.Response5 = string.Empty;
                        response.Response6 = string.Empty;
                        response.Response7 = string.Empty;
                        response.Response8 = string.Empty;
                        response.Response9 = string.Empty;
                        response.Response10 = string.Empty;
                    }
                    //ADDED BY RAYMARK COSME <02/01/2018>
                    else if (
                        SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber]
                            .TypeCode == "comments")
                    {
                        response.Response1 = testControl1.TxtComments.Text;
                        response.Response2 = string.Empty;
                        response.Response3 = string.Empty;
                        response.Response4 = string.Empty;
                        response.Response5 = string.Empty;
                        response.Response6 = string.Empty;
                        response.Response7 = string.Empty;
                        response.Response8 = string.Empty;
                        response.Response9 = string.Empty;
                        response.Response10 = string.Empty;
                    }
                    //ADDED BY RAYMARK COSME <02/01/2018>
                    else if (
                        SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber]
                            .TypeCode == "dropdown")
                    {
                        response.Response1 = testControl1.DdlValues.Text;
                        response.Response2 = string.Empty;
                        response.Response3 = string.Empty;
                        response.Response4 = string.Empty;
                        response.Response5 = string.Empty;
                        response.Response6 = string.Empty;
                        response.Response7 = string.Empty;
                        response.Response8 = string.Empty;
                        response.Response9 = string.Empty;
                        response.Response10 = string.Empty;
                    }
                    //ADDED BY RAYMARK COSME <02/01/2018>
                    else if (
                        SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber]
                            .TypeCode == "masurvey")
                    {
                        if (testControl1.ChkMA1.Text == "Other" || testControl1.ChkMA1.Text == "Others" || testControl1.ChkMA2.Text == "Other" || testControl1.ChkMA2.Text == "Others" || testControl1.ChkMA3.Text == "Other" || testControl1.ChkMA3.Text == "Others" || testControl1.ChkMA4.Text == "Other" || testControl1.ChkMA4.Text == "Others" || testControl1.ChkMA5.Text == "Other" || testControl1.ChkMA5.Text == "Others" || testControl1.ChkMA6.Text == "Other" || testControl1.ChkMA6.Text == "Others" || testControl1.ChkMA7.Text == "Other" || testControl1.ChkMA7.Text == "Others" || testControl1.ChkMA8.Text == "Other" || testControl1.ChkMA8.Text == "Others" || testControl1.ChkMA9.Text == "Other" || testControl1.ChkMA9.Text == "Others" || testControl1.ChkMA10.Text == "Other" || testControl1.ChkMA10.Text == "Others")
                        {
                            if (testControl1.ChkMA1.Checked)
                            {
                                if (testControl1.ChkMA1.Text == "Other" || testControl1.ChkMA1.Text == "Others")
                                    response.Response1 = testControl1.TxtOther.Text;
                                else
                                    response.Response1 = testControl1.ChkMA1.Text;
                            }
                            else
                            {
                                response.Response1 = string.Empty;
                            }
                            if (testControl1.ChkMA2.Checked)
                            {
                                if (testControl1.ChkMA2.Text == "Other" || testControl1.ChkMA2.Text == "Others")
                                    response.Response2 = testControl1.TxtOther.Text;
                                else
                                    response.Response2 = testControl1.ChkMA2.Text;
                            }
                            else
                            {
                                response.Response2 = string.Empty;
                            }
                            if (testControl1.ChkMA3.Checked)
                            {
                                if (testControl1.ChkMA3.Text == "Other" || testControl1.ChkMA3.Text == "Others")
                                    response.Response3 = testControl1.TxtOther.Text;
                                else
                                    response.Response3 = testControl1.ChkMA3.Text;
                            }
                            else
                            {
                                response.Response3 = string.Empty;
                            }
                            if (testControl1.ChkMA4.Checked)
                            {
                                if (testControl1.ChkMA4.Text == "Other" || testControl1.ChkMA4.Text == "Others")
                                    response.Response4 = testControl1.TxtOther.Text;
                                else
                                    response.Response4 = testControl1.ChkMA4.Text;
                            }
                            else
                            {
                                response.Response4 = string.Empty;
                            }
                            if (testControl1.ChkMA5.Checked)
                            {
                                if (testControl1.ChkMA5.Text == "Other" || testControl1.ChkMA5.Text == "Others")
                                    response.Response5 = testControl1.TxtOther.Text;
                                else
                                    response.Response5 = testControl1.ChkMA5.Text;
                            }
                            else
                            {
                                response.Response5 = string.Empty;
                            }
                            if (testControl1.ChkMA6.Checked)
                            {
                                if (testControl1.ChkMA6.Text == "Other" || testControl1.ChkMA6.Text == "Others")
                                    response.Response6 = testControl1.TxtOther.Text;
                                else
                                    response.Response6 = testControl1.ChkMA6.Text;
                            }
                            else
                            {
                                response.Response6 = string.Empty;
                            }
                            if (testControl1.ChkMA7.Checked)
                            {
                                if (testControl1.ChkMA7.Text == "Other" || testControl1.ChkMA7.Text == "Others")
                                    response.Response7 = testControl1.TxtOther.Text;
                                else
                                    response.Response7 = testControl1.ChkMA7.Text;
                            }
                            else
                            {
                                response.Response7 = string.Empty;
                            }
                            if (testControl1.ChkMA8.Checked)
                            {
                                if (testControl1.ChkMA8.Text == "Other" || testControl1.ChkMA8.Text == "Others")
                                    response.Response8 = testControl1.TxtOther.Text;
                                else
                                    response.Response8 = testControl1.ChkMA8.Text;
                            }
                            else
                            {
                                response.Response8 = string.Empty;
                            }
                            if (testControl1.ChkMA9.Checked)
                            {
                                if (testControl1.ChkMA9.Text == "Other" || testControl1.ChkMA9.Text == "Others")
                                    response.Response9 = testControl1.TxtOther.Text;
                                else
                                    response.Response9 = testControl1.ChkMA9.Text;
                            }
                            else
                            {
                                response.Response9 = string.Empty;
                            }
                            if (testControl1.ChkMA10.Checked)
                            {
                                if (testControl1.ChkMA10.Text == "Other" || testControl1.ChkMA10.Text == "Others")
                                    response.Response10 = testControl1.TxtOther.Text;
                                else
                                    response.Response10 = testControl1.ChkMA10.Text;
                            }
                            else
                            {
                                response.Response10 = string.Empty;
                            }
                        }
                        else
                        {
                            response.Response1 = testControl1.ChkMA1.Checked
                                                     ? testControl1.ChkMA1.Text
                                                     : string.Empty;
                            response.Response2 = testControl1.ChkMA2.Checked
                                                     ? testControl1.ChkMA2.Text
                                                     : string.Empty;
                            response.Response3 = testControl1.ChkMA3.Checked
                                                     ? testControl1.ChkMA3.Text
                                                     : string.Empty;
                            response.Response4 = testControl1.ChkMA4.Checked
                                                     ? testControl1.ChkMA4.Text
                                                     : string.Empty;
                            response.Response5 = testControl1.ChkMA5.Checked
                                                     ? testControl1.ChkMA5.Text
                                                     : string.Empty;
                            response.Response6 = testControl1.ChkMA6.Checked
                                                     ? testControl1.ChkMA6.Text
                                                     : string.Empty;
                            response.Response7 = testControl1.ChkMA7.Checked
                                                     ? testControl1.ChkMA7.Text
                                                     : string.Empty;
                            response.Response8 = testControl1.ChkMA8.Checked
                                                     ? testControl1.ChkMA8.Text
                                                     : string.Empty;
                            response.Response9 = testControl1.ChkMA9.Checked
                                                     ? testControl1.ChkMA9.Text
                                                     : string.Empty;
                            response.Response10 = testControl1.ChkMA10.Checked
                                                      ? testControl1.ChkMA10.Text
                                                      : string.Empty;
                        }
                    }
                    else if (
                        SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber]
                            .TypeCode == "calendar")
                    {
                        response.Response1 = testControl1.TxtDate.Text;
                        response.Response2 = string.Empty;
                        response.Response3 = string.Empty;
                        response.Response4 = string.Empty;
                        response.Response5 = string.Empty;
                        response.Response6 = string.Empty;
                        response.Response7 = string.Empty;
                        response.Response8 = string.Empty;
                        response.Response9 = string.Empty;
                        response.Response10 = string.Empty;
                    }
                    else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "hotspot")
                    {
                        response.Response1 = testControl1.ImgChoice1.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice1.ImageUrl : string.Empty;
                        response.Response2 = testControl1.ImgChoice2.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice2.ImageUrl : string.Empty;
                        response.Response3 = testControl1.ImgChoice3.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice3.ImageUrl : string.Empty;
                        response.Response4 = testControl1.ImgChoice4.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice4.ImageUrl : string.Empty;
                        response.Response5 = testControl1.ImgChoice5.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice5.ImageUrl : string.Empty;
                        response.Response6 = testControl1.ImgChoice6.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice6.ImageUrl : string.Empty;
                        response.Response7 = testControl1.ImgChoice7.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice7.ImageUrl : string.Empty;
                        response.Response8 = testControl1.ImgChoice8.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice8.ImageUrl : string.Empty;
                        response.Response9 = testControl1.ImgChoice9.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice9.ImageUrl : string.Empty;
                        response.Response10 = testControl1.ImgChoice10.BorderColor == System.Drawing.Color.Red ? testControl1.ImgChoice10.ImageUrl : string.Empty;
                    }
                    else if (SessionManager.SessionQuestionnaire[SessionManager.SessionQuestionNumber].TypeCode == "matching")
                    {
                        response.Response1 = ((TextBox)testControl1.Objects["txtCorrectMatch1"]).Text.Trim().ToUpper();
                        response.Response2 = ((TextBox)testControl1.Objects["txtCorrectMatch2"]).Text.Trim().ToUpper();
                        response.Response3 = ((TextBox)testControl1.Objects["txtCorrectMatch3"]).Text.Trim().ToUpper();
                        response.Response4 = ((TextBox)testControl1.Objects["txtCorrectMatch4"]).Text.Trim().ToUpper();
                        response.Response5 = ((TextBox)testControl1.Objects["txtCorrectMatch5"]).Text.Trim().ToUpper();
                        response.Response6 = ((TextBox)testControl1.Objects["txtCorrectMatch6"]).Text.Trim().ToUpper();
                        response.Response7 = ((TextBox)testControl1.Objects["txtCorrectMatch7"]).Text.Trim().ToUpper();
                        response.Response8 = ((TextBox)testControl1.Objects["txtCorrectMatch8"]).Text.Trim().ToUpper();
                        response.Response9 = ((TextBox)testControl1.Objects["txtCorrectMatch9"]).Text.Trim().ToUpper();
                        response.Response10 = ((TextBox)testControl1.Objects["txtCorrectMatch10"]).Text.Trim().ToUpper();
                    }

                    if (string.IsNullOrEmpty(response.Response1))
                        response.Response1 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response2))
                        response.Response2 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response3))
                        response.Response3 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response4))
                        response.Response4 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response5))
                        response.Response5 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response6))
                        response.Response6 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response7))
                        response.Response7 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response8))
                        response.Response8 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response9))
                        response.Response9 = string.Empty;
                    if (string.IsNullOrEmpty(response.Response10))
                        response.Response10 = string.Empty;
                    SaveTestResponse response2 = SaveTestResponse.Retrieve(response.UserID, response.TestTakenID, response.QuestionnaireID);
                    if (response2 == null)
                        response.Insert();
                    else
                    {
                        response2.Response1 = response.Response1;
                        response2.Response2 = response.Response2;
                        response2.Response3 = response.Response3;
                        response2.Response4 = response.Response4;
                        response2.Response5 = response.Response5;
                        response2.Response6 = response.Response6;
                        response2.Response7 = response.Response7;
                        response2.Response8 = response.Response8;
                        response2.Response9 = response.Response9;
                        response2.Response10 = response.Response10;
                        response2.EssayResponse = response.EssayResponse;
                        response2.UpdateAnswers();
                    }
                }
                catch (Exception ex)
                {
                    SessionManager.SessionMainText = "Test answers could not be processed.";
                    Response.Redirect("~/error.aspx?" + Request.QueryString.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(4, "allitems.aspx", "ProcessItem", ex.Message);
        }
    }

    protected void ProcessTime()
    {
        int secondsRemaining = Convert.ToInt32(this.lblActualTimeLeft.Text.Trim());
        this.lblTimeRemainingValue.Text = GenericMethods.ConvertSecondsToTime(secondsRemaining);
        secondsRemaining--;
        this.lblActualTimeLeft.Text = secondsRemaining.ToString();
        if (secondsRemaining < 0)
        {
            this.mpePnlTimeUp.Show();
            this.tmrTimeRemaining.Enabled = false;
            TestTaken taken = TestTaken.Select(SessionManager.SessionTestTaken.TestTakenID);
            taken.MinutesRemaining = 0;
            taken.UpdateTime();
        }
        else
            this.tmrTimeRemaining.Enabled = true;

    }

    protected void tmrTimeRemaining_Tick(object sender, EventArgs e)
    {
        this.ProcessTime();
    }

    private bool CheckIfPreviousAnswerCorrect(SaveTestResponse answer, Questionnaire question)
    {
        bool bIsCorrect = false;

        if (question.TypeCode == "matching")
        {
            bIsCorrect =
                   answer.Response1 == question.Ans1.Split('|')[0]
                && answer.Response2 == question.Ans2.Split('|')[0]
                && answer.Response3 == question.Ans3.Split('|')[0]
                && answer.Response4 == question.Ans4.Split('|')[0]
                && answer.Response5 == question.Ans5.Split('|')[0]
                && answer.Response6 == question.Ans6.Split('|')[0]
                && answer.Response7 == question.Ans7.Split('|')[0]
                && answer.Response8 == question.Ans8.Split('|')[0]
                && answer.Response9 == question.Ans9.Split('|')[0]
                && answer.Response10 == question.Ans10.Split('|')[0];
        }
        else if (question.TypeCode == "essay")
        {
            bIsCorrect = true;
        }
        else
        {
            bIsCorrect =
                   answer.Response1 == question.Ans1 && answer.Response2 == question.Ans2
                && answer.Response3 == question.Ans3 && answer.Response4 == question.Ans4
                && answer.Response5 == question.Ans5 && answer.Response6 == question.Ans6
                && answer.Response7 == question.Ans7 && answer.Response8 == question.Ans8
                && answer.Response9 == question.Ans9 && answer.Response10 == question.Ans10;
        }

        return bIsCorrect;
    }
}