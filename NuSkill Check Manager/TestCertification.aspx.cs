using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class TestCertification : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/login.aspx" + Request.QueryString.ToString());
            this.BindData();
        }

    }

    protected void BindData()
    {
        this.gvGroups.DataSource = TestCategoryHarmonyQualificationMatrix.SelectAll();
        this.gvGroups.DataBind();
        this.txtHarmonyQualificationID.Text = string.Empty;
        this.txtTestCategoryID.Text = string.Empty;
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        this.txtHarmonyQualificationID.Text = string.Empty;
        this.txtTestCategoryID.Text = string.Empty;
        this.mpeAddNew.Show();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.BindData();
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        TestCategoryHarmonyQualificationMatrix matrix = new TestCategoryHarmonyQualificationMatrix();
        matrix.HarmonyQualificationID = Convert.ToInt32(this.txtHarmonyQualificationID.Text.Trim());
        matrix.TestCategoryID = Convert.ToInt32(this.txtTestCategoryID.Text.Trim());
        matrix.Insert();
    }

    protected void gvGroups_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Remove")
                TestCategoryHarmonyQualificationMatrix.Delete(Convert.ToInt32(e.CommandArgument));
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "testcetification.aspx", "gvGroups_RowCommand", ex.Message);
        }
    }
}
