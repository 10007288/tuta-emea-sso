using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using System.Data.SqlClient;

public partial class essaytests : System.Web.UI.Page
{
    string _Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserRole();

        if (_Role != "Admin")
        {
            Response.Redirect("Home.aspx");
        }

        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        //if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(9))
        //    Response.Redirect("~/rights.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
            BindItems();
    }

    protected void GetUserRole()
    {
        //Check user role of the current user
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }

    protected void gvEssays_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvEssays.PageIndex = e.NewPageIndex;
        this.gvEssays.DataSource = TestTaken.SelectTestTakensWithEssaysByTest(Convert.ToInt32(this.ddlExams.SelectedValue));
        this.gvEssays.DataBind();
    }

    protected void gvEssays_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            GridViewRow row = this.gvEssays.Rows[e.NewSelectedIndex];
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Label lblUsername = row.FindControl("lblUsername") as Label;
            if (lblTestTakenID != null)
            {
                TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                if (taken != null)
                {
                    SessionManager.SessionTestTakenID = taken.TestTakenID;
                    SessionManager.SessionExamineeName = lblUsername.Text.Trim();
                    Response.Redirect("~/markessays.aspx?" + Request.QueryString.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "essaytests.aspx", "gvEssays_SelectedIndexChanging", ex.Message);
        }
    }

    protected void BindItems()
    {
        //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(9))
        {
            this.ddlExams.DataSource = TestCategory.SelectPendingEssayExams();
        }
        //else
        {
            //this.gvEssays.DataSource = null;
        }
        this.ddlExams.DataBind();
        if (this.ddlExams.Items.Count < 1)
        {
            this.ddlExams.Visible = false;
            this.btnExams.Visible = false;
            this.lblFilterByUser.Text = "No pending essays available.";
        }
    }

    protected void lnkGoToBySingleItem_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essays.aspx?" + Request.QueryString.ToString());
    }

    protected void btnExams_Click(object sender, EventArgs e)
    {
        this.gvEssays.DataSource = TestTaken.SelectTestTakensWithEssaysByTest(Convert.ToInt32(this.ddlExams.SelectedValue));
        this.gvEssays.DataBind();
    }

    protected void btnUserID_Click(object sender, EventArgs e)
    {
        this.gvChecked.DataSource = TestTaken.SelectCheckedcEssays(this.txtUserID.Text.Trim());
        this.gvChecked.DataBind();
    }

    protected void gvChecked_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvChecked.PageIndex = e.NewPageIndex;
        this.gvChecked.DataSource = TestTaken.SelectCheckedcEssays(this.txtUserID.Text.Trim());
        this.gvChecked.DataBind();
    }

    protected void gvChecked_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            GridViewRow row = this.gvChecked.Rows[e.NewSelectedIndex];
            Label lblTestTakenID = row.FindControl("lblTestTakenID") as Label;
            Label lblUsername = row.FindControl("lblUsername") as Label;
            if (lblTestTakenID != null)
            {
                TestTaken taken = TestTaken.Select(Convert.ToInt32(lblTestTakenID.Text.Trim()));
                if (taken != null)
                {
                    SessionManager.SessionTestTakenID = taken.TestTakenID;
                    SessionManager.SessionExamineeName = lblUsername.Text.Trim();
                    Response.Redirect("~/recheckessays.aspx?" + Request.QueryString.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "essaytests.aspx", "gvChecked_SelectedIndexChanging", ex.Message);
        }
    }
}
