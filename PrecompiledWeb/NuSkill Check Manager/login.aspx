<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="login, App_Web_yuclf4ni" title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlAll" runat="server" HorizontalAlign="center">
        <asp:UpdatePanel ID="udpSub" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlUpdate" runat="server" Height="150px">
                    <asp:UpdateProgress ID="udpProgress" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="pnlBuffer" runat="server" Height="150px">
                                <table>
                                    <tr>
                                        <td align="center" valign="middle" style="height: 200px">
                                            <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                            <br />
                                            <asp:Label ID="lblPleaseWait" runat="server" Text="Please Wait..." CssClass="smallText" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:Panel>
                <asp:Panel ID="pnlMain" runat="server" Width="300px" BorderColor="black" DefaultButton="lnkLogin"
                    BorderStyle="None" HorizontalAlign="center" CssClass="panelStandard">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:CustomValidator ID="cvLoginError" runat="server" ErrorMessage="Invalid Login Credentials/Insufficient Rights."
                                    Display="dynamic" CssClass="validatorStyle" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblUsername" runat="server" Text="TWWID" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtUsername" runat="server" Width="120px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblPassword" runat="server" Text="Password" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPassword" runat="server" Width="120px" TextMode="password" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:LinkButton ID="lnkLogin" runat="server" Text="Login" CssClass="linkbutton" OnClick="lnkLogin_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
