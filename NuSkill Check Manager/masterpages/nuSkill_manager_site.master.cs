using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class masterpages_nuSkill_manager_site : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            this.trControls.Visible = false;
        else
            this.trControls.Visible = true;

        UserMenuControl.UserRole = SessionManager.SessionUserRights;
    }

    protected void UserNavigationControl1_Load(object sender, EventArgs e)
    {

    }
}
