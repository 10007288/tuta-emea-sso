<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="suggestnew.aspx.cs" Inherits="sugestnew" %>

<%@ Register TagPrefix="wus" TagName="editexamitemcontrol" Src="~/controls/editexamquestioncontrol.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px" Width="100%">
                    <table cellpadding="10" cellspacing="0" style="font-family: Arial; text-align: left;
                        width: 100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblTeamSummaries" runat="server" Text="Suggest New Question" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="5">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblNew" runat="server" Text="Suggest New Question" Font-Size="14px" />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Label ID="lblForTest" runat="server" Text="For Test:" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlForTest" runat="server" DataValueField="TestCategoryID"
                                                DataTextField="TestName" AppendDataBoundItems="true">
                                                <asp:ListItem Text="Test Not in List" Value="0" />
                                            </asp:DropDownList>
                                            <br />
                                            <asp:Label ID="lblNote" runat="server" Text="Note: If the test cannot be found in the list above, indicate it in the comments below."
                                                Font-Size="10px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Label ID="lblComments" runat="server" Text="Comments:" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtComments" runat="server" TextMode="multiline" Rows="5" MaxLength="1000"
                                                Width="80%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCreateItem" runat="server" Text="Create Item" CssClass="buttons"
                                                OnClick="btnCreateItem_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCurrent" runat="server" Text="Your Suggested Questions" Font-Size="14px" />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvOwnSuggestions" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                ForeColor="black" PageSize="20" CellPadding="5" BorderColor="black" Font-Bold="false"
                                                Width="100%" EmptyDataText="No exams found." OnRowCommand="gvOwnSuggestions_RowCommand" OnPageIndexChanging="gvOwnSuggestions_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Questions">
                                                        <ItemTemplate>
                                                            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblDateCreatedHeader" runat="server" Text="Date Created" Font-Bold="true" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblTestNameHeader" runat="server" Text="Test Name" Font-Bold="true" />
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="lblQuestionHeader" runat="server" Text="Question" Font-Bold="true" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblDateCreated" runat="server" Text='<%#Bind("RecDateCreated") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="lblQuestion" runat="server" Text='<%#Bind("Question") %>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="lblStatusHeader" runat="server" Text="Status" Font-Bold="true" />
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:Label ID="lblApprovedByHeader" runat="server" Text="Approver" Font-Bold="true" />
                                                                    </td>
                                                                    <td style="width: 40%">
                                                                        <asp:Label ID="lblApproverCommentsHeader" runat="server" Text="Approver Comments"
                                                                            Font-Bold="true" />
                                                                    </td>
                                                                    <td rowspan="2" valign="middle" align="center" style="width: 12%">
                                                                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Cancel" CommandArgument='<%#Bind("QuestionnaireID") %>' CommandName="CancelQuestion" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblRecommendedQuestionStatusID" runat="server" Text='<%#Bind("RecommendedQuestionStatusID") %>'
                                                                            Visible="false" />
                                                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Bind("Description") %>' Font-Bold="true" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblApprovedBy" runat="server" Text='<%#Bind("ApprovedBy") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblApproverComments" runat="server" Text='<%#Bind("ApproverComments") %>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="Date Created">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDateCreated" runat="server" Text='<%#Bind("RecDateCreated") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Test Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Question">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblQuestion" runat="server" Text='<%#Bind("Question") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemStyle HorizontalAlign="center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRecommendedQuestionStatusID" runat="server" Text='<%#Bind("RecommendedQuestionStatusID") %>'
                                                                Visible="false" />
                                                            <asp:Label ID="lblDescription" runat="server" Text='<%#Bind("Description") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Approver">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblApprovedBy" runat="server" Text='<%#Bind("ApprovedBy") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comments">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblApproverComents" runat="server" Text='<%#Bind("ApproverComments") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Cancel" CommandArgument='<%#Bind("QuestionnaireID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpePopup" runat="server" TargetControlID="hidPopup"
        PopupControlID="pnlPopup" BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidPopup" runat="server" />
    <asp:Panel ID="pnlPopup" runat="server" HorizontalAlign="center" CssClass="modalPopup">
        <table>
            <tr>
                <td>
                    <wus:editexamitemcontrol ID="wusNewItem" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeSuccess" runat="server" TargetControlID="hidSuccess"
        PopupControlID="pnlSuccess" BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidSuccess" runat="server" />
    <asp:Panel ID="pnlSuccess" runat="server" HorizontalAlign="center" CssClass="modalPopup">
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblSuccess" runat="server" Text="Item succesfully entered." />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSuccess" runat="server" Text="OK" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeCancelConfirm" runat="server" TargetControlID="hidCancelConfirm"
        PopupControlID="pnlCancelConfirm" BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidCancelConfirm" runat="server" />
    <asp:Panel ID="pnlCancelConfirm" runat="server" CssClass="modalPopup" Width="200px">
        <table width="100%" cellpadding="5">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblAreYouSure" runat="server" Text="Are you sure you want to delete this question?" />
                    <asp:Label ID="lblQuestionnaireID" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSureOk" runat="server" Text="OK" CssClass="buttons" />
                </td>
                <td align="right">
                    <asp:Button ID="btnSureCancel" runat="server" Text="Cancel" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
