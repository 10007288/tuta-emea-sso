using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business.Entities
{
    [Serializable]
    public class EQuestionType : IQuestionType
    {
        #region properties
        private string _typeCode;

        public string TypeCode
        {
            get { return _typeCode; }
            set { _typeCode = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }
        #endregion

        #region Constructors
        public EQuestionType()
        {
        }

        public EQuestionType(string typeCode, string description, bool hideFromList)
        {
            this._typeCode = typeCode;
            this._description = description;
            this._hideFromList = hideFromList;
        }
        #endregion

        #region methods
        public void Insert(string username)
        {
            QuestionTypeDAL dal = new QuestionTypeDAL();
            dal.Insert(this._typeCode, this._description, this._hideFromList, username);
        }

        public IQuestionType Select(string typeCode)
        {
            QuestionTypeDAL dal = new QuestionTypeDAL();
            IQuestionType[] types = Conversion.SetProperties<IQuestionType>(dal.Select(typeCode));
            return types == null || types.Length < 1 ? null : types[0];
        }

        public IQuestionType[] SelectAll()
        {
            QuestionTypeDAL dal = new QuestionTypeDAL();
            return Conversion.SetProperties<IQuestionType>(dal.SelectAll());
        }
        #endregion
    }
}
