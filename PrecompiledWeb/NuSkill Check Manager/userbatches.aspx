<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="userbatches, App_Web_yuclf4ni" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="4" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblUserBatches" runat="server" Text="User Batches" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblChoose" runat="server" Text="User Batch:" />
                                <asp:DropDownList ID="ddlUserBatches" runat="server" DataValueField="BatchID" DataTextField="BatchName" />
                                <asp:Button ID="btnUserBatches" runat="server" CssClass="buttons" Text="Search" OnClick="btnUserBatches_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblUserBatchesNull" runat="server" Text="No Users found." Visible="false" />
                                <asp:DataList ID="dtlUserBatches" runat="server" RepeatDirection="horizontal" RepeatColumns="5" Width="100%" CellPadding="3" GridLines="both">
                                    <ItemStyle HorizontalAlign="center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserName" runat="server" Text='<%#Bind("AutoUserID") %>' />
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
