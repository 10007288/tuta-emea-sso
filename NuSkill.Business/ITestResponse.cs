using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace NuSkill.Business
{
    public interface ITestResponse
    {
        int TestResponseID { get; set; }
        string UserID { get; set; }
        IQuestionnaire Questionnaire { get; set; }
        string Response1 { get; set; }
        string Response2 { get; set; }
        string Response3 { get; set; }
        string Response4 { get; set; }
        string Response5 { get; set; }
        string Response6 { get; set; }
        string Response7 { get; set; }
        string Response8 { get; set; }
        string Response9 { get; set; }
        string Response10 { get; set; }
        string EssayResponse { get; set; }

        int Insert();
        void GradeEssayQuestion(int essayScore);
        ITestResponse[] SelectByUser(string userID);
        ITestResponse Select(int testResponseID);
        ITestResponse Select(int testTakenID, int questionnaireID, string userID);
        ITestResponse[] SelectAll();
        int CountTestResponse(int questionnaireID);
        int GetCorrectAnswer(int questionnaireID);
        ITestResponse[] SelectByQuestion(int questionnaireID);
        ITestResponse[] SelectByTestTaken(int testTakenID);
        DataSet GetMultipleEssays(int testTakenID);
    }
}
