<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="HarmonyQualification, App_Web_yuclf4ni" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="4" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lblMassUserCreation" runat="server" Text="Harmony Qualification" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="ddlTestcategoryID" runat="server" DataValueField="TestCategoryID"
                                    DataTextField="TestName" />
                            </td>
                            <td>
                                <asp:Label ID="lblHarmonyQualification" runat="server" Text="Harmony Qualification ID:" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtHarmonyQualificationID" runat="server" Width="100px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <asp:Button ID="btnOK" runat="server" Text="Create" CssClass="buttons" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <br />
                                <br />
                                <br />
                                <asp:GridView ID="gvHarmonyQualifications" runat="server" AutoGenerateColumns="false"
                                    AllowPaging="false" ForeColor="black" CellPadding="5" BorderColor="black" Font-Bold="false"
                                    Width="100%" EmptyDataText="No exams found.">
                                    <HeaderStyle HorizontalAlign="center" />
                                    <RowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" Wrap="true" />
                                    <AlternatingRowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" />
                                    <Columns>
                                        <asp:BoundField DataField="TestCategoryID" HeaderText="Test ID" />
                                        <asp:BoundField DataField="TestName" HeaderText="Test Name" />
                                        <asp:BoundField DataField="HarmonyQualificationID" HeaderText="Harmony Qualification ID" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
