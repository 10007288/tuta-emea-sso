using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls.WebParts;
using System.Threading;
using System.Globalization;
using Microsoft.AspNet.Membership.OpenAuth;

public partial class login : System.Web.UI.Page
{
    #region TUTOld
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    //if (SessionManager.SessionSessionID == null)
    //    //    SessionManager.SessionSessionID = Guid.NewGuid();
    //    //else
    //    //{
    //    //    LoginSession session = LoginSession.Select(SessionManager.SessionUsername);
    //    //    if (session == null)
    //    //        Response.Redirect("~/login.aspx");
    //    //    else if (session.LoginSessionID != SessionManager.SessionSessionID)
    //    //    { 
    //    //        SessionManager.SessionSessionID = Guid.Empty;
    //    //        Response.Redirect("~/login.aspx");
    //    //    }
    //    //}
    //    if (!this.IsPostBack)
    //    {
    //        this.Session.Clear();
    //        //DataSet siteSet = this.LoadCompanySites();
    //        //string siteName = string.Empty;
    //        //if (siteSet.Tables[0].Rows.Count > 0)
    //        //    siteName = siteSet.Tables[0].Rows[0]["CompanySite"].ToString();
    //        //this.ddlSites.DataSource = siteSet.Tables[1];
    //        //this.ddlSites.DataBind();
    //        //this.ddlSites.SelectedIndex = -1;
    //        //if (!string.IsNullOrEmpty(siteName))
    //        //    foreach (ListItem item in this.ddlSites.Items)
    //        //    {
    //        //        if (item.Text == siteName)
    //        //        {
    //        //            item.Selected = true;
    //        //            break;
    //        //        }
    //        //    }
    //    }
    //}

    //private DataSet LoadCompanySites()
    //{
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        NuComm.Connection connection = new NuComm.Connection();
    //        connection.DatabaseServer = Config.Vader2DatabaseServer();
    //        connection.DatabaseName = "Testing"; //Config.TestingDatabaseName();
    //        connection.UserName = "us_Survey";//Config.TestingDatabaseUsername();
    //        connection.Password = "";//Config.TestingDatabasePassword();
    //        connection.AddParameter("@CIMNumber", 0, SqlDbType.Int);
    //        connection.StoredProcedureName = "dbo.pr_WeeklyTesting_Lkp_CompanySiteByCIM";
    //        connection.Fill(ref ds);
    //        connection.DisconnectInstance();
    //        connection = null;
    //        return ds;
    //    }
    //    catch (Exception ex)
    //    {
    //        //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
    //        //service.WriteError(4, "login.aspx", "LoadCompanySites", ex.Message);
    //        return null;
    //    }
    //}

    //protected void lnkLogin_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int temp = 0;
    //        if (int.TryParse(this.txtUsername.Text.Trim(), out temp) && Config.AcceptInternalLogin())
    //        {
    //            if (LoadVerifyUser())
    //            {
    //                SessionManager.SessionSessionID = Guid.NewGuid();
    //                SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //                LoginSession session = LoginSession.Select(this.txtUsername.Text.Trim());
    //                SessionManager.SessionCim = Convert.ToInt32(this.txtUsername.Text.Trim());
    //                //SessionManager.SessionCompanySite = this.ddlSites.SelectedValue;
    //                if (session == null)
    //                {
    //                    try
    //                    {
    //                        session = new LoginSession(this.txtUsername.Text.Trim(), SessionManager.SessionSessionID);
    //                        session.Insert();
    //                    }
    //                    catch
    //                    {
    //                        SessionManager.SessionMainText = "System could not log you in. Please try again later.";
    //                        Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //                    }
    //                }
    //                else if (session.LoginSessionID != SessionManager.SessionSessionID)
    //                    try
    //                    {
    //                        session.Update();
    //                    }
    //                    catch
    //                    {
    //                        SessionManager.SessionMainText = "System could not log you in. Please try again later.";
    //                        Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //                    }
    //                SessionManager.SessionSite = "INT";// this.ddlSites.SelectedValue;
    //                Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    //            }
    //            else
    //                this.cvLoginError.IsValid = false;
    //        }
    //        //if (InternalCredentials.LoginUser(temp, this.txtPassword.Text.Trim()).Result == 0)

    //        else if (Registration.ValidateLogin(this.txtUsername.Text.Trim(), NuComm.Security.Encryption.UTF8.EncryptText(this.txtPassword.Text.Trim())) && Config.AcceptExternalLogin())
    //        {
    //            SessionManager.SessionSessionID = Guid.NewGuid();
    //            SessionManager.SessionUsername = this.txtUsername.Text.Trim();
    //            LoginSession session = LoginSession.Select(this.txtUsername.Text.Trim());
    //            //SessionManager.SessionCompanySite = this.ddlSites.SelectedValue;
    //            if (session == null)
    //            {
    //                try
    //                {
    //                    session = new LoginSession(this.txtUsername.Text.Trim(), SessionManager.SessionSessionID);
    //                    session.Insert();
    //                }
    //                catch (Exception ex)
    //                {
    //                    //ErrorLogger.Write(Config.ApplicationName, "login.aspx", "lnkLogin_Click", ex.Message);
    //                    SessionManager.SessionMainText = "System could not log you in. Please try again later.";
    //                    Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //                }
    //            }
    //            else if (session.LoginSessionID != SessionManager.SessionSessionID)
    //                try
    //                {
    //                    session.Update();
    //                }
    //                catch (Exception ex)
    //                {
    //                    //ErrorLogger.Write(Config.ApplicationName, "login.aspx", "lnkLogin_Click", ex.Message);
    //                    SessionManager.SessionMainText = "System could not log you in. Please try again later.";
    //                    Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //                }
    //            SessionManager.SessionSite = "EXT";//this.ddlSites.SelectedValue;
    //            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        if (!ex.Message.Contains("Thread was being aborted"))
    //        {
    //            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
    //            //service.WriteError(4, "allitems.aspx", "ProcessItem", ex.Message);
    //            SessionManager.SessionMainText = "System has encountered an unknown error. Please contact your examiner/administrator.";
    //            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
    //        }
    //    }
    //}

    //protected void lnkRegister_Click(object sender, EventArgs e)
    //{
    //    if (string.IsNullOrEmpty(SessionManager.SessionMainText))
    //        Response.Redirect("~/registration.aspx?" + Request.QueryString.ToString());
    //    else if (!string.IsNullOrEmpty(SessionManager.SessionUsername))
    //        Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    //}

    //private bool LoadVerifyUser()
    //{
    //    bool isvaliduser = false;
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        NuComm.Connection connection = new NuComm.Connection();
    //        connection.DatabaseServer = Config.Vader2DatabaseServer();// @"Vader2\Callisto";
    //        connection.DatabaseName = Config.Vader2DatabaseName();// "BlackBox";
    //        connection.UserName = Config.Vader2DatabaseUsername();// "us_RAS";
    //        connection.Password = Config.Vader2DatabasePassword();// "";

    //        string passencrypt = string.Empty;
    //        using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
    //        {
    //            oconn.Open();
    //            using (SqlCommand ocomm = new SqlCommand("select password from users where twwid='" + txtUsername.Text + "'", oconn))
    //            {
    //                ocomm.CommandType = CommandType.Text;
    //                SqlDataReader rsdata;
    //                rsdata = ocomm.ExecuteReader();
    //                rsdata.Read();
    //                if (rsdata.HasRows)
    //                    passencrypt = rsdata[0].ToString();
    //            }
    //            oconn.Close();
    //        }
    //        bool isCorrectPasssword = Password.ValidatePassword(txtPassword.Text, passencrypt);
    //        if (isCorrectPasssword)
    //        {
    //            connection.AddParameter("@EmpID", this.txtUsername.Text.Trim(), SqlDbType.VarChar);
    //            connection.AddParameter("@Password", passencrypt, SqlDbType.VarChar);
    //            //connection.AddParameter("@CompanyID", 2, SqlDbType.Int);
    //            connection.StoredProcedureName = "dbo.pr_RAS_ValidateUser";
    //            if (connection.Fill(ref ds) && (ds.Tables[0].Rows.Count > 0))
    //            {
    //                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
    //                    isvaliduser = true;
    //            }
    //            connection.DisconnectInstance();
    //            connection = null;
    //        }
    //        else
    //            isvaliduser = false;
    //        return isvaliduser;
    //    }
    //    catch (Exception ex)
    //    {
    //        //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
    //        //service.WriteError(4, "login.aspx", "LoadVerifyUser", ex.Message);
    //        return false;
    //    }
    //}
    #endregion

    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];
    }

    public IEnumerable<ProviderDetails> GetProviderNames()
    {
        return OpenAuth.AuthenticationClients.GetAll();
    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
}
