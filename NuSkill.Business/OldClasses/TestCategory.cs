using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class TestCategory
    {
        #region properties
        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private DateTime _startDate;

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate;

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private int _numberOfQuestions;

        public int NumberOfQuestions
        {
            get { return _numberOfQuestions; }
            set { _numberOfQuestions = value; }
        }

        private int _passingGrade;

        public int PassingGrade
        {
            get { return _passingGrade; }
            set { _passingGrade = value; }
        }

        private int _testLimit;

        public int TestLimit
        {
            get { return _testLimit; }
            set { _testLimit = value; }
        }

        private int _timeLimit;

        public int TimeLimit
        {
            get { return _timeLimit; }
            set { _timeLimit = value; }
        }

        private bool _isEssay;

        public bool IsEssay
        {
            get { return _isEssay; }
            set { _isEssay = value; }
        }

        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        private DateTime _dateLastUpdated;

        public DateTime DateLastUpdated
        {
            get { return _dateLastUpdated; }
            set { _dateLastUpdated = value; }
        }

        private string _lastUpdated;

        public string LastUpdated
        {
            get { return _lastUpdated; }
            set { _lastUpdated = value; }
        }

        private string _lastUpdatedBy;

        public string LastUpdatedBy
        {
            get { return _lastUpdatedBy; }
            set { _lastUpdatedBy = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        private int _campaignID;

        public int CampaignID
        {
            get { return _campaignID; }
            set { _campaignID = value; }
        }

        private int _accountID;

        public int AccountID
        {
            get { return _accountID; }
            set { _accountID = value; }
        }

        private bool _isCampaign;

        public bool IsCampaign
        {
            get { return _isCampaign; }
            set { _isCampaign = value; }
        }

        private int _courseID;

        public int CourseID
        {
            get { return _courseID; }
            set { _courseID = value; }
        }

        private int _testID;

        public int TestID
        {
            get { return _testID; }
            set { _testID = value; }
        }

        private string _category;

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _subcategory;

        public string Subcategory
        {
            get { return _subcategory; }
            set { _subcategory = value; }
        }

        private string _instructions;

        public string Instructions
        {
            get { return _instructions; }
            set { _instructions = value; }
        }

        private bool _hideScores;

        public bool HideScores
        {
            get { return _hideScores; }
            set { _hideScores = value; }
        }

        private bool _retake;

        public bool Retake
        {
            get { return _retake; }
            set { _retake = value; }
        }

        private int _clientId;

        public int ClientID
        {
            get { return _clientId; }
            set { _clientId = value; }
        }

        //tandim #2265866 - to add additional option to allow to randomize question checkbox.

        private bool _randomizeQuestions;

        public bool RandomizeQuestions
        {
            get { return _randomizeQuestions; }
            set { _randomizeQuestions = value; }
        }

        private bool _hideExamFromTesting;

        public bool HideExamFromTesting
        {
            get { return _hideExamFromTesting; }
            set { _hideExamFromTesting = value; }
        }

        //ADDED BY RAYMARK COSME <11/19/2015>
        private bool _isRate;

        public bool IsRate
        {
            get { return _isRate; }
            set { _isRate = value; }
        }

        //ADDED BY RAYMARK COSME <11/19/2015>
        private bool _isComments;

        public bool IsComments
        {
            get { return _isComments; }
            set { _isComments = value; }
        }

        #endregion

        #region Constructors
        public TestCategory()
        {
        }

        public TestCategory(string testName, DateTime startDate, DateTime endDate, int numberOfQuestions, int passingGrade, int testLimit, int timeLimit, bool essay, DateTime dateCreated, string lastUpdated, bool hideFromList, string instructions, bool hideScores, bool retake, int clientId)
        {
            this._dateCreated = dateCreated;
            this._dateLastUpdated = dateCreated;
            this._endDate = endDate;
            this._hideFromList = hideFromList;
            this._isEssay = essay;
            this._lastUpdated = lastUpdated;
            this._lastUpdatedBy = lastUpdated;
            this._numberOfQuestions = numberOfQuestions;
            this._passingGrade = passingGrade;
            this._startDate = startDate;
            this._testLimit = testLimit;
            this._testName = testName;
            this._timeLimit = timeLimit;
            this._instructions = instructions;
            this._hideScores = hideScores;
            this._retake = retake;
            this._clientId = clientId;
        }
        #endregion

        #region methods
        public int Insert()
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return dal.Insert(this._dateCreated, this._dateLastUpdated, this._endDate, this._hideFromList, this._isEssay, this._lastUpdated, this._lastUpdatedBy,
                this._numberOfQuestions, this._passingGrade, this._startDate, this._testLimit, this._testName, this._timeLimit, this._accountID, this._campaignID, this._courseID, this._isCampaign, this._instructions, this._hideScores, this._randomizeQuestions, this._hideExamFromTesting, this._retake, this._clientId);
        }

        public static TestCategory Select(int testCategoryID, bool includeElapsed)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            TestCategory[] categories = Conversion.SetProperties<TestCategory>(dal.Select(testCategoryID, includeElapsed));
            return categories == null || categories.Length == 0 ? null : categories[0];
        }

        public static int checkRetake(int testCategoryID)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return dal.checkRetake(testCategoryID); 
        }

        public static TestCategory[] SelectByAccountCampaign(int accountCampaignID, int subCategory, bool isCampaign, bool includeElapsed)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return Conversion.SetProperties<TestCategory>(dal.SelectByAccountCampaign(accountCampaignID, subCategory, isCampaign, includeElapsed));
        }

        public static TestCategory[] Search(string searchParam, bool includeElapsed)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return Conversion.SetProperties<TestCategory>(dal.Search(searchParam, includeElapsed));
        }

        public static TestCategory[] SearchByAccountCampaign(string searchParam, int accountCampaignID, int subCategory, bool isCampaign, bool includeElapsed)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return Conversion.SetProperties<TestCategory>(dal.SearchByAccountCampaign(searchParam, accountCampaignID, subCategory, isCampaign, includeElapsed));
        }

        public static TestCategory[] SearchByTestGroup(int testGroupID)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return Conversion.SetProperties<TestCategory>(dal.SearchByTestGroup(testGroupID));
        }

        public static TestCategory[] SelectAll(bool includeElapsed, bool recentOnly)
        {
            TestCategoryDAL dal = new TestCategoryDAL();

            return Conversion.SetProperties<TestCategory>(dal.SelectAll(includeElapsed, recentOnly));
        }

        public void DeleteByHide()
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            dal.DeleteByHide(this._testCategoryID);
        }

        public void Update()
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            dal.Update(this._dateCreated, this._dateLastUpdated, this._endDate, this._hideFromList, this._isEssay, this._lastUpdated, this._lastUpdatedBy,
                this._numberOfQuestions, this._passingGrade, this._startDate, this._testCategoryID, this._testLimit, this._testName, this._timeLimit, this._accountID, this._campaignID, this._courseID, this._isCampaign, this._instructions, this._hideScores, this._randomizeQuestions, this._hideExamFromTesting, this._retake, this._clientId);
        }

        public static bool TestIsEssay(int testCategoryID)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return dal.IsEssay(testCategoryID);
        }

        public static DataSet GetTeamSummaryTests()
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return dal.GetTeamSummaryTests();
        }

        public static TestCategory[] SelectPendingEssayExams()
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return Conversion.SetProperties<TestCategory>(dal.SelectPendingEssayExams());
        }
        #endregion
    }
}
