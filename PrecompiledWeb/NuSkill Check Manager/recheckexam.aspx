<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="recheckexam, App_Web_yuclf4ni" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblHome" runat="server" Text="Recheck Exams" ForeColor="black" Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCategory" runat="server" Text="Caregory" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCategory" runat="server" DataValueField="CampaignID" DataTextField="Campaign"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblSubcategory" runat="server" Text="Subcategory" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSubcategory" runat="server" DataValueField="CampaignID"
                                                DataTextField="Campaign" AutoPostBack="true" OnSelectedIndexChanged="ddlSubcategory_SelectedIndexChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblExam" runat="server" Text="Test" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlExams" runat="server" DataValueField="TestCategoryID" DataTextField="TestName" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnRegrade" runat="server" Text="Recalculate scores" CssClass="buttons"
                                                OnClick="btnRegrade_Click" Width="200px" />
                                        </td>
                                    </tr>
                                </table>
                                <ajax:ModalPopupExtender ID="mpeTestsTaken" runat="server" TargetControlID="hidTestsTaken"
                                    PopupControlID="pnlTestsTaken" BackgroundCssClass="modalBackground" />
                                <asp:HiddenField ID="hidTestsTaken" runat="server" />
                                <asp:Panel ID="pnlTestsTaken" runat="server" Width="400px" HorizontalAlign="center"
                                    CssClass="modalPopup" ScrollBars="vertical" Height="400px">
                                    <asp:Label ID="lblNote" runat="server" Text="These exams will be rechecked. Press OK to continue." />
                                    <asp:GridView ID="gvTestsTaken" runat="server" EmptyDataText="No Exams found." Width="100%"
                                        AutoGenerateColumns="false">
                                        <RowStyle HorizontalAlign="Left" />
                                        <Columns>
                                            <asp:BoundField DataField="UserID" HeaderText="CIM" />
                                            <asp:BoundField DataField="DateStartTaken" HeaderText="Date Taken" />
                                        </Columns>
                                    </asp:GridView>
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="buttons" OnClick="btnOK_Click" />
                                            </td>
                                            <td align="right">
                                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="buttons" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <ajax:ModalPopupExtender ID="mpeResult" runat="server" TargetControlID="hidResult"
                                    PopupControlID="pnlResult" BackgroundCssClass="modalBackground" />
                                <asp:HiddenField ID="hidResult" runat="server" />
                                <asp:Panel ID="pnlResult" runat="server" Width="300px" CssClass="modalPopup" HorizontalAlign="center">
                                    <br />
                                    <asp:Label ID="lblResult" runat="server" />
                                    <br />
                                    <br />
                                    <asp:Button ID="btnResultOK" runat="server" CssClass="buttons" Text="OK" />
                                    <br />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
