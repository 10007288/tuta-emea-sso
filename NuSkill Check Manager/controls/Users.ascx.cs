﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuSkill.Business;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using System.Data;

public partial class controls_Users : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void txtTwwid_TextChanged(object sender, EventArgs e)
    {
        if (!ChkUserIfTrueEmployee(txtTwwid.Text))
        {
            UserIsEmployee.InnerText = String.Format("Your TWWID provided does not exist on valid Employees list.");
            txtTwwid.Text = string.Empty;
            txtTwwid.Focus();
        }
        else if (ChkTwwidIfExist(txtTwwid.Text))
        {
            UserIsEmployee.InnerText = String.Format("Your ID provided is already registered. Please contact Admin for assistance.");
            txtTwwid.Text = string.Empty;
            txtTwwid.Focus();
        }
        else
        {
            UserIsEmployee.InnerText = "";
        }
    }

    private bool ChkTwwidIfExist(string twwid)
    {
        bool exist = false;

        using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            oconn.Open();
            using (SqlCommand ocomm = new SqlCommand("select twwid from Users where twwid='" + twwid + "'", oconn))
            {
                ocomm.CommandType = System.Data.CommandType.Text;
                SqlDataReader rsdata = ocomm.ExecuteReader();
                rsdata.Read();
                if (rsdata.HasRows)
                {
                    exist = true;
                }
            }
            oconn.Close();
        }

        return exist;
    }

    private bool ChkUserIfTrueEmployee(string twwid)
    {
        bool exist = false;

        using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            oconn.Open();
            using (SqlCommand ocomm = new SqlCommand("select twwid from vw_AllEmployees where twwid='" + twwid + "'", oconn))
            {
                ocomm.CommandType = System.Data.CommandType.Text;
                SqlDataReader rsdata = ocomm.ExecuteReader();
                rsdata.Read();
                if (rsdata.HasRows)
                {
                    exist = true;
                }
            }
            oconn.Close();
        }

        return exist;
    }

    protected void rcbRole_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (rcbRole.SelectedItem.Text == "Course Admin")
            lnkBtnAssignClient.Visible = true;
        else
            lnkBtnAssignClient.Visible = false;
    }
}