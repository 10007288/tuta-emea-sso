using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using System.Data.SqlClient;

public partial class temasummaries : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(21))
            //    Response.Redirect("~/rights.aspx?" + Request.QueryString.ToString());
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(22))
            this.lnkTestSummary.Enabled = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(23))
            this.lnkGeneralQuery.Enabled = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(24))
            this.lnkTestsTakenByLocation.Enabled = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(25))
            this.lnkTestsTakenCampaign.Enabled = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(26))
            this.lnkTestsTakenTSR.Enabled = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(27))
            this.lnkPassedFailed.Enabled = true;
            //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(28))
            this.lnkTeamSummaries.Enabled = true;

            //DataSet siteSet = TestSummaries.GetCompanySites(0);
            //string siteName = string.Empty;
            //if (siteSet.Tables[0].Rows.Count > 0)
            //    siteName = siteSet.Tables[0].Rows[0]["CompanySite"].ToString();
            //this.ddlSite.DataSource = siteSet.Tables[1];
            //this.ddlSite.DataBind();
            //this.ddlSite.SelectedIndex = -1;
            //if (!string.IsNullOrEmpty(siteName))
            //    foreach (ListItem item in this.ddlSite.Items)
            //    {
            //        if (item.Text == siteName)
            //        {
            //            item.Selected = true;
            //            break;
            //        }
            //    }
            //this.ddlTeam.DataSource = TeamLeaders.GetTeamLeaders();
            //this.ddlTeam.DataBind();

            //this.ddlCampaign.DataSource = AccountList.SelectAll(1);
            //this.ddlCampaign.DataBind();
            this.ddlPastExam.DataSource = TestCategory.GetTeamSummaryTests();
            this.ddlPastExam.DataBind();
            //using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
            //{
            //    oconn.Open();
            //    using (SqlCommand ocomm = new SqlCommand("select * from refSites where active=1 order by site_desc", oconn))
            //    {
            //        ocomm.CommandType = CommandType.Text;
            //        SqlDataReader rsdata = ocomm.ExecuteReader();
            //        rsdata.Read();
            //        if (rsdata.HasRows)
            //        {
            //            ddlSite.DataSource = rsdata;
            //            ddlSite.DataTextField = "site_desc";
            //            ddlSite.DataValueField = "ID";
            //            ddlSite.DataBind();
            //        }
            //        rsdata.Close();
            //    }                
            //    using (SqlCommand ocomm = new SqlCommand("select * from dbo.refCampaigns where active=1 order by campaign_desc", oconn))
            //    {
            //        ocomm.CommandType = CommandType.Text;
            //        SqlDataReader rsdata = ocomm.ExecuteReader();
            //        rsdata.Read();
            //        if (rsdata.HasRows)
            //        {
            //            ddlCampaign.DataSource = rsdata;
            //            ddlCampaign.DataTextField = "campaign_desc";
            //            ddlCampaign.DataValueField = "ID";
            //            ddlCampaign.DataBind();
            //        }
            //        rsdata.Close();
            //    }
            //    oconn.Close();
            //}
        }
    }

    protected void lnkTestSummary_Click(object sender, EventArgs e)
    {
        this.trYear.Visible = true;
        this.trSite.Visible = true;
        this.trCampaign.Visible = true;
        this.trCIM.Visible = false;
        this.trEndDate.Visible = false;
        this.trPassedFailed.Visible = false;
        this.trStartDate.Visible = false;
        this.trStartQuarter.Visible = false;
        this.trTeam.Visible = false;
        this.trTest.Visible = false;
        this.lblWhatToDo.Text = "TestSummary";
        this.mpePopup.Show();
    }

    protected void lnkGeneralQuery_Click(object sender, EventArgs e)
    {
        this.trYear.Visible = false;
        this.trSite.Visible = true;
        this.trCampaign.Visible = true;
        this.trCIM.Visible = false;
        this.trEndDate.Visible = true;
        this.trPassedFailed.Visible = false;
        this.trStartDate.Visible = true;
        this.trStartQuarter.Visible = false;
        this.trTeam.Visible = false;
        this.trTest.Visible = false;
        this.lblWhatToDo.Text = "GeneralQuery";
        this.mpePopup.Show();
    }

    protected void lnkTestsTakenByLocation_Click(object sender, EventArgs e)
    {
        this.trYear.Visible = true;
        this.trSite.Visible = true;
        this.trCampaign.Visible = false;
        this.trCIM.Visible = false;
        this.trEndDate.Visible = false;
        this.trPassedFailed.Visible = false;
        this.trStartDate.Visible = false;
        this.trStartQuarter.Visible = true;
        this.trTeam.Visible = false;
        this.trTest.Visible = false;
        this.lblWhatToDo.Text = "TestsTakenByLocation";
        this.mpePopup.Show();
    }

    protected void lnkTestsTakenCampaign_Click(object sender, EventArgs e)
    {
        this.trYear.Visible = true;
        this.trSite.Visible = false;
        this.trCampaign.Visible = true;
        this.trCIM.Visible = false;
        this.trEndDate.Visible = false;
        this.trPassedFailed.Visible = false;
        this.trStartDate.Visible = false;
        this.trStartQuarter.Visible = true;
        this.trTeam.Visible = false;
        this.trTest.Visible = false;
        this.lblWhatToDo.Text = "TestsTakenByCampaign";
        this.mpePopup.Show();
    }

    protected void lnkTestsTakenTSR_Click(object sender, EventArgs e)
    {
        this.trYear.Visible = true;
        this.trSite.Visible = false;
        this.trCampaign.Visible = false;
        this.trCIM.Visible = true;
        this.trEndDate.Visible = false;
        this.trPassedFailed.Visible = false;
        this.trStartDate.Visible = false;
        this.trStartQuarter.Visible = true;
        this.trTeam.Visible = false;
        this.trTest.Visible = false;
        this.lblWhatToDo.Text = "TestsTakenTSR";
        this.mpePopup.Show();
    }

    protected void lnkPassedFailed_Click(object sender, EventArgs e)
    {
        this.trYear.Visible = true;
        this.trSite.Visible = true;
        this.trCampaign.Visible = true;
        this.trCIM.Visible = false;
        this.trEndDate.Visible = false;
        this.trPassedFailed.Visible = true;
        this.trStartDate.Visible = false;
        this.trStartQuarter.Visible = true;
        this.trTeam.Visible = false;
        this.trTest.Visible = false;
        this.lblWhatToDo.Text = "PassedFailed";
        this.mpePopup.Show();
    }

    protected void lnkTeamSummaries_Click(object sender, EventArgs e)
    {
        this.trYear.Visible = false;
        this.trSite.Visible = false;
        this.trCampaign.Visible = false;
        this.trCIM.Visible = false;
        this.trEndDate.Visible = true;
        this.trPassedFailed.Visible = false;
        this.trStartDate.Visible = true;
        this.trStartQuarter.Visible = false;
        this.trTeam.Visible = true;
        this.trTest.Visible = true;
        this.lblWhatToDo.Text = "TeamSummaries";
        this.mpePopup.Show();
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        int temp = 0;
        DateTime temp2 = new DateTime();
        bool isValid = true;
        if (this.trYear.Visible == true)
        {
            if (!Int32.TryParse(this.txtYear.Text.Trim(), out temp))
            {
                this.cvYear.IsValid = false;
                isValid = false;
            }
            else if (temp == 0)
            {
                this.cvYear.IsValid = false;
                isValid = false;
            }
        }
        if (this.trCIM.Visible == true)
        {
            if (!Int32.TryParse(this.txtCIM.Text.Trim(), out temp))
            {
                this.cvCIM.IsValid = false;
                isValid = false;
            }
            else if (temp == 0)
            {
                this.cvCIM.IsValid = false;
                isValid = false;
            }
        }
        if (this.trStartDate.Visible == true)
        {
            if (!DateTime.TryParse(this.txtStartDate.Text.Trim(), out temp2))
            {
                this.cvStartDate.IsValid = false;
                isValid = false;
            }
            else
            {
                Regex regex = new Regex(@"(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$");
                if (!regex.IsMatch(this.txtStartDate.Text.Trim()))
                {
                    this.cvStartDate.IsValid = false;
                    isValid = false;
                }
            }
        }
        if (this.trEndDate.Visible == true)
        {
            if (!DateTime.TryParse(this.txtEndDate.Text.Trim(), out temp2))
            {
                this.cvStartDate.IsValid = false;
                isValid = false;
            }
            else
            {
                Regex regex = new Regex(@"(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$");
                if (!regex.IsMatch(this.txtEndDate.Text.Trim()))
                {
                    this.cvEndDate.IsValid = false;
                    isValid = false;
                }
            }
        }
        if (!isValid)
        {
            this.mpePopup.Show();
            return;
        }

        switch (lblWhatToDo.Text.Trim())
        {
            case "TestSummary":
                this.gvTestSummary.DataSource = NuSkill.Business.TestSummaries.GetTestSummary(Convert.ToInt32(this.txtYear.Text.Trim()), this.ddlSite.Text.Trim(), Math.Abs(Convert.ToInt32(this.ddlCampaign.Text)));
                this.gvTestSummary.DataBind();
                this.gvTestSummary.Visible = true;
                this.gvGeneralQuery.Visible = false;
                this.gvTestsTakenByLocation.Visible = false;
                this.gvTestsTakenByCampaign.Visible = false;
                this.gvTestsTakenTSR.Visible = false;
                this.gvPassedFailed.Visible = false;
                this.gvTeamSummaries.Visible = false;
                this.mpeResults.Show();
                break;
            case "GeneralQuery":
                this.gvGeneralQuery.DataSource = NuSkill.Business.TestSummaries.GetGeneralQuery(this.ddlSite.Text, Math.Abs(Convert.ToInt32(this.ddlCampaign.Text)), DateTime.Parse(this.txtStartDate.Text.Trim()), DateTime.Parse(this.txtEndDate.Text.Trim()));
                this.gvGeneralQuery.DataBind();
                this.gvTestSummary.Visible = false;
                this.gvGeneralQuery.Visible = true;
                this.gvTestsTakenByLocation.Visible = false;
                this.gvTestsTakenByCampaign.Visible = false;
                this.gvTestsTakenTSR.Visible = false;
                this.gvPassedFailed.Visible = false;
                this.gvTeamSummaries.Visible = false;
                this.mpeResults.Show();
                break;
            case "TestsTakenByLocation":
                this.gvTestsTakenByLocation.DataSource = NuSkill.Business.TestSummaries.GetTestsTakenByLocation(Convert.ToInt32(this.txtYear.Text.Trim()), this.ddlSite.Text, Convert.ToInt32(this.ddlStartQuarter.Text));
                this.gvTestsTakenByLocation.DataBind();
                this.gvTestSummary.Visible = false;
                this.gvGeneralQuery.Visible = false;
                this.gvTestsTakenByLocation.Visible = true;
                this.gvTestsTakenByCampaign.Visible = false;
                this.gvTestsTakenTSR.Visible = false;
                this.gvPassedFailed.Visible = false;
                this.gvTeamSummaries.Visible = false;
                this.mpeResults.Show();
                break;
            case "TestsTakenByCampaign":
                this.gvTestsTakenByCampaign.DataSource = NuSkill.Business.TestSummaries.GetTestsTakenCmapaign(Convert.ToInt32(this.txtYear.Text.Trim()), Math.Abs(Convert.ToInt32(this.ddlCampaign.Text)), Convert.ToInt32(this.ddlStartQuarter.Text));
                this.gvTestsTakenByCampaign.DataBind();
                this.gvTestSummary.Visible = false;
                this.gvGeneralQuery.Visible = false;
                this.gvTestsTakenByLocation.Visible = false;
                this.gvTestsTakenByCampaign.Visible = true;
                this.gvTestsTakenTSR.Visible = false;
                this.gvPassedFailed.Visible = false;
                this.gvTeamSummaries.Visible = false;
                this.mpeResults.Show();
                break;
            case "TestsTakenTSR":
                this.gvTestsTakenTSR.DataSource = NuSkill.Business.TestSummaries.GetTestsTakenTSR(Convert.ToInt32(this.txtYear.Text.Trim()), Convert.ToInt32(this.txtCIM.Text.Trim()), Convert.ToInt32(this.ddlStartQuarter.Text));
                this.gvTestsTakenTSR.DataBind();
                this.gvTestSummary.Visible = false;
                this.gvGeneralQuery.Visible = false;
                this.gvTestsTakenByLocation.Visible = false;
                this.gvTestsTakenByCampaign.Visible = false;
                this.gvTestsTakenTSR.Visible = true;
                this.gvPassedFailed.Visible = false;
                this.gvTeamSummaries.Visible = false;
                this.mpeResults.Show();
                break;
            case "PassedFailed":
                this.gvPassedFailed.DataSource = NuSkill.Business.TestSummaries.GetPassedFailed(Convert.ToInt32(this.txtYear.Text.Trim()), this.rblPassedFailed.SelectedValue == "True" ? true : false, this.ddlSite.Text.Trim(), Math.Abs(Convert.ToInt32(this.ddlCampaign.Text)), Convert.ToInt32(this.ddlStartQuarter.Text));
                this.gvPassedFailed.DataBind();
                this.gvTestSummary.Visible = false;
                this.gvGeneralQuery.Visible = false;
                this.gvTestsTakenByLocation.Visible = false;
                this.gvTestsTakenByCampaign.Visible = false;
                this.gvTestsTakenTSR.Visible = false;
                this.gvPassedFailed.Visible = true;
                this.gvTeamSummaries.Visible = false;
                this.mpeResults.Show();
                break;
            case "TeamSummaries":
                this.gvTeamSummaries.DataSource = NuSkill.Business.TestSummaries.GetTeamSummaries(Convert.ToInt32(this.ddlTeam.Text), Convert.ToDateTime(this.txtStartDate.Text.Trim()), Convert.ToDateTime(this.txtEndDate.Text.Trim()), Convert.ToInt32(this.ddlPastExam.Text.Trim()));
                this.gvTeamSummaries.DataBind();
                this.gvTestSummary.Visible = false;
                this.gvGeneralQuery.Visible = false;
                this.gvTestsTakenByLocation.Visible = false;
                this.gvTestsTakenByCampaign.Visible = false;
                this.gvTestsTakenTSR.Visible = false;
                this.gvPassedFailed.Visible = false;
                this.gvTeamSummaries.Visible = true;
                this.mpeResults.Show();
                break;
            default:
                break;
        }

    }

    private DataSet LoadCompanySites()
    {
        try
        {
            DataSet ds = new DataSet();
            int num = 0;
            NuComm.Connection connection = new NuComm.Connection();
            connection.DatabaseServer = Config.Vader2DatabaseServer();
            connection.DatabaseName = "NuSkillCheck"; //Config.TestingDatabaseName();
            connection.UserName = "us_Survey";//Config.TestingDatabaseUsername();
            connection.Password = "";//Config.TestingDatabasePassword();
            connection.AddParameter("@CIMNumber", 0, SqlDbType.Int);
            connection.StoredProcedureName = "dbo.pr_WeeklyTesting_Lkp_CompanySiteByCIM";
            connection.Fill(ref ds);
            connection.DisconnectInstance();
            connection = null;
            return ds;
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "teamsummaries.aspx", "LoadCompanySites", ex.Message);
            return null;
        }
    }
}
