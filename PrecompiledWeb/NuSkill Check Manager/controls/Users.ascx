﻿<%@ control language="C#" autoeventwireup="true" inherits="controls_Users, App_Web_nsj5qp0s" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table>
    <tr>
        <td>
            First Name:
        </td>
        <td>
            <asp:TextBox ID="txtFirst" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'
                ReadOnly="true"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                ControlToValidate="txtFirst"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Last Name:
        </td>
        <td>
            <asp:TextBox ID="txtLast" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'
                ReadOnly="true"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                ControlToValidate="txtLast"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            TWWID:
        </td>
        <td>
            <asp:TextBox ID="txtTwwid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TWWID") %>'
                AutoPostBack="true" OnTextChanged="txtTwwid_TextChanged" ReadOnly="true"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                ControlToValidate="txtTwwid"></asp:RequiredFieldValidator>
            <div runat="server" id="UserIsEmployee" style="color: red">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            Transcom Email (optional):
        </td>
        <td>
            <asp:TextBox ID="txtEmail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Mobile No.
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox1" runat="server" SelectionOnFocus="SelectAll"
                Text='<%# DataBinder.Eval(Container, "DataItem.MobileNo") %>'>
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Region:
        </td>
        <td>
            <asp:Label ID="lblRegion" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.region_desc") %>'></asp:Label>
            <%--<telerik:RadComboBox ID="rcbRegion" runat="server" DataSourceID="SqlDataSource1"
                DataTextField="description" DataValueField="id" AppendDataBoundItems="true" AutoPostBack="true"
                CausesValidation="false" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.RegionID") %>' Enabled="false">
                <Items>
                    <telerik:RadComboBoxItem Text="" Value="" />
                </Items>
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT * FROM [Regions] ORDER BY [Description]"></asp:SqlDataSource>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                ControlToValidate="rcbRegion"></asp:RequiredFieldValidator>--%>
        </td>
    </tr>
    <tr>
        <td>
            Country:
        </td>
        <td>
            <asp:Label ID="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.country_desc") %>'></asp:Label>
            <%-- <telerik:RadComboBox ID="rcbCountry" runat="server" DataSourceID="SqlDataSource2"
                DataTextField="country" DataValueField="id" AppendDataBoundItems="true" AutoPostBack="true"
                CausesValidation="false" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.CountryID") %>' Enabled ="true">
                <Items>
                    <telerik:RadComboBoxItem Text="" Value="" />
                </Items>
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT * FROM [Countries] WHERE (([regionID] = @region_ID)) ORDER BY [Country]">
                <SelectParameters>
                    <asp:ControlParameter ControlID="rcbRegion" Name="region_ID" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                ControlToValidate="rcbCountry"></asp:RequiredFieldValidator>--%>
        </td>
    </tr>
    <tr>
        <td>
            Site:
        </td>
        <td>
            <asp:Label ID="lblSite" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.site_desc") %>'></asp:Label>
            <%-- <telerik:RadComboBox ID="rcbSite" runat="server" DataSourceID="SqlDataSource3" DataTextField="description"
                DataValueField="id" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.SiteID") %>'
                Enabled="false">
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT * FROM [Locations] WHERE (([countryID] = @country_ID)) ORDER BY [description]">
                <SelectParameters>
                    <asp:ControlParameter ControlID="rcbCountry" Name="country_ID" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                ControlToValidate="rcbSite"></asp:RequiredFieldValidator>--%>
        </td>
    </tr>
    <tr>
        <td>
            Reporting Manager/Supervisor:
        </td>
        <td>
            <asp:Label ID="lblSupervisor" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.supervisorname") %>'></asp:Label>
            <%--            <telerik:RadComboBox ID="RadComboBox1" runat="server" DataSourceID="SqlDataSource6"
                DataTextField="supervisorname" DataValueField="twwid" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.supervisorid") %>'
                Enabled="true">
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="select * from dbo.vw_AllActiveSupervisors
order by supervisorname"></asp:SqlDataSource>
            --%>
        </td>
    </tr>
    <tr>
        <td>
            Department:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox2" runat="server" DataSourceID="SqlDataSource7"
                DataTextField="description" DataValueField="id" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.departmentid") %>'>
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT [id], [description] FROM [refDepartments] WHERE ([active] = @active) ORDER BY [description]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="True" Name="active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Client:
        </td>
        <td>
            <asp:Label ID="lblClient" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.client_desc") %>'></asp:Label>
            <%-- <telerik:RadComboBox ID="rcbClient" runat="server" DataSourceID="SqlDataSource4"
                DataTextField="description" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ClientID") %>'>
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT * FROM [Clients] WHERE ([countryid] = @countryid OR [countryid] is null) ORDER BY [description]">
                <SelectParameters>
                    <asp:ControlParameter ControlID="rcbCountry" Name="countryid" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                ControlToValidate="rcbClient"></asp:RequiredFieldValidator>--%>
        </td>
    </tr>
    <tr>
        <td>
            Type of Support:
        </td>
        <td>
            <asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.supporttype") %>'
                Visible="false"></asp:TextBox>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" DataSourceID="SqlDataSource8"
                DataTextField="description" DataValueField="id" RepeatColumns="2" RepeatDirection="Horizontal">
            </asp:CheckBoxList>
            <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT [id], [description] FROM [refSupportType] WHERE ([active] = @active) ORDER BY [description]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="True" Name="active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Primary Language:
        </td>
        <td>
            <telerik:RadComboBox runat="server" ID="cbLanguage" DataTextField="Language" DataValueField="Id"
                DataSourceID="DataSource_Language" MaxHeight="250" DropDownAutoWidth="Enabled"
                SelectedValue='<%# DataBinder.Eval(Container, "DataItem.LanguageID") %>' />
            <asp:SqlDataSource ID="DataSource_Language" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT [Id], [Language] FROM [refLanguage] WHERE ([Active] = @Active) ORDER BY [Language]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="true" Name="Active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Current Specialization and Skills:
        </td>
        <td>
            <telerik:RadTextBox runat="server" ID="txtSpecializationSkills" Width="300" Height="50"
                SelectionOnFocus="SelectAll" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.Specialization_Skills") %>' />
        </td>
    </tr>
    <tr>
        <td>
            Education Level:
        </td>
        <td>
            <telerik:RadComboBox runat="server" ID="cbEducLevel" DataValueField="Id" DataTextField="Title"
                DataSourceID="DataSourse_EducLevel" MaxHeight="250" DropDownAutoWidth="Enabled"
                SelectedValue='<%# DataBinder.Eval(Container, "DataItem.EducationLevelID") %>' />
            <asp:SqlDataSource ID="DataSourse_EducLevel" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT [Id], [Title] FROM [refEducationLevel] WHERE ([Active] = @Active) ORDER BY [Id]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="true" Name="Active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Major Subject:
        </td>
        <td>
            <telerik:RadTextBox runat="server" ID="txtCourseMajor" Width="120" SelectionOnFocus="SelectAll"
                Text='<%# DataBinder.Eval(Container, "DataItem.CourseMajor") %>' />
        </td>
    </tr>
    <tr>
        <td>
            Additional Courses and Training Attended (Internal & External):
        </td>
        <td>
            <telerik:RadTextBox runat="server" ID="txtCoursesTrainings" Width="300" Height="50"
                SelectionOnFocus="SelectAll" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.AdditionalCourses_Trainings") %>' />
        </td>
    </tr>
    <tr>
        <td>
            I am interested in pursuing the following career path in Transcom:
        </td>
        <td>
            <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CareerPathID") %>'
                Visible="false"></asp:Label>
            <asp:RadioButtonList runat="server" ID="radioBtnCareerPath" RepeatDirection="Horizontal">
                <asp:ListItem Text="Training" Value="1"></asp:ListItem>
                <asp:ListItem Text="Quality" Value="2"></asp:ListItem>
                <asp:ListItem Text="Operational Management" Value="3"></asp:ListItem>
                <asp:ListItem Text="Human Resources" Value="4"></asp:ListItem>
                <asp:ListItem Text="IT" Value="5"></asp:ListItem>
                <asp:ListItem Text="Project Management" Value="6"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td>
            Please tell me what courses you are interested in taking for your professional growth
            and development
        </td>
        <td>
            <telerik:RadTextBox runat="server" ID="txtCoursesInterested" Width="300" Height="50"
                SelectionOnFocus="SelectAll" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.CourseInterests") %>' />
        </td>
    </tr>
    <tr>
        <td>
            Upload Photograph:
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            I am interested in receiving updates via:
        </td>
        <td>
            <asp:TextBox ID="TextBox2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UpdateVia") %>'
                Visible="false"></asp:TextBox>
            <asp:CheckBoxList ID="radioBtnReceiveUpdates" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">Email</asp:ListItem>
                <asp:ListItem Value="2">Mobile</asp:ListItem>
                <asp:ListItem Value="3">Post</asp:ListItem>
            </asp:CheckBoxList>
        </td>
    </tr>
    <tr>
        <td>
            Role:
        </td>
        <td>
            <telerik:RadComboBox ID="rcbRole" runat="server" DataSourceID="SqlDataSource5" DataTextField="role_desc"
                DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.RoleID") %>'>
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT * FROM [refRoles] WHERE ([active] = @active) ORDER BY [role_desc]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="True" Name="active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Active:
        </td>
        <td>
            <asp:CheckBox ID="chkActive" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Active") %>' />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Reset password?" Visible='<%# !(DataBinder.Eval(Container,"DataItem") is Telerik.Web.UI.GridInsertionObject) %>'></asp:Label>
        </td>
        <td>
            <asp:CheckBox ID="CheckBox1" runat="server" Visible='<%# !(DataBinder.Eval(Container,"DataItem") is Telerik.Web.UI.GridInsertionObject) %>' />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="cmdInsert" runat="server" Text="Insert" CommandName="PerformInsert"
                Visible='<%# DataBinder.Eval(Container,"DataItem") is Telerik.Web.UI.GridInsertionObject %>' />
            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CommandName="Update" Visible='<%# !(DataBinder.Eval(Container,"DataItem") is Telerik.Web.UI.GridInsertionObject) %>' />
        </td>
        <td>
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CausesValidation="false"
                CommandName="Cancel" />
        </td>
    </tr>
</table>
