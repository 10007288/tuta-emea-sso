<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="registration, App_Web_yuclf4ni" %>

<%@ Register TagPrefix="wus" TagName="userRightsControl" Src="~/controls/userRightsControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="Panel1" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="4" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMassUserCreation" runat="server" Text="Mass User Creation" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Panel ID="pnlOption" runat="server" HorizontalAlign="center" CssClass="panelStandard"
                                    Width="100%">
                                    <table cellpadding="10">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCreateAdmin" runat="server" Text="Create NuSkill Administrator"
                                                    CssClass="buttons" Width="200px" OnClick="btnCreateAdmin_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCreateUser" runat="server" Text="Create Examinee" CssClass="buttons"
                                                    Width="200px" OnClick="btnCreateUser_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlCreateAdmin" runat="server" HorizontalAlign="center" Width="100%"
                                    CssClass="panelStandard">
                                    <table cellpadding="2">
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblCimNumber" runat="server" Text="Enter Cim#:" />
                                                <asp:TextBox ID="txtCimNumber" runat="server" />
                                                <asp:Button ID="btnFind" runat="server" Text=" ? " CssClass="buttons" ToolTip="Find user"
                                                    Width="20px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblUserFound" runat="server" Font-Size="12px" ForeColor="#666666" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlRights" runat="server" HorizontalAlign="center">
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <wus:userRightsControl ID="usrRightsControl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="btnRightChangeOk" runat="server" Text="OK" CssClass="buttons" />
                                                </td>
                                                <td align="right">
                                                    <asp:Button ID="btnRightChangeCancel" runat="server" Text="Cancel" CssClass="buttons" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:Panel>
                                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" Width="100%" CssClass="panelStandard">
                                    <table cellpadding="5" style="font-family: Arial">
                                        <tr>
                                            <td colspan="3" align="center">
                                                <asp:Label ID="lblRequired" runat="server" Text="*All fields are required." CssClass="warning" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <asp:CustomValidator ID="cvExists" runat="server" ErrorMessage="User already exists."
                                                    CssClass="errorline" Display="dynamic" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblUsername" runat="server" Text="Username:" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtUsername" runat="server" MaxLength="20" />
                                            </td>
                                            <td align="left">
                                                <asp:CustomValidator ID="cvUsername" runat="server" ErrorMessage="Username is required."
                                                    CssClass="errorline" Display="static" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblPassword" runat="server" Text="Password:" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" />
                                            </td>
                                            <td align="left">
                                                <asp:CustomValidator ID="cvPassword" runat="server" ErrorMessage="Password is required."
                                                    CssClass="errorline" Display="static" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblFirstName" runat="server" Text="First Name:" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" />
                                            </td>
                                            <td align="left">
                                                <asp:CustomValidator ID="cvFirstName" runat="server" ErrorMessage="First Name is required."
                                                    CssClass="errorline" Display="static" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblLastName" runat="server" Text="Last Name:" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" />
                                            </td>
                                            <td align="left">
                                                <asp:CustomValidator ID="cvLastName" runat="server" ErrorMessage="Last Name is required."
                                                    CssClass="errorline" Display="Static" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblPhoneNumber" runat="server" Text="Phone Number:" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhoneNumber" runat="server" MaxLength="20" />
                                            </td>
                                            <td align="left">
                                                <asp:CustomValidator ID="cvPhoneNumber" runat="server" ErrorMessage="Phone Number is required."
                                                    CssClass="errorline" Display="static" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblLocation" runat="server" Text="Location:" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLocation" runat="server" MaxLength="50" />
                                            </td>
                                            <td align="left">
                                                <asp:CustomValidator ID="cvLocation" runat="server" ErrorMessage="Location is required."
                                                    CssClass="errorline" Display="static" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td align="right">
                                                <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="buttons" OnClick="btnRegister_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
