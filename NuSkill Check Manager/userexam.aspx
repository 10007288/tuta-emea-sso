<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="userexam.aspx.cs" Inherits="userexam" Title="Transcom University Testing Admin" %>

<%@ Register TagPrefix="wus" TagName="singleitemcontrol" Src="~/controls/examquestioncontrol.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server">
        <table cellpadding="10" cellspacing="0" style="font-family: Arial">
            <tr>
                <td valign="middle">
                    <asp:Label ID="lblUserID" runat="server" Text="User ID:" />
                </td>
                <td valign="middle">
                    <asp:Label ID="lblUserIDVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <asp:Label ID="lblOffice" runat="server" Text="Office:" />
                </td>
                <td valign="middle">
                    <asp:Label ID="lblOfficeVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <asp:Label ID="lblDateTaken" runat="server" Text="Date Taken:" />
                </td>
                <td valign="middle">
                    <asp:Label ID="lblDateTakenVal" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDateFinished" runat="server" Text="Date Finished:" />
                </td>
                <td valign="middle">
                    <asp:Label ID="lblDateFinishedVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblScore" runat="server" Text="Score:" />
                </td>
                <td valign="middle">
                    <asp:Label ID="lblScoreVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPassed" runat="server" Text="Status:" />
                </td>
                <td valign="middle">
                    <asp:Image ID="imgPassedVal" runat="server" />
                    <asp:Label ID="lblStatusVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblItems" runat="server" Text="Items" />
                </td>
                <td>
                    <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                        AllowSorting="false" CellPadding="5" Font-Size="12" BorderColor="black">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestionID" runat="server" Text='<%#Bind("QuestionnaireID") %>'
                                        Visible="false" />
                                    <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                    <asp:LinkButton ID="lnkQuestion" runat="server" Text='<%#Bind("Question") %>' CssClass="linkButtons"
                                        CommandName="Question" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkViewSingleQuestion" runat="server" Text="View User Answer"
                                        CssClass="linkButtons" CommandName="Answer" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkGradeEssay" runat="server" Text="GradeEssay" CssClass="linkButtons"
                                        CommandName="Grade" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpePopup" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidPopup" PopupControlID="pnlPopup" />
    <asp:HiddenField ID="hidPopup" runat="server" />
    <asp:Panel ID="pnlPopup" runat="server" BorderColor="black" BorderStyle="solid" CssClass="modalPopup">
        <wus:singleitemcontrol ID="wusSingleItemControl" runat="server" />
    </asp:Panel>
</asp:Content>
