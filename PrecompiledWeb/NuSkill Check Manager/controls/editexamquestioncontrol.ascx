<%@ control language="C#" autoeventwireup="true" inherits="controls_editexamquestioncontrol, App_Web_nsj5qp0s" %>
<asp:Panel ID="pnlExamQuestion" runat="server" Width="900px" HorizontalAlign="center"
    ForeColor="black">
    <%--border-color: #4682b4; background-color: White;">--%>
    <table cellpadding="5" width="100%" cellspacing="0" style="height: 400px; vertical-align: text-top">
        <tr>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table width="100%" cellpadding="5" cellspacing="0" style="vertical-align: middle;
                    font-family: Arial; font-size: 12px;">
                    <tr>
                        <td align="left" style="vertical-align: middle; font-size: 12px; font-family: Arial;"
                            colspan="3">
                            <asp:Label ID="lblQuestion" runat="server" Text="Enter question/instructions here:" /><br />
                            <asp:TextBox ID="txtQuestion" runat="server" Width="600px" MaxLength="2000" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="vertical-align:middle;font-size:12px;font-family:Arial" colspan="3">
                            <asp:Label ID="lblHyperlink" runat="server" Text="Enter a URL for the question here, if applicable:" /><br />
                            <asp:TextBox ID="txtHyperlink" runat="server" Width="600px" MaxLength="2000" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="vsmItems" runat="server" DisplayMode="List" ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoQuestion" runat="server" ErrorMessage="Provide a question."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoOptionsMultipleChoice" runat="server" ErrorMessage="Provide at least two choices and at least one correct answer."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvDuplicateMultipleChoice" runat="server" ErrorMessage="Make sure there are no duplicate answers."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvItemSkipMultipleChoice" runat="server" ErrorMessage="Make sure there are no skipped spaces for the answers."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvCorrectBlankMultipleChoice" runat="server" ErrorMessage="Make sure no blanks are marked as correct answers."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoOptionsTrueOrFalse" runat="server" ErrorMessage="Provide the correct answer."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoOptionsYesOrNo" runat="server" ErrorMessage="Provide the correct answer."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoOptionsSequencing" runat="server" ErrorMessage="Provide at least two choices and their proper order."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvDuplicateSequencing" runat="server" ErrorMessage="Make sure there are no duplicate answers."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvOptionMismatchSequencing" runat="server" ErrorMessage="Make sure the answers in the Random Order and the Correct Order are the same."
                                ValidationGroup="ValItems" Display="None" />
                            <asp:CustomValidator ID="cvItemSkipSequencing" runat="server" ErrorMessage="Make sure there are no skipped spaces for both columns."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvCountMismatchSequencing" runat="server" ErrorMessage="Provide the same number of items in the Random Order and the Correct Order columns."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvItemSkipMatching" runat="server" ErrorMessage="Make sure there are no skipped spaces for all columns."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoOptionsMatching" runat="server" ErrorMessage="Provide at least two choices on the right column."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoItemsMatching" runat="server" ErrorMessage="Provide at least two items on the left column."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvInvalidChoiceMatching" runat="server" ErrorMessage="Make sure all answers are valid (no blanks)."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvDuplicateMatching" runat="server" ErrorMessage="Make sure there are no duplicate items in each column."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoOptionsHotspot" runat="server" ErrorMessage="Provide at least two choices and at least one correct answer."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoScoreEssay" runat="server" ErrorMessage="Provide the maximum score for the essay."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoChoiceFillBlanks" runat="server" ErrorMessage="Choose a non-blank correct answer."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvNoSkipFillBlanks" runat="server" ErrorMessage="Make sure there are no skipped spaces for the answers."
                                ValidationGroup="ValItems" Display="none" />
                            <asp:CustomValidator ID="cvDuplicateFillBlanks" runat="server" ErrorMessage="Make sure there are no duplicate answers."
                                ValidationGroup="ValItems" Display="none" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblItemTypes" runat="server" Text="Choose the item type:" /><br />
                            <asp:DropDownList ID="ddlItemtypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlItemtypes_SelectedIndexChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Panel ID="pnlMultipleChoice" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5" cellspacing="0">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblInsertAnswers" runat="server" Text="Enter answers here(up to a maximum of 10), then mark the correct ones." />
                                        </td>
                                    </tr>
                                    <tr id="trCheck1" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice1" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice1" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck2" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice2" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice2" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck3" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice3" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice3" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck4" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice4" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice4" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck5" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice5" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice5" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck6" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice6" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice6" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck7" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice7" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice7" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck8" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice8" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice8" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck9" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice9" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice9" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                    <tr id="trCheck10" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtChoice10" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkChoice10" runat="server" Text="Correct Answer" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlTrueOrFalse" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTrueOrFalseCorrect" runat="server" Text="Please mark the correct answer:" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdoTrue" runat="server" Text="True" TextAlign="right" GroupName="torf" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdoFalse" runat="server" Text="False" TextAlign="right" GroupName="torf" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlYesOrno" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblYesOrNoCorrect" runat="server" Text="Please mark the correct answer:" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdoYes" runat="server" Text="Yes" TextAlign="right" GroupName="yorn" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdoNo" runat="server" Text="No" TextAlign="right" GroupName="yorn" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlSequencing" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblSequencing" runat="server" Text="Enter the answers in the order they will appear in the test in the left column. Enter the correct order in the right column." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblRandomOrder" runat="server" Text="Randomized Order:" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCorrectOrder" runat="server" Text="Correct Order:" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox1" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox1" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox2" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox2" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox3" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox3" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox4" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox4" runat="server" Width="310px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox5" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox5" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox6" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox6" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox7" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox7" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox8" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox8" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox9" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox9" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtItemTextbox10" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnswerTextbox10" runat="server" Width="310px" MaxLength="1000" />
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlFillInTheBlanks" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label1" runat="server" Text="Enter answers here(up to a maximum of 10), then mark the correct answer." />
                                        </td>
                                    </tr>
                                    <tr id="tr1" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill1" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill1" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr2" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill2" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill2" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr3" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill3" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill3" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr4" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill4" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill4" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr5" runat="server">
                                        <td style="height: 34px">
                                            <asp:TextBox ID="txtFill5" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td style="height: 34px">
                                            <asp:RadioButton ID="rdoFill5" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr6" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill6" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill6" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr7" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill7" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill7" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr8" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill8" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill8" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr9" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill9" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill9" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                    <tr id="tr10" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtFill10" runat="server" Width="350px" MaxLength="1000" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdoFill10" runat="server" Text="Correct Answer" GroupName="FillInTheBlanks" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlEssay" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblEssayScore" runat="server" Text="Indicate the maximum score for this essay:" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtEssayScore" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlMatchingType" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5">
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="Label2" runat="server" Text="Enter the items in the order they will appear in the test in the second column. Enter the list of choices in the right column. Enter the corresponding letter of the correct choice in the left column." />
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch1" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch1" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch1" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem1" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter1" runat="server" Text="A:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice1" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch2" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch2" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch2" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem2" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter2" runat="server" Text="B:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice2" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch3" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch3" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch3" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem3" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter3" runat="server" Text="C:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice3" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch4" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch4" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch4" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem4" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter4" runat="server" Text="D:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice4" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch5" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch5" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch5" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem5" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter5" runat="server" Text="E:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice5" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch6" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch6" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch6" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem6" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter6" runat="server" Text="F:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice6" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch7" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch7" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch7" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem7" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter7" runat="server" Text="G:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice7" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch8" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch8" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch8" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem8" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter8" runat="server" Text="H:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice8" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch9" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch9" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch9" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem9" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter9" runat="server" Text="I:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice9" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCorrectMatch10" runat="server" Width="20px" MaxLength="1" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch10" runat="server" FilterMode="validchars"
                                                ValidChars="ABCDEFGHIJabcdefghij" TargetControlID="txtCorrectMatch10" FilterType="Custom" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMatchItem10" runat="server" Width="250px" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblMatchLetter10" runat="server" Text="J:" Width="20px" />
                                            <asp:TextBox ID="txtMatchChoice10" runat="server" Width="250px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlHotspot" runat="server" Width="100%" Visible="false">
                                <table cellpadding="5" width="100%">
                                    <tr>
                                        <td colspan="5">
                                            <asp:Label ID="lblHotspot" runat="server" Text="Choose up to ten images and mark the correct answers by clicking on them." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <asp:FileUpload ID="fupHotspot" runat="server" /><br />
                                            <asp:Button CssClass="buttons" ID="btnAdd" runat="server" Text="Add" Width="60px"
                                                OnClick="btnAdd_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <asp:CustomValidator ID="cvImageExists" runat="server" ErrorMessage="File already exists."
                                                Display="dynamic" />
                                            <asp:CustomValidator ID="cvTooMany" runat="server" ErrorMessage="There are already ten images in this question."
                                                Display="dynamic" />
                                            <asp:CustomValidator ID="cvNoImage" runat="server" ErrorMessage="Browse for an image before clicking the Add button."
                                                Display="dynamic" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="imgCell1" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice1" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove1" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                        <td id="imgCell2" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice2" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove2" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                        <td id="imgCell3" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice3" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove3" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                        <td id="imgCell4" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice4" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove4" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                        <td id="imgCell5" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice5" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove5" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="imgCell6" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice6" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove6" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                        <td id="imgCell7" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice7" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove7" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                        <td id="imgCell8" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice8" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove8" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                        <td id="imgCell9" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice9" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove9" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                        <td id="imgCell10" runat="server" align="center" valign="middle" visible="false">
                                            <asp:ImageButton ID="imgChoice10" runat="server" OnClick="imgClick" />
                                            <br />
                                            <asp:Button CssClass="buttons" ID="btnRemove10" runat="server" Text="Remove" Width="100px"
                                                OnClick="RemoveImage" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" valign="middle" style="font-family: Arial;" colspan="3">
                <asp:Button CssClass="buttons" ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
