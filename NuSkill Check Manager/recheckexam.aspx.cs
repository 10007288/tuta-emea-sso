using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using System.Data.SqlClient;

public partial class recheckexam : System.Web.UI.Page
{
    string _Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserRole();

        if (_Role != "Admin")
        {
            Response.Redirect("Home.aspx");
        }

        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            GetUserRole();

            if (_Role != "Admin")
            {
                Response.Redirect("Home.aspx");
            }

            this.PopulateDropDowns();
        }
    }

    protected void GetUserRole()
    {
        //Check user role of the current user
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }

    protected void btnRegrade_Click(object sender, EventArgs e)
    {
        this.gvTestsTaken.DataSource = TestTaken.SelectByExam(Convert.ToInt32(this.ddlExams.Text));
        this.gvTestsTaken.DataBind();
        this.mpeTestsTaken.Show();
    }

    protected void PopulateDropDowns()
    {
        this.ddlCategory.DataSource = NonCimCampaign.SelectParents();
        this.ddlCategory.DataBind();
        this.PopulateSubCategory();
        this.PopulateExams();
    }

    protected void PopulateSubCategory()
    {
        this.ddlSubcategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlCategory.Text), true);
        this.ddlSubcategory.DataBind();
        this.PopulateExams();
    }

    protected void PopulateExams()
    {
        this.ddlExams.DataSource = TestCategory.SelectByAccountCampaign(Convert.ToInt32(this.ddlCategory.Text), Convert.ToInt32(this.ddlSubcategory.Text), true, true);
        this.ddlExams.DataBind();
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.PopulateSubCategory();
    }

    protected void ddlSubcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.PopulateExams();
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        try
        {
            //TestTaken.UpdateScores(Convert.ToInt32(this.ddlExams.Text));
            //Connection.ExecuteSP("pr_testtaken_sav_updatescores",
            //Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));

            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["NUSkillCheck"].ConnectionString))
            {
                cn.Open();

                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("pr_testtaken_sav_updatescores", cn))
                {
                    cmd.CommandTimeout = 1800; //30 mins
                    cmd.Parameters.AddWithValue("@TestCategoryID", this.ddlExams.Text);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                cn.Close();
            }


            this.lblResult.Text = "Rechecking successful.";
            this.mpeResult.Show();
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "recheckexam.aspx", "btnOK_Click", ex.Message);
            this.lblResult.Text = "Rechecking failed.";
            this.mpeResult.Show();
        }
    }
}
