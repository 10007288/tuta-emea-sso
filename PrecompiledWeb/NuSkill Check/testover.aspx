<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_frontend_site.master" autoeventwireup="true" inherits="testover, App_Web_o5nevnzg" title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="10" cellspacing="0">
        <tr style="height:500px">
            <td align="center" style="vertical-align: middle; font-size: large; font-family: Arial;" valign="middle">
                <table width="90%" cellpadding="5" style="vertical-align: middle; font-family: Arial;
                    font-size: small;">
                    <tr>
                        <td align="center" style="padding: 30px 10px 10px 10px">
                            <asp:Label ID="lblNoMoreTime" runat="server" Text="" />
                            <br /><br /><br />
                            <asp:Button ID="btnContinue" runat="server" Text="" OnClick="btnContinue_Click" CssClass="buttons" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
