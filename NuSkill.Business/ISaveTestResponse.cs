using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace NuSkill.Business
{
    public interface ISaveTestResponse
    {
        int SaveTestResponseID { get; set; }
        string UserID { get; set; }
        IQuestionnaire Questionnaire { get; set; }
        string Response1 { get; set; }
        string Response2 { get; set; }
        string Response3 { get; set; }
        string Response4 { get; set; }
        string Response5 { get; set; }
        string Response6 { get; set; }
        string Response7 { get; set; }
        string Response8 { get; set; }
        string Response9 { get; set; }
        string Response10 { get; set; }
        string EssayResponse { get; set; }

        void Insert();
        void UpdateAnswers();
        ISaveTestResponse Retrieve(string userID, int testTakenID, int questionnaireID);
        ISaveTestResponse[] CheckPendingEssays(string userID, int testTakenID);
        void Delete();
        DataSet GetEssays();
        ISaveTestResponse GetByID(int saveTestResponseID);
        ISaveTestResponse[] GetByTestTaken(int testTakenID);
        DataSet GetMultipleEssays(int testTakenID);
        DataSet GetPendingExamAnswerStatus(int testTakenID);
    }
}
