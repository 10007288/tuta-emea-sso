<%@ page language="C#" autoeventwireup="true" inherits="ViewReports, App_Web_yuclf4ni" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="srs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transcom University Testing Admin</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableScriptLocalization="True">
    </telerik:RadScriptManager>
    <%--<asp:DropDownList ID="ddlReports" runat="server" DataTextField="DisplayName" DataValueField="ReportURL" />
        <asp:Button ID="btnView" runat="server" Text="View" CssClass="buttons" OnClick="btnView_Click" />
        <br />
        <br />--%>
    <%--<srs:ReportViewer ID="ReportViewer1" runat="server" ShowBackButton="false" ShowPrintButton="false"
            ShowZoomControl="false" ShowFindControls="false" ShowExportControls="true" Width="100%"
            Height="600px" ProcessingMode="Remote">
        </srs:ReportViewer>--%>
    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Correct Answer Percentage Per Question By Exam
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox1" runat="server" DataSourceID="SqlDataSource1"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox1_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox2" runat="server" DataSourceID="SqlDataSource2"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox2_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox1" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox3" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker1" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker2" runat="server">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate1" runat="server" Text="Generate" OnClick="cmdGenerate1_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource3" GridLines="None"
            AutoGenerateColumns="False" OnItemCreated="RadGrid1_ItemCreated" ExportSettings-ExportOnlyData="true"
            ExportSettings-IgnorePaging="true">
            <MasterTableView DataSourceID="SqlDataSource3" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="testcategoryid" HeaderText="testcategoryid" SortExpression="testcategoryid"
                        UniqueName="testcategoryid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testname" HeaderText="Test" SortExpression="testname"
                        UniqueName="testname">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="questionnaireid" DataType="System.Int32" HeaderText="questionnaireid"
                        SortExpression="questionnaireid" UniqueName="questionnaireid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="question" HeaderText="Question" SortExpression="question"
                        UniqueName="question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CorrectCount" DataType="System.Double" HeaderText="Correct Count"
                        ReadOnly="True" SortExpression="CorrectCount" UniqueName="CorrectCount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AnsweredCount" DataType="System.Double" HeaderText="Answered Count"
                        SortExpression="AnsweredCount" UniqueName="AnsweredCount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="correct" HeaderText="Correct" ReadOnly="True"
                        SortExpression="correct" UniqueName="correct">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportCorrectAnswerPercentagePerQuestioByExam" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox3" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker1" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker2" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Answers by Employee by Test Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox4" runat="server" DataSourceID="SqlDataSource4"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox4_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox5" runat="server" DataSourceID="SqlDataSource5"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox5_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox4" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox6" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker3" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker4" runat="server">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate2" runat="server" Text="Generate" OnClick="cmdGenerate2_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid2" runat="server" DataSourceID="SqlDataSource6" GridLines="None"
            AutoGenerateColumns="False" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid2_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource6" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="testcategoryid" DataType="System.Int32" HeaderText="Test ID"
                        ReadOnly="True" SortExpression="testcategoryid" UniqueName="testcategoryid">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testname" HeaderText="Test" SortExpression="testname"
                        UniqueName="testname" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testtakenid" DataType="System.Int32" HeaderText="Test Taken ID"
                        ReadOnly="True" SortExpression="testtakenid" UniqueName="testtakenid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="userid" HeaderText="Employee" ReadOnly="True"
                        SortExpression="userid" UniqueName="userid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testresponseid" DataType="System.Int32" HeaderText="testresponseid"
                        ReadOnly="True" SortExpression="testresponseid" UniqueName="testresponseid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question" HeaderText="Question" ReadOnly="True"
                        SortExpression="Question" UniqueName="Question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status"
                        UniqueName="Status">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="responsevalue" HeaderText="Answer" ReadOnly="True"
                        SortExpression="responsevalue" UniqueName="responsevalue">
                    </telerik:GridBoundColumn>
                </Columns>
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldName="testtakenid" HeaderText="Test Taken ID" />
                            <telerik:GridGroupByField FieldName="userid" HeaderText="Employee" />
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="userid" />
                            <telerik:GridGroupByField FieldName="testtakenid" />
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAnswerByEmployeeByTestReportNew" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox6" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker3" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker4" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel3" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Correct Answer Percentage Per Question By Expired Exam
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox7" runat="server" DataSourceID="SqlDataSource7"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox7_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox8" runat="server" DataSourceID="SqlDataSource8"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox8_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox7" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox9" runat="server" DataSourceID="SqlDataSource10"
                        DataTextField="testname" DataValueField="testcategoryid">
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource10" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="SELECT DISTINCT
cast(testcategoryid as varchar) as testcategoryid,  
RTRIM(LTRIM(testname)) as 'testname', 
testname as 'title' 
FROM
tbl_testing_testcategory 
WHERE
EndDate &lt;= GETDATE()
	AND CampaignID = @SubcategoryID OR (CampaignID = 0 AND @CategoryID = 220032)
ORDER BY
testname">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox8" Name="SubcategoryID" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="RadComboBox7" Name="CategoryID" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker5" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker6" runat="server">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate3" runat="server" Text="Generate" OnClick="cmdGenerate3_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource9"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid3_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource9" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="testcategoryid" HeaderText="testcategoryid" SortExpression="testcategoryid"
                        UniqueName="testcategoryid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testname" HeaderText="Test" SortExpression="testname"
                        UniqueName="testname">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="questionnaireid" DataType="System.Int32" HeaderText="questionnaireid"
                        SortExpression="questionnaireid" UniqueName="questionnaireid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="question" HeaderText="Question" SortExpression="question"
                        UniqueName="question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CorrectCount" DataType="System.Double" HeaderText="Correct Count"
                        ReadOnly="True" SortExpression="CorrectCount" UniqueName="CorrectCount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AnsweredCount" DataType="System.Double" HeaderText="Answered Count"
                        SortExpression="AnsweredCount" UniqueName="AnsweredCount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="correct" HeaderText="Correct" ReadOnly="True"
                        SortExpression="correct" UniqueName="correct">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource9" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportCorrectAnswerPercentagePerQuestionByExpiredExam" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox9" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker5" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker6" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel4" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Answer By Employee By Expired Test Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox10" runat="server" DataSourceID="SqlDataSource11"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox10_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource11" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox11" runat="server" DataSourceID="SqlDataSource12"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox11_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource12" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox10" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox12" runat="server" DataSourceID="SqlDataSource13"
                        DataTextField="testname" DataValueField="testcategoryid">
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource13" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="SELECT DISTINCT
cast(testcategoryid as varchar) as testcategoryid,  
RTRIM(LTRIM(testname)) as 'testname', 
testname as 'title' 
FROM
tbl_testing_testcategory 
WHERE
EndDate &lt;= GETDATE()
	AND CampaignID = @SubcategoryID OR (CampaignID = 0 AND @CategoryID = 220032)
ORDER BY
testname">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox11" Name="SubcategoryID" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="RadComboBox10" Name="CategoryID" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker7" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker8" runat="server">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate4" runat="server" Text="Generate" OnClick="cmdGenerate4_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid4" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource14"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid4_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource14" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="testcategoryid" DataType="System.Int32" HeaderText="testcategoryid"
                        ReadOnly="True" SortExpression="testcategoryid" UniqueName="testcategoryid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testname" HeaderText="Test" SortExpression="testname"
                        UniqueName="testname">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testtakenid" DataType="System.Int32" HeaderText="testtakenid"
                        ReadOnly="True" SortExpression="testtakenid" UniqueName="testtakenid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="userid" HeaderText="userid" ReadOnly="True" SortExpression="userid"
                        UniqueName="userid">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testresponseid" DataType="System.Int32" HeaderText="testresponseid"
                        ReadOnly="True" SortExpression="testresponseid" UniqueName="testresponseid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="responsevalue" HeaderText="Response" ReadOnly="True"
                        SortExpression="responsevalue" UniqueName="responsevalue">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status"
                        UniqueName="Status">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource14" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAnswerByEmployeeByExpiredTest" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox12" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker7" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker8" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel5" runat="server" Visible="false">
        <table>
            <tr>
                <td>
                    Exam Results By Test Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox13" runat="server" DataSourceID="SqlDataSource15"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox13_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource15" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox14" runat="server" DataSourceID="SqlDataSource16"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox14_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource16" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox13" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox15" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker9" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker10" runat="server">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate5" runat="server" Text="Generate" OnClick="cmdGenerate5_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid5" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource17"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid5_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource17" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="CIM" HeaderText="TWWID" SortExpression="CIM" UniqueName="CIM">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName"
                        UniqueName="FirstName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="TestName" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Score" HeaderText="Score" ReadOnly="True" SortExpression="Score"
                        UniqueName="Score">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Passed" HeaderText="Passed" ReadOnly="True" SortExpression="Passed"
                        UniqueName="Passed">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DateTaken" DataType="System.DateTime" HeaderText="DateTaken"
                        SortExpression="DateTaken" UniqueName="DateTaken">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Site" HeaderText="Site" SortExpression="Site"
                        UniqueName="Site">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource17" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportExamResultsByTest" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox15" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker9" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker10" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel6" runat="server" Visible="false">
    </asp:Panel>
    <asp:Panel ID="Panel7" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Exam Results By Expired Test Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox16" runat="server" DataSourceID="SqlDataSource18"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox16_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource18" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox17" runat="server" DataSourceID="SqlDataSource19"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox17_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource19" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox16" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox18" runat="server" DataSourceID="SqlDataSource20"
                        DataTextField="testname" DataValueField="testcategoryid">
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource20" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="SELECT DISTINCT
cast(testcategoryid as varchar) as testcategoryid,  
RTRIM(LTRIM(testname)) as 'testname', 
testname as 'title' 
FROM
tbl_testing_testcategory 
WHERE
EndDate &lt;= GETDATE()
	AND CampaignID = @SubcategoryID OR (CampaignID = 0 AND @CategoryID = 220032)
ORDER BY
testname">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox17" Name="SubcategoryID" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="RadComboBox16" Name="CategoryID" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker11" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker12" runat="server">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate6" runat="server" Text="Generate" OnClick="cmdGenerate6_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid6" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource21"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid6_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource21" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="CIM" HeaderText="CIM" SortExpression="CIM" UniqueName="CIM">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                        UniqueName="FirstName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="Test" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Score" HeaderText="Score" ReadOnly="True" SortExpression="Score"
                        UniqueName="Score">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Passed" HeaderText="Passed" ReadOnly="True" SortExpression="Passed"
                        UniqueName="Passed">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DateTaken" DataType="System.DateTime" HeaderText="Date Taken"
                        SortExpression="DateTaken" UniqueName="DateTaken">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Site" HeaderText="Site" SortExpression="Site"
                        UniqueName="Site">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource21" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportExamResultsByExpiredTest" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox18" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker8" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker9" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel8" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Average Score By Exam
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox19" runat="server" DataSourceID="SqlDataSource22"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox19_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource22" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox20" runat="server" DataSourceID="SqlDataSource23"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox20_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource23" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox19" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox21" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker13" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker14" runat="server">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate7" runat="server" Text="Generate" OnClick="cmdGenerate7_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid7" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource24"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid7_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource24" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="Test" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="average" DataType="System.Double" HeaderText="Average"
                        ReadOnly="True" SortExpression="average" UniqueName="average">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource24" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAverageScoreByExam" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox21" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker13" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker14" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel9" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Answer Per Question Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox22" runat="server" DataSourceID="SqlDataSource25"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox22_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource25" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox23" runat="server" DataSourceID="SqlDataSource26"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox23_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource26" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox22" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox24" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate8" runat="server" Text="Generate" OnClick="cmdGenerate8_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid8" runat="server" DataSourceID="SqlDataSource27" GridLines="None"
            AutoGenerateColumns="false" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid8_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource27" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="TestCategoryID" HeaderText="TestCategoryID" SortExpression="TestCategoryID"
                        UniqueName="TestCategoryID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="Test" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question" HeaderText="Question" SortExpression="Question"
                        UniqueName="Question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice1" HeaderText="Choice1" SortExpression="Choice1"
                        UniqueName="Choice1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice2" HeaderText="Choice2" SortExpression="Choice2"
                        UniqueName="Choice2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice3" HeaderText="Choice3" SortExpression="Choice3"
                        UniqueName="Choice3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice4" HeaderText="Choice4" SortExpression="Choice4"
                        UniqueName="Choice4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice5" HeaderText="Choice5" SortExpression="Choice5"
                        UniqueName="Choice5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans1" HeaderText="Ans1" SortExpression="Ans1"
                        UniqueName="Ans1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans2" HeaderText="Ans2" SortExpression="Ans2"
                        UniqueName="Ans2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans3" HeaderText="Ans3" SortExpression="Ans3"
                        UniqueName="Ans3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans4" HeaderText="Ans4" SortExpression="Ans4"
                        UniqueName="Ans4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans5" HeaderText="Ans5" SortExpression="Ans5"
                        UniqueName="Ans5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice1%" HeaderText="Choice1%" ReadOnly="True"
                        SortExpression="Choice1%" UniqueName="Choice1%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice2%" HeaderText="Choice2%" ReadOnly="True"
                        SortExpression="Choice2%" UniqueName="Choice2%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice3%" HeaderText="Choice3%" ReadOnly="True"
                        SortExpression="Choice3%" UniqueName="Choice3%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice4%" HeaderText="Choice4%" ReadOnly="True"
                        SortExpression="Choice4%" UniqueName="Choice4%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice5%" HeaderText="Choice5%" ReadOnly="True"
                        SortExpression="Choice5%" UniqueName="Choice5%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Correct" DataType="System.Double" HeaderText="Correct"
                        ReadOnly="True" SortExpression="Correct" UniqueName="Correct">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Total" DataType="System.Double" HeaderText="Total"
                        SortExpression="Total" UniqueName="Total">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" ReadOnly="True"
                        SortExpression="Percentage" UniqueName="Percentage">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource27" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAnswerPerQuestion" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox24" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel10" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Answer Per Question Report - Expired Tests
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox25" runat="server" DataSourceID="SqlDataSource28"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox25_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource28" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox26" runat="server" DataSourceID="SqlDataSource29"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox26_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource29" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox25" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox27" runat="server" DataSourceID="SqlDataSource30"
                        DataTextField="testname" DataValueField="testcategoryid">
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource30" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="SELECT DISTINCT
cast(testcategoryid as varchar) as testcategoryid,  
RTRIM(LTRIM(testname)) as 'testname', 
testname as 'title' 
FROM
tbl_testing_testcategory 
WHERE
EndDate &lt;= GETDATE()
	AND CampaignID = @SubcategoryID OR (CampaignID = 0 AND @CategoryID = 220032)
ORDER BY
testname">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox26" Name="SubcategoryID" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="RadComboBox25" Name="CategoryID" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate9" runat="server" Text="Generate" OnClick="cmdGenerate9_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid9" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource31"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid9_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource31" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="TestCategoryID" HeaderText="TestCategoryID" SortExpression="TestCategoryID"
                        UniqueName="TestCategoryID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="Test" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question" HeaderText="Question" SortExpression="Question"
                        UniqueName="Question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice1" HeaderText="Choice1" SortExpression="Choice1"
                        UniqueName="Choice1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice2" HeaderText="Choice2" SortExpression="Choice2"
                        UniqueName="Choice2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice3" HeaderText="Choice3" SortExpression="Choice3"
                        UniqueName="Choice3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice4" HeaderText="Choice4" SortExpression="Choice4"
                        UniqueName="Choice4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice5" HeaderText="Choice5" SortExpression="Choice5"
                        UniqueName="Choice5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans1" HeaderText="Ans1" SortExpression="Ans1"
                        UniqueName="Ans1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans2" HeaderText="Ans2" SortExpression="Ans2"
                        UniqueName="Ans2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans3" HeaderText="Ans3" SortExpression="Ans3"
                        UniqueName="Ans3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans4" HeaderText="Ans4" SortExpression="Ans4"
                        UniqueName="Ans4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans5" HeaderText="Ans5" SortExpression="Ans5"
                        UniqueName="Ans5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice1%" HeaderText="Choice1%" ReadOnly="True"
                        SortExpression="Choice1%" UniqueName="Choice1%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice2%" HeaderText="Choice2%" ReadOnly="True"
                        SortExpression="Choice2%" UniqueName="Choice2%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice3%" HeaderText="Choice3%" ReadOnly="True"
                        SortExpression="Choice3%" UniqueName="Choice3%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice4%" HeaderText="Choice4%" ReadOnly="True"
                        SortExpression="Choice4%" UniqueName="Choice4%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice5%" HeaderText="Choice5%" ReadOnly="True"
                        SortExpression="Choice5%" UniqueName="Choice5%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Correct" DataType="System.Double" HeaderText="Correct"
                        ReadOnly="True" SortExpression="Correct" UniqueName="Correct">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Total" DataType="System.Double" HeaderText="Total"
                        SortExpression="Total" UniqueName="Total">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" ReadOnly="True"
                        SortExpression="Percentage" UniqueName="Percentage">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource31" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAnswerPerQuestionExpiredTest" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox27" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    </form>
</body>
</html>
