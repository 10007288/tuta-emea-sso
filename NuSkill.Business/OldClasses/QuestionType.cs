using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class QuestionType
    {
        #region properties
        private string _typeCode;

        public string TypeCode
        {
            get { return _typeCode; }
            set { _typeCode = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }
        #endregion

        #region Constructors
        public QuestionType()
        {
        }

        public QuestionType(string typeCode, string description, bool hideFromList)
        {
            this._typeCode = typeCode;
            this._description = description;
            this._hideFromList = hideFromList;
        }
        #endregion

        #region methods
        public void Insert(string username)
        {
            QuestionTypeDAL dal = new QuestionTypeDAL();
            dal.Insert(this._typeCode, this._description, this._hideFromList, username);
        }

        public static QuestionType Select(string typeCode)
        {
            QuestionTypeDAL dal = new QuestionTypeDAL();
            QuestionType[] types = Conversion.SetProperties<QuestionType>(dal.Select(typeCode));
            return types == null || types.Length < 1 ? null : types[0];
        }

        public static QuestionType[] SelectAll()
        {
            QuestionTypeDAL dal = new QuestionTypeDAL();
            return Conversion.SetProperties<QuestionType>(dal.SelectAll());
        }
        #endregion
    }
}
