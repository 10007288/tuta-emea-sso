using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business.Entities
{
    [Serializable]
    public class InternalCredentials : IInternalCredentials
    {
        #region Properties
        private int _result;

        public int Result
        {
            get { return _result; }
            set { _result = value; }
        }

        private int _cimNumber;

        public int CimNumber
        {
            get { return _cimNumber; }
            set { _cimNumber = value; }
        }

        private int _roleID;

        public int RoleID
        {
            get { return _roleID; }
            set { _roleID = value; }
        }

        private int _accountID;

        public int AccountID
        {
            get { return _accountID; }
            set { _accountID = value; }
        }

        private string _account;

        public string Account
        {
            get { return _account; }
            set { _account = value; }
        }
        #endregion

        #region Constructors
        public InternalCredentials()
        { }
        #endregion

        #region Methods
        public IInternalCredentials LoginUser(int cimNumber, string password)
        {
            InternalCredentialsDal dal = new InternalCredentialsDal();
            IInternalCredentials[] creds = Conversion.SetProperties<IInternalCredentials>(dal.LoginUser(cimNumber, password));
            return creds == null || creds.Length == 0 ? null : creds[0];
        }
        #endregion


    }
}
