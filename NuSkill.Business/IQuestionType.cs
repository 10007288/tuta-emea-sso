using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IQuestionType
    {
        #region Properties
        string TypeCode { get; set; }
        string Description { get; set; }
        bool HideFromList { get; set; }
        #endregion

        #region Methods
        void Insert(string username);
        IQuestionType Select(string typecode);
        IQuestionType[] SelectAll();
        #endregion
    }
}
