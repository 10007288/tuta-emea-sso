using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class markessay : System.Web.UI.Page
{
    private SaveTestResponse NewResponse
    {
        get { return this.ViewState["response"] as SaveTestResponse; }
        set { this.ViewState["response"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        try
        {
            SaveTestResponse response = SessionManager.SessionSingleSaveTestResponse;
            if (response != null)
            {
                this.NewResponse = response;
                Questionnaire question = Questionnaire.Select(response.QuestionnaireID, true);
                if (question != null)
                {
                    TestCategory category = TestCategory.Select(question.TestCategoryID, true);
                    if (category != null)
                    {
                        this.txtAnswerVal.Text = response.EssayResponse;
                        this.lblExamIDVal.Text = category.TestName;
                        this.lblQuestionVal.Text = question.Question;
                        this.lblUserIDVal.Text = response.UserID;
                        this.lblMaxScore.Text = question.EssayMaxScore.ToString();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "markessay.aspx", "Page_Load", ex.Message);
        }
    }

    protected void btnGrade_Click(object sender, EventArgs e)
    {
        SaveTestResponse response = SessionManager.SessionSingleSaveTestResponse;
        Questionnaire questionnaire = Questionnaire.Select(response.QuestionnaireID, true);
        TestCategory category = TestCategory.Select(questionnaire.TestCategoryID, true);
        int score = 0;
        if (!int.TryParse(this.txtGradeVal.Text.Trim(), out score))
        {
            this.cvScoreOverflow.IsValid = false;
            return;
        }
        else if (score > questionnaire.EssayMaxScore)
        {
            this.cvScoreOverflow.IsValid = false;
            return;
        }
        try
        {
            int temp = 0;
            if (int.TryParse(this.txtGradeVal.Text.Trim(), out temp))
            {
                if (temp > Convert.ToInt32(lblMaxScore.Text.Trim()))
                {
                    this.cvScoreOverflow.IsValid = false;
                    return;
                }
                if (response != null)
                {
                    if (response.EssayResponse == this.txtAnswerVal.Text.Trim())
                    {
                        TestResponse existing = TestResponse.Select(response.TestTakenID, response.QuestionnaireID, response.UserID);
                        if (existing == null)
                        {
                            TestResponse saveResponse = new TestResponse(response);
                            saveResponse.Insert();
                            saveResponse.GradeEssayQuestion(Convert.ToInt32(this.txtGradeVal.Text.Trim()));
                            TestTaken taken = TestTaken.Select(saveResponse.TestTakenID);
                            taken.UpdateScore(Convert.ToInt32(this.txtGradeVal.Text.Trim()), category.PassingGrade);
                            response.Delete();
                            this.lblScored.Text = "Essay has been successfully graded.";
                            this.mpeScored.Show();
                        }
                        else
                        {
                            this.lblScored.Text = "This essay has already been graded.";
                            this.mpeScored.Show();
                        }
                    }
                }
                else
                {
                    this.lblScored.Text = "This essay has already been graded.";
                    this.mpeScored.Show();
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "markessay.aspx", "btnGrade_Click", ex.Message);
        }
    }

    protected void lblReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essays.aspx?" + Request.QueryString.ToString());
    }
}
