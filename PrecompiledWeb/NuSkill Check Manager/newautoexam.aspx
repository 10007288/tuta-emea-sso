<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="newautoexam, App_Web_yuclf4ni" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="4" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblQuickItems" runat="server" Text="Quick Items" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTest" runat="server" Text="Choose Exam:" />
                                <asp:DropDownList ID="ddlExams" runat="server" DataTextField="TestName" DataValueField="TestCategoryID"
                                    AutoPostBack="false" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:TextBox ID="txtExamItems" runat="server" TextMode="multiline" Width="98%" Rows="11"
                                    Wrap="false" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnCreateQuestions" runat="server" Text="Add" CssClass="buttons"
                                    OnClick="btnCreateQuestions_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeResult" runat="server" TargetControlID="hidResult" PopupControlID="pnlResult" BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidResult" runat="server" />
    <asp:Panel ID="pnlResult" runat="server" Width="500px" CssClass="modalPopup" HorizontalAlign="center">
        <table cellpadding="5">
            <tr>
                <td align="center">
                    <asp:Label ID="lblResult" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnResultOK" runat="server" CssClass="buttons" Text="OK" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeQuestionPopup" runat="server" TargetControlID="hidQuestionPopup" PopupControlID="pnlQuestionPopup" BackgroundCssClass="modalBackground" />
    <asp:HiddenField ID="hidQuestionPopup" runat="server" />
    <asp:Panel ID="pnlQuestionPopup" runat="server" CssClass="modalPopup" Width="800px">
        <table>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblPopupQuestion" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect1" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect2" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion2" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect3" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion3" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect4" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion4" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect5" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion5" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect6" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion6" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect7" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion7" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect8" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion8" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect9" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion9" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgCorrect10" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblQuestion10" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnCreate" runat="server" Text="Create" CssClass="buttons" OnClick="btnCreate_Click" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
