using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;

namespace NuSkill.Business.Entities
{
    [Serializable]
    public class ETestScoreSummary : ITestScoreSummary
    {
        private int _testTakenID;

        public int TestTakenID
        {
            get { return _testTakenID; }
            set { _testTakenID = value; }
        }

        private ITestTaken _testTaken;

        public ITestTaken TestTaken
        {
            get { return _testTaken; }
            set { _testTaken = value; }
        }

        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private ITestCategory _testCategory;

        public ITestCategory TestCategory
        {
            get { return _testCategory; }
            set { _testCategory = value; }
        }

        private int _cimNumber;

        public int CimNumber
        {
            get { return _cimNumber; }
            set { _cimNumber = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private double _score;

        public double Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private bool _passed;

        public bool Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private int _accountID;

        public int AccountID
        {
            get { return _accountID; }
            set { _accountID = value; }
        }

        private DateTime _dateLastSave;

        public DateTime DateLastSave
        {
            get { return _dateLastSave; }
            set { _dateLastSave = value; }
        }

        public void Insert()
        {
            TestScoreSummaryDal dal = new TestScoreSummaryDal();
            dal.Insert(this._testTakenID, this._testCategoryID, this._cimNumber, this._score, this._passed, this._accountID);
        }
    }
}
