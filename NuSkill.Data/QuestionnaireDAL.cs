using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class QuestionnaireDAL : TheLibrary.DBImportTool.Base
    {
        public QuestionnaireDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public QuestionnaireDAL(string connString)
        {
            this.ConnectionString = connString;
        }

        public int  Insert(int testCategoryID, string typeCode, string question, string choice1, string choice2,
            string choice3, string choice4, string choice5, string choice6, string choice7, string choice8, string choice9,
            string choice10, string ans1, string ans2, string ans3, string ans4, string ans5, string ans6, string ans7, string ans8,
            string ans9, string ans10, string lastUpdated, int essayMaxScore, bool hideFromList, string hyperLink)
        {
            List<SqlParameter> paramlist = new List<SqlParameter>();
            paramlist.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            paramlist.Add(Helper.CreateParam("@pTypeCode", SqlDbType.VarChar, typeCode));
            paramlist.Add(Helper.CreateParam("@pQuestion", SqlDbType.VarChar, question));
            paramlist.Add(Helper.CreateParam("@pChoice1", SqlDbType.VarChar, choice1));
            paramlist.Add(Helper.CreateParam("@pChoice2", SqlDbType.VarChar, choice2));
            paramlist.Add(Helper.CreateParam("@pChoice3", SqlDbType.VarChar, choice3));
            paramlist.Add(Helper.CreateParam("@pChoice4", SqlDbType.VarChar, choice4));
            paramlist.Add(Helper.CreateParam("@pChoice5", SqlDbType.VarChar, choice5));
            paramlist.Add(Helper.CreateParam("@pChoice6", SqlDbType.VarChar, choice6));
            paramlist.Add(Helper.CreateParam("@pChoice7", SqlDbType.VarChar, choice7));
            paramlist.Add(Helper.CreateParam("@pChoice8", SqlDbType.VarChar, choice8));
            paramlist.Add(Helper.CreateParam("@pChoice9", SqlDbType.VarChar, choice9));
            paramlist.Add(Helper.CreateParam("@pChoice10", SqlDbType.VarChar, choice10));
            paramlist.Add(Helper.CreateParam("@pAns1", SqlDbType.VarChar, ans1));
            paramlist.Add(Helper.CreateParam("@pAns2", SqlDbType.VarChar, ans2));
            paramlist.Add(Helper.CreateParam("@pAns3", SqlDbType.VarChar, ans3));
            paramlist.Add(Helper.CreateParam("@pAns4", SqlDbType.VarChar, ans4));
            paramlist.Add(Helper.CreateParam("@pAns5", SqlDbType.VarChar, ans5));
            paramlist.Add(Helper.CreateParam("@pAns6", SqlDbType.VarChar, ans6));
            paramlist.Add(Helper.CreateParam("@pAns7", SqlDbType.VarChar, ans7));
            paramlist.Add(Helper.CreateParam("@pAns8", SqlDbType.VarChar, ans8));
            paramlist.Add(Helper.CreateParam("@pAns9", SqlDbType.VarChar, ans9));
            paramlist.Add(Helper.CreateParam("@pAns10", SqlDbType.VarChar, ans10));
            paramlist.Add(Helper.CreateParam("@pLastUpdated", SqlDbType.VarChar, lastUpdated));
            paramlist.Add(Helper.CreateParam("@pEssayMaxScore", SqlDbType.Int, essayMaxScore));
            paramlist.Add(Helper.CreateParam("@pHideFromList", SqlDbType.Bit, hideFromList));
            paramlist.Add(Helper.CreateParam("@HyperLink", SqlDbType.VarChar, hyperLink));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_questionnaire_sav_newquestion", paramlist.ToArray()));
        }

        public void Update(int questionnaireID, int testCategoryID, string typeCode, string question, string choice1, string choice2,
            string choice3, string choice4, string choice5, string choice6, string choice7, string choice8, string choice9,
            string choice10, string ans1, string ans2, string ans3, string ans4, string ans5, string ans6, string ans7, string ans8,
            string ans9, string ans10, int essayMaxScore, string hyperLink)
        {
            List<SqlParameter> paramlist = new List<SqlParameter>();
            paramlist.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            paramlist.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            paramlist.Add(Helper.CreateParam("@pTypeCode", SqlDbType.VarChar, typeCode));
            paramlist.Add(Helper.CreateParam("@pQuestion", SqlDbType.VarChar, question));
            paramlist.Add(Helper.CreateParam("@pChoice1", SqlDbType.VarChar, choice1));
            paramlist.Add(Helper.CreateParam("@pChoice2", SqlDbType.VarChar, choice2));
            paramlist.Add(Helper.CreateParam("@pChoice3", SqlDbType.VarChar, choice3));
            paramlist.Add(Helper.CreateParam("@pChoice4", SqlDbType.VarChar, choice4));
            paramlist.Add(Helper.CreateParam("@pChoice5", SqlDbType.VarChar, choice5));
            paramlist.Add(Helper.CreateParam("@pChoice6", SqlDbType.VarChar, choice6));
            paramlist.Add(Helper.CreateParam("@pChoice7", SqlDbType.VarChar, choice7));
            paramlist.Add(Helper.CreateParam("@pChoice8", SqlDbType.VarChar, choice8));
            paramlist.Add(Helper.CreateParam("@pChoice9", SqlDbType.VarChar, choice9));
            paramlist.Add(Helper.CreateParam("@pChoice10", SqlDbType.VarChar, choice10));
            paramlist.Add(Helper.CreateParam("@pAns1", SqlDbType.VarChar, ans1));
            paramlist.Add(Helper.CreateParam("@pAns2", SqlDbType.VarChar, ans2));
            paramlist.Add(Helper.CreateParam("@pAns3", SqlDbType.VarChar, ans3));
            paramlist.Add(Helper.CreateParam("@pAns4", SqlDbType.VarChar, ans4));
            paramlist.Add(Helper.CreateParam("@pAns5", SqlDbType.VarChar, ans5));
            paramlist.Add(Helper.CreateParam("@pAns6", SqlDbType.VarChar, ans6));
            paramlist.Add(Helper.CreateParam("@pAns7", SqlDbType.VarChar, ans7));
            paramlist.Add(Helper.CreateParam("@pAns8", SqlDbType.VarChar, ans8));
            paramlist.Add(Helper.CreateParam("@pAns9", SqlDbType.VarChar, ans9));
            paramlist.Add(Helper.CreateParam("@pAns10", SqlDbType.VarChar, ans10));
            paramlist.Add(Helper.CreateParam("@EssayMaxScore", SqlDbType.Int, essayMaxScore));
            paramlist.Add(Helper.CreateParam("@HyperLink", SqlDbType.VarChar, hyperLink));
            Connection.ExecuteSP("pr_questionnaire_sav_update", paramlist.ToArray());
        }

        public DataSet Select(int questionnaireID, int testCategoryID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            return Connection.ExecuteSPQuery("pr_questionnaire_lkp_getquestions", paramList.ToArray());
        }

        public DataSet Select(int questionnaireID, bool includeHidden)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            paramList.Add(Helper.CreateParam("@pIncludeHidden", SqlDbType.Bit, includeHidden));
            return Connection.ExecuteSPQuery("pr_questionnaire_lkp_getbyid", paramList.ToArray());
        }

        public DataSet SelectByCategory(int testCategoryID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestCategoryID", SqlDbType.Int, testCategoryID));
            return Connection.ExecuteSPQuery("pr_questionnaire_lkp_getsingle", paramList.ToArray());
        }

        public DataSet SelectSuggested()
        {
            return Connection.ExecuteSPQuery("pr_questionnaire_lkp_getsuggested");
        }

        public DataSet SelectSuggestedUser(string username)
        {
            return Connection.ExecuteSPQuery("pr_questionnaire_lkp_getsuggesteduser",
                Helper.CreateParam("@Username", SqlDbType.VarChar, username));
        }


        public void Delete(int questionnaireID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            Connection.ExecuteSPQuery("pr_questionnaire_sav_delete", paramList.ToArray());
        }
    }
}
