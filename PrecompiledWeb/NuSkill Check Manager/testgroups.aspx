<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="testgroups, App_Web_yuclf4ni" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblTestGroups" runat="server" Text="Test Groups" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvGroups" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                    ForeColor="black" CellPadding="5" BorderColor="black" Font-Bold="false"
                                    EmptyDataText="No test groups." OnRowCommand="gvGroups_RowCommand">
                                    <HeaderStyle HorizontalAlign="center" />
                                    <RowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" Wrap="true" />
                                    <AlternatingRowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Test Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestGroupID" runat="server" Text='<%#Bind("TestGroupID") %>' Visible="false" />
                                                <asp:Label ID="lblTestGroup" runat="server" Text='<%#Bind("TestGroupName") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View Exams For This Group">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkView" runat="server" Text="View" CommandName="View" CommandArgument='<%#Bind("TestGroupID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeViewExams" runat="server" BackgroundCssClass="modalBackground" TargetControlID="hidViewExams" PopupControlID="pnlViewExams" />
    <asp:HiddenField ID="hidViewExams" runat="server" />
    <asp:Panel ID="pnlViewExams" runat="server" Width="900px" CssClass="modalPopup" HorizontalAlign="center">
        <table width="100%" cellpadding="3" cellspacing="0">
            <tr>
                <td>
                    <asp:GridView ID="gvExams" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                        ForeColor="black" PageSize="20" CellPadding="5" BorderColor="black" Font-Bold="false"
                        Width="100%" EmptyDataText="No exams found." OnSelectedIndexChanged="gvExams_SelectedIndexChanged">
                        <HeaderStyle HorizontalAlign="center" />
                        <RowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" Wrap="true" />
                        <AlternatingRowStyle ForeColor="Black" Font-Size="12px" BorderColor="black" />
                        <Columns>
                            <asp:TemplateField HeaderText="Exam Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblExamID" runat="server" Text='<%#Bind("TestCategoryID") %>' Visible="false" />
                                    <asp:Label ID="lblExam" runat="server" Text='<%#Bind("TestName") %>' Font-Bold="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Bind("StartDate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Bind("EndDate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time Limit">
                                <ItemTemplate>
                                    <asp:Label ID="lblTimeLimitVal" runat="server" Text='<%#Bind("TimeLimit") %>' Visible="false" />
                                    <asp:Label ID="lblTimeLimit" runat="server" Text="" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tries Allowed">
                                <ItemTemplate>
                                    <asp:Label ID="lblTestLimit" runat="server" Text='<%#Bind("TestLimit") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkExam" runat="server" CommandName="Select" Text="View" CommandArgument='<%#Bind("TestCategoryID") %>'
                                        ForeColor="black" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="buttons" OnClick="btnBack_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
