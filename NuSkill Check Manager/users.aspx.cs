using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using Telerik.Web.UI;
using System.Data.SqlClient;
using System.Text;

public partial class users : System.Web.UI.Page
{
    string _Role;
    string _Supervisor;
    string _Client;

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserRole();

        if (_Role != "Admin")
        {
            Response.Redirect("Home.aspx");
        }

        try
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());

            if (!this.IsPostBack)
            {
                this.gvUsers.DataSource = Registration.SelectByBatch(0);
                this.gvUsers.DataBind();
                this.BindBatches();
                this.lnkCompleteInformation.Visible = false;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void GetUserRole()
    {
        //Check user role of the current user
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionUsername;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }

    protected void btnUserSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.txtSearch.Text.Trim()))
        {
            this.gvUsers.DataSource = Registration.SearchByBatch(this.txtSearch.Text.Trim(), Convert.ToInt32(this.ddlBatch.Text));
            this.gvUsers.DataBind();
        }
        else
        {
            this.gvUsers.DataSource = Registration.SelectByBatch(Convert.ToInt32(this.ddlBatch.Text));
            this.gvUsers.DataBind();
        }
    }

    protected void BindGrid()
    {
        if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(11))
        {
            this.gvUsers.DataSource = null;
            this.btnUserSearch.Visible = false;
        }
        else
        {
            this.gvUsers.DataSource = Registration.SearchByBatch(this.txtSearch.Text.Trim(), Convert.ToInt32(this.ddlBatch.Text));
        }
        this.gvUsers.DataBind();
    }

    protected void BindBatches()
    {
        this.ddlBatch.DataSource = UserBatch.SelectAll();
        this.ddlBatch.DataBind();
    }

    protected void gvUsers_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridViewRow row = this.gvUsers.Rows[e.NewSelectedIndex];
        if (row != null)
        {
            {
                Label lblUsername = row.FindControl("lblUsername") as Label;
                Registration user = Registration.Select(lblUsername.Text);
                this.lblUserIDVal.Text = user.AutoUserID;
                this.lblCimVal.Text = "connect to DB";
                SessionManager.SessionExamineeName = this.lblUserIDVal.Text.Trim();
                Response.Redirect("~/userinfo.aspx?" + Request.QueryString.ToString());
            }
        }
    }

    protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvUsers.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }

    protected void lnkCompleteInformation_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/userinfo.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkViewAll_Click(object sender, EventArgs e)
    {
        Registration[] registration = Registration.SelectAll();
        this.gvUsers.DataSource = registration;
        this.gvUsers.DataBind();
    }

    protected void btnPopup_Click(object sender, EventArgs e)
    {
        Registration[] registration = Registration.SelectAll();
        this.gvUsers.DataSource = registration;
        this.gvUsers.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SessionManager.SessionExamineeName = this.txtCim.Text.Trim();
        Response.Redirect("~/userinfo.aspx?" + Request.QueryString.ToString());
    }

    protected void RadGrid1_InsertCommand(object sender, GridCommandEventArgs e)
    {
        if (e.Item is GridEditableItem)
        {
            GridEditableItem editedItem = e.Item as GridEditableItem;
            UserControl userControl = (UserControl)e.Item.FindControl(GridEditFormItem.EditFormUserControlID);
            string username = string.Empty;
            string firstname = ((TextBox)userControl.FindControl("txtFirst")).Text.Trim();
            string lastname = ((TextBox)userControl.FindControl("txtLast")).Text.Trim();
            string twwid = ((TextBox)userControl.FindControl("txtTwwid")).Text.Trim();
            string email = ((TextBox)userControl.FindControl("txtEmail")).Text.Trim();
            bool active = ((CheckBox)userControl.FindControl("chkActive")).Checked;
            string roleid = ((RadComboBox)userControl.FindControl("rcbRole")).SelectedValue;
            string regionid = ((RadComboBox)userControl.FindControl("rcbRegion")).SelectedValue;
            string countryid = ((RadComboBox)userControl.FindControl("rcbCountry")).SelectedValue;
            string siteid = ((RadComboBox)userControl.FindControl("rcbSite")).SelectedValue;
            string clientid = ((RadComboBox)userControl.FindControl("rcbClient")).SelectedValue;
            string password = "1000:Jvj9wJbKBLbt+tUD2m63JGoClXjChzQWdjgGV/Auktk=:fNtpSklwaXgWRB1D/Ugm1EDLqiK4jzKF03ra9qzUKoE=";//Tr@nscom01
            string mobileno = ((RadTextBox)userControl.FindControl("RadTextBox1")).Text;
            string supervisorid = ((RadComboBox)userControl.FindControl("RadComboBox1")).SelectedValue;
            string departmentid = ((RadComboBox)userControl.FindControl("RadComboBox2")).SelectedValue;
            CheckBoxList chksupporttype = ((CheckBoxList)userControl.FindControl("CheckBoxList1"));

            StringBuilder stringbuild1 = new StringBuilder();

            foreach (ListItem item in chksupporttype.Items)
            {
                if (item.Selected)
                    stringbuild1.Append(item.Value).Append(",");
            }

            if (stringbuild1.Length > 0)
                stringbuild1 = stringbuild1.Remove(stringbuild1.Length - 1, 1);

            int languageid = Convert.ToInt32(((RadComboBox)userControl.FindControl("cbLanguage")).SelectedValue);
            string specskill = ((RadTextBox)userControl.FindControl("txtSpecializationSkills")).Text;
            int educlevel = Convert.ToInt32(((RadComboBox)userControl.FindControl("cbEducLevel")).SelectedValue);
            string coursemajor = ((RadTextBox)userControl.FindControl("txtCourseMajor")).Text;
            string addcourse = ((RadTextBox)userControl.FindControl("txtCoursesTrainings")).Text;
            int careerpathid = Convert.ToInt32(((RadioButtonList)userControl.FindControl("radioBtnCareerPath")).SelectedValue);
            string courseinterest = ((RadTextBox)userControl.FindControl("txtCoursesInterested")).Text;
            CheckBoxList chkupdatevia = ((CheckBoxList)userControl.FindControl("radioBtnReceiveUpdates"));

            StringBuilder stringbuild2 = new StringBuilder();

            foreach (ListItem item in chkupdatevia.Items)
            {
                if (item.Selected)
                    stringbuild2.Append(item.Value).Append(",");
            }

            if (stringbuild2.Length > 0)
                stringbuild2 = stringbuild2.Remove(stringbuild2.Length - 1, 1);

            using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
            {
                oconn.Open();
                using (SqlCommand ocomm = new SqlCommand("sp_InsertUser", oconn))
                {
                    ocomm.CommandType = CommandType.StoredProcedure;
                    ocomm.Parameters.AddWithValue("username", username);
                    ocomm.Parameters.AddWithValue("firstname", firstname);
                    ocomm.Parameters.AddWithValue("lastname", lastname);
                    ocomm.Parameters.AddWithValue("twwid", twwid);
                    ocomm.Parameters.AddWithValue("email", email);
                    ocomm.Parameters.AddWithValue("active", active);
                    ocomm.Parameters.AddWithValue("roleid", roleid);
                    ocomm.Parameters.AddWithValue("regionid", regionid);
                    ocomm.Parameters.AddWithValue("countryid", countryid);
                    ocomm.Parameters.AddWithValue("siteid", siteid);
                    ocomm.Parameters.AddWithValue("clientid", clientid);
                    ocomm.Parameters.AddWithValue("password", password);
                    ocomm.Parameters.AddWithValue("mobileno", mobileno);
                    ocomm.Parameters.AddWithValue("supervisorid", supervisorid);
                    ocomm.Parameters.AddWithValue("departmentid", departmentid);
                    ocomm.Parameters.AddWithValue("supporttype", stringbuild1.ToString());
                    ocomm.Parameters.AddWithValue("languageid", languageid);
                    ocomm.Parameters.AddWithValue("specskill", specskill);
                    ocomm.Parameters.AddWithValue("educlevel", educlevel);
                    ocomm.Parameters.AddWithValue("coursemajor", coursemajor);
                    ocomm.Parameters.AddWithValue("addcourse", addcourse);
                    ocomm.Parameters.AddWithValue("careerpathid", careerpathid);
                    ocomm.Parameters.AddWithValue("courseinterest", courseinterest);
                    ocomm.Parameters.AddWithValue("updatevia", stringbuild2.ToString());
                    ocomm.ExecuteNonQuery();
                }
                oconn.Close();
            }
        }
    }

    protected void RadGrid1_UpdateCommand(object sender, GridCommandEventArgs e)
    {
        if (e.Item is GridEditableItem)
        {
            GridEditableItem editedItem = e.Item as GridEditableItem;
            UserControl userControl = (UserControl)e.Item.FindControl(GridEditFormItem.EditFormUserControlID);
            string ID = editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["UserID"].ToString();
            //string username = ((TextBox)userControl.FindControl("txtUsername")).Text.Trim();
            string username = string.Empty;
            //string firstname = ((TextBox)userControl.FindControl("txtFirst")).Text.Trim();
            //string lastname = ((TextBox)userControl.FindControl("txtLast")).Text.Trim();
            //string twwid = ((TextBox)userControl.FindControl("txtTwwid")).Text.Trim();
            string email = ((TextBox)userControl.FindControl("txtEmail")).Text.Trim();
            bool active = ((CheckBox)userControl.FindControl("chkActive")).Checked;
            string roleid = ((RadComboBox)userControl.FindControl("rcbRole")).SelectedValue;
            //string regionid = ((RadComboBox)userControl.FindControl("rcbRegion")).SelectedValue;
            //string countryid = ((RadComboBox)userControl.FindControl("rcbCountry")).SelectedValue;
            //string siteid = ((RadComboBox)userControl.FindControl("rcbSite")).SelectedValue;
            string clientid = ((RadComboBox)userControl.FindControl("rcbClient")).SelectedValue;
            bool resetpassword = ((CheckBox)userControl.FindControl("CheckBox1")).Checked;
            string password = "1000:Jvj9wJbKBLbt+tUD2m63JGoClXjChzQWdjgGV/Auktk=:fNtpSklwaXgWRB1D/Ugm1EDLqiK4jzKF03ra9qzUKoE=";//Tr@nscom01
            string mobileno = ((RadTextBox)userControl.FindControl("RadTextBox1")).Text;
            string supervisorid = ((RadComboBox)userControl.FindControl("rcbSupervisor")).SelectedValue;
            string departmentid = ((RadComboBox)userControl.FindControl("RadComboBox2")).SelectedValue;

            CheckBoxList chksupporttype = ((CheckBoxList)userControl.FindControl("CheckBoxList1"));
            StringBuilder stringbuild1 = new StringBuilder();

            foreach (ListItem item in chksupporttype.Items)
            {
                if (item.Selected)
                    stringbuild1.Append(item.Value).Append(",");
            }
            if (stringbuild1.Length > 0)
                stringbuild1 = stringbuild1.Remove(stringbuild1.Length - 1, 1);

            int languageid = Convert.ToInt32(((RadComboBox)userControl.FindControl("cbLanguage")).SelectedValue);
            string specskill = ((RadTextBox)userControl.FindControl("txtSpecializationSkills")).Text;
            int educlevel = Convert.ToInt32(((RadComboBox)userControl.FindControl("cbEducLevel")).SelectedValue);
            string coursemajor = ((RadTextBox)userControl.FindControl("txtCourseMajor")).Text;
            string addcourse = ((RadTextBox)userControl.FindControl("txtCoursesTrainings")).Text;
            int careerpathid = Convert.ToInt32(((RadioButtonList)userControl.FindControl("radioBtnCareerPath")).SelectedValue);
            string courseinterest = ((RadTextBox)userControl.FindControl("txtCoursesInterested")).Text;

            CheckBoxList chkupdatevia = ((CheckBoxList)userControl.FindControl("radioBtnReceiveUpdates"));
            StringBuilder stringbuild2 = new StringBuilder();

            foreach (ListItem item in chkupdatevia.Items)
            {
                if (item.Selected)
                    stringbuild2.Append(item.Value).Append(",");
            }
            if (stringbuild2.Length > 0)
                stringbuild2 = stringbuild2.Remove(stringbuild2.Length - 1, 1);

            using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
            {
                oconn.Open();
                using (SqlCommand ocomm = new SqlCommand("sp_UpdateUser", oconn))
                {
                    ocomm.CommandType = CommandType.StoredProcedure;
                    ocomm.Parameters.AddWithValue("username", username);
                    //ocomm.Parameters.AddWithValue("firstname", firstname);
                    //ocomm.Parameters.AddWithValue("lastname", lastname);
                    //ocomm.Parameters.AddWithValue("twwid", twwid);
                    ocomm.Parameters.AddWithValue("email", email);
                    ocomm.Parameters.AddWithValue("active", active);
                    ocomm.Parameters.AddWithValue("roleid", roleid);
                    //ocomm.Parameters.AddWithValue("regionid", regionid);
                    //ocomm.Parameters.AddWithValue("countryid", countryid);
                    //ocomm.Parameters.AddWithValue("siteid", siteid);
                    ocomm.Parameters.AddWithValue("clientid", clientid);
                    ocomm.Parameters.AddWithValue("userid", ID);

                    if (resetpassword)
                        ocomm.Parameters.AddWithValue("password", password);

                    ocomm.Parameters.AddWithValue("mobileno", mobileno);
                    ocomm.Parameters.AddWithValue("supervisorid", supervisorid);
                    ocomm.Parameters.AddWithValue("departmentid", departmentid);
                    ocomm.Parameters.AddWithValue("supporttype", stringbuild1.ToString());
                    ocomm.Parameters.AddWithValue("languageid", languageid);
                    ocomm.Parameters.AddWithValue("specskill", specskill);
                    ocomm.Parameters.AddWithValue("educlevel", educlevel);
                    ocomm.Parameters.AddWithValue("coursemajor", coursemajor);
                    ocomm.Parameters.AddWithValue("addcourse", addcourse);
                    ocomm.Parameters.AddWithValue("careerpathid", careerpathid);
                    ocomm.Parameters.AddWithValue("courseinterest", courseinterest);
                    ocomm.Parameters.AddWithValue("updatevia", stringbuild2.ToString());
                    ocomm.ExecuteNonQuery();
                }
                oconn.Close();
            }

            if (roleid != "6")
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM AssignedClients WHERE TWWID = @TWWID";
                    SqlCommand cmd = new SqlCommand(sql, connection);

                    cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionCurrentGridUsers;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            TableCell cell1 = item["Active"];
            TableCell cell2 = item["IsLockedOut"];

            _Supervisor = item.GetDataKeyValue("SupervisorID").ToString();
            _Client = item.GetDataKeyValue("ClientID").ToString();

            if (cell1.Text == "True")
                cell1.Text = "Yes";
            else
                cell1.Text = "No";

            if (cell2.Text == "True")
                cell2.Text = "Yes";
            else
                cell2.Text = "No";
        }

        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {
            GridEditableItem editItem = e.Item as GridEditableItem;

            UserControl userControl = (UserControl)e.Item.FindControl(GridEditFormItem.EditFormUserControlID);
            TextBox tmpids = (TextBox)userControl.FindControl("TextBox1");
            CheckBoxList chk = (CheckBoxList)userControl.FindControl("CheckBoxList1");
            RadComboBox rcbSupervisor = (RadComboBox)userControl.FindControl("rcbSupervisor");
            RadComboBox rcbClient = (RadComboBox)userControl.FindControl("rcbClient");

            rcbSupervisor.SelectedValue = _Supervisor;
            rcbClient.SelectedValue = _Client;

            string temp = tmpids.Text;
            string[] temps = temp.Split(',');

            foreach (string item in temps)
            {
                if (!string.IsNullOrEmpty(item))
                    chk.Items.FindByValue(item.Trim()).Selected = true;
            }

            TextBox tmpupdateviaids = (TextBox)userControl.FindControl("TextBox2");
            CheckBoxList chkupdatevia = (CheckBoxList)userControl.FindControl("radioBtnReceiveUpdates");
            string temp1 = tmpupdateviaids.Text;
            string[] temps1 = temp1.Split(',');

            foreach (string item in temps1)
            {
                if (!string.IsNullOrEmpty(item))
                    chkupdatevia.Items.FindByValue(item.Trim()).Selected = true;
            }

            Label careerpathid = (Label)userControl.FindControl("Label2");
            RadioButtonList rdo = (RadioButtonList)userControl.FindControl("radioBtnCareerPath");

            if (!string.IsNullOrEmpty(careerpathid.Text))
                rdo.SelectedValue = careerpathid.Text;

            RadComboBox role = (RadComboBox)userControl.FindControl("rcbRole");
            LinkButton assignClients = (LinkButton)userControl.FindControl("lnkBtnAssignClient");

            if (role.SelectedItem.Text == "Course Admin")
                assignClients.Visible = true;
            else
                assignClients.Visible = false;

            if (assignClients != null)
            {
                assignClients.OnClientClick = string.Format("javascript:return ShowWindow({0})", editItem.GetDataKeyValue("UserID"));
                SessionManager.SessionCurrentGridUsers = editItem.GetDataKeyValue("TWWID").ToString();
            }
        }
    }
}
