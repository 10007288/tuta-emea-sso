<%@ control language="C#" autoeventwireup="true" inherits="controls_footercontrol, App_Web_ixuq4ve0" %>
<asp:Panel ID="pnlMain" runat="server" Height="100px" HorizontalAlign="center">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: White;
            background-color: #FFFFFF;">
        <tr>
            <td align="center">
                <asp:Label ID="lblNuSkillVersion" runat="server" Text="Transcom University Testing" Font-Names="Arial"
                    Font-Size="10px" />
                <br />
                <asp:Label ID="lblCopyright" runat="server" Text="Copyright � 2010 Transcom North America and Asia"
                    Font-Names="Arial" Font-Size="10px" />
            </td>
        </tr>
    </table>
</asp:Panel>
