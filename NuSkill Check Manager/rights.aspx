<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master"
    AutoEventWireup="true" CodeFile="rights.aspx.cs" Inherits="rights" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlAll" runat="server" HorizontalAlign="center" Width="100%">
        <asp:UpdatePanel ID="udpSub" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMain" runat="server" Width="400px" BorderColor="black" BorderStyle="None"
                    HorizontalAlign="center">
                    <table>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblInsufficientRights" runat="server" Text="You do not have enough rights to view that page."
                                    Font-Names="Arial" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
