using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class controls_menuControl : System.Web.UI.UserControl
{
    private string _userrole = string.Empty;
    
    public string UserRole 
    {
        get { return _userrole; }
        set { _userrole = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
        {
            this.trReports.Visible = false;
            this.trExams.Visible = false;
            this.trExaminees.Visible = false;
            this.trEssays.Visible = false;
            this.trLogout.Visible = false;
        }
        else
        {
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(1))
            {
                this.trExams.Visible = true;
            }
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(11))
                this.trExaminees.Visible = true;
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(9))
                this.trEssays.Visible = true;
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(0))
                this.trLogout.Visible = true;
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(32))
                this.trNonCimCampaigns.Visible = true;

            if (UserRole.Contains("5")) //Super User
            {
                this.lnkExams.Enabled = false;
                this.lnkQuickItems.Enabled = false;
                this.lnkEssays.Enabled = false;
                this.lnkNonCimCampaigns.Enabled = false;
                this.lnkRecheckExams.Enabled = false;
                this.lnkExaminees.Enabled = false;
                this.lnkDirectReports.Enabled = false;
                this.lnkReports.Enabled = true;

                this.lnkExams.ForeColor = System.Drawing.Color.Gray;
                this.lnkQuickItems.ForeColor = System.Drawing.Color.Gray;
                this.lnkEssays.ForeColor = System.Drawing.Color.Gray;
                this.lnkNonCimCampaigns.ForeColor = System.Drawing.Color.Gray;
                this.lnkRecheckExams.ForeColor = System.Drawing.Color.Gray;
                this.lnkExaminees.ForeColor = System.Drawing.Color.Gray;
                this.lnkDirectReports.ForeColor = System.Drawing.Color.Gray;
            }

            if (UserRole.Contains("6")) //Course Admin
            {
                this.lnkExams.Enabled = true;
                this.lnkQuickItems.Enabled = false;
                this.lnkEssays.Enabled = false;
                this.lnkNonCimCampaigns.Enabled = false;
                this.lnkRecheckExams.Enabled = false;
                this.lnkExaminees.Enabled = false;
                this.lnkDirectReports.Enabled = false;
                this.lnkReports.Enabled = true;

                this.lnkQuickItems.ForeColor = System.Drawing.Color.Gray;
                this.lnkEssays.ForeColor = System.Drawing.Color.Gray;
                this.lnkNonCimCampaigns.ForeColor = System.Drawing.Color.Gray;
                this.lnkRecheckExams.ForeColor = System.Drawing.Color.Gray;
                this.lnkExaminees.ForeColor = System.Drawing.Color.Gray;
                this.lnkDirectReports.ForeColor = System.Drawing.Color.Gray;
            }
        }
    }

    protected void lnkExams_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkEssays_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/essaytests.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkExaminees_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/users.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkDirectReports_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/directreports.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkReports_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/reports.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkTeamSummaries_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/teamsummaries.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();

        FormsAuthentication.SignOut();
        Response.Redirect("~/login.aspx");
    }

    protected void lnkSuggest_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/suggestnew.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkCreateUserBatch_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/massusercreation.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkViewUserBatch_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/userbatches.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkCheckSuggestions_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/viewsuggestions.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkNonCimCampaigns_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/customcampaigns.aspx?" + Request.QueryString.ToString());
    }

    protected void lnkQuickItems_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/newautoexam.aspx" + Request.QueryString.ToString());
    }

    protected void lnkRecheckExams_Click(object sender, EventArgs e)
    {
        Response.Redirect("recheckexam.aspx" + Request.QueryString.ToString());
    }
}
