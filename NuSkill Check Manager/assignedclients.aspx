﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="assignedclients.aspx.cs" Inherits="assignedclients" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assign Client/s</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <script type="text/javascript" language="javascript">
            function pageLoad() {
                var $ = $telerik.$;
                var listBox = $find("lboxAvailableClients");

                $(".rlbTransferAllFrom", listBox.get_element()).css("display", "none");
                $(".rlbTransferAllTo", listBox.get_element()).css("display", "none");
                $(".rlbTransferAllToDisabled", listBox.get_element()).css("display", "none");
            }
        </script>

        <telerik:RadScriptManager ID="ScriptManager1" runat="server">
        </telerik:RadScriptManager>

        <telerik:RadAjaxLoadingPanel runat="server" ID="RadLoadingPanel" BackgroundPosition="Center" Transparency="10" MinDisplayTime="2000">
            <img id="imgLoader" src="images/ajax-loader-small.gif" />
        </telerik:RadAjaxLoadingPanel>

        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="lboxAvailableClients">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="lboxAvailableClients" LoadingPanelID="RadLoadingPanel" />
                        <telerik:AjaxUpdatedControl ControlID="lboxTakenClients" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="lboxTakenClients">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="lboxAvailableClients" />
                        <telerik:AjaxUpdatedControl ControlID="lboxTakenClients" LoadingPanelID="RadLoadingPanel" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <table align="center">
            <tr>
                <td>
                    <asp:Label ID="lblColl" runat="server" Text="Collection:"></asp:Label>
                    <br />
                    <br />
                    <telerik:RadListBox ID="lboxAvailableClients" runat="server" Height="250" Sort="Ascending"
                        Width="200" TransferMode="Move" TransferToID="lboxTakenClients" AllowTransfer="True"
                        SelectionMode="Single" OnTransferred="lboxAvailableClients_Transferred" AutoPostBackOnTransfer="True" AutoPostBack="True">
                    </telerik:RadListBox>
                </td>
                <td>
                    <asp:Label ID="lblAssignedClients" runat="server" Text="Assigned Client/s:"></asp:Label>
                    <br />
                    <br />
                    <telerik:RadListBox ID="lboxTakenClients" runat="server" Height="250" Width="170" Sort="Ascending"
                    SelectionMode="Single" AutoPostBackOnTransfer="True" AutoPostBack="True">
                    </telerik:RadListBox>
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                    <telerik:RadButton runat="server" ID="btnSubmit" Text="Submit " OnClick="btnSubmit_Click" AutoPostBack="True"/>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
