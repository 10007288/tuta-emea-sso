using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IUserRole
    {
        int UserRoleID { get; set; }
        string UserID { get; set; }
        string RoleCode { get; set; }
        DateTime DateCreated { get; set; }
        DateTime DateLastUpdated { get; set; }
        string LastUpdated { get; set; }
        string LastUpdatedBy { get; set; }
        bool HideFromList { get; set; }

        IUserRole GetRole(string userID, string roleCode);
        IUserRole[] GetUserRoles(string userID);
    }
}
