<%@ control language="C#" autoeventwireup="true" inherits="controls_userinfocontrol, App_Web_2jcq2tmn" %>
<%@ Register TagPrefix="wus" TagName="userRightsControl" Src="~/controls/userRightsControl.ascx" %>
<asp:Panel ID="pnlExam" runat="server" Width="100%" HorizontalAlign="center">
    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMainUserInfo" runat="server">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial">
                        <tr>
                            <td align="center" style="vertical-align: middle; font-size: large; font-family: Arial;">
                                <asp:Label ID="lblUserInfo" runat="server" Text="Information for user:" />
                                <asp:Label ID="lblUserInfoValue" runat="server" />
                                <asp:Panel ID="pnlRights" runat="server" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkIsAdminBasic" runat="server" Text="Basic Rights Management"
                                                    Font-Bold="true" Font-Size="12px" OnClick="lnkIsAdminBasic_Click" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkIsAdmin" runat="server" Text="Advanced Rights Management"
                                                    Font-Bold="true" Font-Size="12px" OnClick="lnkIsAdmin_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblExamsPending" runat="server" Text="Exams Pending Rating" Font-Bold="true"
                                    Font-Size="16px" />
                                <br />
                                <asp:GridView ID="gvExamsPending" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    Font-Size="12px" PageSize="10" CellPadding="5" EmptyDataText="This user has no exams pending rating."
                                    OnPageIndexChanging="gvExamsPending_PageIndexChanging" Width="100%" OnRowCommand="gvExamsPending_RowCommand"
                                    AllowSorting="True" OnSorting="gvExamsPending_Sorting">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <PagerStyle CssClass="gridPager" HorizontalAlign="Center" ForeColor="Black" />
                                    <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NextPreviousFirstLast"
                                        NextPageText="&gt;" PreviousPageText="&lt;" />
                                    <RowStyle BorderColor="Black" />
                                    <Columns>
                                        <asp:BoundField DataField="TestCategoryID" HeaderText="Test ID" SortExpression="TestCategoryID" />
                                        <asp:BoundField DataField="TestTakenID" HeaderText="Test Taken ID" SortExpression="TestTakenID" />
                                        <asp:TemplateField HeaderText="Date Taken" SortExpression="DateStartTaken">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("DateStartTaken", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date Finished" SortExpression="DateEndTaken">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDateEndTaken" runat="server" Text='<%#Bind("DateEndTaken", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblExamsFinished" runat="server" Text="Exams Finished" Font-Bold="true"
                                    Font-Size="16px" />
                                <br />
                                <asp:GridView ID="gvExamsFinished" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    Font-Size="12px" PageSize="10" CellPadding="5" EmptyDataText="This user has not finished any exams."
                                    OnPageIndexChanging="gvExamsFinished_PageIndexChanging" Width="100%" OnRowCommand="gvExamsFinished_RowCommand"
                                    AllowSorting="True" OnSorting="gvExamsFinished_Sorting">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <PagerStyle CssClass="gridPager" HorizontalAlign="Center" ForeColor="Black" />
                                    <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NextPreviousFirstLast"
                                        NextPageText="&gt;" PreviousPageText="&lt;" />
                                    <RowStyle BorderColor="Black" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Test Name" SortExpression="TestName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestName" runat="server" Text='<%#Bind("TestName") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date Taken" SortExpression="StartDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("StartDate", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date Finished" SortExpression="EndDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDateEndTaken" runat="server" Text='<%#Bind("EndDate", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Image ID="imgPassOrFail" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                                <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                                    Visible="false" />
                                                <asp:LinkButton ID="lnkView" runat="server" Text="View" CommandName="View" CssClass="linkButtons"
                                                    ForeColor="black" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="Remove"
                                                    CssClass="linkButtons" ForeColor="black" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblExamsUnfinished" runat="server" Text="Exams Pending Completion"
                                    Font-Bold="true" Font-Size="16px" />
                                <br />
                                <asp:GridView ID="gvExamsUnfinished" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    Font-Size="12px" PageSize="10" CellPadding="5" EmptyDataText="This user has no unfinished exams."
                                    OnPageIndexChanging="gvExamsUnfinished_PageIndexChanging" Width="100%" OnRowCommand="gvExamsUnfinished_RowCommand"
                                    AllowSorting="True" OnSorting="gvExamsUnfinished_Sorting">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <PagerStyle CssClass="gridPager" HorizontalAlign="Center" ForeColor="Black" />
                                    <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NextPreviousFirstLast"
                                        NextPageText="&gt;" PreviousPageText="&lt;" />
                                    <RowStyle BorderColor="Black" />
                                    <Columns>
                                        <asp:BoundField DataField="TestName" HeaderText="Test Name" SortExpression="TestName">
                                            <ItemStyle Width="260px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Date Taken" SortExpression="StartDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDateStartTaken" runat="server" Text='<%#Bind("StartDate", "{0:yyyy/MM/dd hh:mm tt}") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date Last Saved" SortExpression="LastSave">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDateLastSave" runat="server" Text='<%#Bind("LastSave","{0:yyyy/MM/dd hh:mm tt}") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Left (mm:ss)" SortExpression="TimeRemaining">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTimeRemaining" runat="server" Text='<%#Bind("TimeRemaining") %>'
                                                    Visible="false" />
                                                <asp:Label ID="lblTimeRemainingVal" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                                <asp:Label ID="lblTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                                    Visible="false" />
                                                <asp:LinkButton ID="lnkView" runat="server" Text="View" CommandName="View" CssClass="linkButtons"
                                                    ForeColor="black" CommandArgument='<%#Bind("TestTakenID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblEssayExams" runat="server" Text="Essay Exams" Font-Bold="true"
                                    Font-Size="16px" />
                                <br />
                                <asp:GridView ID="gvEssays" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    Font-Size="12px" PageSize="10" CellPadding="5" EmptyDataText="This user has not finished any essay exams."
                                    OnPageIndexChanging="gvEssayExams_PageIndexChanging" Width="100%" OnRowCommand="gvEssays_RowCommand"
                                    BorderStyle="solid" BorderColor="black" AllowSorting="True" OnSorting="gvEssays_Sorting">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <PagerStyle CssClass="gridPager" HorizontalAlign="Center" ForeColor="Black" />
                                    <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NextPreviousFirstLast"
                                        NextPageText="&gt;" PreviousPageText="&lt;" />
                                    <RowStyle BorderColor="Black" />
                                    <Columns>
                                        <asp:BoundField DataField="TestTakenID" HeaderText="Test Taken ID" SortExpression="TestTakenID">
                                            <ItemStyle Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TestName" HeaderText="Test Name" SortExpression="TestName">
                                            <ItemStyle Width="300px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DateEndTaken" HeaderText="Date Finished" SortExpression="DateEndTaken" />
                                        <asp:TemplateField>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestTakenID" runat="server" Text='<%#Bind("TestTakenID") %>' Visible="false" />
                                                <asp:LinkButton ID="lnkView" runat="server" Text="View" CommandName="View" CssClass="linkButtons"
                                                    ForeColor="black" CommandArgument='<%#Bind("TestTakenID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle">
                                <asp:Button CssClass="buttons" ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:HiddenField ID="hidPending" runat="server" />
<asp:Panel ID="pnlPending" runat="server" HorizontalAlign="center" CssClass="modalPopup"
    Width="680px" ScrollBars="vertical" Height="600px">
    <table width="660px" cellpadding="5" border="1" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblStatsForUnfinishedExam" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblUnfinishedExamName" runat="server" />
                <br />
                <asp:Label ID="lblUnfinishedExamNameVal" runat="server" Font-Size="16px" Font-Bold="true" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblUnfinishedDateStart" runat="server" Text="Date Started:" />
                <br />
                <asp:Label ID="lblUnfinishedDateStartVal" runat="server" Font-Bold="true" />
            </td>
            <td>
                <asp:Label ID="lblUnfinishedDateLastSave" runat="server" Text="Date Last Saved:" />
                <br />
                <asp:Label ID="lblUnfinishedDatelastSaveVal" runat="server" Font-Bold="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvUnfinishedInfoItems" runat="server" AutoGenerateColumns="false"
                    AllowPaging="false" Width="100%" CellPadding="5" BorderColor="black">
                    <Columns>
                        <asp:TemplateField HeaderText="Item Type">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblItemType" runat="server" Text='<%#Bind("QType") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblQuestionnaireID" runat="server" Visible="false" Text='<%#Bind("QuestionID") %>' />
                                <asp:Label ID="lblQuestion" runat="server" Text='<%#Bind("Question") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Has Answered">
                            <ItemTemplate>
                                <asp:Label ID="lblSavedCount" runat="server" Text='<%#Bind("ResponseCount") %>' />
                                <asp:Label ID="lblAnswered" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="buttons" Width="80px" />
            </td>
            <td align="right">
                <asp:Button ID="btnEndExam" runat="server" Text="Grade Exam" CssClass="buttons" Width="80px" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpePending" runat="server" BackgroundCssClass="modalBackground"
    TargetControlID="hidPending" PopupControlID="pnlPending" />
<asp:HiddenField ID="hidClosePending" runat="server" />
<asp:Panel ID="pnlClosePending" runat="server" HorizontalAlign="center" CssClass="modalPopup"
    Width="680px" ScrollBars="vertical" Height="600px">
    <table width="660px" cellpadding="5">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblCloseWarning" runat="server" Text="Grade and close exam based on currently provided answers?" />
                <br />
                <asp:Label ID="lblCloseWarning2" runat="server" Text="User will not be able to edit exam answers afterwards." />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button ID="btnContinueClose" runat="server" Text="Continue" />
            </td>
            <td align="right">
                <asp:Button ID="btnContinueCancel" runat="server" Text="Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeClosePending" runat="server" BackgroundCssClass="modalBackground"
    TargetControlID="hidClosePending" PopupControlID="pnlClosePending" />
<asp:HiddenField ID="hidAnswers" runat="server" />
<asp:Panel ID="pnlAnswers" runat="server" CssClass="modalPopup" HorizontalAlign="center"
    Width="680px" ScrollBars="vertical" Height="600px">
    <table cellpadding="5" cellspacing="0" border="1" width="660px">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblResultsExamName" runat="server" Font-Size="16px" Font-Bold="true" />
            </td>
        </tr>
        <tr>
            <td style="width: 50%">
                <asp:Label ID="lblDateStarted" runat="server" Text="Date Started:" />
                <br />
                <asp:Label ID="lblDateStartedVal" runat="server" Font-Bold="true" />
            </td>
            <td style="width: 50%">
                <asp:Label ID="lblDateFinished" runat="server" Text="Date Finished:" />
                <br />
                <asp:Label ID="lblDateFinishedVal" runat="server" Font-Bold="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvExamResults" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                    AllowSorting="True" CellPadding="5" EmptyDataText="No questions were answered for this exam."
                    BorderColor="black" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Question">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %>'
                                    Visible="false" />
                                <asp:Label ID="lblTestResponseID" runat="server" Text='<%#Bind("TestResponseID") %>'
                                    Visible="false" />
                                <asp:Label ID="lblQuestion" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Image ID="imgResult" runat="server" />
                                <asp:Label ID="lblEssayScore" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnCloseSummary" runat="server" Text="Back" OnClick="btnCloseSummary_Click"
                    CssClass="buttons" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeAnswers" runat="server" BackgroundCssClass="modalBackground"
    TargetControlID="hidAnswers" PopupControlID="pnlAnswers" />
<asp:HiddenField ID="hidRightsPopup" runat="server" />
<asp:Panel ID="pnlRightsPopup" runat="server" CssClass="modalPopup" HorizontalAlign="center"
    Width="60%">
    <table width="100%">
        <tr>
            <td colspan="2">
                <wus:userRightsControl ID="usrRightsControl" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button ID="btnRightChangeOk" runat="server" Text="OK" CssClass="buttons" OnClick="btnRightChangeOk_Click" />
            </td>
            <td align="right">
                <asp:Button ID="btnRightChangeCancel" runat="server" Text="Cancel" CssClass="buttons" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeRightsPopup" runat="server" BackgroundCssClass="modalBackground"
    TargetControlID="hidRightsPopup" PopupControlID="pnlRightsPopup" />
<asp:HiddenField ID="hidEssays" runat="server" />
<asp:Panel ID="pnlEssays" runat="server" CssClass="modalPopup" HorizontalAlign="center"
    Width="60%">
    <table width="100%">
        <tr>
            <td>
                <asp:GridView ID="gvEssayInfo" runat="server" AutoGenerateColumns="false" Width="100%"
                    CellPadding="5" EmptyDataText="No essay questions were answered for this exam."
                    BorderColor="black">
                    <HeaderStyle HorizontalAlign="center" />
                    <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                    <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                        NextPageText=">" Position="Bottom" PreviousPageText="<" />
                    <RowStyle BorderColor="black" />
                    <Columns>
                        <asp:BoundField DataField="TestResponseID" HeaderText="Response ID" ItemStyle-Width="120px" />
                        <asp:BoundField DataField="Checker" HeaderText="Checker" />
                        <asp:BoundField DataField="DateChecked" HeaderText="Date Checked" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="buttons" />
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeEssays" runat="server" TargetControlID="hidEssays"
    PopupControlID="pnlEssays" BackgroundCssClass="modalBackground" />
