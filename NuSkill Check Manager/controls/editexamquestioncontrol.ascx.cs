using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class controls_editexamquestioncontrol : System.Web.UI.UserControl
{
    private string Qtype
    {
        get
        {
            return this.ViewState["qtype"] as string;
        }
        set
        {
            this.ViewState["qtype"] = value;
        }
    }

    private Dictionary<int, string> Buttons
    {
        get
        {
            if (this.ViewState["buttons"] == null)
            {
                this.ViewState["buttons"] = new Dictionary<int, string>();
            }
            return (Dictionary<int, string>)this.ViewState["buttons"];
        }
        set { this.ViewState["buttons"] = value; }
    }

    private List<string> Images
    {
        get
        {
            if (this.ViewState["images"] == null)
            {
                this.ViewState["images"] = new List<string>();
            }
            return (List<string>)this.ViewState["images"];
        }
        set { this.ViewState["images"] = value; }
    }

    private void InitDictionary()
    {
        for (int i = 1; i <= 10; i++)
        {
            ImageButton button = this.FindControl("imgChoice" + i.ToString()) as ImageButton;
            if (button != null)
                Buttons.Add(i, button.ID);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());
        if (!this.IsPostBack)
        {
            for (int i = 0; i < 10; i++)
                this.Images.Add(string.Empty);
            this.PopulateDropDownList();
            this.SetPanelVisibility();
            this.InitDictionary();
            #region Loading when Editing an Existing Question
            if (SessionManager.SessionQuestionAction == "editquestion")
            {
                Questionnaire question = SessionManager.SessionSingleQuestion;
                if (question != null)
                {
                    this.txtQuestion.Text = question.Question;
                    this.Qtype = question.TypeCode;
                    this.ddlItemtypes.SelectedIndex = -1;
                    foreach (ListItem item in this.ddlItemtypes.Items)
                        if (item.Value == question.TypeCode)
                        {
                            item.Selected = true;
                            SetPanelVisibility();
                            break;
                        }
                    if (question.TypeCode == "multiple")
                    {
                        this.txtChoice1.Text = string.IsNullOrEmpty(question.Choice1) ? string.Empty : question.Choice1.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice2.Text = string.IsNullOrEmpty(question.Choice2) ? string.Empty : question.Choice2.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice3.Text = string.IsNullOrEmpty(question.Choice3) ? string.Empty : question.Choice3.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice4.Text = string.IsNullOrEmpty(question.Choice4) ? string.Empty : question.Choice4.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice5.Text = string.IsNullOrEmpty(question.Choice5) ? string.Empty : question.Choice5.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice6.Text = string.IsNullOrEmpty(question.Choice6) ? string.Empty : question.Choice6.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice7.Text = string.IsNullOrEmpty(question.Choice7) ? string.Empty : question.Choice7.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice8.Text = string.IsNullOrEmpty(question.Choice8) ? string.Empty : question.Choice8.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice9.Text = string.IsNullOrEmpty(question.Choice9) ? string.Empty : question.Choice9.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtChoice10.Text = string.IsNullOrEmpty(question.Choice10) ? string.Empty : question.Choice10.Replace("\r", "").Replace("\n", "").Trim();
                        this.chkChoice1.Checked = string.IsNullOrEmpty(question.Ans1) ? false : true;
                        this.chkChoice2.Checked = string.IsNullOrEmpty(question.Ans2) ? false : true;
                        this.chkChoice3.Checked = string.IsNullOrEmpty(question.Ans3) ? false : true;
                        this.chkChoice4.Checked = string.IsNullOrEmpty(question.Ans4) ? false : true;
                        this.chkChoice5.Checked = string.IsNullOrEmpty(question.Ans5) ? false : true;
                        this.chkChoice6.Checked = string.IsNullOrEmpty(question.Ans6) ? false : true;
                        this.chkChoice7.Checked = string.IsNullOrEmpty(question.Ans7) ? false : true;
                        this.chkChoice8.Checked = string.IsNullOrEmpty(question.Ans8) ? false : true;
                        this.chkChoice9.Checked = string.IsNullOrEmpty(question.Ans9) ? false : true;
                        this.chkChoice10.Checked = string.IsNullOrEmpty(question.Ans10) ? false : true;
                    }
                    else if (question.TypeCode == "torf")
                    {
                        this.rdoTrue.Checked = question.Ans1.ToLower() == "true" ? true : false;
                        this.rdoFalse.Checked = question.Ans1.ToLower() == "false" ? true : false;
                    }
                    else if (question.TypeCode == "yorn")
                    {
                        this.rdoYes.Checked = question.Ans1.ToLower() == "yes" ? true : false;
                        this.rdoNo.Checked = question.Ans1.ToLower() == "no" ? true : false;
                    }
                    else if (question.TypeCode == "sequencing")
                    {
                        this.txtItemTextbox1.Text = question.Choice1.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox2.Text = question.Choice2.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox3.Text = question.Choice3.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox4.Text = question.Choice4.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox5.Text = question.Choice5.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox6.Text = question.Choice6.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox7.Text = question.Choice7.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox8.Text = question.Choice8.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox9.Text = question.Choice9.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtItemTextbox10.Text = question.Choice10.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox1.Text = question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox2.Text = question.Ans2.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox3.Text = question.Ans3.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox4.Text = question.Ans4.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox5.Text = question.Ans5.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox6.Text = question.Ans6.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox7.Text = question.Ans7.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox8.Text = question.Ans8.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox9.Text = question.Ans9.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtAnswerTextbox10.Text = question.Ans10.Replace("\r", "").Replace("\n", "").Trim();
                    }
                    else if (question.TypeCode == "fillblanks")
                    {
                        this.txtFill1.Text = question.Choice1.Replace("\r","").Replace("\n","").Trim();
                        this.txtFill2.Text = question.Choice2.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFill3.Text = question.Choice3.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFill4.Text = question.Choice4.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFill5.Text = question.Choice5.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFill6.Text = question.Choice6.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFill7.Text = question.Choice7.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFill8.Text = question.Choice8.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFill9.Text = question.Choice9.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFill10.Text = question.Choice10.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill1.Checked = question.Choice1.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill2.Checked = question.Choice2.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill3.Checked = question.Choice3.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill4.Checked = question.Choice4.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill5.Checked = question.Choice5.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill6.Checked = question.Choice6.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill7.Checked = question.Choice7.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill8.Checked = question.Choice8.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill9.Checked = question.Choice9.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoFill10.Checked = question.Choice10.Replace("\r", "").Replace("\n", "").Trim() == question.Ans1.Replace("\r", "").Replace("\n", "").Trim();

                        //this.txtAnswer.Text = question.Ans1;
                    }
                    else if (question.TypeCode == "matching")
                    {
                        string[] parts = new string[2];
                        this.txtMatchItem1.Text = string.IsNullOrEmpty(question.Choice1) ? string.Empty : question.Choice1.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem2.Text = string.IsNullOrEmpty(question.Choice2) ? string.Empty : question.Choice2.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem3.Text = string.IsNullOrEmpty(question.Choice3) ? string.Empty : question.Choice3.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem4.Text = string.IsNullOrEmpty(question.Choice4) ? string.Empty : question.Choice4.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem5.Text = string.IsNullOrEmpty(question.Choice5) ? string.Empty : question.Choice5.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem6.Text = string.IsNullOrEmpty(question.Choice6) ? string.Empty : question.Choice6.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem7.Text = string.IsNullOrEmpty(question.Choice7) ? string.Empty : question.Choice7.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem8.Text = string.IsNullOrEmpty(question.Choice8) ? string.Empty : question.Choice8.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem9.Text = string.IsNullOrEmpty(question.Choice9) ? string.Empty : question.Choice9.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchItem10.Text = string.IsNullOrEmpty(question.Choice10) ? string.Empty : question.Choice10.Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans1.Trim().Split('|');
                        this.txtCorrectMatch1.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice1.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans2.Trim().Split('|');
                        this.txtCorrectMatch2.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice2.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans3.Trim().Split('|');
                        this.txtCorrectMatch3.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice3.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans4.Trim().Split('|');
                        this.txtCorrectMatch4.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice4.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans5.Trim().Split('|');
                        this.txtCorrectMatch5.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice5.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans6.Trim().Split('|');
                        this.txtCorrectMatch6.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice6.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans7.Trim().Split('|');
                        this.txtCorrectMatch7.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice7.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans8.Trim().Split('|');
                        this.txtCorrectMatch8.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice8.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        parts = question.Ans9.Trim().Split('|');
                        this.txtCorrectMatch9.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice9.Text = parts[1].Trim();

                        parts = question.Ans10.Trim().Split('|');
                        this.txtCorrectMatch10.Text = parts[0].Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMatchChoice10.Text = parts[1].Replace("\r", "").Replace("\n", "").Trim();

                        #region Old
                        /*
                        for (int i = 1; i <= 10; i++)
                        {
                            TextBox txtMatchChoice = this.FindControl("txtMatchChoice" + i.ToString()) as TextBox;
                            Label lblMatchLetter = this.FindControl("lblMatchLetter" + i.ToString()) as Label;
                            if (!string.IsNullOrEmpty(question.Ans1))
                            {
                                parts = question.Ans1.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans2))
                            {
                                parts = question.Ans2.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans3))
                            {
                                parts = question.Ans3.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans4))
                            {
                                parts = question.Ans4.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans5))
                            {
                                parts = question.Ans5.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans6))
                            {
                                parts = question.Ans6.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans7))
                            {
                                parts = question.Ans7.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans8))
                            {
                                parts = question.Ans8.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans9))
                            {
                                parts = question.Ans9.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(question.Ans10))
                            {
                                parts = question.Ans10.Trim().Split('|');
                                if (parts[0].ToUpper() == lblMatchLetter.Text)
                                {
                                    txtMatchChoice.Text = parts[1];
                                    break;
                                }
                            }
                        }
                        */
                        #endregion
                    }
                    else if (question.TypeCode == "hotspot")
                    {
                        this.imgChoice1.ImageUrl = string.IsNullOrEmpty(question.Choice1) ? string.Empty : question.Choice1;
                        this.imgChoice2.ImageUrl = string.IsNullOrEmpty(question.Choice2) ? string.Empty : question.Choice2;
                        this.imgChoice3.ImageUrl = string.IsNullOrEmpty(question.Choice3) ? string.Empty : question.Choice3;
                        this.imgChoice4.ImageUrl = string.IsNullOrEmpty(question.Choice4) ? string.Empty : question.Choice4;
                        this.imgChoice5.ImageUrl = string.IsNullOrEmpty(question.Choice5) ? string.Empty : question.Choice5;
                        this.imgChoice6.ImageUrl = string.IsNullOrEmpty(question.Choice6) ? string.Empty : question.Choice6;
                        this.imgChoice7.ImageUrl = string.IsNullOrEmpty(question.Choice7) ? string.Empty : question.Choice7;
                        this.imgChoice8.ImageUrl = string.IsNullOrEmpty(question.Choice8) ? string.Empty : question.Choice8;
                        this.imgChoice9.ImageUrl = string.IsNullOrEmpty(question.Choice9) ? string.Empty : question.Choice9;
                        this.imgChoice10.ImageUrl = string.IsNullOrEmpty(question.Choice10) ? string.Empty : question.Choice10;
                        this.imgChoice1.BorderColor = string.IsNullOrEmpty(question.Ans1) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice2.BorderColor = string.IsNullOrEmpty(question.Ans2) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice3.BorderColor = string.IsNullOrEmpty(question.Ans3) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice4.BorderColor = string.IsNullOrEmpty(question.Ans4) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice5.BorderColor = string.IsNullOrEmpty(question.Ans5) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice6.BorderColor = string.IsNullOrEmpty(question.Ans6) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice7.BorderColor = string.IsNullOrEmpty(question.Ans7) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice8.BorderColor = string.IsNullOrEmpty(question.Ans8) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice9.BorderColor = string.IsNullOrEmpty(question.Ans9) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice10.BorderColor = string.IsNullOrEmpty(question.Ans10) ? System.Drawing.Color.Empty : System.Drawing.Color.Red;
                        this.imgChoice1.BorderWidth = string.IsNullOrEmpty(question.Ans1) ? 0 : 5;
                        this.imgChoice2.BorderWidth = string.IsNullOrEmpty(question.Ans2) ? 0 : 5;
                        this.imgChoice3.BorderWidth = string.IsNullOrEmpty(question.Ans3) ? 0 : 5;
                        this.imgChoice4.BorderWidth = string.IsNullOrEmpty(question.Ans4) ? 0 : 5;
                        this.imgChoice5.BorderWidth = string.IsNullOrEmpty(question.Ans5) ? 0 : 5;
                        this.imgChoice6.BorderWidth = string.IsNullOrEmpty(question.Ans6) ? 0 : 5;
                        this.imgChoice7.BorderWidth = string.IsNullOrEmpty(question.Ans7) ? 0 : 5;
                        this.imgChoice8.BorderWidth = string.IsNullOrEmpty(question.Ans8) ? 0 : 5;
                        this.imgChoice9.BorderWidth = string.IsNullOrEmpty(question.Ans9) ? 0 : 5;
                        this.imgChoice10.BorderWidth = string.IsNullOrEmpty(question.Ans10) ? 0 : 5;
                        for (int i = 0; i < 10; i++)
                        {
                            int t = i + 1;
                            ImageButton button = this.FindControl("imgChoice" + t.ToString()) as ImageButton;
                            HtmlTableCell cell = this.FindControl("imgCell" + t.ToString()) as HtmlTableCell;
                            if (!string.IsNullOrEmpty(button.ImageUrl))
                            {
                                this.Images[i] = button.ImageUrl;
                                cell.Visible = true;
                            }
                            else
                                cell.Visible = false;
                        }
                    }
                    else if (question.TypeCode == "essay")
                    {
                        this.txtEssayScore.Text = question.EssayMaxScore.ToString();
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (question.TypeCode == "rate")
                    {
                        this.txtFillRate1.Text = question.Choice1.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate2.Text = question.Choice2.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate3.Text = question.Choice3.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate4.Text = question.Choice4.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate5.Text = question.Choice5.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate6.Text = question.Choice6.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate7.Text = question.Choice7.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate8.Text = question.Choice8.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate9.Text = question.Choice9.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillRate10.Text = question.Choice10.Replace("\r", "").Replace("\n", "").Trim();
                        this.rdoRate1.Checked = false;
                        this.rdoRate2.Checked = false;
                        this.rdoRate3.Checked = false;
                        this.rdoRate4.Checked = false;
                        this.rdoRate5.Checked = false;
                        this.rdoRate6.Checked = false;
                        this.rdoRate7.Checked = false;
                        this.rdoRate8.Checked = false;
                        this.rdoRate9.Checked = false;
                        this.rdoRate10.Checked = false;
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (question.TypeCode == "comments")
                    {
                        this.txtComments.Text = string.Empty;
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (question.TypeCode == "dropdown")
                    {
                        this.txtFillDD1.Text = question.Choice1.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD2.Text = question.Choice2.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD3.Text = question.Choice3.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD4.Text = question.Choice4.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD5.Text = question.Choice5.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD6.Text = question.Choice6.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD7.Text = question.Choice7.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD8.Text = question.Choice8.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD9.Text = question.Choice9.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtFillDD10.Text = question.Choice10.Replace("\r", "").Replace("\n", "").Trim();
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (question.TypeCode == "masurvey")
                    {
                        this.txtMA1.Text = string.IsNullOrEmpty(question.Choice1) ? string.Empty : question.Choice1.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA2.Text = string.IsNullOrEmpty(question.Choice2) ? string.Empty : question.Choice2.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA3.Text = string.IsNullOrEmpty(question.Choice3) ? string.Empty : question.Choice3.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA4.Text = string.IsNullOrEmpty(question.Choice4) ? string.Empty : question.Choice4.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA5.Text = string.IsNullOrEmpty(question.Choice5) ? string.Empty : question.Choice5.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA6.Text = string.IsNullOrEmpty(question.Choice6) ? string.Empty : question.Choice6.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA7.Text = string.IsNullOrEmpty(question.Choice7) ? string.Empty : question.Choice7.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA8.Text = string.IsNullOrEmpty(question.Choice8) ? string.Empty : question.Choice8.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA9.Text = string.IsNullOrEmpty(question.Choice9) ? string.Empty : question.Choice9.Replace("\r", "").Replace("\n", "").Trim();
                        this.txtMA10.Text = string.IsNullOrEmpty(question.Choice10) ? string.Empty : question.Choice10.Replace("\r", "").Replace("\n", "").Trim();

                        this.chkMA1.Checked = false;
                        this.chkMA2.Checked = false;
                        this.chkMA3.Checked = false;
                        this.chkMA4.Checked = false;
                        this.chkMA5.Checked = false;
                        this.chkMA6.Checked = false;
                        this.chkMA7.Checked = false;
                        this.chkMA8.Checked = false;
                        this.chkMA9.Checked = false;
                        this.chkMA10.Checked = false;
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (question.TypeCode == "calendar")
                    {
                        txtDate.Text = string.Empty;
                    }
                }
                else
                {
                    Response.Redirect("~/exam.aspx?" + Request.QueryString.ToString());
                }
            }
            #endregion
        }
    }

    private void PopulateDropDownList()
    {
        try
        {
            this.ddlItemtypes.Items.Clear();
            QuestionType[] questions = QuestionType.SelectAll();

            for (int i = 0; i < questions.Length; i++)
            {
                if (SessionManager.SessionQuestionAction == "suggestquestion")
                {
                    if (questions[i].TypeCode != "hotspot")
                        this.ddlItemtypes.Items.Add(new ListItem(questions[i].Description, questions[i].TypeCode));
                }
                else
                    this.ddlItemtypes.Items.Add(new ListItem(questions[i].Description, questions[i].TypeCode));
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "editexamquestioncontrol.ascx", "PopulateDropDownList", ex.Message);
        }
        //this.ddlItemtypes.Items.Add(new ListItem("Multiple Choice", "multiple"));
        //this.ddlItemtypes.Items.Add(new ListItem("True or False", "torf"));
        //this.ddlItemtypes.Items.Add(new ListItem("Sequencing", "sequencing"));
        //this.ddlItemtypes.Items.Add(new ListItem("Fill In The Blanks", "fillblanks"));
        //this.ddlItemtypes.Items.Add(new ListItem("Essay", "essay"));
        //this.ddlItemtypes.Items.Add(new ListItem("Hotspot", "hotspot"));
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(this.fupHotspot.FileName.Trim()))
            {
                this.cvNoImage.IsValid = false;
                return;
            }

            string virtualLocation = HttpContext.Current.Request.Path.Substring(0, HttpContext.Current.Request.Path.LastIndexOf("/")) + "/hotspotimages/";
            //string location = Server.MapPath(HttpContext.Current.Request.Path.Substring(0, HttpContext.Current.Request.Path.LastIndexOf("/")) + "/images/itemimages/");
            virtualLocation = virtualLocation.Replace("Manager", "Check");
            string location = Config.HotspotImageLocation();
            //virtualLocation = virtualLocation.Replace('/', '\\');
            if (!File.Exists(location + fupHotspot.FileName.Trim()))
            {
                int ctr = 0;
                foreach (string image in this.Images)
                    if (!string.IsNullOrEmpty(image))
                        ctr++;

                if (ctr < 10)
                {
                    string filepath = location + fupHotspot.FileName.Trim();
                    string virtualFilepath = virtualLocation + fupHotspot.FileName.Trim();
                    fupHotspot.SaveAs(filepath);

                    for (int i = 0; i < 10; i++)
                        if (this.Images[i] == string.Empty)
                        {
                            this.Images[i] = virtualFilepath;
                            break;
                        }
                    //this.Images.Add(virtualFilepath);
                    this.BindImages();
                }
                else
                    this.cvTooMany.IsValid = false;
            }
            else
                this.cvImageExists.IsValid = false;
        }
        catch (Exception ex)
        {
            //using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            //{
            //    service.WriteError(Config.ApplicationID(), "editexamquestioncontrol.ascx", "btnAdd_Click", ex.Message);
            //}
        }
    }

    protected void imgClick(object sender, ImageClickEventArgs e)
    {
        ImageButton button = (ImageButton)sender;
        if (button.BorderWidth.Value > 0)
        {
            button.BorderColor = System.Drawing.Color.Empty;
            button.BorderWidth = 0;
        }
        else
        {
            button.BorderColor = System.Drawing.Color.Red;
            button.BorderWidth = 5;
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            Questionnaire questionnaire = new Questionnaire();
            if (DetermineValidity())
            {
                try
                {
                    if (this.Qtype == "multiple")
                    {
                        questionnaire = new Questionnaire
                        (
                            "multiple",
                            this.txtQuestion.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice1.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice2.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice3.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice4.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice5.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice6.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice7.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice8.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice9.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtChoice10.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.chkChoice1.Checked ? this.txtChoice1.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice2.Checked ? this.txtChoice2.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice3.Checked ? this.txtChoice3.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice4.Checked ? this.txtChoice4.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice5.Checked ? this.txtChoice5.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice6.Checked ? this.txtChoice6.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice7.Checked ? this.txtChoice7.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice8.Checked ? this.txtChoice8.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice9.Checked ? this.txtChoice9.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.chkChoice10.Checked ? this.txtChoice10.Text.Replace("\r","").Replace("\n","").Trim() : string.Empty,
                            this.txtHyperlink.Text.Replace("\r","").Replace("\n","").Trim()
                        );
                    }
                    else if (this.Qtype == "torf")
                    {
                        questionnaire = new Questionnaire
                        (
                            "torf",
                            this.txtQuestion.Text.Replace("\r","").Replace("\n","").Trim(),
                            "True",
                            "False",
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.rdoTrue.Checked.ToString(),
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.txtHyperlink.Text.Replace("\r","").Replace("\n","").Trim()
                        );
                    }
                    else if (this.Qtype == "yorn")
                    {
                        questionnaire = new Questionnaire
                        (
                            "yorn",
                            this.txtQuestion.Text.Replace("\r","").Replace("\n","").Trim(),
                            "Yes",
                            "No",
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.rdoYes.Checked ? "Yes" : "No",
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.txtHyperlink.Text.Replace("\r","").Replace("\n","").Trim()
                        );
                    }
                    else if (this.Qtype == "sequencing")
                    {
                        questionnaire = new Questionnaire
                        (
                            "sequencing",
                            this.txtQuestion.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox1.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox2.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox3.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox4.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox5.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox6.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox7.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox8.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox9.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtItemTextbox10.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox1.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox2.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox3.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox4.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox5.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox6.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox7.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox8.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox9.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtAnswerTextbox10.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtHyperlink.Text.Replace("\r","").Replace("\n","").Trim()
                        );
                    }
                    else if (this.Qtype == "fillblanks")
                    {
                        questionnaire = new Questionnaire
                        (
                            "fillblanks",
                            this.txtQuestion.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill1.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill2.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill3.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill4.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill5.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill6.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill7.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill8.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill9.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtFill10.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.rdoFill1.Checked ? this.txtFill1.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.rdoFill2.Checked ? this.txtFill2.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.rdoFill3.Checked ? this.txtFill3.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.rdoFill4.Checked ? this.txtFill4.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.rdoFill5.Checked ? this.txtFill5.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.rdoFill6.Checked ? this.txtFill6.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.rdoFill7.Checked ? this.txtFill7.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.rdoFill8.Checked ? this.txtFill8.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.rdoFill9.Checked ? this.txtFill9.Text.Replace("\r","").Replace("\n","").Trim() :
                            this.txtFill10.Text.Replace("\r","").Replace("\n","").Trim(),
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.txtHyperlink.Text.Replace("\r","").Replace("\n","").Trim()
                        );
                    }
                    else if (this.Qtype == "matching")
                    {
                        questionnaire = new Questionnaire
                        (
                            "matching",
                            this.txtQuestion.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem1.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem2.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem3.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem4.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem5.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem6.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem7.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem8.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem9.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtMatchItem10.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch1.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice1.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch2.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice2.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch3.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice3.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch4.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice4.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch5.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice5.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch6.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice6.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch7.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice7.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch8.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice8.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch9.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice9.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtCorrectMatch10.Text.Replace("\r","").Replace("\n","").Trim().ToUpper() + "|" + this.txtMatchChoice10.Text.Replace("\r","").Replace("\n","").Trim(),
                            this.txtHyperlink.Text.Replace("\r","").Replace("\n","").Trim()
                        );
                    }
                    else if (this.Qtype == "essay")
                    {
                        questionnaire = new Questionnaire
                        (
                            "essay",
                            this.txtQuestion.Text.Replace("\r","").Replace("\n","").Trim(),
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            Convert.ToInt32(this.txtEssayScore.Text.Replace("\r","").Replace("\n","").Trim()),
                            this.txtHyperlink.Text.Replace("\r","").Replace("\n","").Trim()
                        );
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (this.Qtype == "rate")
                    {
                        questionnaire = new Questionnaire
                        (
                            "rate",
                            this.txtQuestion.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate1.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate2.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate3.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate4.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate5.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate6.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate7.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate8.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate9.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillRate10.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.rdoRate1.Checked ? this.txtFillRate1.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.rdoRate2.Checked ? this.txtFillRate2.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.rdoRate3.Checked ? this.txtFillRate3.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.rdoRate4.Checked ? this.txtFillRate4.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.rdoRate5.Checked ? this.txtFillRate5.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.rdoRate6.Checked ? this.txtFillRate6.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.rdoRate7.Checked ? this.txtFillRate7.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.rdoRate8.Checked ? this.txtFillRate8.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.rdoRate9.Checked ? this.txtFillRate9.Text.Replace("\r", "").Replace("\n", "").Trim() :
                            this.txtFillRate10.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.txtHyperlink.Text.Replace("\r", "").Replace("\n", "").Trim()
                        );
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (this.Qtype == "comments")
                    {
                        questionnaire = new Questionnaire
                        (
                            "comments",
                            this.txtQuestion.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.txtHyperlink.Text.Replace("\r", "").Replace("\n", "").Trim()
                        );
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (this.Qtype == "dropdown")
                    {
                        questionnaire = new Questionnaire
                        (
                            "dropdown",
                            this.txtQuestion.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD1.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD2.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD3.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD4.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD5.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD6.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD7.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD8.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD9.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtFillDD10.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.txtHyperlink.Text.Replace("\r", "").Replace("\n", "").Trim()
                        );
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (this.Qtype == "masurvey")
                    {
                        questionnaire = new Questionnaire
                        (
                            "masurvey",
                            this.txtQuestion.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA1.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA2.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA3.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA4.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA5.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA6.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA7.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA8.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA9.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.txtMA10.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            this.chkMA1.Checked ? this.txtMA1.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA2.Checked ? this.txtMA2.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA3.Checked ? this.txtMA3.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA4.Checked ? this.txtMA4.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA5.Checked ? this.txtMA5.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA6.Checked ? this.txtMA6.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA7.Checked ? this.txtMA7.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA8.Checked ? this.txtMA8.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA9.Checked ? this.txtMA9.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.chkMA10.Checked ? this.txtMA10.Text.Replace("\r", "").Replace("\n", "").Trim() : string.Empty,
                            this.txtHyperlink.Text.Replace("\r", "").Replace("\n", "").Trim()
                        );
                    }
                    //ADDED BY RAYMARK COSME <01/31/2018>
                    else if (this.Qtype == "calendar")
                    {
                        questionnaire = new Questionnaire
                        (
                            "calendar",
                            this.txtQuestion.Text.Replace("\r", "").Replace("\n", "").Trim(),
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            this.txtHyperlink.Text.Replace("\r", "").Replace("\n", "").Trim()
                        );
                    }
                    else if (this.Qtype == "hotspot")
                    {
                        questionnaire = new Questionnaire
                        (
                            "hotspot",
                            this.txtQuestion.Text.Trim(),
                            string.IsNullOrEmpty(this.Images[0]) ? string.Empty : this.Images[0],
                            string.IsNullOrEmpty(this.Images[1]) ? string.Empty : this.Images[1],
                            string.IsNullOrEmpty(this.Images[2]) ? string.Empty : this.Images[2],
                            string.IsNullOrEmpty(this.Images[3]) ? string.Empty : this.Images[3],
                            string.IsNullOrEmpty(this.Images[4]) ? string.Empty : this.Images[4],
                            string.IsNullOrEmpty(this.Images[5]) ? string.Empty : this.Images[5],
                            string.IsNullOrEmpty(this.Images[6]) ? string.Empty : this.Images[6],
                            string.IsNullOrEmpty(this.Images[7]) ? string.Empty : this.Images[7],
                            string.IsNullOrEmpty(this.Images[8]) ? string.Empty : this.Images[8],
                            string.IsNullOrEmpty(this.Images[9]) ? string.Empty : this.Images[9],
                            this.imgChoice1.BorderColor == System.Drawing.Color.Red ? this.Images[0] : string.Empty,
                            this.imgChoice2.BorderColor == System.Drawing.Color.Red ? this.Images[1] : string.Empty,
                            this.imgChoice3.BorderColor == System.Drawing.Color.Red ? this.Images[2] : string.Empty,
                            this.imgChoice4.BorderColor == System.Drawing.Color.Red ? this.Images[3] : string.Empty,
                            this.imgChoice5.BorderColor == System.Drawing.Color.Red ? this.Images[4] : string.Empty,
                            this.imgChoice6.BorderColor == System.Drawing.Color.Red ? this.Images[5] : string.Empty,
                            this.imgChoice7.BorderColor == System.Drawing.Color.Red ? this.Images[6] : string.Empty,
                            this.imgChoice8.BorderColor == System.Drawing.Color.Red ? this.Images[7] : string.Empty,
                            this.imgChoice9.BorderColor == System.Drawing.Color.Red ? this.Images[8] : string.Empty,
                            this.imgChoice10.BorderColor == System.Drawing.Color.Red ? this.Images[9] : string.Empty,
                            this.txtHyperlink.Text.Trim()
                        );
                    }
                    questionnaire.LastUpdated = SessionManager.SessionUsername;
                }
                catch
                {
                    ///TODO:Catch error when page can't initialize a NuSkill.Business.Questionnaire from the data
                }
                if (SessionManager.SessionQuestionAction == "editquestion" && SessionManager.SessionSingleQuestionIndex > -1)
                {
                    try
                    {
                        questionnaire.QuestionnaireID = SessionManager.SessionQuestionnaire[SessionManager.SessionSingleQuestionIndex].QuestionnaireID;
                        questionnaire.TestCategoryID = SessionManager.SessionQuestionnaire[SessionManager.SessionSingleQuestionIndex].TestCategoryID;
                        SessionManager.SessionQuestionnaire[SessionManager.SessionSingleQuestionIndex] = questionnaire;
                        SessionManager.SessionSingleQuestion = null;
                        SessionManager.SessionSingleQuestionIndex = -1;
                        questionnaire.Update();
                    }
                    catch
                    {
                        ///TODO:Catch error when page can't overwrite an existing Questionnaire 
                    }
                    SessionManager.SessionQuestionAction = null;
                    Response.Redirect("~/editexam.aspx?" + Request.QueryString.ToString());
                }
                else if (SessionManager.SessionQuestionAction == "newquestion")
                {
                    SessionManager.SessionSingleQuestion = null;
                    questionnaire.TestCategoryID = SessionManager.SessionTestCategoryID;
                    questionnaire.QuestionnaireID = questionnaire.Insert();
                    SessionManager.SessionQuestionnaire.Add(questionnaire);
                    questionnaire.Update();
                    SessionManager.SessionQuestionAction = null;
                    Response.Redirect("~/editexam.aspx?" + Request.QueryString.ToString());
                }
                else if (SessionManager.SessionQuestionAction == "suggestquestion")
                {
                    questionnaire.TestCategoryID = 0;
                    int x = questionnaire.Insert();
                    RecommendedQuestion question = new RecommendedQuestion();
                    question.CreatedBy = SessionManager.SessionUsername;
                    question.QuestionnaireID = x;
                    question.TestCategoryID = SessionManager.SessionRecTestCategoryID;
                    question.CreatorComments = SessionManager.SessionCreatorComments;
                    question.Insert();
                    SessionManager.SessionQuestionAction = null;
                    Response.Redirect("~/suggestnew.aspx?" + Request.QueryString.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            //using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            //{
            //    service.WriteError(Config.ApplicationID(), "editexamquestioncontrol.ascx", "btnContinue_Click", ex.Message);
            //}
        }
    }

    protected void ddlItemtypes_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetPanelVisibility();
    }

    private void SetPanelVisibility()
    {
        this.Qtype = this.ddlItemtypes.SelectedItem.Value;
        if (this.ddlItemtypes.SelectedItem.Value == "multiple")
        {
            this.pnlMultipleChoice.Visible = true;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        else if (this.ddlItemtypes.SelectedItem.Value == "torf")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = true;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        else if (this.ddlItemtypes.SelectedItem.Value == "sequencing")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = true;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        else if (this.ddlItemtypes.SelectedItem.Value == "fillblanks")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = true;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        else if (this.ddlItemtypes.SelectedItem.Value == "essay")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = true;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        else if (this.ddlItemtypes.SelectedItem.Value == "hotspot")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = true;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        else if (this.ddlItemtypes.SelectedItem.Value == "matching")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = true;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        else if (this.ddlItemtypes.SelectedItem.Value == "yorn")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = true;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        //<ADDED BY RAYMARK COSME <01/31/2018>
        else if (this.ddlItemtypes.SelectedItem.Value == "rate")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = true;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        //<ADDED BY RAYMARK COSME <01/31/2018>
        else if (this.ddlItemtypes.SelectedItem.Value == "comments")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = true;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        //<ADDED BY RAYMARK COSME <01/31/2018>
        else if (this.ddlItemtypes.SelectedItem.Value == "dropdown")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = true;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = false;
        }
        //<ADDED BY RAYMARK COSME <01/31/2018>
        else if (this.ddlItemtypes.SelectedItem.Value == "masurvey")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = true;
            this.pnlCalendar.Visible = false;
        }
        //<ADDED BY RAYMARK COSME <01/31/2018>
        else if (this.ddlItemtypes.SelectedItem.Value == "calendar")
        {
            this.pnlMultipleChoice.Visible = false;
            this.pnlTrueOrFalse.Visible = false;
            this.pnlSequencing.Visible = false;
            this.pnlFillInTheBlanks.Visible = false;
            this.pnlEssay.Visible = false;
            this.pnlHotspot.Visible = false;
            this.pnlMatchingType.Visible = false;
            this.pnlYesOrno.Visible = false;
            this.pnlRate.Visible = false;
            this.pnlComments.Visible = false;
            this.pnlDropDown.Visible = false;
            this.pnlMultipleAnswer.Visible = false;
            this.pnlCalendar.Visible = true;
        }
    }

    private void BindImages()
    {
        try
        {
            for (int i = 0; i < this.Images.Count; i++)
            {
                int x = i + 1;
                ImageButton tempButton = this.FindControl("imgChoice" + x.ToString()) as ImageButton;
                HtmlTableCell imgCell = this.FindControl("imgCell" + x.ToString()) as HtmlTableCell;
                if (!string.IsNullOrEmpty(this.Images[i]))
                {
                    if (tempButton != null)
                    {
                        //this.Buttons.Add(i, tempButton.ID);
                        tempButton.ImageUrl = this.Images[i];
                        imgCell.Visible = true;
                        tempButton.BorderColor = System.Drawing.Color.Empty;
                        tempButton.BorderWidth = 0;
                    }
                }
                else
                    imgCell.Visible = false;
            }
        }
        catch (Exception ex)
        {
            //using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            //{
            //    service.WriteError(Config.ApplicationID(), "editexamquestioncontrol.ascx", "BindImages", ex.Message);
            //}
            ///TODO:Catch error when page can't bind images to image list
        }
    }

    protected void RemoveImage(object sender, EventArgs e)
    {
        try
        {
            Button button = (Button)sender;
            string subpart = button.ID.Substring(9, button.ID.Length - 9);
            int index = Convert.ToInt32(subpart);
            ImageButton imgButton = this.FindControl("imgChoice" + index.ToString()) as ImageButton;
            HtmlTableCell cell = this.FindControl("imgCell" + this.Images.Count.ToString()) as HtmlTableCell;
            List<string> newList = new List<string>();
            if (button != null)
            {
                string imageUrl = imgButton.ImageUrl;
                foreach (string str in this.Images)
                    if (str == imageUrl)
                    {
                        this.Images.Remove(str);
                        this.Images.Add(string.Empty);
                        break;
                    }
                //for (int i = 0; i < 10; i++)
                //{
                //    newList.Add(string.Empty);
                //}
                //foreach (string str in this.Images)
                //{
                //    if (!string.IsNullOrEmpty(str))
                //        for (int i = 0; i < 10; i++)
                //            if (string.IsNullOrEmpty(newList[i]))
                //            {
                //                newList[i] = str;
                //                break;
                //            }
                //}
                //for (int i = 0; i < 10; i++)
                //{
                //    this.Images[i] = newList[i];
                //}
                BindImages();
                if (cell != null)
                    cell.Visible = false;
            }
        }
        catch (Exception ex)
        {
            //using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            //{
            //    service.WriteError(Config.ApplicationID(), "editexamquestioncontrol.ascx", "RemoveImage", ex.Message);
            //}
            ///TODO:Try error when page can't remove an image from a list
        }
    }

    protected bool DetermineValidity()
    {
        try
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(this.txtQuestion.Text.Trim()))
            {
                this.cvNoQuestion.IsValid = false;
                isValid = false;
            }
            if (this.Qtype == "multiple")
            {
                List<string> answers = new List<string>();
                bool blankFound = false;
                int totChoice = 0, totAns = 0;
                for (int i = 1; i <= 10; i++)
                {
                    string tempChoice = "txtChoice" + i.ToString();
                    string tempAns = "chkChoice" + i.ToString();
                    TextBox txtChoice = this.FindControl(tempChoice) as TextBox;
                    CheckBox chkAns = this.FindControl(tempAns) as CheckBox;
                    if (!string.IsNullOrEmpty(txtChoice.Text.Trim()))
                    {
                        if (answers.Contains(txtChoice.Text.Trim()))
                        {
                            this.cvDuplicateMultipleChoice.IsValid = false;
                            isValid = false;
                        }
                        else
                            answers.Add(txtChoice.Text.Trim());
                        totChoice++;
                        if (blankFound)
                        {
                            this.cvItemSkipMultipleChoice.IsValid = false;
                            isValid = false;
                        }
                    }
                    else
                        blankFound = true;
                    if (chkAns.Checked)
                    {
                        if (string.IsNullOrEmpty(txtChoice.Text.Trim()))
                        {
                            this.cvCorrectBlankMultipleChoice.IsValid = false;
                            isValid = false;
                        }
                        else
                            totAns++;
                    }
                }
                if (totChoice < 2 || totAns > totChoice || totAns < 1)
                {
                    this.cvNoOptionsMultipleChoice.IsValid = false;
                    isValid = false;
                }
            }
            else if (this.Qtype == "torf")
            {
                if (!this.rdoFalse.Checked && !this.rdoTrue.Checked)
                {
                    this.cvNoOptionsTrueOrFalse.IsValid = false;
                    isValid = false;
                }
            }
            else if (this.Qtype == "yorn")
            {
                if (!this.rdoYes.Checked && !this.rdoNo.Checked)
                {
                    this.cvNoOptionsYesOrNo.IsValid = false;
                    isValid = false;
                }
            }
            else if (this.Qtype == "sequencing")
            {
                List<string> listItems = new List<string>(), listAnswers = new List<string>();
                bool ansFound = false, questionFound = false;
                int totItems = 0, totAns = 0;
                for (int i = 10; i >= 1; i--)
                {
                    //int tempi = i - 1;
                    //string tempPreviousChoice = "txtItemTextbox" + tempi.ToString();
                    //string tempPreviousAns = "txtAnswerTextbox" + tempi.ToString();

                    string tempChoice = "txtItemTextbox" + i.ToString();
                    string tempAns = "txtAnswerTextbox" + i.ToString();

                    TextBox txtChoice = this.FindControl(tempChoice) as TextBox;
                    TextBox txtAns = this.FindControl(tempAns) as TextBox;

                    if (i != 10)
                    {
                        //check if there are blanks skipped
                        if (string.IsNullOrEmpty(txtChoice.Text.Trim()) && questionFound)
                        {
                            this.cvItemSkipSequencing.IsValid = false;
                            isValid = false;
                        }
                        if (string.IsNullOrEmpty(txtAns.Text.Trim()) && ansFound)
                        {
                            this.cvItemSkipSequencing.IsValid = false;
                            isValid = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(txtChoice.Text.Trim()))
                    {
                        if (listItems.Contains(txtChoice.Text.Trim()))
                        {
                            this.cvDuplicateSequencing.IsValid = false;
                            isValid = false;
                        }
                        else
                            listItems.Add(txtChoice.Text.Trim());
                        questionFound = true;
                        totItems++;
                    }
                    if (!string.IsNullOrEmpty(txtAns.Text.Trim()))
                    {
                        if (listAnswers.Contains(txtAns.Text.Trim()))
                        {
                            this.cvDuplicateSequencing.IsValid = false;
                            isValid = false;
                        }
                        else
                            listAnswers.Add(txtAns.Text.Trim());
                        ansFound = true;
                        totAns++;

                    }
                }
                //check if there are two or more questions
                if (totAns < 2 || totItems < 2)
                {
                    this.cvNoOptionsSequencing.IsValid = false;
                    isValid = false;
                }

                //check if there is an equal number of questions and answers
                if (totAns != totItems)
                {
                    this.cvCountMismatchSequencing.IsValid = false;
                    isValid = false;
                }

                //check if the items of the two columns are the same
                {
                    if (!this.cvCountMismatchSequencing.IsValid || !this.cvNoOptionsSequencing.IsValid || !cvDuplicateFillBlanks.IsValid)
                    {
                        isValid = false;
                    }
                    else
                    {
                        listAnswers.Sort();
                        listItems.Sort();
                        for (int i = 0; i < listItems.Count; i++)
                        {
                            if (listItems[i] != listAnswers[i])
                            {
                                this.cvOptionMismatchSequencing.IsValid = false;
                                isValid = false;
                            }
                        }
                    }
                }
            }
            else if (this.Qtype == "fillblanks")
            {
                int totChoices = 0;
                int totAns = 0;
                bool blankFound = false;
                List<string> answers = new List<string>();
                for (int i = 1; i <= 10; i++)
                {
                    string fillChoice = "txtFill" + i.ToString();
                    string fillAns = "rdoFill" + i.ToString();
                    TextBox txtFill = this.FindControl(fillChoice) as TextBox;
                    RadioButton rdoFill = this.FindControl(fillAns) as RadioButton;
                    if (!string.IsNullOrEmpty(txtFill.Text.Trim()))
                    {
                        if (answers.Contains(txtFill.Text.Trim()))
                        {
                            this.cvDuplicateFillBlanks.IsValid = false;
                            isValid = false;
                        }
                        else
                            answers.Add(txtFill1.Text.Trim());
                        totChoices++;
                        if (blankFound)
                        {
                            this.cvNoSkipFillBlanks.IsValid = false;
                            isValid = false;
                        }
                    }
                    else
                    {
                        blankFound = true;
                        if (i == 1)
                        {
                            this.cvNoSkipFillBlanks.IsValid = false;
                            isValid = false;
                        }
                    }
                    if (rdoFill.Checked)
                    {
                        if (string.IsNullOrEmpty(txtFill.Text.Trim()))
                        {
                            this.cvNoChoiceFillBlanks.IsValid = false;
                            isValid = false;
                        }
                        else
                            totAns++;
                    }
                }
            }
            else if (this.Qtype == "hotspot")
            {
                int viscount = 0;
                for (int i = 1; i <= 10; i++)
                {
                    ImageButton button = this.FindControl("imgChoice" + i.ToString()) as ImageButton;
                    if (button.Visible)
                        viscount++;
                }
                int borderCount = 0;
                foreach (string buttonVal in this.Buttons.Values)
                {
                    ImageButton button = this.FindControl(buttonVal) as ImageButton;
                    if (button.BorderColor == System.Drawing.Color.Red)
                        borderCount++;
                }
                if (borderCount < 1 || viscount < 2)
                {
                    this.cvNoOptionsHotspot.IsValid = false;
                    isValid = false;
                }
            }
            else if (this.Qtype == "essay")
            {
                int temp;
                if (!int.TryParse(this.txtEssayScore.Text.Trim(), out temp))
                {
                    this.cvNoScoreEssay.IsValid = false;
                    isValid = false;
                }
            }
            else if (this.Qtype == "matching")
            {
                List<string> listItems = new List<string>(), listChoices = new List<string>(), listLetters = new List<string>(), listCorrectMatches = new List<string>();
                bool ansFound = false, questionFound = false, letterFound = false;
                int totItems = 0, totAns = 0, totLetters = 0;
                for (int i = 10; i >= 1; i--)
                {
                    //int tempi = i - 1;
                    //string tempPreviousChoice = "txtItemTextbox" + tempi.ToString();
                    //string tempPreviousAns = "txtAnswerTextbox" + tempi.ToString();

                    string tempChoice = "txtMatchChoice" + i.ToString();
                    string tempItem = "txtMatchItem" + i.ToString();
                    string tempLetter = "lblMatchLetter" + i.ToString();
                    string tempCorrect = "txtCorrectMatch" + i.ToString();

                    TextBox txtChoice = this.FindControl(tempChoice) as TextBox;
                    TextBox txtItem = this.FindControl(tempItem) as TextBox;
                    TextBox txtCorrect = this.FindControl(tempCorrect) as TextBox;
                    Label txtLetter = this.FindControl(tempLetter) as Label;

                    if (i != 10)
                    {
                        //check if there are blanks skipped
                        if (string.IsNullOrEmpty(txtChoice.Text.Trim()) && questionFound)
                        {
                            this.cvItemSkipMatching.IsValid = false;
                            isValid = false;
                        }
                        if (string.IsNullOrEmpty(txtItem.Text.Trim()) && ansFound)
                        {
                            this.cvItemSkipMatching.IsValid = false;
                            isValid = false;
                        }
                        if (string.IsNullOrEmpty(txtCorrect.Text.Trim()) && letterFound)
                        {
                            this.cvItemSkipMatching.IsValid = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(txtChoice.Text.Trim()))
                    {
                        if (listChoices.Contains(txtChoice.Text.Trim()))
                        {
                            this.cvDuplicateMatching.IsValid = false;
                            isValid = false;
                        }
                        else
                            listChoices.Add(txtChoice.Text.Trim());
                        questionFound = true;
                        totItems++;
                        listLetters.Add(txtLetter.Text.Replace(":", "").Trim().ToUpper());
                    }
                    if (!string.IsNullOrEmpty(txtItem.Text.Trim()))
                    {
                        if (listItems.Contains(txtItem.Text.Trim()))
                        {
                            this.cvDuplicateMatching.IsValid = false;
                            isValid = false;
                        }
                        else
                            listItems.Add(txtItem.Text.Trim());
                        ansFound = true;
                        totAns++;
                    }

                    if (!string.IsNullOrEmpty(txtCorrect.Text.Trim()))
                        listCorrectMatches.Add(txtCorrect.Text.Trim());
                }
                //check if there are two or more questions
                if (totAns < 2 || totItems < 2)
                {
                    this.cvNoOptionsMatching.IsValid = false;
                    isValid = false;
                }

                //check if the letters chosen are valid
                {
                    foreach (string correctMatch in listCorrectMatches)
                    {
                        if (!listLetters.Contains(correctMatch.ToUpper()))
                        {
                            this.cvInvalidChoiceMatching.IsValid = false;
                            isValid = false;
                            break;
                        }
                    }
                }
                //bracket that follows closes the if(this.Qtype == "matching") clause and also the entire if clause
            }
            return isValid;
        }
        catch (Exception ex)
        {
            //using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            //{
            //    service.WriteError(Config.ApplicationID(), "editexamquestioncontrol.ascx", "DetermineValidity", ex.Message);
            //}
            ///TODO:Catch error when page can't determine if the question created/edited produced a valid output
            return false;
        }
    }
}
