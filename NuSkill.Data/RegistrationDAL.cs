using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class RegistrationDAL : TheLibrary.DBImportTool.Base
    {
        public RegistrationDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public RegistrationDAL(string connString)
        {
            this.ConnectionString = connString;
        }

        public void Insert(string lastName, string firstName, string phoneNumber, string location, string autoUserID, string autoPassword, int cimnumber)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pLastName", SqlDbType.VarChar, lastName));
            paramList.Add(Helper.CreateParam("@pFirstName", SqlDbType.VarChar, firstName));
            paramList.Add(Helper.CreateParam("@pPhoneNumber", SqlDbType.VarChar, phoneNumber));
            paramList.Add(Helper.CreateParam("@pLocation", SqlDbType.VarChar, location));
            paramList.Add(Helper.CreateParam("@pAutoUserID", SqlDbType.VarChar, autoUserID));
            paramList.Add(Helper.CreateParam("@pAutoPassword", SqlDbType.VarChar, autoPassword));
            paramList.Add(Helper.CreateParam("@pCimNumber", SqlDbType.Int, cimnumber));
            Connection.ExecuteSP("pr_registration_sav_insert", paramList.ToArray());
        }

        public DataSet CheckExists(string firstName, string lastName, string phoneNumber, string location)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pFirstName", SqlDbType.VarChar, firstName));
            paramList.Add(Helper.CreateParam("@pLastName", SqlDbType.VarChar, lastName));
            paramList.Add(Helper.CreateParam("@pPhoneNumber", SqlDbType.VarChar, phoneNumber));
            paramList.Add(Helper.CreateParam("@pLocation", SqlDbType.VarChar, location));
            return Connection.ExecuteSPQuery("pr_registration_sav_checkexists", paramList.ToArray());
        }

        public bool ValidateLogin(string autoUserID, string autoPassword)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pAutoUserID", SqlDbType.VarChar, autoUserID));
            paramList.Add(Helper.CreateParam("@pAutoPassword", SqlDbType.VarChar, autoPassword));
            return Convert.ToBoolean(Connection.ExecuteScalar("pr_registration_lkp_userpass", paramList.ToArray()));
        }

        public DataSet Search(string searchParam, int batchID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@SearchParam", SqlDbType.VarChar, searchParam));
            paramList.Add(Helper.CreateParam("@BatchID", SqlDbType.Int, batchID));
            return Connection.ExecuteSPQuery("pr_registration_lkp_search", paramList.ToArray());
        }

        public DataSet Select(string userID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            return Connection.ExecuteSPQuery("pr_registration_lkp_select", paramList.ToArray());
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_registration_lkp_getall");
        }

        public DataSet LoginToCim(string userid, string password)
        {
            this.ConnectionString = Properties.Settings.Default.Vader2GenericWebConnectionString;
            return Connection.ExecuteSPQuery("pr_Security_Chk_LoginUser",
                Helper.CreateParam("@ApplicationCode", SqlDbType.VarChar, userid),
                Helper.CreateParam("@PassPhrase", SqlDbType.VarChar, password),
                Helper.CreateParam("@CompanyID", SqlDbType.Int, 2));
        }

        public int GetCim(string username)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, username));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_registration_lkp_getcim",
                paramList.ToArray()));
        }

        public void InsertByBatch(int batchID, string prefix, int minseed, int maxseed, string suffix)
        {
            Connection.ExecuteSP("pr_registration_sav_insertbybatch",
                Helper.CreateParam("@BatchID", SqlDbType.Int, batchID),
                Helper.CreateParam("@Prefix", SqlDbType.VarChar, prefix),
                Helper.CreateParam("@MinSeed", SqlDbType.Int, minseed),
                Helper.CreateParam("@MaxSeed", SqlDbType.Int, maxseed),
                Helper.CreateParam("@Suffix", SqlDbType.VarChar, suffix));
        }

        public DataSet GetExistingByBatch(string prefix, int minseed, int maxseed, string suffix)
        {
            return Connection.ExecuteSPQuery("pr_registration_lkp_findexistingbybatch",
                Helper.CreateParam("@Prefix", SqlDbType.VarChar, prefix),
                Helper.CreateParam("@MinSeed", SqlDbType.Int, minseed),
                Helper.CreateParam("@MaxSeed", SqlDbType.Int, maxseed),
                Helper.CreateParam("@Suffix", SqlDbType.VarChar, suffix));
        }

        public DataSet SelectByBatch(int batchID)
        {
            return Connection.ExecuteSPQuery("pr_registration_lkp_selectbyBatch",
                Helper.CreateParam("@BatchID", SqlDbType.Int, batchID));
        }

        public int SearchEmployee(int cimNumber)
        {
            return Convert.ToInt32(Connection.ExecuteScalar("pr_registration_lkp_SearchEmployee",
                Helper.CreateParam("@CimNumber", SqlDbType.Int, cimNumber)));
        }        
    }
}
