using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Business
{
    public class PresetRights
    {
        private int _presetRightsID;

        public int PresetRightsID
        {
            get { return _presetRightsID; }
            set { _presetRightsID = value; }
        }

        private string _rightsName;

        public string RightsName
        {
            get { return _rightsName; }
            set { _rightsName = value; }
        }

        private string _presetRightsValue;

        public string PresetRightsValue
        {
            get { return _presetRightsValue; }
            set { _presetRightsValue = value; }
        }

        public static PresetRights[] SelectAll()
        {
            PresetRightsDal dal = new PresetRightsDal();
            return Conversion.SetProperties<PresetRights>(dal.SelectAll());
        }
    }
}
