<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="viewsuggestions, App_Web_yuclf4ni" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblTeamSummaries" runat="server" Text="Suggested Questions" ForeColor="black"
                                    Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvSuggestedQuestions" runat="server" AutoGenerateColumns="false"
                                    AllowPaging="true" ForeColor="black" PageSize="20" CellPadding="5" BorderColor="black"
                                    Font-Bold="false" Width="100%" EmptyDataText="No exams found." GridLines="horizontal"
                                    BorderWidth="0px" OnRowCommand="gvSuggestedQuestions_RowCommand" OnPageIndexChanging="gvSuggestedQuestions_PageIndexChangeing">
                                    <RowStyle BackColor="Beige" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Panel ID="pnlItem" runat="server" BorderColor="black" BorderWidth="1px">
                                                    <table cellpadding="5" width="100%" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label1" runat="server" Text="QUESTION" Font-Bold="true" Font-Size="14px" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="5" width="100%" cellspacing="0" border="1" style="background-color: White">
                                                                    <tr>
                                                                        <td colspan="4" style="background-color: #B0C4DE">
                                                                            <asp:Label ID="lblItem" runat="server" Text="ITEM DETAILS" Font-Bold="true" Font-Size="12px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 18%">
                                                                            <asp:Label ID="lblTestName" runat="server" Text="For Test:" />
                                                                        </td>
                                                                        <td style="width: 32%">
                                                                            <asp:Label ID="lblTestNameVal" runat="server" Text='<%#Bind("TestName") %>' />
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text="Created By:" />
                                                                        </td>
                                                                        <td style="width: 32%">
                                                                            <asp:Label ID="lblCreatedByVal" runat="server" Text='<%#Bind("CreatedBy") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lbldateCreated" runat="server" Text="Date Created:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblDateCreatedVal" runat="server" Text='<%#Bind("RecDateCreated") %>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblCreatorComments" runat="server" Text="Creator Comments:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblCreatorCommentsVal" runat="server" Text='<%#Bind("CreatorComments") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="vertical-align: middle; font-size: 16px; font-family: Arial;" colspan="4">
                                                                            <asp:Label ID="lblQuestionnaireID" runat="server" Visible="false" Text='<%#Bind("QuestionnaireID") %>' />
                                                                            <asp:Label ID="lblQuestion" runat="server" CssClass="testQuestion" Text='<%#Bind("Question") %>'></asp:Label>
                                                                            <asp:Label ID="lblTypeCode" runat="server" Text='<%#Bind("TypeCode") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns1" runat="server" Text='<%#Bind("Ans1") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns2" runat="server" Text='<%#Bind("Ans2") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns3" runat="server" Text='<%#Bind("Ans3") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns4" runat="server" Text='<%#Bind("Ans4") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns5" runat="server" Text='<%#Bind("Ans5") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns6" runat="server" Text='<%#Bind("Ans6") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns7" runat="server" Text='<%#Bind("Ans7") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns8" runat="server" Text='<%#Bind("Ans8") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns9" runat="server" Text='<%#Bind("Ans9") %>' Visible="false" />
                                                                            <asp:Label ID="lblAns10" runat="server" Text='<%#Bind("Ans10") %>' Visible="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="vertical-align: middle; font-size: 12px; font-family: Arial;" colspan="4">
                                                                            <asp:Label ID="lblHyperlink" runat="server" Text='<%#Bind("Hyperlink") %>' Visible="false" />
                                                                            <asp:Label ID="lblHyperlinkValue" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" style="vertical-align: middle; font-size: large; font-family: Arial;"
                                                                            colspan="4">
                                                                            <table width="100%" cellpadding="5" style="vertical-align: middle; font-family: Arial;
                                                                                font-size: 12px;">
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:Panel ID="pnlMultipleChoice" runat="server" Width="100%" Visible="false">
                                                                                            <table cellpadding="5" width="100%" border="1" cellspacing="0">
                                                                                                <tr id="trCheck1" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice1" runat="server" Text='<%#Bind("Choice1") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck2" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice2" runat="server" Text='<%#Bind("Choice2") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck3" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice3" runat="server" Text='<%#Bind("Choice3") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck4" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice4" runat="server" Text='<%#Bind("Choice4") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck5" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice5" runat="server" Text='<%#Bind("Choice5") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck6" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice6" runat="server" Text='<%#Bind("Choice6") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck7" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice7" runat="server" Text='<%#Bind("Choice7") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck8" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice8" runat="server" Text='<%#Bind("Choice8") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck9" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice9" runat="server" Text='<%#Bind("Choice9") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trCheck10" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChoice10" runat="server" Text='<%#Bind("Choice10") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <asp:Panel ID="pnlTrueOrFalse" runat="server" Width="100%" Visible="false">
                                                                                            <table cellpadding="5" width="100%" border="1" cellspacing="0">
                                                                                                <tr id="trTrue" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="rdoTrue" runat="server" Text='<%#Bind("Choice1") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trFalse" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="rdoFalse" runat="server" Text='<%#Bind("Choice2") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <asp:Panel ID="pnlSequencing" runat="server" Width="100%" Visible="false">
                                                                                            <table cellpadding="5" width="100%" border="1" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblQuestionOrder" runat="server" Text="Order In Exam:" />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblCorrectOrder" runat="server" Text="Correct Order:" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice1" runat="server" Text='<%#Bind("Choice1") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns1" runat="server" Text='<%#Bind("Ans1") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice2" runat="server" Text='<%#Bind("Choice2") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns2" runat="server" Text='<%#Bind("Ans2") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice3" runat="server" Text='<%#Bind("Choice3") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns3" runat="server" Text='<%#Bind("Ans3") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice4" runat="server" Text='<%#Bind("Choice4") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns4" runat="server" Text='<%#Bind("Ans4") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice5" runat="server" Text='<%#Bind("Choice5") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns5" runat="server" Text='<%#Bind("Ans5") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice6" runat="server" Text='<%#Bind("Choice6") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns6" runat="server" Text='<%#Bind("Ans6") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice7" runat="server" Text='<%#Bind("Choice7") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns7" runat="server" Text='<%#Bind("Ans7") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice8" runat="server" Text='<%#Bind("Choice8") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns8" runat="server" Text='<%#Bind("Ans8") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice9" runat="server" Text='<%#Bind("Choice9") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns9" runat="server" Text='<%#Bind("Ans9") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSeqChoice10" runat="server" Text='<%#Bind("Choice10") %>' />
                                                                                                    </td>
                                                                                                    <td style="background-color: lightgreen">
                                                                                                        <asp:Label ID="lblSeqAns10" runat="server" Text='<%#Bind("Ans10") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <asp:Panel ID="pnlFillInTheBlanks" runat="server" Width="100%" Visible="false">
                                                                                            <table cellpadding="5">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblFillInTheBlanks" runat="server" Width="400px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <asp:Panel ID="pnlMatchingType" runat="server" Width="100%" Visible="false">
                                                                                            <table cellpadding="5">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch1" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem1" runat="server" Width="250px" Text='<%#Bind("Choice1") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter1" runat="server" Text="A:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice1" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch2" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem2" runat="server" Width="250px" Text='<%#Bind("Choice2") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter2" runat="server" Text="B:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice2" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch3" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem3" runat="server" Width="250px" Text='<%#Bind("Choice3") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter3" runat="server" Text="C:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice3" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch4" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem4" runat="server" Width="250px" Text='<%#Bind("Choice4") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter4" runat="server" Text="D:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice4" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch5" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem5" runat="server" Width="250px" Text='<%#Bind("Choice5") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter5" runat="server" Text="E:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice5" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch6" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem6" runat="server" Width="250px" Text='<%#Bind("Choice6") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter6" runat="server" Text="F:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice6" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch7" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem7" runat="server" Width="250px" Text='<%#Bind("Choice7") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter7" runat="server" Text="G:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice7" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch8" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem8" runat="server" Width="250px" Text='<%#Bind("Choice8") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter8" runat="server" Text="H:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice8" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch9" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem9" runat="server" Width="250px" Text='<%#Bind("Choice9") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter9" runat="server" Text="I:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice9" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCorrectMatch10" runat="server" Width="20px" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblMatchItem10" runat="server" Width="250px" Text='<%#Bind("Choice10") %>' />
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <asp:Label ID="lblMatchLetter10" runat="server" Text="J:" Width="20px" />
                                                                                                        <asp:Label ID="lblMatchChoice10" runat="server" Width="250px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <asp:Panel ID="pnlYesOrNo" runat="server" Width="100%" Visible="false">
                                                                                            <table cellpadding="5" width="100%" border="1" cellspacing="0">
                                                                                                <tr id="trYes" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblYes" runat="server" Text='<%#Bind("Choice1") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trNo" runat="server">
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblNo" runat="server" Text='<%#Bind("Choice2") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <asp:Panel ID="pnlHotspot" runat="server" Width="100%" Visible="false">
                                                                                            <table cellpadding="5" width="100%" border="1" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td id="imgCell1" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice1" runat="server" />
                                                                                                    </td>
                                                                                                    <td id="imgCell2" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice2" runat="server" />
                                                                                                    </td>
                                                                                                    <td id="imgCell3" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice3" runat="server" />
                                                                                                    </td>
                                                                                                    <td id="imgCell4" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice4" runat="server" />
                                                                                                    </td>
                                                                                                    <td id="imgCell5" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice5" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td id="imgCell6" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice6" runat="server" />
                                                                                                    </td>
                                                                                                    <td id="imgCell7" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice7" runat="server" />
                                                                                                    </td>
                                                                                                    <td id="imgCell8" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice8" runat="server" />
                                                                                                    </td>
                                                                                                    <td id="imgCell9" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice9" runat="server" />
                                                                                                    </td>
                                                                                                    <td id="imgCell10" runat="server" align="center" valign="middle">
                                                                                                        <asp:Image ID="imgChoice10" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                                <%-- <tr>
                                                                    <td style="vertical-align: middle; font-family: Arial; font-size: small;">
                                                                        <asp:Label ID="lblCorrectAnswer" runat="server" Text="Correct Answer:" />
                                                                    </td>
                                                                </tr>--%>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="lblCorrectAnswerVal" runat="server" Visible="false" Font-Bold="true" />
                                                                                        <asp:ListBox ID="lstCorrectAnswers" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer1" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer2" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer3" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer4" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer5" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer6" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer7" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer8" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer9" runat="server" Visible="false" />
                                                                                        <asp:Image ID="imgCorrectAnswer10" runat="server" Visible="false" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" style="background-color: #B0C4DE">
                                                                            <asp:Label ID="lblApproval" runat="server" Text="APPROVAL" Font-Bold="true" Font-Size="12px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:Label ID="lblApproverComments" runat="server" Text="Comments:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtComments" runat="server" TextMode="multiline" Rows="5" Width="98%" />
                                                                                        <br />
                                                                                        <asp:CustomValidator ID="cvNoComments" runat="server" ErrorMessage="Please provide comments when disapproving a question."
                                                                                            Display="dynamic" />
                                                                                    </td>
                                                                                    <td align="center" valign="middle" style="width: 15%">
                                                                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="buttons" BackColor="Green"
                                                                                            ForeColor="White" CommandName="Approve" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                                        <br />
                                                                                        <br />
                                                                                        <asp:Button ID="Button1" runat="server" Text="Disapprove" CssClass="buttons" BackColor="Tomato"
                                                                                            ForeColor="White" CommandName="Disapprove" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
