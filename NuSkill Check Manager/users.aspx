<%@ Page Language="C#" MasterPageFile="~/masterpages/nuSkill_manager_site.master" AutoEventWireup="true" CodeFile="users.aspx.cs" Inherits="users" Title="Transcom University Testing Admin" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <telerik:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function ShowWindow(userID) {
                var oWnd = $find("<%= rdAssignClient.ClientID %>");
                oWnd.setUrl('assignedclients.aspx?UserID=' + userID);
                oWnd.show();
                return false;
            }
        </script>
    </telerik:RadScriptBlock>

    <telerik:RadWindow runat="server" ID="rdAssignClient" Modal="true" Width="600px"
        Height="400px" EnableViewState="True" VisibleOnPageLoad="False" Skin="Telerik"
        Behaviors="Close, Move, Resize, Maximize, Minimize, Pin" >
    </telerik:RadWindow>

    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px" Visible="false">
                    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblUsers" runat="server" Text="Users" ForeColor="black" Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblSearchCim" runat="server" Text="View Results for Employee:" /><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCimNumber" runat="server" Text="Cim Number:" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCim" runat="server" />
                                            <ajax:FilteredTextBoxExtender ID="ftbCim" runat="server" FilterMode="validchars"
                                                FilterType="numbers" TargetControlID="txtCim" />
                                            <asp:Button ID="btnSearch" runat="server" CssClass="buttons" Text="Search" OnClick="btnSearch_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:CustomValidator ID="cvNoSearch" runat="server" ErrorMessage="Enter a correct and active Cim Number."
                                                Display="static" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblSearch" runat="server" Text="Search for Non-Cim User:" ForeColor="black" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblBatch" runat="server" Text="User Batch:" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlBatch" runat="server" AppendDataBoundItems="true" DataValueField="BatchID"
                                                DataTextField="BatchName">
                                                <asp:ListItem Text="Non-generated users" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblUsername" runat="server" Text="Username:" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSearch" runat="server" />
                                            <asp:Button ID="btnUserSearch" runat="server" CssClass="buttons" Text="Search" OnClick="btnUserSearch_Click" />
                                            <asp:Label ID="lblVert" runat="server" Text="          " ForeColor="black" />
                                            <asp:Button ID="btnViewAll" runat="server" CssClass="buttons" Text="View All" OnClick="lnkViewAll_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    PageSize="20" CellPadding="5" BorderColor="black" OnPageIndexChanging="gvUsers_PageIndexChanging"
                                    Width="100%" EmptyDataText="No users found." OnSelectedIndexChanging="gvUsers_SelectedIndexChanging"
                                    Visible="false">
                                    <RowStyle BorderColor="black" ForeColor="Black" Font-Bold="true" Font-Size="12px" />
                                    <AlternatingRowStyle ForeColor="Black" Font-Bold="true" Font-Size="12px" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        Position="Bottom" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="User ID">
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsername" runat="server" Text='<%#Bind("AutoUserID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle Width="20%" HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSelecte" runat="server" Text="View" CommandName="Select" ForeColor="black"
                                                    Font-Bold="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1" GridLines="None"
                    OnInsertCommand="RadGrid1_InsertCommand" OnUpdateCommand="RadGrid1_UpdateCommand"
                    OnItemDataBound="RadGrid1_ItemDataBound" AllowFilteringByColumn="true" AllowPaging="true"
                    PageSize="10">
                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="UserID, TWWID, SupervisorID, ClientID" DataSourceID="SqlDataSource1"
                        CommandItemDisplay="Top">
                        <CommandItemSettings ExportToPdfText="Export to Pdf" ShowAddNewRecordButton="false" />
                        <RowIndicatorColumn>
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn>
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="UserID" DataType="System.Decimal" HeaderText="UserID"
                                ReadOnly="True" SortExpression="UserID" UniqueName="UserID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                                UniqueName="FirstName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName"
                                UniqueName="LastName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="TWWID" HeaderText="TWWID" SortExpression="TWWID"
                                UniqueName="TWWID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DomainName" HeaderText="DomainName" SortExpression="DomainName"
                                UniqueName="DomainName" Display="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="WindowsUsername" HeaderText="WindowsUsername"
                                SortExpression="WindowsUsername" UniqueName="WindowsUsername" Display="false"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                                UniqueName="Email">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="supervisorname" HeaderText="Supervisor" SortExpression="supervisorname"
                                UniqueName="supervisorname">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="MobileNo" HeaderText="MobileNo" SortExpression="MobileNo"
                                UniqueName="MobileNo" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="supervisorid" HeaderText="supervisorid" SortExpression="supervisorid"
                                UniqueName="supervisorid" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="departmentid" HeaderText="departmentid" SortExpression="departmentid"
                                UniqueName="departmentid" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FailedPasswordAttempCount" HeaderText="FailedPasswordAttempCount"
                                SortExpression="FailedPasswordAttempCount" UniqueName="FailedPasswordAttempCount"
                                DataType="System.Int32" Display="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RegionID" DataType="System.Int32" HeaderText="RegionID"
                                SortExpression="RegionID" UniqueName="RegionID" Display="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CountryID" DataType="System.Int32" HeaderText="CountryID"
                                SortExpression="CountryID" UniqueName="CountryID" Display="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SiteID" DataType="System.Int32" HeaderText="SiteID"
                                SortExpression="SiteID" UniqueName="SiteID" Display="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="client_desc" HeaderText="Client" SortExpression="client_desc"
                                UniqueName="client_desc">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RoleID" HeaderText="RoleID" SortExpression="RoleID"
                                UniqueName="RoleID" DataType="System.Int32" Display="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="role_desc" HeaderText="Role" SortExpression="role_desc"
                                UniqueName="role_desc">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClientID" DataType="System.Int32" HeaderText="ClientID"
                                SortExpression="ClientID" UniqueName="ClientID" Display="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="region_desc" HeaderText="Region" SortExpression="region_desc"
                                UniqueName="region_desc">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="country_desc" HeaderText="Country" SortExpression="country_desc"
                                UniqueName="country_desc">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="site_desc" HeaderText="Site" SortExpression="site_desc"
                                UniqueName="site_desc">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Active" DataType="System.Boolean" HeaderText="Active"
                                SortExpression="Active" UniqueName="Active" DefaultInsertValue="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IsLockedOut" DataType="System.Boolean" HeaderText="Locked Out?"
                                SortExpression="IsLockedOut" UniqueName="IsLockedOut" DefaultInsertValue="true"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CreateDate" DataType="System.DateTime" HeaderText="CreateDate"
                                SortExpression="CreateDate" UniqueName="CreateDate" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" ButtonType="LinkButton"
                                EditText="Edit">
                            </telerik:GridEditCommandColumn>
                        </Columns>
                        <EditFormSettings UserControlName="~/controls/Users.ascx" EditFormType="WebUserControl">
                            <EditColumn UniqueName="EditCommandColumn1">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <GroupingSettings CaseSensitive="false" />
                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
                    </HeaderContextMenu>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                    SelectCommand="sp_GetUsers" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpePopup" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlPopup" TargetControlID="hidPopup" />
    <asp:HiddenField ID="hidPopup" runat="server" />
    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Font-Size="12px">
        <table cellpadding="10" cellspacing="0" style="width: 300">
            <tr>
                <td>
                    <asp:Label ID="lblUserID" runat="server" Text="UserID:" />
                </td>
                <td>
                    <asp:Label ID="lblUserIDVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCim" runat="server" Text="CIM Number:" />
                </td>
                <td>
                    <asp:Label ID="lblCimVal" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:LinkButton ID="lnkCompleteInformation" runat="server" Text="View complete user information"
                        CssClass="linkButtons" OnClick="lnkCompleteInformation_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:Button ID="btnPopup" runat="server" Text="Back" CssClass="buttons" Width="60"
                        OnClick="btnPopup_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeNoRights" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlNoRights" TargetControlID="hidNoRights" />
    <asp:HiddenField ID="hidNoRights" runat="server" />
    <asp:Panel ID="pnlNoRights" runat="server" CssClass="modalPopup" Font-Size="12px">
        <table cellpadding="10" cellspacing="0" style="width: 300">
            <tr>
                <td>
                    <asp:Label ID="lblNoRights" runat="server" Text="You have no rights to view this user." />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnNoRights" runat="server" Text="Return" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
