<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site_noajax.master" autoeventwireup="true" inherits="debugpage, App_Web_yuclf4ni" title="Transcom University Testing Admin" %>

<%@ Register TagPrefix="wus" TagName="editexamitemcontrol" Src="~/controls/editexamquestioncontrol.ascx" %>
<%@ Register TagPrefix="wus" TagName="editexamcontrol" Src="~/controls/editexamcontrol.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server" EnableViewState="true">
    <wus:editexamcontrol ID="wusEditQuestion" runat="server" EnableViewState="true" />
</asp:Content>
