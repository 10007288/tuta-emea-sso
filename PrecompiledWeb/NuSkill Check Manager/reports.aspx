<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="reports, App_Web_yuclf4ni" title="Transcom University Testing Admin" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="wus" TagName="reportcontrol" Src="~/controls/reportcontrol.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <%--<wus:reportcontrol ID="wusReport" runat="server" />--%>
    <table cellpadding="5" width="100%" cellspacing="0" style="font-family: Arial; font-size: 12px">
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" ForeColor="black"
                    Font-Bold="true" Font-Size="12px">
                    <table cellpadding="4" width="100%" cellspacing="0" style="font-family: Arial; text-align: left">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblReports" runat="server" Text="Reports" ForeColor="black" Font-Size="16" /><br />
                                <hr style="color: Black; width: 100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1"
                                    SelectedIndex="0">
                                    <Tabs>
                                        <telerik:RadTab Text="Current Reports" Selected="True">
                                        </telerik:RadTab>
                                        <telerik:RadTab Text="Expired Reports">
                                        </telerik:RadTab>
                                        <telerik:RadTab Text="Feedback Report">
                                        </telerik:RadTab>
                                    </Tabs>
                                </telerik:RadTabStrip>
                                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
                                    <telerik:RadPageView ID="RadPageView1" runat="server" Selected="true">
                                        <asp:GridView ID="gvReports" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                                            Width="100%" OnRowCommand="gvReports_RowCommand" EmptyDataText="No reports available."
                                            CellPadding="3">
                                            <EmptyDataRowStyle Font-Size="small" Font-Italic="true" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%--<asp:LinkButton ID="lnkReport" runat="server" Text='<%#Bind("DisplayName") %>' CommandArgument='<%#Bind("ReportURL") %>'
                                                    CommandName="Report" ForeColor="black" OnClientClick="aspnetForm.target='_blank';" />--%>
                                                        <asp:LinkButton ID="lnkReport" runat="server" Text='<%#Bind("DisplayName") %>' CommandArgument='<%#Bind("ReportID") %>'
                                                            CommandName="Report" ForeColor="black" OnClientClick="aspnetForm.target='_blank';" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="RadPageView2" runat="server">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                                            Width="100%" OnRowCommand="gvReports_RowCommand" EmptyDataText="No reports available."
                                            CellPadding="3">
                                            <EmptyDataRowStyle Font-Size="small" Font-Italic="true" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%--<asp:LinkButton ID="lnkReport" runat="server" Text='<%#Bind("DisplayName") %>' CommandArgument='<%#Bind("ReportURL") %>'
                                                    CommandName="Report" ForeColor="black" OnClientClick="aspnetForm.target='_blank';" />--%>
                                                        <asp:LinkButton ID="lnkReport" runat="server" Text='<%#Bind("DisplayName") %>' CommandArgument='<%#Bind("ReportID") %>'
                                                            CommandName="Report" ForeColor="black" OnClientClick="aspnetForm.target='_blank';" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="RadPageView3" runat="server">
                                        <table width="100%" style="border: 1px solid gray">
                                            <tr>
                                                <td>
                                                    <asp:HyperLink runat="server" ID="lnkViewFeed" NavigateUrl="~/ReportFeedback.aspx"
                                                        Text="View Feedback Report" Target="_blank" ForeColor="black" />
                                                </td>
                                            </tr>
                                        </table>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
