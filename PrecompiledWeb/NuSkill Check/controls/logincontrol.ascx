<%@ control language="C#" autoeventwireup="true" inherits="controls_logincontrol, App_Web_ixuq4ve0" %>
<table width="100%">
    <tr>
        <td align="right">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblUsername" runat="server" Text="Username:" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtUsername" runat="server" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPassword" runat="server" Text="Password:" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="password" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td align="left">
                        <asp:LinkButton ID="lnkLogin" runat="server" Text="Login" Font-Bold="true" CssClass="linkbutton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
