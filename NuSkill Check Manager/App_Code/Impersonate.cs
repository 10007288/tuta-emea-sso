using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;
using System.Security.Principal;

/// <summary>
/// Summary description for Impersonate
/// </summary>
public class Impersonate
{
    public Impersonate()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    static public extern bool LogonUser(string userName, string domain, string passWord, int logonType, int logonProvider, ref IntPtr accessToken);

    private const int LOGON_TYPE_INTERACTIVE = 2;
    private const int LOGON_TYPE_PROVIDER_DEFAULT = 0;
}
