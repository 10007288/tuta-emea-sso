using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using NuComm.Security.Encryption;


public partial class NuSkillCheckV2Reports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.ddlReports.DataSource = NuSkill.Business.TestReports.SelectAll();
            this.ddlReports.DataBind();
            foreach (ListItem item in this.ddlReports.Items)
            {
                item.Value = UTF8.DecryptText(Server.UrlDecode(item.Value)).Replace("http://home.nucomm.net/applications/ReportingServicesPrototype/CustomView.aspx?Report=[NuSkillCheckV2]", "[NuSkillCheckV2Updated]");
            }
        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        this.ReportViewer1.ServerReport.ReportServerUrl = new System.Uri("http://DB042k3CN1/reportserver");
        this.ReportViewer1.ServerReport.ReportPath = @"/Reports/" + this.ddlReports.Text;
        this.ReportViewer1.Visible = true;
    }
}