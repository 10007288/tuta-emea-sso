using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class UserRole
    {
        private int _userRoleID;

        public int UserRoleID
        {
            get { return _userRoleID; }
            set { _userRoleID = value; }
        }

        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _roleCode;

        public string RoleCode
        {
            get { return _roleCode; }
            set { _roleCode = value; }
        }

        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        private DateTime _dateLastUpdated;

        public DateTime DateLastUpdated
        {
            get { return _dateLastUpdated; }
            set { _dateLastUpdated = value; }
        }

        private string _LastUpdated;

        public string LastUpdated
        {
            get { return _LastUpdated; }
            set { _LastUpdated = value; }
        }

        private string _lastUpdatedBy;

        public string LastUpdatedBy
        {
            get { return _lastUpdatedBy; }
            set { _lastUpdatedBy = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        public static UserRole GetRole(string userID, string roleCode)
        {
            UserRoleDAL dal = new UserRoleDAL();
            UserRole[] roles = Conversion.SetProperties<UserRole>(dal.GetRole(userID, roleCode));
            return roles == null || roles.Length == 0 ? null : roles[0];
        }

        public static UserRole[] GetUserRoles(string userID)
        {
            UserRoleDAL dal = new UserRoleDAL();
            return Conversion.SetProperties<UserRole>(dal.GetUserRoles(userID));
        }
    }
}
