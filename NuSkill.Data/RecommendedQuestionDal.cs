using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Data
{
    public class RecommendedQuestionDal : Base
    {
        public RecommendedQuestionDal()
        { }

        public RecommendedQuestionDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public void Insert(int questionnaireID, int testCategoryID, string username, string comments)
        {
            Connection.ExecuteSP("pr_RecommendedQuestion_Sav_Insert",
                Helper.CreateParam("@QuestionnaireID", SqlDbType.Int, questionnaireID),
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID),
                Helper.CreateParam("@Username", SqlDbType.VarChar, username),
                Helper.CreateParam("@Comments", SqlDbType.VarChar, comments));
        }

        public DataSet SelectByQuestionnaireID(int questionnaireID)
        {
            return Connection.ExecuteSPQuery("pr_RecommendedQuestion_Lkp_Select",
                Helper.CreateParam("@QuestionnaireID", SqlDbType.Int, questionnaireID));
        }

        public void Update(int questionnaireID, int recommendedQuestionStatusID, string approverComments, string approvedBy)
        {
            Connection.ExecuteSPQuery("pr_RecommendedQuestion_Sav_Update",
                Helper.CreateParam("@QuestionnaireID", SqlDbType.Int, questionnaireID),
                Helper.CreateParam("@RecommendedQuestionStatusID", SqlDbType.Int, recommendedQuestionStatusID),
                Helper.CreateParam("@ApproverComments", SqlDbType.VarChar, approverComments),
                Helper.CreateParam("@ApprovedBy", SqlDbType.VarChar, approvedBy));
        }
    }
}
