using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

/// <summary>
/// Summary description for SessionManager
/// </summary>z
public class SessionManager
{
    public SessionManager()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private const string _sessionQuestionnaire = "SessionQuestionnaire";
    private const string _sessionUsername = "SessionUsername";
    private const string _sessionExamAction = "SessionAction";
    private const string _sessionQuestionAction = "SessionQuestionAction";
    private const string _sessionSingleQuestion = "SessionSingleQuestion";
    private const string _sessionSingleExam = "SessionSingleExam";
    private const string _sessionSingleQuestionIndex = "SessionSingleQuestionIndex";
    private const string _sessionSaveTestResponseID = "SessionSaveTestResponseID";
    private const string _sessionExamineeName = "SessionExamineeName";
    private const string _sessionTestTakenID = "SessionTestTakenID";
    private const string _sessionSaveTestResponse = "sessionSaveTestResponse";
    private const string _sessionTestTaken = "sessionTestTaken";
    private const string _sessionQuestionNumber = "sessionQuestionNumber";
    private const string _sessionTestCategoryID = "sessionTestCategoryID";
    private const string _sessionTempCategory = "sessionTempCategory";
    private const string _sessionSingleSaveTestResponse = "sessionSingleSaveTestResponse";
    private const string _sessionTestCategoryGroupMatrixDataSet = "sessionTestCategoryGroupMatrixDataSet";
    private const string _sessionUserRights = "sessionUserRights";
    private const string _sessionRole = "sessionRole";
    private const string _sessionTestCampaignAccount = "sessionTestCampaignAccount";
    private const string _sessionRecTestCategoryID = "sessionRecTestCategoryID";
    private const string _sessionCreatorComments = "sessionCreatorComments";
    private const string _sessionCurrentGridUsers = "SessionCurrentGridUsers";

    public static string SessionCurrentGridUsers
    {
        get { return HttpContext.Current.Session[_sessionCurrentGridUsers] as string; }
        set { HttpContext.Current.Session[_sessionCurrentGridUsers] = value; }
    }

    public static List<Questionnaire> SessionQuestionnaire
    {
        get { return HttpContext.Current.Session[_sessionQuestionnaire] as List<Questionnaire>; }
        set { HttpContext.Current.Session[_sessionQuestionnaire] = value; }
    }

    public static string SessionUsername
    {
        get { return HttpContext.Current.Session[_sessionUsername] as string; }
        set { HttpContext.Current.Session[_sessionUsername] = value; }
    }

    public static string SessionExamAction
    {
        get { return HttpContext.Current.Session[_sessionExamAction] as string; }
        set { HttpContext.Current.Session[_sessionExamAction] = value; }
    }

    public static string SessionQuestionAction
    {
        get { return HttpContext.Current.Session[_sessionQuestionAction] as string; }
        set { HttpContext.Current.Session[_sessionQuestionAction] = value; }
    }

    public static Questionnaire SessionSingleQuestion
    {
        get { return HttpContext.Current.Session[_sessionSingleQuestion] as Questionnaire; }
        set { HttpContext.Current.Session[_sessionSingleQuestion] = value; }
    }

    public static TestCategory SessionSingleExam
    {
        get { return HttpContext.Current.Session[_sessionSingleExam] as TestCategory; }
        set { HttpContext.Current.Session[_sessionSingleExam] = value; }
    }

    public static int SessionSingleQuestionIndex
    {
        get { return (int)HttpContext.Current.Session[_sessionSingleQuestionIndex]; }
        set { HttpContext.Current.Session[_sessionSingleQuestionIndex] = value; }
    }

    public static int SessionSaveTestResponseID
    {
        get { return (int)HttpContext.Current.Session[_sessionSaveTestResponseID]; }
        set { HttpContext.Current.Session[_sessionSaveTestResponseID] = value; }
    }

    public static string SessionExamineeName
    {
        get { return HttpContext.Current.Session[_sessionExamineeName].ToString(); }
        set { HttpContext.Current.Session[_sessionExamineeName] = value; }
    }

    public static int SessionTestTakenID
    {
        get { return (int)HttpContext.Current.Session[_sessionTestTakenID]; }
        set { HttpContext.Current.Session[_sessionTestTakenID] = value; }
    }

    public static List<SaveTestResponse> SessionSaveTestResponse
    {
        get { return HttpContext.Current.Session[_sessionSaveTestResponse] as List<SaveTestResponse>; }
        set { HttpContext.Current.Session[_sessionSaveTestResponse] = value; }
    }

    public static TestTaken SessionTestTaken
    {
        get { return HttpContext.Current.Session[_sessionTestTaken] as TestTaken; }
        set { HttpContext.Current.Session[_sessionTestTaken] = value; }
    }

    public static int SessionQuestionNumber
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[_sessionQuestionNumber]); }
        set { HttpContext.Current.Session[SessionQuestionNumber] = value; }
    }

    public static int SessionTestCategoryID
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[_sessionTestCategoryID]); }
        set { HttpContext.Current.Session[_sessionTestCategoryID] = value; }
    }

    public static TestCategory SessionTempCategory
    {
        get { return HttpContext.Current.Session[_sessionTempCategory] as TestCategory; }
        set { HttpContext.Current.Session[_sessionTempCategory] = value; }
    }

    public static SaveTestResponse SessionSingleSaveTestResponse
    {
        get { return HttpContext.Current.Session[_sessionSingleSaveTestResponse] as SaveTestResponse; }
        set { HttpContext.Current.Session[_sessionSingleSaveTestResponse] = value; }
    }

    public static DataSet SessionTestCategoryGroupMatrixDataSet
    {
        get { return HttpContext.Current.Session[_sessionTestCategoryGroupMatrixDataSet] as DataSet; }
        set { HttpContext.Current.Session[_sessionTestCategoryGroupMatrixDataSet] = value; }
    }

    public static string SessionUserRights
    {
        get { return HttpContext.Current.Session[_sessionUserRights] as string; }
        set { HttpContext.Current.Session[_sessionUserRights] = value; }
    }

    public static int SessionRole
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[_sessionRole]); }
        set { HttpContext.Current.Session[_sessionRole] = value; }
    }

    public static TestCampaignAccount SessionTestCampaignAccount
    {
        get { return HttpContext.Current.Session[_sessionTestCampaignAccount] as TestCampaignAccount; }
        set { HttpContext.Current.Session[_sessionTestCampaignAccount] = value; }
    }

    public static int SessionRecTestCategoryID
    {
        get { return Convert.ToInt32(HttpContext.Current.Session[_sessionRecTestCategoryID]); }
        set { HttpContext.Current.Session[_sessionRecTestCategoryID] = value; }
    }

    public static string SessionCreatorComments
    {
        get { return HttpContext.Current.Session[_sessionCreatorComments] as string; }
        set { HttpContext.Current.Session[_sessionCreatorComments] = value; }
    }
}
