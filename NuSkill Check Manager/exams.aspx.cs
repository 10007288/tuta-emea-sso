using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using NuSkill.Business;
using System.Data.SqlClient;

public partial class exams : System.Web.UI.Page
{
    string _LastUpdated;
    string _Role;
    private TestCategory[] TestCategories
    {
        get
        {
            object testCategories = ViewState["DataSource"];
            return (TestCategory[])testCategories;
        }
        set
        {
            ViewState["DataSource"] = value;
        }
    }

    private string SortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
                ViewState["SortDirection"] = "ASC";
            return ViewState["SortDirection"].ToString();
        }
        set
        {
            ViewState["SortDirection"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager.SessionUsername))
            Response.Redirect("~/login.aspx?" + Request.QueryString.ToString());

        if (!this.IsPostBack)
        {
            this.BindCategories();
            SessionManager.SessionTestCategoryID = 0;

            string _clientID;
            using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
            {
                oconn.Open();
                using (SqlCommand ocomm = new SqlCommand("sp_UserClient_Checker", oconn))
                {
                    ocomm.CommandType = CommandType.StoredProcedure;
                    ocomm.Parameters.AddWithValue("@TWWID", SessionManager.SessionUsername);

                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(ocomm);
                    da.Fill(ds);
                    ds.Tables[0].Rows[0]["ClientID"].ToString();

                    _clientID = ds.Tables[0].Rows[0]["ClientID"].ToString();
                    ocomm.ExecuteNonQuery();
                }
                oconn.Close();
            }

            foreach (GridViewRow row in this.gvExams.Rows)
            {
                Label lblClientID = row.FindControl("lblClientID") as Label;
                Label lblExamID = row.FindControl("lblExamID") as Label;

                //Check who created/uploaded the course
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NUSkillCheck"].ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT LastUpdated FROM NuSkillCheck.dbo.tbl_Testing_TestCategory WHERE TestCategoryID = @TestCategoryID";
                    SqlCommand cmd = new SqlCommand(sql, connection);

                    cmd.Parameters.Add("@TestCategoryID", SqlDbType.Int).Value = lblExamID.Text;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _LastUpdated = String.Format("{0}", reader["LastUpdated"].ToString());
                        }
                    }
                }

                //Check user role of the current user
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
                    SqlCommand cmd = new SqlCommand(sql, connection);

                    cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = SessionManager.SessionUsername;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _Role = String.Format("{0}", reader["Role"].ToString());
                        }
                    }
                }

                //ClientID of Course vs ClientID of User
                if (lblClientID.Text == _clientID || lblClientID.Text == "0" || _LastUpdated == SessionManager.SessionUsername || _Role == "Admin") 
                {
                    row.Visible = true;
                }
                else
                {
                    row.Visible = false;
                }
            }
        }
    }

    protected void btnNewExam_Click(object sender, EventArgs e)
    {
        SessionManager.SessionTempCategory = new TestCategory();
        SessionManager.SessionQuestionnaire = new System.Collections.Generic.List<Questionnaire>();
        SessionManager.SessionExamAction = "newexam";
        Response.Redirect("~/editexam.aspx?" + Request.QueryString.ToString());
    }

    protected void gvExams_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(2))
        {
            Label lblExamID = this.gvExams.Rows[this.gvExams.SelectedIndex].FindControl("lblExamID") as Label;
            TestCategory category = null;
            if (this.chkIncludeElapsed.Checked)
                category = TestCategory.Select(Convert.ToInt32(lblExamID.Text.Trim()), true);
            else
                category = TestCategory.Select(Convert.ToInt32(lblExamID.Text.Trim()), false);
            if (category != null)
            {
                SessionManager.SessionTestCategoryID = category.TestCategoryID;
                Response.Redirect("~/exam.aspx?" + Request.QueryString.ToString());
            }
            else
                this.BindCategories();
        }
        //else
        {

        }
    }

    protected void gvExams_RowEditing(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditCommand")
        {
            Int32 x = Convert.ToInt32(e.CommandArgument);
            SessionManager.SessionExamAction = "editexam";
            SessionManager.SessionSingleExam = TestCategory.Select(x, true);
            SessionManager.SessionQuestionnaire = new List<Questionnaire>();
            SessionManager.SessionTestCategoryID = Convert.ToInt16(e.CommandArgument);

            Response.Redirect("~/editexam.aspx?" + Request.QueryString.ToString());
        }
    }

    private void BindCategories()
    {
        try
        {
            this.ddlAccountCampaign.DataSource = NonCimCampaign.SelectParents();
            this.ddlAccountCampaign.DataBind();
            this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccountCampaign.SelectedValue), true);
            this.ddlSubCategory.DataBind();

            TestCategory[] categories = null;
            {
                if (this.chkIncludeElapsed.Checked)
                    categories = TestCategory.SelectAll(true, false);
                else
                    categories = TestCategory.SelectAll(false, false);
            }

            this.gvExams.DataSource = categories;
            this.TestCategories = categories;
            this.gvExams.DataBind();
            this.GetTimeLimits();
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "exams.aspx", "BindCategories", ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            System.Threading.Thread.Sleep(1500);
            TestCategory[] categories = null;
            if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(1))
                if (Convert.ToInt32(this.ddlAccountCampaign.SelectedValue) == 0)
                    if (!string.IsNullOrEmpty(this.txtSearch.Text.Trim()))
                        if (this.chkIncludeElapsed.Checked)
                            categories = TestCategory.Search(this.txtSearch.Text.Trim(), true);
                        else
                            categories = TestCategory.Search(this.txtSearch.Text.Trim(), false);
                    else
                        if (this.chkIncludeElapsed.Checked)
                            categories = TestCategory.SelectAll(true, false);
                        else
                            categories = TestCategory.SelectAll(false, false);
                else
                    if (!string.IsNullOrEmpty(this.txtSearch.Text.Trim()))
                        if (this.chkIncludeElapsed.Checked)
                            categories = TestCategory.SearchByAccountCampaign(this.txtSearch.Text.Trim(), Math.Abs(Convert.ToInt32(this.ddlAccountCampaign.SelectedValue)), Convert.ToInt32(this.ddlSubCategory.SelectedValue), this.IsCampaignSelected(), true);
                        else
                            categories = TestCategory.SearchByAccountCampaign(this.txtSearch.Text.Trim(), Math.Abs(Convert.ToInt32(this.ddlAccountCampaign.SelectedValue)), Convert.ToInt32(this.ddlSubCategory.SelectedValue), this.IsCampaignSelected(), true);
                    else
                        if (this.chkIncludeElapsed.Checked)
                            categories = TestCategory.SelectByAccountCampaign(Math.Abs(Convert.ToInt32(this.ddlAccountCampaign.SelectedValue)), Convert.ToInt32(this.ddlSubCategory.SelectedValue), this.IsCampaignSelected(), true);
                        else
                            categories = TestCategory.SelectByAccountCampaign(Math.Abs(Convert.ToInt32(this.ddlAccountCampaign.SelectedValue)), Convert.ToInt32(this.ddlSubCategory.SelectedValue), this.IsCampaignSelected(), false);
            this.gvExams.DataSource = categories;
            this.TestCategories = categories;
            this.gvExams.DataBind();
            this.GetTimeLimits();
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "exams.aspx", "btnSearch_Click", ex.Message);
        }
    }

    protected void chkIncludeElapsed_CheckedChanged(object sender, EventArgs e)
    {
        TestCategory[] categories = null;
        if (RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(1))
            if (Convert.ToInt32(this.ddlAccountCampaign.SelectedValue) == 0)
                if (this.chkIncludeElapsed.Checked)
                    categories = TestCategory.SelectAll(true, false);
                else
                    categories = TestCategory.SelectAll(false, false);
            else
                if (chkIncludeElapsed.Checked)
                    categories = TestCategory.SelectByAccountCampaign(Convert.ToInt32(this.ddlAccountCampaign.SelectedValue), Convert.ToInt32(this.ddlSubCategory.SelectedValue), this.IsCampaignSelected(), true);
                else
                    categories = TestCategory.SelectByAccountCampaign(Convert.ToInt32(this.ddlAccountCampaign.SelectedValue), Convert.ToInt32(this.ddlSubCategory.SelectedValue), this.IsCampaignSelected(), false);
        
        this.gvExams.DataSource = categories;
        this.TestCategories = categories;
        this.gvExams.DataBind();
        this.GetTimeLimits();
    }

    protected void GetTimeLimits()
    {
        foreach (GridViewRow row in this.gvExams.Rows)
        {
            Label lblTimeLimitVal = row.FindControl("lblTimeLimitVal") as Label;
            Label lblTimeLimit = row.FindControl("lblTimeLimit") as Label;

            if (Convert.ToInt32(lblTimeLimitVal.Text) > 0)
            {
                decimal timeLimit = Convert.ToDecimal(lblTimeLimitVal.Text.Trim()) / 60;
                lblTimeLimit.Text = timeLimit.ToString("00.##") + " min.";
            }
            else
                lblTimeLimit.Text = "No Time Limit";

            Label lblTestLimitVal = row.FindControl("lblTestLimitVal") as Label;
            Label lblTestLimit = row.FindControl("lblTestLimit") as Label;

            if (Convert.ToInt32(lblTestLimitVal.Text) > 0)
                lblTestLimit.Text = lblTestLimitVal.Text;
            else
                lblTestLimit.Text = "No Test Limit";
        }
    }

    private bool IsCampaignSelected()
    {
        AccountList[] accounts = AccountList.SelectAccountsVader2(1);
        for (int x = 0; x < accounts.Length; x++)
            if (Convert.ToInt32(this.ddlAccountCampaign.SelectedValue) == accounts[x].AccountID)
                return false;
        accounts = AccountList.SelectGenericCampaigns(1);
        for (int x = 0; x < accounts.Length; x++)
            if (Convert.ToInt32(this.ddlAccountCampaign.SelectedValue) == accounts[x].AccountID)
                return true;
        return false;
    }

    protected void gvExams_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            if (this.SortDirection == "ASC")
                this.SortDirection = "DESC";
            else
                this.SortDirection = "ASC";

            if (this.TestCategories != null)
            {
                PropertyInfo[] properties = this.TestCategories.GetType().GetElementType().GetProperties();
                DataTable dt = new DataTable();
                DataColumn dc = null;

                foreach (PropertyInfo pi in properties)
                {
                    dc = new DataColumn();
                    dc.ColumnName = pi.Name;
                    dc.DataType = pi.PropertyType;
                    dt.Columns.Add(dc);
                }

                if (this.TestCategories.Length != 0)
                {
                    foreach (object testCategory in this.TestCategories)
                    {
                        DataRow dr = dt.NewRow();
                        foreach (PropertyInfo pi in properties)
                            dr[pi.Name] = pi.GetValue(testCategory, null);
                        dt.Rows.Add(dr);
                    }
                }

                if (dt != null)
                {
                    DataView dvSortedView = new DataView(dt);
                    dvSortedView.Sort = e.SortExpression + " " + this.SortDirection;

                    gvExams.DataSource = dvSortedView;
                    gvExams.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            //service.WriteError(Config.ApplicationID(), "exams.aspx", "gvExams_Sorting", ex.Message);
        }
    }

    protected void ddlAccountCampaign_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccountCampaign.SelectedValue), true);
        this.ddlSubCategory.DataBind();
    }
}