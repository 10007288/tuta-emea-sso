﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class assignedclients : System.Web.UI.Page
{
    public static DataTable dtSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillClients();
            LoadClients();
        }
    }

    protected void FillClients()
    {
        lboxAvailableClients.DataSource = GetClientsAvail();
        lboxAvailableClients.DataTextField = "Client";
        lboxAvailableClients.DataValueField = "ClientID";
        lboxAvailableClients.DataBind();
    }

    private void LoadClients()
    {
        string query = "SELECT ClientID, Client FROM AssignedClients WHERE TWWID = @TWWID ORDER BY Client ASC";
        dtSelected = GetClientsTaken(query);
        lboxTakenClients.DataSource = dtSelected;
        lboxTakenClients.DataTextField = "Client";
        lboxTakenClients.DataValueField = "ClientID";
        lboxTakenClients.DataBind();
    }

    protected DataTable GetClientsAvail()
    {
        DataTable dt = new DataTable();

        using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["INTRANETConnectionString"].ConnectionString))
        {
            oconn.Open();
            using (SqlCommand ocomm = new SqlCommand("SELECT DISTINCT A.ID AS 'ClientID', A.[Description] AS 'Client'FROM TranscomUniv_local.dbo.Clients A WHERE InactiveDate IS NULL AND Active = 1 AND A.ID NOT IN (SELECT ClientID FROM TranscomUniv_Local.dbo.AssignedClients WHERE TWWID = @TWWID)ORDER BY A.[Description]", oconn))
            {
                ocomm.CommandType = CommandType.Text;
                ocomm.Parameters.AddWithValue("@TWWID", SessionManager.SessionCurrentGridUsers);
                SqlDataAdapter da = new SqlDataAdapter(ocomm);
                da.Fill(dt);
            }
        }
        return dt;
    }

    private DataTable GetClientsTaken(string query)
    {
        String ConnString = ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString;
        SqlConnection conn = new SqlConnection(ConnString);
        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.SelectCommand = new SqlCommand(query, conn);
        adapter.SelectCommand.Parameters.AddWithValue("@TWWID", SessionManager.SessionCurrentGridUsers);

        DataTable myDataTable = new DataTable();

        conn.Open();
        try
        {
            adapter.Fill(myDataTable);
        }
        finally
        {
            conn.Close();
        }

        return myDataTable;
    }

    protected void lboxAvailableClients_Transferred(object sender, Telerik.Web.UI.RadListBoxTransferredEventArgs e)
    {
        lboxAvailableClients.Sort = RadListBoxSort.Ascending;
        lboxAvailableClients.SortItems();
        lboxTakenClients.Sort = RadListBoxSort.Ascending;
        lboxTakenClients.SortItems();

        if (e.SourceListBox.ID == "lboxTakenClients")
        {
            var _item = e.Items.FirstOrDefault();
            var _TwwId = SessionManager.SessionCurrentGridUsers;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
            {
                connection.Open();

                string sql = "DELETE FROM AssignedClients WHERE ClientID = @ClientID AND TWWID = @TWWID";
                SqlCommand cmd = new SqlCommand(sql, connection);

                cmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = _item.Value;
                cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = _TwwId;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (lboxTakenClients.Items.Count <= 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Please select atleast 1 client.');", true);
            return;
        }

        List<RadListBoxItem> _Clients = lboxTakenClients.Items.ToList();
        foreach (RadListBoxItem item in _Clients)
        {
            var _ClientID = Convert.ToInt32(item.Value);
            var _Client = item.Text.ToString();
            var _TwwId = SessionManager.SessionCurrentGridUsers;

            using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["NuSkillCheckV2ConnectionString"].ConnectionString))
            {
                oconn.Open();
                using (SqlCommand ocomm = new SqlCommand("sp_Client_Checker", oconn))
                {
                    ocomm.CommandType = CommandType.StoredProcedure;
                    ocomm.Parameters.AddWithValue("@ClientID", _ClientID);
                    ocomm.Parameters.AddWithValue("@Client", _Client);
                    ocomm.Parameters.AddWithValue("@TWWID", _TwwId);
                    ocomm.ExecuteNonQuery();
                }
                oconn.Close();
            }
        }
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Client/s updated.');", true);
    }
}