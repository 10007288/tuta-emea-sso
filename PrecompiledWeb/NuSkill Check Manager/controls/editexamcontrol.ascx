<%@ control language="C#" autoeventwireup="true" inherits="controls_editexamcontrol, App_Web_2jcq2tmn" %>
<%@ Register TagPrefix="wus" TagName="editexamitemcontrol" Src="~/controls/editexamquestioncontrol.ascx" %>
<asp:Panel ID="pnlExam" runat="server" Width="100%" HorizontalAlign="center" Font-Size="12px">
    <asp:UpdatePanel ID="udpSub" runat="server">
        <ContentTemplate>
            <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial">
                <tr>
                    <td align="center" style="vertical-align: middle; font-size: large;" colspan="2">
                        <table cellpadding="5" cellspacing="0" style="vertical-align: middle; font-family: Arial;
                            font-size: 12px" width="100%" border="1">
                            <tr>
                                <td align="center" colspan="6">
                                    <asp:Panel ID="pnlUpdate" runat="server">
                                        <asp:UpdateProgress ID="udpProgress" runat="server">
                                            <ProgressTemplate>
                                                <asp:Panel ID="pnlBuffer" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                                                <br />
                                                                <asp:Label ID="lblPleaseWait" runat="server" Text="Please Wait..." CssClass="smallText" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </asp:Panel>
                                    <asp:ValidationSummary ID="vsmMain" runat="server" ValidationGroup="ValExam" DisplayMode="list"
                                        CssClass="validatorStyle" />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" valign="top" style="width: 32%">
                                    <asp:Label ID="lblAccounts" runat="server" Text="Test Category:" />
                                    <br />
                                    <asp:DropDownList ID="ddlAccounts" runat="server" DataTextField="Campaign" DataValueField="CampaignID"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlAccounts_SelectedIndexChanged" />
                                    <br />
                                    <br />
                                    <asp:Label ID="lblSubCategory" runat="server" Text="Sub Category:" />
                                    <br />
                                    <asp:DropDownList ID="ddlSubCategory" runat="server" DataTextField="Campaign" DataValueField="CampaignID" />
                                </td>
                                <td colspan="2" align="center" valign="top" style="width: 36%">
                                    <asp:Label ID="lblCourseID" runat="server" Text="CourseID:" />
                                    <br />
                                    <asp:TextBox ID="txtCourseID" runat="server" />
                                    <ajax:FilteredTextBoxExtender ID="ftbCourseID" runat="server" FilterMode="validchars"
                                        FilterType="numbers" TargetControlID="txtCourseID" />
                                </td>
                                <td colspan="2" align="center" valign="middle" style="width: 32%">
                                    <asp:CheckBox ID="chkHideGrades" runat="server" Text="Hide exam grades from examinees" />
                                    <br />
                                    <br />
                                    <asp:CheckBox ID="chkRetake" runat="server" Text="Retake for passed test" />
                                    <ajax:Accordion ID="accordion1" runat="server" Width="90%" RequireOpenedPane="false"
                                        AutoSize="none" SelectedIndex="-1" Visible="false">
                                        <Panes>
                                            <ajax:AccordionPane ID="AccordionPane1" runat="server" HeaderCssClass="accordionHeader"
                                                ContentCssClass="accordionContent">
                                                <Header>
                                                    <asp:Label ID="lblTestGroupHeader" runat="server" Text="Choose Test Group/s:" />
                                                </Header>
                                                <Content>
                                                    <asp:Panel ID="pnlInside" runat="server" Width="95%">
                                                        <asp:Button ID="btnAddNewGroup" runat="server" Text="Add New Group" CssClass="buttons"
                                                            Width="150px" Height="20px" Font-Size="X-Small" OnClick="btnAddNewGroup_Click" />
                                                        <asp:Panel ID="Panel1" runat="server" Width="100%" Height="200px" ScrollBars="Auto"
                                                            HorizontalAlign="left">
                                                            <asp:Panel ID="pnlAddGroup" runat="server" Width="80%" Visible="false" ScrollBars="none">
                                                                <table cellpadding="3" cellspacing="0">
                                                                    <tr align="left">
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="txtAddNewGroup" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Button ID="btnAddGroupOK" runat="server" Text="Add" CssClass="buttons" OnClick="btnAddGroupOK_Click"
                                                                                Font-Size="x-Small" />
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Button ID="btnAddGroupCancel" runat="server" Text="Cancel" CssClass="buttons"
                                                                                OnClick="btnAddGroupCancel_Click" Font-Size="x-small" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:GridView ID="gvGroups" runat="server" AutoGenerateColumns="false" Width="90%"
                                                                BorderWidth="0px" Visible="false" BorderColor="Black" AllowPaging="false">
                                                                <RowStyle BorderColor="black" ForeColor="black" Font-Size="11px" />
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <table cellpadding="1" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkGroup" runat="server" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lblTestGroupID" runat="server" Text='<%#Bind("TestGroupID") %>' Visible="false" />
                                                                                        <asp:Label ID="lblGroupName" runat="server" Text='<%#Bind("TestGroupName") %>' />
                                                                                        <asp:Label ID="lblIsPartOfGroup" runat="server" Visible="false" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="gvGroupsEdit" runat="server" AutoGenerateColumns="false" Width="90%"
                                                                BorderWidth="0px" Visible="false" BorderColor="Black" AllowPaging="false">
                                                                <RowStyle BorderColor="black" ForeColor="black" Font-Size="11px" />
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <table cellpadding="1" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkGroup" runat="server" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lblTestGroupID" runat="server" Text='<%#Bind("TestGroupID") %>' Visible="false" />
                                                                                        <asp:Label ID="lblGroupName" runat="server" Text='<%#Bind("TestGroupName") %>' />
                                                                                        <asp:Label ID="lblIsPartOfGroup" runat="server" Text='<%#Bind("IsPartOfGroup") %>'
                                                                                            Visible="false" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                </Content>
                                            </ajax:AccordionPane>
                                        </Panes>
                                    </ajax:Accordion>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;" colspan="6" align="center">
                                    <asp:Label ID="Label3" runat="server" Visible="false" />
                                    <asp:Label ID="lblTestNameEdit" runat="server" Text="Name of Test:" />
                                    <br />
                                    <asp:TextBox ID="txtExamName" runat="server" MaxLength="200" Width="70%" />
                                    <asp:CustomValidator ID="cvExamName" runat="server" ErrorMessage="Enter an Exam Name."
                                        Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;" colspan="6" align="center">
                                    <asp:CheckBox ID="chkRandomizeQuestions" runat="server" Text="Not Randomize Questions" />
                                    <asp:CheckBox ID="chkHideExamFromTesting" runat="server" Text="Hide from TUT" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Label ID="lblInstructions" runat="server" Text="Instructions (please do not use HTML):" />
                                    <br />
                                    <asp:TextBox ID="txtInstructions" runat="server" TextMode="multiline" Rows="6" Width="70%" />
                                    <ajax:FilteredTextBoxExtender ID="ftbInstructions" runat="server" TargetControlID="txtInstructions"
                                        FilterMode="invalidchars" FilterType="custom" InvalidChars="<>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="width: 50%">
                                    <asp:Label ID="lblStartDateEdit" runat="server" Text="Start Date:" />
                                    <br />
                                    <asp:TextBox ID="txtStartDate" runat="server" /><asp:ImageButton ID="imgCalendarStartDate"
                                        runat="server" ImageUrl="~/images/icon-calendar.gif" />
                                    <ajax:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtStartDate"
                                        PopupButtonID="imgCalendarStartDate" Format="yyyy/MM/dd" />
                                    <asp:DropDownList ID="ddlStartHour" runat="server" />
                                    <asp:Label ID="lblStartDateColon" runat="server" Text=":" Font-Bold="true" />
                                    <asp:DropDownList ID="ddlStartMinute" runat="server" />
                                    <asp:CustomValidator ID="cvStartDate" runat="server" ErrorMessage="Make sure the Start Date follows the format stated."
                                        Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                                </td>
                                <td colspan="3" style="width: 50%">
                                    <asp:Label ID="lblEndDateEdit" runat="server" Text="End Date:" />
                                    <br />
                                    <asp:TextBox ID="txtEndDate" runat="server" /><asp:ImageButton ID="imgCalendarEndDate"
                                        runat="server" ImageUrl="~/images/icon-calendar.gif" />
                                    <ajax:CalendarExtender ID="calEndDate" runat="server" TargetControlID="txtEndDate"
                                        PopupButtonID="imgCalendarEndDate" Format="yyyy/MM/dd" />
                                    <asp:DropDownList ID="ddlEndHour" runat="server" />
                                    <asp:Label ID="lblEndDateColon" runat="server" Text=":" Font-Bold="true" />
                                    <asp:DropDownList ID="ddlEndMinute" runat="server" />
                                    <asp:CustomValidator ID="cvEndDate" runat="server" ErrorMessage="Make sure the End Date follows the format stated."
                                        Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width: 33%">
                                    <asp:Label ID="lblDurationEdit" runat="server" Text="Duration<br />(Minutes - use 0 for no time limit):" />
                                    <br />
                                    <asp:TextBox ID="txtMinutes" runat="server" />
                                    <ajax:FilteredTextBoxExtender ID="ftbMinutes" runat="server" FilterMode="validchars"
                                        FilterType="numbers" TargetControlID="txtMinutes" />
                                    <asp:CustomValidator ID="cvMinutes" runat="server" ErrorMessage="Enter a whole number for the duration."
                                        Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                                </td>
                                <td colspan="2" style="width: 34%">
                                    <asp:Label ID="lblNumberOfTriesEdit" runat="server" Text="Number of Tries Allowed<br />(use 0 for unlimited tries):" />
                                    <br />
                                    <asp:TextBox ID="txtNumTries" runat="server" />
                                    <ajax:FilteredTextBoxExtender ID="ftbNumTries" runat="server" FilterMode="validchars"
                                        FilterType="numbers" TargetControlID="txtNumTries" />
                                    <asp:CustomValidator ID="cvNumTries" runat="server" ErrorMessage="Enter a whole number for the number of tries."
                                        Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                                </td>
                                <td colspan="2" style="width: 33%">
                                    <asp:Label ID="lblPassingScoreEdit" runat="server" Text="Passing Score<br />(Percentage - i.e. use 80 for 80%):" />
                                    <br />
                                    <asp:TextBox ID="txtPassingGrade" runat="server" MaxLength="3" />
                                    <ajax:FilteredTextBoxExtender ID="ftbPassingGrade" runat="server" FilterMode="validchars"
                                        FilterType="numbers" TargetControlID="txtPassingGrade" />
                                    <asp:CustomValidator ID="cvPassingGrade" runat="server" ErrorMessage="Enter a valid whole number for the passing grade."
                                        Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                                </td>
                            </tr>
                            <%--<tr>
                        <td colspan="6">
                            <ajax:Accordion ID="accordionTestGroups" runat="server" Width="100%" BorderColor="black"
                                BorderWidth="3px" RequireOpenedPane="false" AutoSize="none" SelectedIndex="0">
                                <Panes>
                                    <ajax:AccordionPane ID="paneGroup" runat="server" HeaderCssClass="accordionHeader"
                                        ContentCssClass="accordionContent">
                                        <Header>
                                            <asp:Label ID="lblGroupHeader" runat="server" Text="Choose Test Group/s:" />
                                        </Header>
                                        <Content>
                                            <asp:Panel ID="pnlGroupContent" runat="server" Width="100%" Height="400px">
                                            </asp:Panel>
                                        </Content>
                                    </ajax:AccordionPane>
                                </Panes>
                            </ajax:Accordion>
                        </td>
                    </tr>--%>
                            <tr>
                                <td colspan="6">
                                    <asp:Label ID="lblExamItemsEdit" runat="server" Text="Exam Items:" Font-Bold="true" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="left">
                                    <asp:Button CssClass="buttons" ID="btnAddItem" runat="server" Text="Add Item" OnClick="btnAddItem_Click" /><br />
                                    <asp:GridView ID="gvGridItems" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        BorderColor="black" Font-Size="12px" CellPadding="5" BorderWidth="1" OnRowDeleting="gvGridItems_RowDeleting"
                                        OnSelectedIndexChanging="gvGridItems_SelectedIndexChanging" EmptyDataText="This test currently has no items."
                                        Width="100%" OnRowEditing="gvGridItems_RowEditing">
                                        <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                                        <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                                            NextPageText=">" Position="Bottom" PreviousPageText="<" />
                                        <RowStyle BorderColor="black" ForeColor="black" Font-Size="11px" />
                                        <HeaderStyle BorderColor="#C1D7E7" Height="1px" />
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="center" HeaderText="Question" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %>'
                                                        Visible="false" />
                                                    <asp:LinkButton ID="lnkItemQuestion" runat="server" ForeColor="black" Text='<%#Bind("Question") %>'
                                                        CommandName="Select" />
                                                    <asp:Label ID="lblItemQuestion" runat="server" ForeColor="black" Text='<%#Bind("Question") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" ForeColor="black"
                                                        Font-Bold="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="Delete"
                                                        ForeColor="black" Font-Bold="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                              <tr>
                            <td colspan="6">
                                <asp:Label ID="lblHotlink" runat="server" Text="Exam Hotlink:" />
                                <br />
                                <asp:TextBox ID="txtHotlink" runat="server" ReadOnly="true" Width="70%" />
                            </td>
                        </tr>
                            <%--<tr>
            <td colspan="6">
                <asp:LinkButton ID="LinkButton1" runat="server" Text="Click here for a list of users who have taken the exam"
                    Font-Bold="true" OnClick="lnkExamTakers_Click" ForeColor="black" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left">
                <asp:Button ID="btnSaveExamEdit" runat="server" Text="Save" CssClass="buttons" Width="80px" />
            </td>
            <td colspan="3" align="right">
                <asp:Button ID="btnCancelExamEdit" runat="server" Text="Cancel" CssClass="buttons"
                    Width="80px" />
            </td>
        </tr>--%>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="middle" colspan="3">
                        <asp:Button CssClass="buttons" ID="btnContinue" runat="server" Text="Create" OnClick="btnContinue_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:HiddenField ID="hidStatus" runat="server" />
<asp:HiddenField ID="hidDelete" runat="server" />
<ajax:ModalPopupExtender ID="mpeStatus" runat="server" TargetControlID="hidStatus"
    PopupControlID="pnlStatus" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlStatus" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
        font-family: Arial; font-size: 12px; width: 200px">
        <tr>
            <td align="center">
                <asp:Label ID="lblStatus" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button CssClass="buttons" ID="btnStatus" runat="server" Text="Return" OnClick="btnStatus_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeDeleteExam" runat="server" TargetControlID="hidDelete"
    PopupControlID="pnlDeleteExam" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlDeleteExam" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
        font-family: Arial; font-size: 12px; width: 400px">
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblConfirmDelete" runat="server" Text="Delete Item?" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button CssClass="buttons" ID="btnConfirmDelete" runat="server" Text="Delete"
                    OnClick="btnConfirmDelete_Click" />
            </td>
            <td align="right">
                <asp:Button CssClass="buttons" ID="btnConfirmCancel" runat="server" Text="Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeNoItems" runat="server" TargetControlID="hidNoItems"
    PopupControlID="pnlNoItems" BackgroundCssClass="modalBackground" />
<asp:HiddenField ID="hidNoItems" runat="server" />
<asp:Panel ID="pnlNoItems" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
        font-family: Arial; font-size: 12px; width: 230px">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblNoItems" runat="server" Text="There are no items for this exam yet. Do you wish to continue saving?" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button CssClass="buttons" ID="btnNoItemsConfirm" runat="server" Text="Save"
                    Width="60px" OnClick="btnNoItemsConfirm_Click" />
            </td>
            <td align="right">
                <asp:Button CssClass="buttons" ID="btnNoItemsCancel" runat="server" Text="Cancel"
                    Width="60px" />
            </td>
        </tr>
    </table>
</asp:Panel>
