<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewReports.aspx.cs" Inherits="ViewReports" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="srs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transcom University Testing Admin</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableScriptLocalization="True">
    </telerik:RadScriptManager>
    <%--<asp:DropDownList ID="ddlReports" runat="server" DataTextField="DisplayName" DataValueField="ReportURL" />
        <asp:Button ID="btnView" runat="server" Text="View" CssClass="buttons" OnClick="btnView_Click" />
        <br />
        <br />--%>
    <%--<srs:ReportViewer ID="ReportViewer1" runat="server" ShowBackButton="false" ShowPrintButton="false"
            ShowZoomControl="false" ShowFindControls="false" ShowExportControls="true" Width="100%"
            Height="600px" ProcessingMode="Remote">
        </srs:ReportViewer>--%>
    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Correct Answer Percentage Per Question By Exam
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox1" runat="server" DataSourceID="SqlDataSource1"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox1_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox2" runat="server" DataSourceID="SqlDataSource2"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox2_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox1" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox3" runat="server">
                    </telerik:RadComboBox>
                    <telerik:RadComboBox ID="rcbHiddenClient" runat="server" Visible="false">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker1" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker2" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID1" runat="server" Enabled="false"></asp:TextBox>
                    <asp:Label runat="server" ID="lblClientID" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate1" runat="server" Text="Generate" OnClick="cmdGenerate1_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource3" GridLines="None"
            AutoGenerateColumns="False" OnItemCreated="RadGrid1_ItemCreated" ExportSettings-ExportOnlyData="true"
            ExportSettings-IgnorePaging="true">
            <MasterTableView DataSourceID="SqlDataSource3" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="testcategoryid" HeaderText="testcategoryid" SortExpression="testcategoryid"
                        UniqueName="testcategoryid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testname" HeaderText="Test" SortExpression="testname"
                        UniqueName="testname">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="questionnaireid" DataType="System.Int32" HeaderText="questionnaireid"
                        SortExpression="questionnaireid" UniqueName="questionnaireid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="question" HeaderText="Question" SortExpression="question"
                        UniqueName="question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CorrectCount" DataType="System.Double" HeaderText="Correct Count"
                        ReadOnly="True" SortExpression="CorrectCount" UniqueName="CorrectCount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AnsweredCount" DataType="System.Double" HeaderText="Answered Count"
                        SortExpression="AnsweredCount" UniqueName="AnsweredCount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="correct" HeaderText="Correct" ReadOnly="True"
                        SortExpression="correct" UniqueName="correct">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportCorrectAnswerPercentagePerQuestioByExam" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox3" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker1" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker2" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="txtTWWID1" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Answers by Employee by Test Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox4" runat="server" DataSourceID="SqlDataSource4"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox4_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox5" runat="server" DataSourceID="SqlDataSource5"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox5_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox4" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox6" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker3" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker4" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID2" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate2" runat="server" Text="Generate" OnClick="cmdGenerate2_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid2" runat="server" DataSourceID="SqlDataSource6" GridLines="None"
            AutoGenerateColumns="False" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid2_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource6" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="testcategoryid" DataType="System.Int32" HeaderText="Test ID"
                        ReadOnly="True" SortExpression="testcategoryid" UniqueName="testcategoryid">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testname" HeaderText="Test" SortExpression="testname"
                        UniqueName="testname" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testtakenid" DataType="System.Int32" HeaderText="Test Taken ID"
                        ReadOnly="True" SortExpression="testtakenid" UniqueName="testtakenid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="userid" HeaderText="Employee" ReadOnly="True"
                        SortExpression="userid" UniqueName="userid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testresponseid" DataType="System.Int32" HeaderText="testresponseid"
                        ReadOnly="True" SortExpression="testresponseid" UniqueName="testresponseid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question" HeaderText="Question" ReadOnly="True"
                        SortExpression="Question" UniqueName="Question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status"
                        UniqueName="Status">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="responsevalue" HeaderText="Answer" ReadOnly="True"
                        SortExpression="responsevalue" UniqueName="responsevalue">
                    </telerik:GridBoundColumn>
                </Columns>
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldName="testtakenid" HeaderText="Test Taken ID" />
                            <telerik:GridGroupByField FieldName="userid" HeaderText="Employee" />
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="userid" />
                            <telerik:GridGroupByField FieldName="testtakenid" />
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAnswerByEmployeeByTestReportNew" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox6" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker3" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker4" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="txtTWWID2" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel3" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Correct Answer Percentage Per Question By Expired Exam
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox7" runat="server" DataSourceID="SqlDataSource7"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox7_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox8" runat="server" DataSourceID="SqlDataSource8"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox8_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox7" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox9" runat="server" DataSourceID="SqlDataSource10"
                        DataTextField="testname" DataValueField="testcategoryid">
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource10" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="SELECT DISTINCT
cast(testcategoryid as varchar) as testcategoryid,  
RTRIM(LTRIM(testname)) as 'testname', 
testname as 'title' 
FROM
tbl_testing_testcategory 
WHERE
EndDate &lt;= GETDATE()
	AND CampaignID = @SubcategoryID OR (CampaignID = 0 AND @CategoryID = 220032)
ORDER BY
testname">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox8" Name="SubcategoryID" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="RadComboBox7" Name="CategoryID" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker5" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker6" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID6" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate3" runat="server" Text="Generate" OnClick="cmdGenerate3_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource9"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid3_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource9" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="testcategoryid" HeaderText="testcategoryid" SortExpression="testcategoryid"
                        UniqueName="testcategoryid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testname" HeaderText="Test" SortExpression="testname"
                        UniqueName="testname">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="questionnaireid" DataType="System.Int32" HeaderText="questionnaireid"
                        SortExpression="questionnaireid" UniqueName="questionnaireid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="question" HeaderText="Question" SortExpression="question"
                        UniqueName="question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CorrectCount" DataType="System.Double" HeaderText="Correct Count"
                        ReadOnly="True" SortExpression="CorrectCount" UniqueName="CorrectCount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AnsweredCount" DataType="System.Double" HeaderText="Answered Count"
                        SortExpression="AnsweredCount" UniqueName="AnsweredCount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="correct" HeaderText="Correct" ReadOnly="True"
                        SortExpression="correct" UniqueName="correct">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource9" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportCorrectAnswerPercentagePerQuestionByExpiredExam" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox9" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker5" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker6" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="txtTWWID6" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel4" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Answer By Employee By Expired Test Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox10" runat="server" DataSourceID="SqlDataSource11"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox10_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource11" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox11" runat="server" DataSourceID="SqlDataSource12"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox11_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource12" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox10" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox12" runat="server" DataSourceID="SqlDataSource13"
                        DataTextField="testname" DataValueField="testcategoryid">
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource13" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="SELECT DISTINCT
cast(testcategoryid as varchar) as testcategoryid,  
RTRIM(LTRIM(testname)) as 'testname', 
testname as 'title' 
FROM
tbl_testing_testcategory 
WHERE
EndDate &lt;= GETDATE()
	AND CampaignID = @SubcategoryID OR (CampaignID = 0 AND @CategoryID = 220032)
ORDER BY
testname">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox11" Name="SubcategoryID" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="RadComboBox10" Name="CategoryID" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker7" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker8" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID7" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate4" runat="server" Text="Generate" OnClick="cmdGenerate4_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid4" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource14"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid4_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource14" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="testcategoryid" DataType="System.Int32" HeaderText="testcategoryid"
                        ReadOnly="True" SortExpression="testcategoryid" UniqueName="testcategoryid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testname" HeaderText="Test" SortExpression="testname"
                        UniqueName="testname">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testtakenid" DataType="System.Int32" HeaderText="testtakenid"
                        ReadOnly="True" SortExpression="testtakenid" UniqueName="testtakenid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="userid" HeaderText="userid" ReadOnly="True" SortExpression="userid"
                        UniqueName="userid">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="testresponseid" DataType="System.Int32" HeaderText="testresponseid"
                        ReadOnly="True" SortExpression="testresponseid" UniqueName="testresponseid" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="responsevalue" HeaderText="Response" ReadOnly="True"
                        SortExpression="responsevalue" UniqueName="responsevalue">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status"
                        UniqueName="Status">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource14" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAnswerByEmployeeByExpiredTest" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox12" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker7" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker8" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="txtTWWID7" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel5" runat="server" Visible="false">
        <table>
            <tr>
                <td>
                    Exam Results By Test Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox13" runat="server" DataSourceID="SqlDataSource15"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox13_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource15" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox14" runat="server" DataSourceID="SqlDataSource16"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox14_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource16" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox13" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox15" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker9" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker10" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID3" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate5" runat="server" Text="Generate" OnClick="cmdGenerate5_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid5" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource17"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid5_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource17" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="CIM" HeaderText="TWWID" SortExpression="CIM" UniqueName="CIM">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName"
                        UniqueName="FirstName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="TestName" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Score" HeaderText="Score" ReadOnly="True" SortExpression="Score"
                        UniqueName="Score">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Passed" HeaderText="Passed" ReadOnly="True" SortExpression="Passed"
                        UniqueName="Passed">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DateTaken" DataType="System.DateTime" HeaderText="DateTaken"
                        SortExpression="DateTaken" UniqueName="DateTaken">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Site" HeaderText="Site" SortExpression="Site"
                        UniqueName="Site">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource17" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportExamResultsByTest" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox15" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker9" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker10" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="txtTWWID3" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel6" runat="server" Visible="false">
    </asp:Panel>
    <asp:Panel ID="Panel7" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Exam Results By Expired Test Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox16" runat="server" DataSourceID="SqlDataSource18"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox16_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource18" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox17" runat="server" DataSourceID="SqlDataSource19"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox17_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource19" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox16" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox18" runat="server" DataSourceID="SqlDataSource20"
                        DataTextField="testname" DataValueField="testcategoryid">
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource20" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="SELECT DISTINCT
cast(testcategoryid as varchar) as testcategoryid,  
RTRIM(LTRIM(testname)) as 'testname', 
testname as 'title' 
FROM
tbl_testing_testcategory 
WHERE
EndDate &lt;= GETDATE()
	AND CampaignID = @SubcategoryID OR (CampaignID = 0 AND @CategoryID = 220032)
ORDER BY
testname">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox17" Name="SubcategoryID" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="RadComboBox16" Name="CategoryID" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker11" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker12" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID8" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate6" runat="server" Text="Generate" OnClick="cmdGenerate6_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid6" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource21"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid6_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource21" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="CIM" HeaderText="CIM" SortExpression="CIM" UniqueName="CIM">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                        UniqueName="FirstName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="Test" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Score" HeaderText="Score" ReadOnly="True" SortExpression="Score"
                        UniqueName="Score">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Passed" HeaderText="Passed" ReadOnly="True" SortExpression="Passed"
                        UniqueName="Passed">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DateTaken" DataType="System.DateTime" HeaderText="Date Taken"
                        SortExpression="DateTaken" UniqueName="DateTaken">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Site" HeaderText="Site" SortExpression="Site"
                        UniqueName="Site">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource21" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportExamResultsByExpiredTest" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox18" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker11" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker12" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="txtTWWID8" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel8" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Average Score By Exam
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox19" runat="server" DataSourceID="SqlDataSource22"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox19_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource22" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox20" runat="server" DataSourceID="SqlDataSource23"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox20_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource23" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox19" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox21" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker13" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker14" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID4" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate7" runat="server" Text="Generate" OnClick="cmdGenerate7_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid7" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource24"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid7_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource24" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="Test" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="average" DataType="System.Double" HeaderText="Average"
                        ReadOnly="True" SortExpression="average" UniqueName="average">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource24" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAverageScoreByExam" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox21" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker13" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker14" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="txtTWWID4" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel9" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Answer Per Question Report
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox22" runat="server" DataSourceID="SqlDataSource25"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox22_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource25" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox23" runat="server" DataSourceID="SqlDataSource26"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox23_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource26" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox22" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox24" runat="server">
                    </telerik:RadComboBox>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID5" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate8" runat="server" Text="Generate" OnClick="cmdGenerate8_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid8" runat="server" DataSourceID="SqlDataSource27" GridLines="None"
            AutoGenerateColumns="false" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid8_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource27" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="TestCategoryID" HeaderText="TestCategoryID" SortExpression="TestCategoryID"
                        UniqueName="TestCategoryID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="Test" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question" HeaderText="Question" SortExpression="Question"
                        UniqueName="Question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice1" HeaderText="Choice1" SortExpression="Choice1"
                        UniqueName="Choice1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice2" HeaderText="Choice2" SortExpression="Choice2"
                        UniqueName="Choice2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice3" HeaderText="Choice3" SortExpression="Choice3"
                        UniqueName="Choice3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice4" HeaderText="Choice4" SortExpression="Choice4"
                        UniqueName="Choice4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice5" HeaderText="Choice5" SortExpression="Choice5"
                        UniqueName="Choice5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans1" HeaderText="Ans1" SortExpression="Ans1"
                        UniqueName="Ans1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans2" HeaderText="Ans2" SortExpression="Ans2"
                        UniqueName="Ans2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans3" HeaderText="Ans3" SortExpression="Ans3"
                        UniqueName="Ans3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans4" HeaderText="Ans4" SortExpression="Ans4"
                        UniqueName="Ans4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans5" HeaderText="Ans5" SortExpression="Ans5"
                        UniqueName="Ans5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice1%" HeaderText="Choice1%" ReadOnly="True"
                        SortExpression="Choice1%" UniqueName="Choice1%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice2%" HeaderText="Choice2%" ReadOnly="True"
                        SortExpression="Choice2%" UniqueName="Choice2%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice3%" HeaderText="Choice3%" ReadOnly="True"
                        SortExpression="Choice3%" UniqueName="Choice3%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice4%" HeaderText="Choice4%" ReadOnly="True"
                        SortExpression="Choice4%" UniqueName="Choice4%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice5%" HeaderText="Choice5%" ReadOnly="True"
                        SortExpression="Choice5%" UniqueName="Choice5%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Correct" DataType="System.Double" HeaderText="Correct"
                        ReadOnly="True" SortExpression="Correct" UniqueName="Correct">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Total" DataType="System.Double" HeaderText="Total"
                        SortExpression="Total" UniqueName="Total">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" ReadOnly="True"
                        SortExpression="Percentage" UniqueName="Percentage">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource27" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAnswerPerQuestion" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox24" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="txtTWWID5" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="Panel10" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="6">
                    Answer Per Question Report - Expired Tests
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox25" runat="server" DataSourceID="SqlDataSource28"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox25_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource28" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParents" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox26" runat="server" DataSourceID="SqlDataSource29"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox26_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource29" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox25" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox27" runat="server" DataSourceID="SqlDataSource30"
                        DataTextField="testname" DataValueField="testcategoryid">
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource30" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="SELECT DISTINCT
cast(testcategoryid as varchar) as testcategoryid,  
RTRIM(LTRIM(testname)) as 'testname', 
testname as 'title' 
FROM
tbl_testing_testcategory 
WHERE
EndDate &lt;= GETDATE()
	AND CampaignID = @SubcategoryID OR (CampaignID = 0 AND @CategoryID = 220032)
ORDER BY
testname">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox26" Name="SubcategoryID" PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="RadComboBox25" Name="CategoryID" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID9" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate9" runat="server" Text="Generate" OnClick="cmdGenerate9_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid9" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource31"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid9_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource31" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="TestCategoryID" HeaderText="TestCategoryID" SortExpression="TestCategoryID"
                        UniqueName="TestCategoryID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TestName" HeaderText="Test" SortExpression="TestName"
                        UniqueName="TestName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question" HeaderText="Question" SortExpression="Question"
                        UniqueName="Question">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice1" HeaderText="Choice1" SortExpression="Choice1"
                        UniqueName="Choice1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice2" HeaderText="Choice2" SortExpression="Choice2"
                        UniqueName="Choice2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice3" HeaderText="Choice3" SortExpression="Choice3"
                        UniqueName="Choice3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice4" HeaderText="Choice4" SortExpression="Choice4"
                        UniqueName="Choice4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice5" HeaderText="Choice5" SortExpression="Choice5"
                        UniqueName="Choice5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans1" HeaderText="Ans1" SortExpression="Ans1"
                        UniqueName="Ans1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans2" HeaderText="Ans2" SortExpression="Ans2"
                        UniqueName="Ans2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans3" HeaderText="Ans3" SortExpression="Ans3"
                        UniqueName="Ans3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans4" HeaderText="Ans4" SortExpression="Ans4"
                        UniqueName="Ans4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ans5" HeaderText="Ans5" SortExpression="Ans5"
                        UniqueName="Ans5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice1%" HeaderText="Choice1%" ReadOnly="True"
                        SortExpression="Choice1%" UniqueName="Choice1%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice2%" HeaderText="Choice2%" ReadOnly="True"
                        SortExpression="Choice2%" UniqueName="Choice2%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice3%" HeaderText="Choice3%" ReadOnly="True"
                        SortExpression="Choice3%" UniqueName="Choice3%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice4%" HeaderText="Choice4%" ReadOnly="True"
                        SortExpression="Choice4%" UniqueName="Choice4%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Choice5%" HeaderText="Choice5%" ReadOnly="True"
                        SortExpression="Choice5%" UniqueName="Choice5%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Correct" DataType="System.Double" HeaderText="Correct"
                        ReadOnly="True" SortExpression="Correct" UniqueName="Correct">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Total" DataType="System.Double" HeaderText="Total"
                        SortExpression="Total" UniqueName="Total">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" ReadOnly="True"
                        SortExpression="Percentage" UniqueName="Percentage">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource31" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportAnswerPerQuestionExpiredTest" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox27" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="txtTWWID9" Name="TWWID" PropertyName="Text"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <!--ADDED BY RAYMARK COSME <01/31/2018> -->
    <asp:Panel ID="Panel11" runat="server" Visible="false">
        <table>
            <tr>
                <td>
                    Survey
                </td>
            </tr>
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox28" runat="server" DataSourceID="SqlDataSource32"
                        DataTextField="Campaign" DataValueField="CampaignID" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox28_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource32" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectParentSurvey" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </td>
                <td>
                    Sub category:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox29" runat="server" DataSourceID="SqlDataSource33"
                        DataTextField="Campaign" DataValueField="CampaignID" Height="100px" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="RadComboBox29_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource33" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
                        SelectCommand="pr_Campaign_SelectFromParent" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="RadComboBox28" Name="CampaignID" PropertyName="SelectedValue"
                                Type="Int32" />
                            <asp:Parameter Name="IncludeNone" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    Test:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBox30" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Start date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker15" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End date:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePicker16" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID10" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="cmdGenerate10" runat="server" Text="Generate" OnClick="cmdGenerate10_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid10" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource34"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true"
            OnItemCreated="RadGrid10_ItemCreated">
            <MasterTableView DataSourceID="SqlDataSource34" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="TWWID" HeaderText="TWWID" SortExpression="TWWID" UniqueName="TWWID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Name" HeaderText="Name" SortExpression="Name"
                        UniqueName="Name">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Position" HeaderText="Position" SortExpression="Position"
                        UniqueName="Position">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Department" HeaderText="Department" ReadOnly="True" SortExpression="Department"
                        UniqueName="Department">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Program" HeaderText="Program" ReadOnly="True" SortExpression="Program"
                        UniqueName="Program">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReportsTo" HeaderText="ReportsTo" ReadOnly="True" SortExpression="ReportsTo"
                        UniqueName="ReportsTo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SurveyName" HeaderText="SurveyName" ReadOnly="True" SortExpression="SurveyName"
                        UniqueName="SurveyName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DateTaken" DataType="System.DateTime" HeaderText="DateTaken"
                        SortExpression="DateTaken" UniqueName="DateTaken">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question1" HeaderText="Question1" SortExpression="Question1"
                        UniqueName="Question1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer1" HeaderText="Answer1" SortExpression="Answer1"
                        UniqueName="Answer1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question2" HeaderText="Question2" SortExpression="Question2"
                        UniqueName="Question2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer2" HeaderText="Answer2" SortExpression="Answer2"
                        UniqueName="Answer2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question3" HeaderText="Question3" SortExpression="Question3"
                        UniqueName="Question3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer3" HeaderText="Answer3" SortExpression="Answer3"
                        UniqueName="Answer3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question4" HeaderText="Question4" SortExpression="Question4"
                        UniqueName="Question4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer4" HeaderText="Answer4" SortExpression="Answer4"
                        UniqueName="Answer4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question5" HeaderText="Question5" SortExpression="Question5"
                        UniqueName="Question5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer5" HeaderText="Answer5" SortExpression="Answer5"
                        UniqueName="Answer5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question6" HeaderText="Question6" SortExpression="Question6"
                        UniqueName="Question6">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer6" HeaderText="Answer6" SortExpression="Answer6"
                        UniqueName="Answer6">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question7" HeaderText="Question7" SortExpression="Question7"
                        UniqueName="Question7">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer7" HeaderText="Answer7" SortExpression="Answer7"
                        UniqueName="Answer7">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question8" HeaderText="Question8" SortExpression="Question8"
                        UniqueName="Question8">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer8" HeaderText="Answer8" SortExpression="Answer8"
                        UniqueName="Answer8">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question9" HeaderText="Question9" SortExpression="Question9"
                        UniqueName="Question9">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer9" HeaderText="Answer9" SortExpression="Answer9"
                        UniqueName="Answer9">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question10" HeaderText="Question10" SortExpression="Question10"
                        UniqueName="Question10">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer10" HeaderText="Answer10" SortExpression="Answer10"
                        UniqueName="Answer10">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question11" HeaderText="Question11" SortExpression="Question11"
                        UniqueName="Question11">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer11" HeaderText="Answer11" SortExpression="Answer11"
                        UniqueName="Answer11">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question12" HeaderText="Question12" SortExpression="Question12"
                        UniqueName="Question12">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer12" HeaderText="Answer12" SortExpression="Answer12"
                        UniqueName="Answer12">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question13" HeaderText="Question13" SortExpression="Question13"
                        UniqueName="Question13">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer13" HeaderText="Answer13" SortExpression="Answer13"
                        UniqueName="Answer13">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question14" HeaderText="Question14" SortExpression="Question14"
                        UniqueName="Question14">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer14" HeaderText="Answer14" SortExpression="Answer14"
                        UniqueName="Answer14">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question15" HeaderText="Question15" SortExpression="Question15"
                        UniqueName="Question15">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer15" HeaderText="Answer15" SortExpression="Answer15"
                        UniqueName="Answer15">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question16" HeaderText="Question16" SortExpression="Question16"
                        UniqueName="Question16">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer16" HeaderText="Answer16" SortExpression="Answer16"
                        UniqueName="Answer16">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question17" HeaderText="Question17" SortExpression="Question17"
                        UniqueName="Question17">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer17" HeaderText="Answer17" SortExpression="Answer17"
                        UniqueName="Answer17">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question18" HeaderText="Question18" SortExpression="Question18"
                        UniqueName="Question18">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer18" HeaderText="Answer18" SortExpression="Answer18"
                        UniqueName="Answer18">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question19" HeaderText="Question19" SortExpression="Question19"
                        UniqueName="Question19">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer19" HeaderText="Answer19" SortExpression="Answer19"
                        UniqueName="Answer19">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question20" HeaderText="Question20" SortExpression="Question20"
                        UniqueName="Question20">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer20" HeaderText="Answer20" SortExpression="Answer20"
                        UniqueName="Answer20">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question21" HeaderText="Question21" SortExpression="Question21"
                        UniqueName="Question21">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer21" HeaderText="Answer21" SortExpression="Answer21"
                        UniqueName="Answer21">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question22" HeaderText="Question22" SortExpression="Question22"
                        UniqueName="Question22">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer22" HeaderText="Answer22" SortExpression="Answer22"
                        UniqueName="Answer22">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question23" HeaderText="Question23" SortExpression="Question23"
                        UniqueName="Question23">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer23" HeaderText="Answer23" SortExpression="Answer23"
                        UniqueName="Answer23">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question24" HeaderText="Question24" SortExpression="Question24"
                        UniqueName="Question24">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer24" HeaderText="Answer24" SortExpression="Answer24"
                        UniqueName="Answer24">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question25" HeaderText="Question25" SortExpression="Question25"
                        UniqueName="Question25">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer25" HeaderText="Answer25" SortExpression="Answer25"
                        UniqueName="Answer25">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question26" HeaderText="Question26" SortExpression="Question26"
                        UniqueName="Question26">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer26" HeaderText="Answer26" SortExpression="Answer26"
                        UniqueName="Answer26">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question27" HeaderText="Question27" SortExpression="Question27"
                        UniqueName="Question27">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer27" HeaderText="Answer27" SortExpression="Answer27"
                        UniqueName="Answer27">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question28" HeaderText="Question28" SortExpression="Question28"
                        UniqueName="Question28">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer28" HeaderText="Answer28" SortExpression="Answer28"
                        UniqueName="Answer28">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question29" HeaderText="Question29" SortExpression="Question29"
                        UniqueName="Question29">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer29" HeaderText="Answer29" SortExpression="Answer29"
                        UniqueName="Answer29">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question30" HeaderText="Question30" SortExpression="Question30"
                        UniqueName="Question30">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer30" HeaderText="Answer30" SortExpression="Answer30"
                        UniqueName="Answer30">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question31" HeaderText="Question31" SortExpression="Question31"
                        UniqueName="Question31">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer31" HeaderText="Answer31" SortExpression="Answer31"
                        UniqueName="Answer31">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question32" HeaderText="Question32" SortExpression="Question32"
                        UniqueName="Question32">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer32" HeaderText="Answer32" SortExpression="Answer32"
                        UniqueName="Answer32">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question33" HeaderText="Question33" SortExpression="Question33"
                        UniqueName="Question33">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer33" HeaderText="Answer33" SortExpression="Answer33"
                        UniqueName="Answer33">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question34" HeaderText="Question34" SortExpression="Question34"
                        UniqueName="Question34">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer34" HeaderText="Answer34" SortExpression="Answer34"
                        UniqueName="Answer34">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question35" HeaderText="Question35" SortExpression="Question35"
                        UniqueName="Question35">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer35" HeaderText="Answer35" SortExpression="Answer35"
                        UniqueName="Answer35">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question36" HeaderText="Question36" SortExpression="Question36"
                        UniqueName="Question36">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer36" HeaderText="Answer36" SortExpression="Answer36"
                        UniqueName="Answer36">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question37" HeaderText="Question37" SortExpression="Question37"
                        UniqueName="Question37">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer37" HeaderText="Answer37" SortExpression="Answer37"
                        UniqueName="Answer37">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question38" HeaderText="Question38" SortExpression="Question38"
                        UniqueName="Question38">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer38" HeaderText="Answer38" SortExpression="Answer38"
                        UniqueName="Answer38">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question39" HeaderText="Question39" SortExpression="Question39"
                        UniqueName="Question39">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer39" HeaderText="Answer39" SortExpression="Answer39"
                        UniqueName="Answer39">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question40" HeaderText="Question40" SortExpression="Question40"
                        UniqueName="Question40">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer40" HeaderText="Answer40" SortExpression="Answer40"
                        UniqueName="Answer40">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question41" HeaderText="Question41" SortExpression="Question41"
                        UniqueName="Question41">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer41" HeaderText="Answer41" SortExpression="Answer41"
                        UniqueName="Answer41">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question42" HeaderText="Question42" SortExpression="Question42"
                        UniqueName="Question42">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer42" HeaderText="Answer42" SortExpression="Answer42"
                        UniqueName="Answer42">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question43" HeaderText="Question43" SortExpression="Question43"
                        UniqueName="Question43">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer43" HeaderText="Answer43" SortExpression="Answer43"
                        UniqueName="Answer43">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question44" HeaderText="Question44" SortExpression="Question44"
                        UniqueName="Question44">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer44" HeaderText="Answer44" SortExpression="Answer44"
                        UniqueName="Answer44">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question45" HeaderText="Question45" SortExpression="Question45"
                        UniqueName="Question45">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer45" HeaderText="Answer45" SortExpression="Answer45"
                        UniqueName="Answer45">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question46" HeaderText="Question46" SortExpression="Question46"
                        UniqueName="Question46">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer46" HeaderText="Answer46" SortExpression="Answer46"
                        UniqueName="Answer46">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question47" HeaderText="Question47" SortExpression="Question47"
                        UniqueName="Question47">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer47" HeaderText="Answer47" SortExpression="Answer47"
                        UniqueName="Answer47">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question48" HeaderText="Question48" SortExpression="Question48"
                        UniqueName="Question48">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer48" HeaderText="Answer48" SortExpression="Answer48"
                        UniqueName="Answer48">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question49" HeaderText="Question49" SortExpression="Question49"
                        UniqueName="Question49">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer49" HeaderText="Answer49" SortExpression="Answer49"
                        UniqueName="Answer49">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Question50" HeaderText="Question50" SortExpression="Question50"
                        UniqueName="Question50">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Answer50" HeaderText="Answer50" SortExpression="Answer50"
                        UniqueName="Answer50">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource34" runat="server" ConnectionString="<%$ ConnectionStrings:NUSkillCheck %>"
            SelectCommand="sp_GetReportSurvey" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadComboBox30" Name="TestCategoryID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="RadDatePicker15" Name="StartDate" PropertyName="SelectedDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="RadDatePicker16" Name="EndDate" PropertyName="SelectedDate"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <!--GDPR Report Added By Harold Senier <09/19/2018> -->
    <asp:Panel ID="pnlGdprReport" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="2">
                    General Data Protection Completion Report
                </td>
            </tr>
            <tr>
            <td>
             Course Name:</td>  
                <td colspan="5">
                
                    <telerik:RadComboBox ID="rcbGdprCourse" runat="server" DataSourceID="SqlDataSource35"
                        DataTextField="Title" DataValueField="CourseID" AppendDataBoundItems="true" Width="400">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:SqlDataSource ID="SqlDataSource35" runat="server" ConnectionString="<%$ ConnectionStrings:INTRANETConnectionString %>"
                        SelectCommand="Select CourseID, Title FROM tbl_transcomuniversity_cor_course where title like '%general data%' AND HideFromList = 0" >
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
            <td>
              Start date:</td>  
                <td>
              
                    <telerik:RadDatePicker ID="rdpGdprStartDate" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                 End date:
                    <telerik:RadDatePicker ID="rdpGdprEndDate" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Current User:
                </td>
                <td>
                    <asp:TextBox ID="txtTWWID11" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnGenerateGdprReport" runat="server" Text="Generate" OnClick="btnGenerateGdprReport_Click" />
                </td>
            </tr>
        </table>
        
        <strong>COURSE NAME: <asp:Label ID="lblCourseName" runat="server" /></strong>
        <telerik:RadGrid ID="gridGdprReport" runat="server" AutoGenerateColumns="False" OnNeedDataSource="gridGdprReport_NeedDataSource"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true" AllowPaging="true" PageSize="50">
            <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" NoMasterRecordsText="No Records to display">
                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                
                <Columns>
                    <telerik:GridBoundColumn UniqueName="TWWID" DataField="TWWID" HeaderText="TWWID"
                        HeaderStyle-Font-Bold="true" AllowFiltering="true" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Firstname" DataField="Firstname" HeaderText="Firstname"
                        HeaderStyle-Font-Bold="true" AllowFiltering="true" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="LastName" DataField="LastName" HeaderText="LastName"
                        HeaderStyle-Font-Bold="true" AllowFiltering="true" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Program" DataField="Program" HeaderText="Program"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Campaign" DataField="Campaign" HeaderText="Campaign"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Country" DataField="Country" HeaderText="Country"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="ReportingManager" DataField="ReportingManager"
                        HeaderText="ReportingManager" HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="CourseType" DataField="CourseType" HeaderText="CourseType"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn UniqueName="Title" DataField="Title" HeaderText="Title"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Enrollment/AssignedDate" DataField="Enrollment/AssignedDate"
                        HeaderText="Enrollment/AssignedDate" HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="DueDate" DataField="DueDate" HeaderText="DueDate"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Progress" DataField="Progress" HeaderText="Progress"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    
                </Columns>
                
            </MasterTableView>
        </telerik:RadGrid>
     
    </asp:Panel>
    <!-- DAILY Views REPORT MDQUERUBIN 05022019 -->
    <asp:Panel ID="pnl_DailyReport" runat="server" Visible="false">
        <table width="50%">
            <tr>
                <td colspan="4">
                    Daily Views Report
                </td>
            </tr>
            <tr>
                <td><label>Start Date :</label></td>
                <td>
                    <telerik:RadDatePicker ID="rad_DailyViewsDateStart" runat="server"></telerik:RadDatePicker>
                </td>
                <td><label>Current User:</label></td>
                <td>
                    <asp:TextBox ID="txtTWWID12" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td><label>End Date :</label></td>
                <td>
                    <telerik:RadDatePicker ID="rad_DailyViewsDateEnd" runat="server"></telerik:RadDatePicker>
                </td>
                <td colspan="2">
                    <asp:Button ID="btn_DailyViewsGenerateReport" runat="server" Text="Generate" OnClick="btn_DailyViewsGenerateReport_Click" />
                </td>
            </tr>
        </table>

        <telerik:RadGrid ID="grid_DailyViewsReport" runat="server" AutoGenerateColumns="False" OnNeedDataSource="grid_DailyViewsReport_NeedDataSource"
            GridLines="None" ExportSettings-ExportOnlyData="true" ExportSettings-IgnorePaging="true" AllowPaging="true" PageSize="50">
            <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" NoMasterRecordsText="No Records to display">
                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                
                <Columns>
                    <telerik:GridBoundColumn UniqueName="DATE" DataField="DATE" HeaderText="DATE"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Time" DataField="Time" HeaderText="DATE"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="CIM" DataField="CIM" HeaderText="CIM"
                        HeaderStyle-Font-Bold="true" AllowFiltering="true" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Name" DataField="Name" HeaderText="Name"
                        HeaderStyle-Font-Bold="true" AllowFiltering="true" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Program" DataField="Program" HeaderText="Program"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="RoleJob" DataField="RoleJob" HeaderText="Role/Job"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Country" DataField="Country" HeaderText="Country"
                        HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                </Columns>
                
            </MasterTableView>
        </telerik:RadGrid>
    </asp:Panel>
    </form>
</body>
</html>
