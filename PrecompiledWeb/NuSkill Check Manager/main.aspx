<%@ page language="C#" masterpagefile="~/masterpages/nuSkill_manager_site.master" autoeventwireup="true" inherits="main, App_Web_yuclf4ni" title="Transcom University Testing Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSidebar" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" Width="100%" HorizontalAlign="center">
        <asp:Panel ID="pnlStyle" runat="server" Width="50%" HorizontalAlign="center">
            <table cellpadding="5" style="text-align:left">
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkExam" runat="server" Text="Exams" CssClass="linkButtons" OnClick="lnkExam_Click"  />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkEssay" runat="server" Text="Mark Essays" CssClass="linkButtons" OnClick="lnkEssay_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkExaminee" runat="server" Text="Examinees" CssClass="linkButtons" OnClick="lnkExaminee_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkReports" runat="server" Text="Reports" CssClass="linkButtons" OnClick="lnkReports_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
