﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Users.ascx.cs" Inherits="controls_Users" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table>
    <tr>
        <td>
            First Name:
        </td>
        <td>
            <asp:TextBox ID="txtFirst" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'
                ReadOnly="true"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                ControlToValidate="txtFirst"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Last Name:
        </td>
        <td>
            <asp:TextBox ID="txtLast" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'
                ReadOnly="true"></asp:TextBox>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                ControlToValidate="txtLast"></asp:RequiredFieldValidator>--%>
        </td>
    </tr>
    <tr>
        <td>
            TWWID:
        </td>
        <td>
            <asp:TextBox ID="txtTwwid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TWWID") %>'
                AutoPostBack="true" OnTextChanged="txtTwwid_TextChanged" ReadOnly="true"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                ControlToValidate="txtTwwid"></asp:RequiredFieldValidator>
            <div runat="server" id="UserIsEmployee" style="color: red">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            Transcom Email (optional):
        </td>
        <td>
            <asp:TextBox ID="txtEmail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Mobile No.
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox1" runat="server" SelectionOnFocus="SelectAll"
                Text='<%# DataBinder.Eval(Container, "DataItem.MobileNo") %>'>
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Region:
        </td>
        <td>
            <asp:Label ID="lblRegion" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.region_desc") %>'></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Country:
        </td>
        <td>
            <asp:Label ID="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.country_desc") %>'></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Site:
        </td>
        <td>
            <asp:Label ID="lblSite" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.site_desc") %>'></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Reporting Manager/Supervisor:
        </td>
        <td>
            <%--<asp:Label ID="lblSupervisor" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.supervisorname") %>'></asp:Label>--%>
            <telerik:RadComboBox ID="rcbSupervisor" runat="server" DataSourceID="dsSupervisor"
                DataTextField="ManagerName" DataValueField="ManagerCIM">
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="dsSupervisor" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT 0 AS ManagerCIM, '' AS ManagerName
                               UNION ALL
                               SELECT E.TWWID AS ManagerCIM, E.Nombre AS ManagerName
                               FROM TranscomUniv_local.dbo.Empleados E
                                    INNER JOIN TranscomUniv_local.dbo.Users U
                                        ON E.TWWID = U.TWWID
                               WHERE E.NombreCatTranscomDesc LIKE '%Manager%'
                               AND E.TWWID IS NOT NULL
                               UNION ALL
                               SELECT E.TWWID AS ManagerCIM, E.Nombre AS ManagerName
                               FROM TranscomUniv_local.dbo.Empleados E
                                    INNER JOIN TranscomUniv_local.dbo.Users U
                                        ON E.TWWID = U.TWWID
                               WHERE E.NombreCatTranscomDesc LIKE '%Team Leader%'
                               AND E.TWWID IS NOT NULL
                               ORDER BY 2 ASC">
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Department:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox2" runat="server" DataSourceID="SqlDataSource7"
                DataTextField="description" DataValueField="id" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.departmentid") %>'>
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT [id], [description] FROM [refDepartments] WHERE ([active] = @active) ORDER BY [description]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="True" Name="active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Client:
        </td>
        <td>
            <%--<asp:Label ID="lblClient" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.client_desc") %>'></asp:Label>--%>
            <telerik:RadComboBox ID="rcbClient" runat="server" DataSourceID="dsClients"
                DataTextField="Program" DataValueField="ProgramID">
            </telerik:RadComboBox>
            <asp:SqlDataSource ID="dsClients" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT 0 AS ProgramID,'' AS Program
                               UNION ALL
                               SELECT DISTINCT ID AS ProgramID, Description AS Program 
                               FROM TranscomUniv_local.dbo.Clients
                               WHERE Active = 1  ORDER BY 2">
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Type of Support:
        </td>
        <td>
            <asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.supporttype") %>'
                Visible="false"></asp:TextBox>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" DataSourceID="SqlDataSource8"
                DataTextField="description" DataValueField="id" RepeatColumns="2" RepeatDirection="Horizontal">
            </asp:CheckBoxList>
            <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT [id], [description] FROM [refSupportType] WHERE ([active] = @active) ORDER BY [description]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="True" Name="active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Primary Language:
        </td>
        <td>
            <telerik:RadComboBox runat="server" ID="cbLanguage" DataTextField="Language" DataValueField="Id"
                DataSourceID="DataSource_Language" MaxHeight="250" DropDownAutoWidth="Enabled"
                SelectedValue='<%# DataBinder.Eval(Container, "DataItem.LanguageID") %>' />
            <asp:SqlDataSource ID="DataSource_Language" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT [Id], [Language] FROM [refLanguage] WHERE ([Active] = @Active) ORDER BY [Language]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="true" Name="Active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Current Specialization and Skills:
        </td>
        <td>
            <telerik:RadTextBox runat="server" ID="txtSpecializationSkills" Width="300" Height="50"
                SelectionOnFocus="SelectAll" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.Specialization_Skills") %>' />
        </td>
    </tr>
    <tr>
        <td>
            Education Level:
        </td>
        <td>
            <telerik:RadComboBox runat="server" ID="cbEducLevel" DataValueField="Id" DataTextField="Title"
                DataSourceID="DataSourse_EducLevel" MaxHeight="250" DropDownAutoWidth="Enabled"
                SelectedValue='<%# DataBinder.Eval(Container, "DataItem.EducationLevelID") %>' />
            <asp:SqlDataSource ID="DataSourse_EducLevel" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT [Id], [Title] FROM [refEducationLevel] WHERE ([Active] = @Active) ORDER BY [Id]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="true" Name="Active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Major Subject:
        </td>
        <td>
            <telerik:RadTextBox runat="server" ID="txtCourseMajor" Width="120" SelectionOnFocus="SelectAll"
                Text='<%# DataBinder.Eval(Container, "DataItem.CourseMajor") %>' />
        </td>
    </tr>
    <tr>
        <td>
            Additional Courses and Training Attended (Internal & External):
        </td>
        <td>
            <telerik:RadTextBox runat="server" ID="txtCoursesTrainings" Width="300" Height="50"
                SelectionOnFocus="SelectAll" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.AdditionalCourses_Trainings") %>' />
        </td>
    </tr>
    <tr>
        <td>
            I am interested in pursuing the following career path in Transcom:
        </td>
        <td>
            <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CareerPathID") %>'
                Visible="false"></asp:Label>
            <asp:RadioButtonList runat="server" ID="radioBtnCareerPath" RepeatDirection="Horizontal">
                <asp:ListItem Text="Training" Value="1"></asp:ListItem>
                <asp:ListItem Text="Quality" Value="2"></asp:ListItem>
                <asp:ListItem Text="Operational Management" Value="3"></asp:ListItem>
                <asp:ListItem Text="Human Resources" Value="4"></asp:ListItem>
                <asp:ListItem Text="IT" Value="5"></asp:ListItem>
                <asp:ListItem Text="Project Management" Value="6"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td>
            Please tell me what courses you are interested in taking for your professional growth
            and development
        </td>
        <td>
            <telerik:RadTextBox runat="server" ID="txtCoursesInterested" Width="300" Height="50"
                SelectionOnFocus="SelectAll" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.CourseInterests") %>' />
        </td>
    </tr>
    <tr>
        <td>
            Upload Photograph:
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            I am interested in receiving updates via:
        </td>
        <td>
            <asp:TextBox ID="TextBox2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UpdateVia") %>'
                Visible="false"></asp:TextBox>
            <asp:CheckBoxList ID="radioBtnReceiveUpdates" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">Email</asp:ListItem>
                <asp:ListItem Value="2">Mobile</asp:ListItem>
                <asp:ListItem Value="3">Post</asp:ListItem>
            </asp:CheckBoxList>
        </td>
    </tr>
    <tr>
        <td>
            Role:
        </td>
        <td>
            <telerik:RadComboBox ID="rcbRole" runat="server" DataSourceID="SqlDataSource5" DataTextField="role_desc"
                AutoPostBack="True" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.RoleID") %>'
                OnSelectedIndexChanged="rcbRole_SelectedIndexChanged">
            </telerik:RadComboBox>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBtnAssignClient" runat="server" Font-Underline="True">Assign Client/s</asp:LinkButton>
            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckV2ConnectionString %>"
                SelectCommand="SELECT * FROM [refRoles] WHERE ([active] = @active) ORDER BY [role_desc]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="True" Name="active" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            Active:
        </td>
        <td>
            <asp:CheckBox ID="chkActive" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Active") %>' />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Reset password?" Visible='<%# !(DataBinder.Eval(Container,"DataItem") is Telerik.Web.UI.GridInsertionObject) %>'></asp:Label>
        </td>
        <td>
            <asp:CheckBox ID="CheckBox1" runat="server" Visible='<%# !(DataBinder.Eval(Container,"DataItem") is Telerik.Web.UI.GridInsertionObject) %>' />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="cmdInsert" runat="server" Text="Insert" CommandName="PerformInsert"
                Visible='<%# DataBinder.Eval(Container,"DataItem") is Telerik.Web.UI.GridInsertionObject %>' />
            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CommandName="Update" Visible='<%# !(DataBinder.Eval(Container,"DataItem") is Telerik.Web.UI.GridInsertionObject) %>' />
        </td>
        <td>
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CausesValidation="false"
                CommandName="Cancel" />
        </td>
    </tr>
</table>
