using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class AccountListDal : TheLibrary.DBImportTool.Base
    {
        public AccountListDal()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public AccountListDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll(int showAll)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@ShowAll", SqlDbType.Int, showAll));
            //return Connection.ExecuteSPQuery("pr_accountlist_lkp_selectall", paramList.ToArray());
            return Connection.ExecuteSPQuery("pr_testing_accountlist", paramList.ToArray());
        }

        public DataSet SelectAllVader2(int showAll)
        {
            this.ConnectionString = Properties.Settings.Default.Vader2TestingConnectionString;
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@ShowAll", SqlDbType.Int, showAll));
            return Connection.ExecuteSPQuery("pr_weeklytesting_accountlist", paramList.ToArray());
        }

        public DataSet SelectGenericParentCampaigns(int showAll)
        {
            return Connection.ExecuteSPQuery("pr_weeklytesting_parentgenericcampaigns",
                Helper.CreateParam("@ShowAll", SqlDbType.Int, showAll));
        }
    }
}
