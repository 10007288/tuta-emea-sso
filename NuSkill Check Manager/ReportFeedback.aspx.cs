﻿using System;
using System.Web.UI;

public partial class ReportFeedback : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(SessionManager.SessionUsername))
                Response.Redirect("~/login.aspx?" + Request.QueryString);
        }

        gridFeedback.Rebind();
    }

    protected void BtnViewReportClick(object sender, EventArgs e)
    {
        gridFeedback.Rebind();
    }

    protected void BtnClearParamsClick(object sender, EventArgs e)
    {
        Response.Redirect("~/ReportFeedback.aspx");
    }

    protected void BtnExportToExcelClick(object sender, EventArgs e)
    {
        if (gridFeedback.MasterTableView.Items.Count > 0)
        {
            gridFeedback.ExportSettings.FileName = "UserFeedback(" + DateTime.Now + ")";
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q1_A_Ans1").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q1_B_Ans2").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q1_C_Ans3").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q1_D_Ans4").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q1_E_Ans5").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q1_F_Ans6").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q1_G_Ans7").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q2_A_Ans1").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q2_B_Ans2").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q2_C_Ans3").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q2_D_Ans4").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q2_E_Ans5").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q2_F_Ans6").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q2_G_Ans7").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q3_A_Ans1").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q3_B_Ans2").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q3_C_Ans3").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q4_A_Ans1").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q4_B_Ans2").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q4_C_Ans3").Display = true;
            gridFeedback.MasterTableView.Columns.FindByUniqueName("Q4_D_Ans4").Display = true;
            gridFeedback.MasterTableView.ExportToExcel();
        }
        else
        {
            AlertNoExportData();
        }
    }

    public void AlertNoExportData()
    {
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                    "alert('No data to be exported.');",
                                                                    true);
    }
}